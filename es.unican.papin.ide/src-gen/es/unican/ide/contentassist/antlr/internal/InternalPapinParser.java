package es.unican.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import es.unican.services.PapinGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPapinParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'count'", "'sum'", "'avg'", "'max'", "'min'", "'domainModelNSURI'", "'domainModelInstance'", "'dataset'", "'using'", "'{'", "'}'", "'include'", "'by'", "'calculate'", "'as'", "'('", "')'", "'only_as'", "'['", "']'", "','", "'where'", "'and'", "'or'", "'='", "'!='", "'>'", "'>='", "'<'", "'<='", "'.'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPapinParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPapinParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPapinParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPapin.g"; }


    	private PapinGrammarAccess grammarAccess;

    	public void setGrammarAccess(PapinGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDatasets"
    // InternalPapin.g:53:1: entryRuleDatasets : ruleDatasets EOF ;
    public final void entryRuleDatasets() throws RecognitionException {
        try {
            // InternalPapin.g:54:1: ( ruleDatasets EOF )
            // InternalPapin.g:55:1: ruleDatasets EOF
            {
             before(grammarAccess.getDatasetsRule()); 
            pushFollow(FOLLOW_1);
            ruleDatasets();

            state._fsp--;

             after(grammarAccess.getDatasetsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDatasets"


    // $ANTLR start "ruleDatasets"
    // InternalPapin.g:62:1: ruleDatasets : ( ( rule__Datasets__Group__0 ) ) ;
    public final void ruleDatasets() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:66:2: ( ( ( rule__Datasets__Group__0 ) ) )
            // InternalPapin.g:67:2: ( ( rule__Datasets__Group__0 ) )
            {
            // InternalPapin.g:67:2: ( ( rule__Datasets__Group__0 ) )
            // InternalPapin.g:68:3: ( rule__Datasets__Group__0 )
            {
             before(grammarAccess.getDatasetsAccess().getGroup()); 
            // InternalPapin.g:69:3: ( rule__Datasets__Group__0 )
            // InternalPapin.g:69:4: rule__Datasets__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Datasets__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDatasetsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDatasets"


    // $ANTLR start "entryRuleDataset"
    // InternalPapin.g:78:1: entryRuleDataset : ruleDataset EOF ;
    public final void entryRuleDataset() throws RecognitionException {
        try {
            // InternalPapin.g:79:1: ( ruleDataset EOF )
            // InternalPapin.g:80:1: ruleDataset EOF
            {
             before(grammarAccess.getDatasetRule()); 
            pushFollow(FOLLOW_1);
            ruleDataset();

            state._fsp--;

             after(grammarAccess.getDatasetRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataset"


    // $ANTLR start "ruleDataset"
    // InternalPapin.g:87:1: ruleDataset : ( ( rule__Dataset__Group__0 ) ) ;
    public final void ruleDataset() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:91:2: ( ( ( rule__Dataset__Group__0 ) ) )
            // InternalPapin.g:92:2: ( ( rule__Dataset__Group__0 ) )
            {
            // InternalPapin.g:92:2: ( ( rule__Dataset__Group__0 ) )
            // InternalPapin.g:93:3: ( rule__Dataset__Group__0 )
            {
             before(grammarAccess.getDatasetAccess().getGroup()); 
            // InternalPapin.g:94:3: ( rule__Dataset__Group__0 )
            // InternalPapin.g:94:4: rule__Dataset__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Dataset__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDatasetAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataset"


    // $ANTLR start "entryRuleIncludedReference"
    // InternalPapin.g:103:1: entryRuleIncludedReference : ruleIncludedReference EOF ;
    public final void entryRuleIncludedReference() throws RecognitionException {
        try {
            // InternalPapin.g:104:1: ( ruleIncludedReference EOF )
            // InternalPapin.g:105:1: ruleIncludedReference EOF
            {
             before(grammarAccess.getIncludedReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getIncludedReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIncludedReference"


    // $ANTLR start "ruleIncludedReference"
    // InternalPapin.g:112:1: ruleIncludedReference : ( ( rule__IncludedReference__Alternatives ) ) ;
    public final void ruleIncludedReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:116:2: ( ( ( rule__IncludedReference__Alternatives ) ) )
            // InternalPapin.g:117:2: ( ( rule__IncludedReference__Alternatives ) )
            {
            // InternalPapin.g:117:2: ( ( rule__IncludedReference__Alternatives ) )
            // InternalPapin.g:118:3: ( rule__IncludedReference__Alternatives )
            {
             before(grammarAccess.getIncludedReferenceAccess().getAlternatives()); 
            // InternalPapin.g:119:3: ( rule__IncludedReference__Alternatives )
            // InternalPapin.g:119:4: rule__IncludedReference__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__IncludedReference__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getIncludedReferenceAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIncludedReference"


    // $ANTLR start "entryRuleSimpleReference"
    // InternalPapin.g:128:1: entryRuleSimpleReference : ruleSimpleReference EOF ;
    public final void entryRuleSimpleReference() throws RecognitionException {
        try {
            // InternalPapin.g:129:1: ( ruleSimpleReference EOF )
            // InternalPapin.g:130:1: ruleSimpleReference EOF
            {
             before(grammarAccess.getSimpleReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleSimpleReference();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleReference"


    // $ANTLR start "ruleSimpleReference"
    // InternalPapin.g:137:1: ruleSimpleReference : ( ( rule__SimpleReference__Group__0 ) ) ;
    public final void ruleSimpleReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:141:2: ( ( ( rule__SimpleReference__Group__0 ) ) )
            // InternalPapin.g:142:2: ( ( rule__SimpleReference__Group__0 ) )
            {
            // InternalPapin.g:142:2: ( ( rule__SimpleReference__Group__0 ) )
            // InternalPapin.g:143:3: ( rule__SimpleReference__Group__0 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getGroup()); 
            // InternalPapin.g:144:3: ( rule__SimpleReference__Group__0 )
            // InternalPapin.g:144:4: rule__SimpleReference__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleReference"


    // $ANTLR start "entryRuleAggregatedReference"
    // InternalPapin.g:153:1: entryRuleAggregatedReference : ruleAggregatedReference EOF ;
    public final void entryRuleAggregatedReference() throws RecognitionException {
        try {
            // InternalPapin.g:154:1: ( ruleAggregatedReference EOF )
            // InternalPapin.g:155:1: ruleAggregatedReference EOF
            {
             before(grammarAccess.getAggregatedReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleAggregatedReference();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAggregatedReference"


    // $ANTLR start "ruleAggregatedReference"
    // InternalPapin.g:162:1: ruleAggregatedReference : ( ( rule__AggregatedReference__Group__0 ) ) ;
    public final void ruleAggregatedReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:166:2: ( ( ( rule__AggregatedReference__Group__0 ) ) )
            // InternalPapin.g:167:2: ( ( rule__AggregatedReference__Group__0 ) )
            {
            // InternalPapin.g:167:2: ( ( rule__AggregatedReference__Group__0 ) )
            // InternalPapin.g:168:3: ( rule__AggregatedReference__Group__0 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getGroup()); 
            // InternalPapin.g:169:3: ( rule__AggregatedReference__Group__0 )
            // InternalPapin.g:169:4: rule__AggregatedReference__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAggregatedReference"


    // $ANTLR start "entryRuleAggFunction"
    // InternalPapin.g:178:1: entryRuleAggFunction : ruleAggFunction EOF ;
    public final void entryRuleAggFunction() throws RecognitionException {
        try {
            // InternalPapin.g:179:1: ( ruleAggFunction EOF )
            // InternalPapin.g:180:1: ruleAggFunction EOF
            {
             before(grammarAccess.getAggFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleAggFunction();

            state._fsp--;

             after(grammarAccess.getAggFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAggFunction"


    // $ANTLR start "ruleAggFunction"
    // InternalPapin.g:187:1: ruleAggFunction : ( ( rule__AggFunction__Alternatives ) ) ;
    public final void ruleAggFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:191:2: ( ( ( rule__AggFunction__Alternatives ) ) )
            // InternalPapin.g:192:2: ( ( rule__AggFunction__Alternatives ) )
            {
            // InternalPapin.g:192:2: ( ( rule__AggFunction__Alternatives ) )
            // InternalPapin.g:193:3: ( rule__AggFunction__Alternatives )
            {
             before(grammarAccess.getAggFunctionAccess().getAlternatives()); 
            // InternalPapin.g:194:3: ( rule__AggFunction__Alternatives )
            // InternalPapin.g:194:4: rule__AggFunction__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AggFunction__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAggFunctionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAggFunction"


    // $ANTLR start "entryRuleTypeFilter"
    // InternalPapin.g:203:1: entryRuleTypeFilter : ruleTypeFilter EOF ;
    public final void entryRuleTypeFilter() throws RecognitionException {
        try {
            // InternalPapin.g:204:1: ( ruleTypeFilter EOF )
            // InternalPapin.g:205:1: ruleTypeFilter EOF
            {
             before(grammarAccess.getTypeFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getTypeFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeFilter"


    // $ANTLR start "ruleTypeFilter"
    // InternalPapin.g:212:1: ruleTypeFilter : ( ( rule__TypeFilter__Alternatives ) ) ;
    public final void ruleTypeFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:216:2: ( ( ( rule__TypeFilter__Alternatives ) ) )
            // InternalPapin.g:217:2: ( ( rule__TypeFilter__Alternatives ) )
            {
            // InternalPapin.g:217:2: ( ( rule__TypeFilter__Alternatives ) )
            // InternalPapin.g:218:3: ( rule__TypeFilter__Alternatives )
            {
             before(grammarAccess.getTypeFilterAccess().getAlternatives()); 
            // InternalPapin.g:219:3: ( rule__TypeFilter__Alternatives )
            // InternalPapin.g:219:4: rule__TypeFilter__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TypeFilter__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeFilterAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeFilter"


    // $ANTLR start "entryRuleTypeCompletion"
    // InternalPapin.g:228:1: entryRuleTypeCompletion : ruleTypeCompletion EOF ;
    public final void entryRuleTypeCompletion() throws RecognitionException {
        try {
            // InternalPapin.g:229:1: ( ruleTypeCompletion EOF )
            // InternalPapin.g:230:1: ruleTypeCompletion EOF
            {
             before(grammarAccess.getTypeCompletionRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeCompletion();

            state._fsp--;

             after(grammarAccess.getTypeCompletionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeCompletion"


    // $ANTLR start "ruleTypeCompletion"
    // InternalPapin.g:237:1: ruleTypeCompletion : ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) ) ;
    public final void ruleTypeCompletion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:241:2: ( ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) ) )
            // InternalPapin.g:242:2: ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) )
            {
            // InternalPapin.g:242:2: ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) )
            // InternalPapin.g:243:3: ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* )
            {
            // InternalPapin.g:243:3: ( ( rule__TypeCompletion__Group__0 ) )
            // InternalPapin.g:244:4: ( rule__TypeCompletion__Group__0 )
            {
             before(grammarAccess.getTypeCompletionAccess().getGroup()); 
            // InternalPapin.g:245:4: ( rule__TypeCompletion__Group__0 )
            // InternalPapin.g:245:5: rule__TypeCompletion__Group__0
            {
            pushFollow(FOLLOW_3);
            rule__TypeCompletion__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeCompletionAccess().getGroup()); 

            }

            // InternalPapin.g:248:3: ( ( rule__TypeCompletion__Group__0 )* )
            // InternalPapin.g:249:4: ( rule__TypeCompletion__Group__0 )*
            {
             before(grammarAccess.getTypeCompletionAccess().getGroup()); 
            // InternalPapin.g:250:4: ( rule__TypeCompletion__Group__0 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==25) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPapin.g:250:5: rule__TypeCompletion__Group__0
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__TypeCompletion__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getTypeCompletionAccess().getGroup()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeCompletion"


    // $ANTLR start "entryRuleTypeSelection"
    // InternalPapin.g:260:1: entryRuleTypeSelection : ruleTypeSelection EOF ;
    public final void entryRuleTypeSelection() throws RecognitionException {
        try {
            // InternalPapin.g:261:1: ( ruleTypeSelection EOF )
            // InternalPapin.g:262:1: ruleTypeSelection EOF
            {
             before(grammarAccess.getTypeSelectionRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeSelection();

            state._fsp--;

             after(grammarAccess.getTypeSelectionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeSelection"


    // $ANTLR start "ruleTypeSelection"
    // InternalPapin.g:269:1: ruleTypeSelection : ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) ) ;
    public final void ruleTypeSelection() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:273:2: ( ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) ) )
            // InternalPapin.g:274:2: ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) )
            {
            // InternalPapin.g:274:2: ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) )
            // InternalPapin.g:275:3: ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* )
            {
            // InternalPapin.g:275:3: ( ( rule__TypeSelection__Group__0 ) )
            // InternalPapin.g:276:4: ( rule__TypeSelection__Group__0 )
            {
             before(grammarAccess.getTypeSelectionAccess().getGroup()); 
            // InternalPapin.g:277:4: ( rule__TypeSelection__Group__0 )
            // InternalPapin.g:277:5: rule__TypeSelection__Group__0
            {
            pushFollow(FOLLOW_4);
            rule__TypeSelection__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeSelectionAccess().getGroup()); 

            }

            // InternalPapin.g:280:3: ( ( rule__TypeSelection__Group__0 )* )
            // InternalPapin.g:281:4: ( rule__TypeSelection__Group__0 )*
            {
             before(grammarAccess.getTypeSelectionAccess().getGroup()); 
            // InternalPapin.g:282:4: ( rule__TypeSelection__Group__0 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==28) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPapin.g:282:5: rule__TypeSelection__Group__0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__TypeSelection__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getTypeSelectionAccess().getGroup()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeSelection"


    // $ANTLR start "entryRuleTypeCustomization"
    // InternalPapin.g:292:1: entryRuleTypeCustomization : ruleTypeCustomization EOF ;
    public final void entryRuleTypeCustomization() throws RecognitionException {
        try {
            // InternalPapin.g:293:1: ( ruleTypeCustomization EOF )
            // InternalPapin.g:294:1: ruleTypeCustomization EOF
            {
             before(grammarAccess.getTypeCustomizationRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeCustomization();

            state._fsp--;

             after(grammarAccess.getTypeCustomizationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeCustomization"


    // $ANTLR start "ruleTypeCustomization"
    // InternalPapin.g:301:1: ruleTypeCustomization : ( ( rule__TypeCustomization__Group__0 ) ) ;
    public final void ruleTypeCustomization() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:305:2: ( ( ( rule__TypeCustomization__Group__0 ) ) )
            // InternalPapin.g:306:2: ( ( rule__TypeCustomization__Group__0 ) )
            {
            // InternalPapin.g:306:2: ( ( rule__TypeCustomization__Group__0 ) )
            // InternalPapin.g:307:3: ( rule__TypeCustomization__Group__0 )
            {
             before(grammarAccess.getTypeCustomizationAccess().getGroup()); 
            // InternalPapin.g:308:3: ( rule__TypeCustomization__Group__0 )
            // InternalPapin.g:308:4: rule__TypeCustomization__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeCustomizationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeCustomization"


    // $ANTLR start "entryRuleAttributeFilter"
    // InternalPapin.g:317:1: entryRuleAttributeFilter : ruleAttributeFilter EOF ;
    public final void entryRuleAttributeFilter() throws RecognitionException {
        try {
            // InternalPapin.g:318:1: ( ruleAttributeFilter EOF )
            // InternalPapin.g:319:1: ruleAttributeFilter EOF
            {
             before(grammarAccess.getAttributeFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getAttributeFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttributeFilter"


    // $ANTLR start "ruleAttributeFilter"
    // InternalPapin.g:326:1: ruleAttributeFilter : ( ( rule__AttributeFilter__Group__0 ) ) ;
    public final void ruleAttributeFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:330:2: ( ( ( rule__AttributeFilter__Group__0 ) ) )
            // InternalPapin.g:331:2: ( ( rule__AttributeFilter__Group__0 ) )
            {
            // InternalPapin.g:331:2: ( ( rule__AttributeFilter__Group__0 ) )
            // InternalPapin.g:332:3: ( rule__AttributeFilter__Group__0 )
            {
             before(grammarAccess.getAttributeFilterAccess().getGroup()); 
            // InternalPapin.g:333:3: ( rule__AttributeFilter__Group__0 )
            // InternalPapin.g:333:4: rule__AttributeFilter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeFilterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttributeFilter"


    // $ANTLR start "entryRuleInstancesFilter"
    // InternalPapin.g:342:1: entryRuleInstancesFilter : ruleInstancesFilter EOF ;
    public final void entryRuleInstancesFilter() throws RecognitionException {
        try {
            // InternalPapin.g:343:1: ( ruleInstancesFilter EOF )
            // InternalPapin.g:344:1: ruleInstancesFilter EOF
            {
             before(grammarAccess.getInstancesFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getInstancesFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInstancesFilter"


    // $ANTLR start "ruleInstancesFilter"
    // InternalPapin.g:351:1: ruleInstancesFilter : ( ( rule__InstancesFilter__Group__0 ) ) ;
    public final void ruleInstancesFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:355:2: ( ( ( rule__InstancesFilter__Group__0 ) ) )
            // InternalPapin.g:356:2: ( ( rule__InstancesFilter__Group__0 ) )
            {
            // InternalPapin.g:356:2: ( ( rule__InstancesFilter__Group__0 ) )
            // InternalPapin.g:357:3: ( rule__InstancesFilter__Group__0 )
            {
             before(grammarAccess.getInstancesFilterAccess().getGroup()); 
            // InternalPapin.g:358:3: ( rule__InstancesFilter__Group__0 )
            // InternalPapin.g:358:4: rule__InstancesFilter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InstancesFilter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInstancesFilterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInstancesFilter"


    // $ANTLR start "entryRuleAndConjunction"
    // InternalPapin.g:367:1: entryRuleAndConjunction : ruleAndConjunction EOF ;
    public final void entryRuleAndConjunction() throws RecognitionException {
        try {
            // InternalPapin.g:368:1: ( ruleAndConjunction EOF )
            // InternalPapin.g:369:1: ruleAndConjunction EOF
            {
             before(grammarAccess.getAndConjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleAndConjunction();

            state._fsp--;

             after(grammarAccess.getAndConjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAndConjunction"


    // $ANTLR start "ruleAndConjunction"
    // InternalPapin.g:376:1: ruleAndConjunction : ( ( rule__AndConjunction__Group__0 ) ) ;
    public final void ruleAndConjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:380:2: ( ( ( rule__AndConjunction__Group__0 ) ) )
            // InternalPapin.g:381:2: ( ( rule__AndConjunction__Group__0 ) )
            {
            // InternalPapin.g:381:2: ( ( rule__AndConjunction__Group__0 ) )
            // InternalPapin.g:382:3: ( rule__AndConjunction__Group__0 )
            {
             before(grammarAccess.getAndConjunctionAccess().getGroup()); 
            // InternalPapin.g:383:3: ( rule__AndConjunction__Group__0 )
            // InternalPapin.g:383:4: rule__AndConjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndConjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAndConjunction"


    // $ANTLR start "entryRuleOrConjunction"
    // InternalPapin.g:392:1: entryRuleOrConjunction : ruleOrConjunction EOF ;
    public final void entryRuleOrConjunction() throws RecognitionException {
        try {
            // InternalPapin.g:393:1: ( ruleOrConjunction EOF )
            // InternalPapin.g:394:1: ruleOrConjunction EOF
            {
             before(grammarAccess.getOrConjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleOrConjunction();

            state._fsp--;

             after(grammarAccess.getOrConjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrConjunction"


    // $ANTLR start "ruleOrConjunction"
    // InternalPapin.g:401:1: ruleOrConjunction : ( ( rule__OrConjunction__Group__0 ) ) ;
    public final void ruleOrConjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:405:2: ( ( ( rule__OrConjunction__Group__0 ) ) )
            // InternalPapin.g:406:2: ( ( rule__OrConjunction__Group__0 ) )
            {
            // InternalPapin.g:406:2: ( ( rule__OrConjunction__Group__0 ) )
            // InternalPapin.g:407:3: ( rule__OrConjunction__Group__0 )
            {
             before(grammarAccess.getOrConjunctionAccess().getGroup()); 
            // InternalPapin.g:408:3: ( rule__OrConjunction__Group__0 )
            // InternalPapin.g:408:4: rule__OrConjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrConjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrConjunction"


    // $ANTLR start "entryRulePrimary"
    // InternalPapin.g:417:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalPapin.g:418:1: ( rulePrimary EOF )
            // InternalPapin.g:419:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalPapin.g:426:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:430:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalPapin.g:431:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalPapin.g:431:2: ( ( rule__Primary__Alternatives ) )
            // InternalPapin.g:432:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalPapin.g:433:3: ( rule__Primary__Alternatives )
            // InternalPapin.g:433:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleComparison"
    // InternalPapin.g:442:1: entryRuleComparison : ruleComparison EOF ;
    public final void entryRuleComparison() throws RecognitionException {
        try {
            // InternalPapin.g:443:1: ( ruleComparison EOF )
            // InternalPapin.g:444:1: ruleComparison EOF
            {
             before(grammarAccess.getComparisonRule()); 
            pushFollow(FOLLOW_1);
            ruleComparison();

            state._fsp--;

             after(grammarAccess.getComparisonRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // InternalPapin.g:451:1: ruleComparison : ( ( rule__Comparison__Alternatives ) ) ;
    public final void ruleComparison() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:455:2: ( ( ( rule__Comparison__Alternatives ) ) )
            // InternalPapin.g:456:2: ( ( rule__Comparison__Alternatives ) )
            {
            // InternalPapin.g:456:2: ( ( rule__Comparison__Alternatives ) )
            // InternalPapin.g:457:3: ( rule__Comparison__Alternatives )
            {
             before(grammarAccess.getComparisonAccess().getAlternatives()); 
            // InternalPapin.g:458:3: ( rule__Comparison__Alternatives )
            // InternalPapin.g:458:4: rule__Comparison__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRulePath"
    // InternalPapin.g:467:1: entryRulePath : rulePath EOF ;
    public final void entryRulePath() throws RecognitionException {
        try {
            // InternalPapin.g:468:1: ( rulePath EOF )
            // InternalPapin.g:469:1: rulePath EOF
            {
             before(grammarAccess.getPathRule()); 
            pushFollow(FOLLOW_1);
            rulePath();

            state._fsp--;

             after(grammarAccess.getPathRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePath"


    // $ANTLR start "rulePath"
    // InternalPapin.g:476:1: rulePath : ( ( rule__Path__Group__0 ) ) ;
    public final void rulePath() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:480:2: ( ( ( rule__Path__Group__0 ) ) )
            // InternalPapin.g:481:2: ( ( rule__Path__Group__0 ) )
            {
            // InternalPapin.g:481:2: ( ( rule__Path__Group__0 ) )
            // InternalPapin.g:482:3: ( rule__Path__Group__0 )
            {
             before(grammarAccess.getPathAccess().getGroup()); 
            // InternalPapin.g:483:3: ( rule__Path__Group__0 )
            // InternalPapin.g:483:4: rule__Path__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Path__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPathAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePath"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalPapin.g:492:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalPapin.g:493:1: ( ruleQualifiedName EOF )
            // InternalPapin.g:494:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalPapin.g:501:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:505:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalPapin.g:506:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalPapin.g:506:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalPapin.g:507:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalPapin.g:508:3: ( rule__QualifiedName__Group__0 )
            // InternalPapin.g:508:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "rule__IncludedReference__Alternatives"
    // InternalPapin.g:516:1: rule__IncludedReference__Alternatives : ( ( ruleSimpleReference ) | ( ruleAggregatedReference ) );
    public final void rule__IncludedReference__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:520:1: ( ( ruleSimpleReference ) | ( ruleAggregatedReference ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==22) ) {
                alt3=1;
            }
            else if ( (LA3_0==24) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalPapin.g:521:2: ( ruleSimpleReference )
                    {
                    // InternalPapin.g:521:2: ( ruleSimpleReference )
                    // InternalPapin.g:522:3: ruleSimpleReference
                    {
                     before(grammarAccess.getIncludedReferenceAccess().getSimpleReferenceParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSimpleReference();

                    state._fsp--;

                     after(grammarAccess.getIncludedReferenceAccess().getSimpleReferenceParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPapin.g:527:2: ( ruleAggregatedReference )
                    {
                    // InternalPapin.g:527:2: ( ruleAggregatedReference )
                    // InternalPapin.g:528:3: ruleAggregatedReference
                    {
                     before(grammarAccess.getIncludedReferenceAccess().getAggregatedReferenceParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAggregatedReference();

                    state._fsp--;

                     after(grammarAccess.getIncludedReferenceAccess().getAggregatedReferenceParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludedReference__Alternatives"


    // $ANTLR start "rule__AggFunction__Alternatives"
    // InternalPapin.g:537:1: rule__AggFunction__Alternatives : ( ( 'count' ) | ( 'sum' ) | ( 'avg' ) | ( 'max' ) | ( 'min' ) );
    public final void rule__AggFunction__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:541:1: ( ( 'count' ) | ( 'sum' ) | ( 'avg' ) | ( 'max' ) | ( 'min' ) )
            int alt4=5;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt4=1;
                }
                break;
            case 12:
                {
                alt4=2;
                }
                break;
            case 13:
                {
                alt4=3;
                }
                break;
            case 14:
                {
                alt4=4;
                }
                break;
            case 15:
                {
                alt4=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalPapin.g:542:2: ( 'count' )
                    {
                    // InternalPapin.g:542:2: ( 'count' )
                    // InternalPapin.g:543:3: 'count'
                    {
                     before(grammarAccess.getAggFunctionAccess().getCountKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getCountKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPapin.g:548:2: ( 'sum' )
                    {
                    // InternalPapin.g:548:2: ( 'sum' )
                    // InternalPapin.g:549:3: 'sum'
                    {
                     before(grammarAccess.getAggFunctionAccess().getSumKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getSumKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPapin.g:554:2: ( 'avg' )
                    {
                    // InternalPapin.g:554:2: ( 'avg' )
                    // InternalPapin.g:555:3: 'avg'
                    {
                     before(grammarAccess.getAggFunctionAccess().getAvgKeyword_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getAvgKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalPapin.g:560:2: ( 'max' )
                    {
                    // InternalPapin.g:560:2: ( 'max' )
                    // InternalPapin.g:561:3: 'max'
                    {
                     before(grammarAccess.getAggFunctionAccess().getMaxKeyword_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getMaxKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalPapin.g:566:2: ( 'min' )
                    {
                    // InternalPapin.g:566:2: ( 'min' )
                    // InternalPapin.g:567:3: 'min'
                    {
                     before(grammarAccess.getAggFunctionAccess().getMinKeyword_4()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getMinKeyword_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggFunction__Alternatives"


    // $ANTLR start "rule__TypeFilter__Alternatives"
    // InternalPapin.g:576:1: rule__TypeFilter__Alternatives : ( ( ruleTypeCompletion ) | ( ruleTypeSelection ) );
    public final void rule__TypeFilter__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:580:1: ( ( ruleTypeCompletion ) | ( ruleTypeSelection ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==25) ) {
                alt5=1;
            }
            else if ( (LA5_0==28) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalPapin.g:581:2: ( ruleTypeCompletion )
                    {
                    // InternalPapin.g:581:2: ( ruleTypeCompletion )
                    // InternalPapin.g:582:3: ruleTypeCompletion
                    {
                     before(grammarAccess.getTypeFilterAccess().getTypeCompletionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleTypeCompletion();

                    state._fsp--;

                     after(grammarAccess.getTypeFilterAccess().getTypeCompletionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPapin.g:587:2: ( ruleTypeSelection )
                    {
                    // InternalPapin.g:587:2: ( ruleTypeSelection )
                    // InternalPapin.g:588:3: ruleTypeSelection
                    {
                     before(grammarAccess.getTypeFilterAccess().getTypeSelectionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleTypeSelection();

                    state._fsp--;

                     after(grammarAccess.getTypeFilterAccess().getTypeSelectionParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeFilter__Alternatives"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalPapin.g:597:1: rule__Primary__Alternatives : ( ( ruleComparison ) | ( ( rule__Primary__Group_1__0 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:601:1: ( ( ruleComparison ) | ( ( rule__Primary__Group_1__0 ) ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID) ) {
                alt6=1;
            }
            else if ( (LA6_0==26) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalPapin.g:602:2: ( ruleComparison )
                    {
                    // InternalPapin.g:602:2: ( ruleComparison )
                    // InternalPapin.g:603:3: ruleComparison
                    {
                     before(grammarAccess.getPrimaryAccess().getComparisonParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleComparison();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getComparisonParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPapin.g:608:2: ( ( rule__Primary__Group_1__0 ) )
                    {
                    // InternalPapin.g:608:2: ( ( rule__Primary__Group_1__0 ) )
                    // InternalPapin.g:609:3: ( rule__Primary__Group_1__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_1()); 
                    // InternalPapin.g:610:3: ( rule__Primary__Group_1__0 )
                    // InternalPapin.g:610:4: rule__Primary__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Comparison__Alternatives"
    // InternalPapin.g:618:1: rule__Comparison__Alternatives : ( ( ( rule__Comparison__Group_0__0 ) ) | ( ( rule__Comparison__Group_1__0 ) ) | ( ( rule__Comparison__Group_2__0 ) ) | ( ( rule__Comparison__Group_3__0 ) ) | ( ( rule__Comparison__Group_4__0 ) ) | ( ( rule__Comparison__Group_5__0 ) ) );
    public final void rule__Comparison__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:622:1: ( ( ( rule__Comparison__Group_0__0 ) ) | ( ( rule__Comparison__Group_1__0 ) ) | ( ( rule__Comparison__Group_2__0 ) ) | ( ( rule__Comparison__Group_3__0 ) ) | ( ( rule__Comparison__Group_4__0 ) ) | ( ( rule__Comparison__Group_5__0 ) ) )
            int alt7=6;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // InternalPapin.g:623:2: ( ( rule__Comparison__Group_0__0 ) )
                    {
                    // InternalPapin.g:623:2: ( ( rule__Comparison__Group_0__0 ) )
                    // InternalPapin.g:624:3: ( rule__Comparison__Group_0__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_0()); 
                    // InternalPapin.g:625:3: ( rule__Comparison__Group_0__0 )
                    // InternalPapin.g:625:4: rule__Comparison__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPapin.g:629:2: ( ( rule__Comparison__Group_1__0 ) )
                    {
                    // InternalPapin.g:629:2: ( ( rule__Comparison__Group_1__0 ) )
                    // InternalPapin.g:630:3: ( rule__Comparison__Group_1__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_1()); 
                    // InternalPapin.g:631:3: ( rule__Comparison__Group_1__0 )
                    // InternalPapin.g:631:4: rule__Comparison__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPapin.g:635:2: ( ( rule__Comparison__Group_2__0 ) )
                    {
                    // InternalPapin.g:635:2: ( ( rule__Comparison__Group_2__0 ) )
                    // InternalPapin.g:636:3: ( rule__Comparison__Group_2__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_2()); 
                    // InternalPapin.g:637:3: ( rule__Comparison__Group_2__0 )
                    // InternalPapin.g:637:4: rule__Comparison__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalPapin.g:641:2: ( ( rule__Comparison__Group_3__0 ) )
                    {
                    // InternalPapin.g:641:2: ( ( rule__Comparison__Group_3__0 ) )
                    // InternalPapin.g:642:3: ( rule__Comparison__Group_3__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_3()); 
                    // InternalPapin.g:643:3: ( rule__Comparison__Group_3__0 )
                    // InternalPapin.g:643:4: rule__Comparison__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalPapin.g:647:2: ( ( rule__Comparison__Group_4__0 ) )
                    {
                    // InternalPapin.g:647:2: ( ( rule__Comparison__Group_4__0 ) )
                    // InternalPapin.g:648:3: ( rule__Comparison__Group_4__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_4()); 
                    // InternalPapin.g:649:3: ( rule__Comparison__Group_4__0 )
                    // InternalPapin.g:649:4: rule__Comparison__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalPapin.g:653:2: ( ( rule__Comparison__Group_5__0 ) )
                    {
                    // InternalPapin.g:653:2: ( ( rule__Comparison__Group_5__0 ) )
                    // InternalPapin.g:654:3: ( rule__Comparison__Group_5__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_5()); 
                    // InternalPapin.g:655:3: ( rule__Comparison__Group_5__0 )
                    // InternalPapin.g:655:4: rule__Comparison__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Alternatives"


    // $ANTLR start "rule__Datasets__Group__0"
    // InternalPapin.g:663:1: rule__Datasets__Group__0 : rule__Datasets__Group__0__Impl rule__Datasets__Group__1 ;
    public final void rule__Datasets__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:667:1: ( rule__Datasets__Group__0__Impl rule__Datasets__Group__1 )
            // InternalPapin.g:668:2: rule__Datasets__Group__0__Impl rule__Datasets__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Datasets__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Datasets__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__0"


    // $ANTLR start "rule__Datasets__Group__0__Impl"
    // InternalPapin.g:675:1: rule__Datasets__Group__0__Impl : ( ( ( 'domainModelNSURI' ) ) ( ( 'domainModelNSURI' )* ) ) ;
    public final void rule__Datasets__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:679:1: ( ( ( ( 'domainModelNSURI' ) ) ( ( 'domainModelNSURI' )* ) ) )
            // InternalPapin.g:680:1: ( ( ( 'domainModelNSURI' ) ) ( ( 'domainModelNSURI' )* ) )
            {
            // InternalPapin.g:680:1: ( ( ( 'domainModelNSURI' ) ) ( ( 'domainModelNSURI' )* ) )
            // InternalPapin.g:681:2: ( ( 'domainModelNSURI' ) ) ( ( 'domainModelNSURI' )* )
            {
            // InternalPapin.g:681:2: ( ( 'domainModelNSURI' ) )
            // InternalPapin.g:682:3: ( 'domainModelNSURI' )
            {
             before(grammarAccess.getDatasetsAccess().getDomainModelNSURIKeyword_0()); 
            // InternalPapin.g:683:3: ( 'domainModelNSURI' )
            // InternalPapin.g:683:4: 'domainModelNSURI'
            {
            match(input,16,FOLLOW_6); 

            }

             after(grammarAccess.getDatasetsAccess().getDomainModelNSURIKeyword_0()); 

            }

            // InternalPapin.g:686:2: ( ( 'domainModelNSURI' )* )
            // InternalPapin.g:687:3: ( 'domainModelNSURI' )*
            {
             before(grammarAccess.getDatasetsAccess().getDomainModelNSURIKeyword_0()); 
            // InternalPapin.g:688:3: ( 'domainModelNSURI' )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==16) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalPapin.g:688:4: 'domainModelNSURI'
            	    {
            	    match(input,16,FOLLOW_6); 

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getDatasetsAccess().getDomainModelNSURIKeyword_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__0__Impl"


    // $ANTLR start "rule__Datasets__Group__1"
    // InternalPapin.g:697:1: rule__Datasets__Group__1 : rule__Datasets__Group__1__Impl rule__Datasets__Group__2 ;
    public final void rule__Datasets__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:701:1: ( rule__Datasets__Group__1__Impl rule__Datasets__Group__2 )
            // InternalPapin.g:702:2: rule__Datasets__Group__1__Impl rule__Datasets__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Datasets__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Datasets__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__1"


    // $ANTLR start "rule__Datasets__Group__1__Impl"
    // InternalPapin.g:709:1: rule__Datasets__Group__1__Impl : ( ( rule__Datasets__DomainModelNSURIAssignment_1 ) ) ;
    public final void rule__Datasets__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:713:1: ( ( ( rule__Datasets__DomainModelNSURIAssignment_1 ) ) )
            // InternalPapin.g:714:1: ( ( rule__Datasets__DomainModelNSURIAssignment_1 ) )
            {
            // InternalPapin.g:714:1: ( ( rule__Datasets__DomainModelNSURIAssignment_1 ) )
            // InternalPapin.g:715:2: ( rule__Datasets__DomainModelNSURIAssignment_1 )
            {
             before(grammarAccess.getDatasetsAccess().getDomainModelNSURIAssignment_1()); 
            // InternalPapin.g:716:2: ( rule__Datasets__DomainModelNSURIAssignment_1 )
            // InternalPapin.g:716:3: rule__Datasets__DomainModelNSURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Datasets__DomainModelNSURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDatasetsAccess().getDomainModelNSURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__1__Impl"


    // $ANTLR start "rule__Datasets__Group__2"
    // InternalPapin.g:724:1: rule__Datasets__Group__2 : rule__Datasets__Group__2__Impl rule__Datasets__Group__3 ;
    public final void rule__Datasets__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:728:1: ( rule__Datasets__Group__2__Impl rule__Datasets__Group__3 )
            // InternalPapin.g:729:2: rule__Datasets__Group__2__Impl rule__Datasets__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Datasets__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Datasets__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__2"


    // $ANTLR start "rule__Datasets__Group__2__Impl"
    // InternalPapin.g:736:1: rule__Datasets__Group__2__Impl : ( ( ( 'domainModelInstance' ) ) ( ( 'domainModelInstance' )* ) ) ;
    public final void rule__Datasets__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:740:1: ( ( ( ( 'domainModelInstance' ) ) ( ( 'domainModelInstance' )* ) ) )
            // InternalPapin.g:741:1: ( ( ( 'domainModelInstance' ) ) ( ( 'domainModelInstance' )* ) )
            {
            // InternalPapin.g:741:1: ( ( ( 'domainModelInstance' ) ) ( ( 'domainModelInstance' )* ) )
            // InternalPapin.g:742:2: ( ( 'domainModelInstance' ) ) ( ( 'domainModelInstance' )* )
            {
            // InternalPapin.g:742:2: ( ( 'domainModelInstance' ) )
            // InternalPapin.g:743:3: ( 'domainModelInstance' )
            {
             before(grammarAccess.getDatasetsAccess().getDomainModelInstanceKeyword_2()); 
            // InternalPapin.g:744:3: ( 'domainModelInstance' )
            // InternalPapin.g:744:4: 'domainModelInstance'
            {
            match(input,17,FOLLOW_8); 

            }

             after(grammarAccess.getDatasetsAccess().getDomainModelInstanceKeyword_2()); 

            }

            // InternalPapin.g:747:2: ( ( 'domainModelInstance' )* )
            // InternalPapin.g:748:3: ( 'domainModelInstance' )*
            {
             before(grammarAccess.getDatasetsAccess().getDomainModelInstanceKeyword_2()); 
            // InternalPapin.g:749:3: ( 'domainModelInstance' )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==17) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalPapin.g:749:4: 'domainModelInstance'
            	    {
            	    match(input,17,FOLLOW_8); 

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getDatasetsAccess().getDomainModelInstanceKeyword_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__2__Impl"


    // $ANTLR start "rule__Datasets__Group__3"
    // InternalPapin.g:758:1: rule__Datasets__Group__3 : rule__Datasets__Group__3__Impl rule__Datasets__Group__4 ;
    public final void rule__Datasets__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:762:1: ( rule__Datasets__Group__3__Impl rule__Datasets__Group__4 )
            // InternalPapin.g:763:2: rule__Datasets__Group__3__Impl rule__Datasets__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Datasets__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Datasets__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__3"


    // $ANTLR start "rule__Datasets__Group__3__Impl"
    // InternalPapin.g:770:1: rule__Datasets__Group__3__Impl : ( ( rule__Datasets__DomainModelInstanceAssignment_3 ) ) ;
    public final void rule__Datasets__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:774:1: ( ( ( rule__Datasets__DomainModelInstanceAssignment_3 ) ) )
            // InternalPapin.g:775:1: ( ( rule__Datasets__DomainModelInstanceAssignment_3 ) )
            {
            // InternalPapin.g:775:1: ( ( rule__Datasets__DomainModelInstanceAssignment_3 ) )
            // InternalPapin.g:776:2: ( rule__Datasets__DomainModelInstanceAssignment_3 )
            {
             before(grammarAccess.getDatasetsAccess().getDomainModelInstanceAssignment_3()); 
            // InternalPapin.g:777:2: ( rule__Datasets__DomainModelInstanceAssignment_3 )
            // InternalPapin.g:777:3: rule__Datasets__DomainModelInstanceAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Datasets__DomainModelInstanceAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getDatasetsAccess().getDomainModelInstanceAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__3__Impl"


    // $ANTLR start "rule__Datasets__Group__4"
    // InternalPapin.g:785:1: rule__Datasets__Group__4 : rule__Datasets__Group__4__Impl ;
    public final void rule__Datasets__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:789:1: ( rule__Datasets__Group__4__Impl )
            // InternalPapin.g:790:2: rule__Datasets__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Datasets__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__4"


    // $ANTLR start "rule__Datasets__Group__4__Impl"
    // InternalPapin.g:796:1: rule__Datasets__Group__4__Impl : ( ( rule__Datasets__DatasetsAssignment_4 )* ) ;
    public final void rule__Datasets__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:800:1: ( ( ( rule__Datasets__DatasetsAssignment_4 )* ) )
            // InternalPapin.g:801:1: ( ( rule__Datasets__DatasetsAssignment_4 )* )
            {
            // InternalPapin.g:801:1: ( ( rule__Datasets__DatasetsAssignment_4 )* )
            // InternalPapin.g:802:2: ( rule__Datasets__DatasetsAssignment_4 )*
            {
             before(grammarAccess.getDatasetsAccess().getDatasetsAssignment_4()); 
            // InternalPapin.g:803:2: ( rule__Datasets__DatasetsAssignment_4 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==18) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalPapin.g:803:3: rule__Datasets__DatasetsAssignment_4
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Datasets__DatasetsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getDatasetsAccess().getDatasetsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__Group__4__Impl"


    // $ANTLR start "rule__Dataset__Group__0"
    // InternalPapin.g:812:1: rule__Dataset__Group__0 : rule__Dataset__Group__0__Impl rule__Dataset__Group__1 ;
    public final void rule__Dataset__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:816:1: ( rule__Dataset__Group__0__Impl rule__Dataset__Group__1 )
            // InternalPapin.g:817:2: rule__Dataset__Group__0__Impl rule__Dataset__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Dataset__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dataset__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__0"


    // $ANTLR start "rule__Dataset__Group__0__Impl"
    // InternalPapin.g:824:1: rule__Dataset__Group__0__Impl : ( 'dataset' ) ;
    public final void rule__Dataset__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:828:1: ( ( 'dataset' ) )
            // InternalPapin.g:829:1: ( 'dataset' )
            {
            // InternalPapin.g:829:1: ( 'dataset' )
            // InternalPapin.g:830:2: 'dataset'
            {
             before(grammarAccess.getDatasetAccess().getDatasetKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getDatasetAccess().getDatasetKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__0__Impl"


    // $ANTLR start "rule__Dataset__Group__1"
    // InternalPapin.g:839:1: rule__Dataset__Group__1 : rule__Dataset__Group__1__Impl rule__Dataset__Group__2 ;
    public final void rule__Dataset__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:843:1: ( rule__Dataset__Group__1__Impl rule__Dataset__Group__2 )
            // InternalPapin.g:844:2: rule__Dataset__Group__1__Impl rule__Dataset__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__Dataset__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dataset__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__1"


    // $ANTLR start "rule__Dataset__Group__1__Impl"
    // InternalPapin.g:851:1: rule__Dataset__Group__1__Impl : ( ( rule__Dataset__NameAssignment_1 ) ) ;
    public final void rule__Dataset__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:855:1: ( ( ( rule__Dataset__NameAssignment_1 ) ) )
            // InternalPapin.g:856:1: ( ( rule__Dataset__NameAssignment_1 ) )
            {
            // InternalPapin.g:856:1: ( ( rule__Dataset__NameAssignment_1 ) )
            // InternalPapin.g:857:2: ( rule__Dataset__NameAssignment_1 )
            {
             before(grammarAccess.getDatasetAccess().getNameAssignment_1()); 
            // InternalPapin.g:858:2: ( rule__Dataset__NameAssignment_1 )
            // InternalPapin.g:858:3: rule__Dataset__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Dataset__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDatasetAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__1__Impl"


    // $ANTLR start "rule__Dataset__Group__2"
    // InternalPapin.g:866:1: rule__Dataset__Group__2 : rule__Dataset__Group__2__Impl rule__Dataset__Group__3 ;
    public final void rule__Dataset__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:870:1: ( rule__Dataset__Group__2__Impl rule__Dataset__Group__3 )
            // InternalPapin.g:871:2: rule__Dataset__Group__2__Impl rule__Dataset__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__Dataset__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dataset__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__2"


    // $ANTLR start "rule__Dataset__Group__2__Impl"
    // InternalPapin.g:878:1: rule__Dataset__Group__2__Impl : ( 'using' ) ;
    public final void rule__Dataset__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:882:1: ( ( 'using' ) )
            // InternalPapin.g:883:1: ( 'using' )
            {
            // InternalPapin.g:883:1: ( 'using' )
            // InternalPapin.g:884:2: 'using'
            {
             before(grammarAccess.getDatasetAccess().getUsingKeyword_2()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getDatasetAccess().getUsingKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__2__Impl"


    // $ANTLR start "rule__Dataset__Group__3"
    // InternalPapin.g:893:1: rule__Dataset__Group__3 : rule__Dataset__Group__3__Impl rule__Dataset__Group__4 ;
    public final void rule__Dataset__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:897:1: ( rule__Dataset__Group__3__Impl rule__Dataset__Group__4 )
            // InternalPapin.g:898:2: rule__Dataset__Group__3__Impl rule__Dataset__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__Dataset__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dataset__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__3"


    // $ANTLR start "rule__Dataset__Group__3__Impl"
    // InternalPapin.g:905:1: rule__Dataset__Group__3__Impl : ( ( rule__Dataset__UsingAssignment_3 ) ) ;
    public final void rule__Dataset__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:909:1: ( ( ( rule__Dataset__UsingAssignment_3 ) ) )
            // InternalPapin.g:910:1: ( ( rule__Dataset__UsingAssignment_3 ) )
            {
            // InternalPapin.g:910:1: ( ( rule__Dataset__UsingAssignment_3 ) )
            // InternalPapin.g:911:2: ( rule__Dataset__UsingAssignment_3 )
            {
             before(grammarAccess.getDatasetAccess().getUsingAssignment_3()); 
            // InternalPapin.g:912:2: ( rule__Dataset__UsingAssignment_3 )
            // InternalPapin.g:912:3: rule__Dataset__UsingAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Dataset__UsingAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getDatasetAccess().getUsingAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__3__Impl"


    // $ANTLR start "rule__Dataset__Group__4"
    // InternalPapin.g:920:1: rule__Dataset__Group__4 : rule__Dataset__Group__4__Impl rule__Dataset__Group__5 ;
    public final void rule__Dataset__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:924:1: ( rule__Dataset__Group__4__Impl rule__Dataset__Group__5 )
            // InternalPapin.g:925:2: rule__Dataset__Group__4__Impl rule__Dataset__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__Dataset__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dataset__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__4"


    // $ANTLR start "rule__Dataset__Group__4__Impl"
    // InternalPapin.g:932:1: rule__Dataset__Group__4__Impl : ( '{' ) ;
    public final void rule__Dataset__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:936:1: ( ( '{' ) )
            // InternalPapin.g:937:1: ( '{' )
            {
            // InternalPapin.g:937:1: ( '{' )
            // InternalPapin.g:938:2: '{'
            {
             before(grammarAccess.getDatasetAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getDatasetAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__4__Impl"


    // $ANTLR start "rule__Dataset__Group__5"
    // InternalPapin.g:947:1: rule__Dataset__Group__5 : rule__Dataset__Group__5__Impl rule__Dataset__Group__6 ;
    public final void rule__Dataset__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:951:1: ( rule__Dataset__Group__5__Impl rule__Dataset__Group__6 )
            // InternalPapin.g:952:2: rule__Dataset__Group__5__Impl rule__Dataset__Group__6
            {
            pushFollow(FOLLOW_14);
            rule__Dataset__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dataset__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__5"


    // $ANTLR start "rule__Dataset__Group__5__Impl"
    // InternalPapin.g:959:1: rule__Dataset__Group__5__Impl : ( ( rule__Dataset__AttributeFilterAssignment_5 )? ) ;
    public final void rule__Dataset__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:963:1: ( ( ( rule__Dataset__AttributeFilterAssignment_5 )? ) )
            // InternalPapin.g:964:1: ( ( rule__Dataset__AttributeFilterAssignment_5 )? )
            {
            // InternalPapin.g:964:1: ( ( rule__Dataset__AttributeFilterAssignment_5 )? )
            // InternalPapin.g:965:2: ( rule__Dataset__AttributeFilterAssignment_5 )?
            {
             before(grammarAccess.getDatasetAccess().getAttributeFilterAssignment_5()); 
            // InternalPapin.g:966:2: ( rule__Dataset__AttributeFilterAssignment_5 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==29) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalPapin.g:966:3: rule__Dataset__AttributeFilterAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__Dataset__AttributeFilterAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDatasetAccess().getAttributeFilterAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__5__Impl"


    // $ANTLR start "rule__Dataset__Group__6"
    // InternalPapin.g:974:1: rule__Dataset__Group__6 : rule__Dataset__Group__6__Impl rule__Dataset__Group__7 ;
    public final void rule__Dataset__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:978:1: ( rule__Dataset__Group__6__Impl rule__Dataset__Group__7 )
            // InternalPapin.g:979:2: rule__Dataset__Group__6__Impl rule__Dataset__Group__7
            {
            pushFollow(FOLLOW_14);
            rule__Dataset__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dataset__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__6"


    // $ANTLR start "rule__Dataset__Group__6__Impl"
    // InternalPapin.g:986:1: rule__Dataset__Group__6__Impl : ( ( rule__Dataset__InstancesFilterAssignment_6 )? ) ;
    public final void rule__Dataset__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:990:1: ( ( ( rule__Dataset__InstancesFilterAssignment_6 )? ) )
            // InternalPapin.g:991:1: ( ( rule__Dataset__InstancesFilterAssignment_6 )? )
            {
            // InternalPapin.g:991:1: ( ( rule__Dataset__InstancesFilterAssignment_6 )? )
            // InternalPapin.g:992:2: ( rule__Dataset__InstancesFilterAssignment_6 )?
            {
             before(grammarAccess.getDatasetAccess().getInstancesFilterAssignment_6()); 
            // InternalPapin.g:993:2: ( rule__Dataset__InstancesFilterAssignment_6 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==32) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalPapin.g:993:3: rule__Dataset__InstancesFilterAssignment_6
                    {
                    pushFollow(FOLLOW_2);
                    rule__Dataset__InstancesFilterAssignment_6();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDatasetAccess().getInstancesFilterAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__6__Impl"


    // $ANTLR start "rule__Dataset__Group__7"
    // InternalPapin.g:1001:1: rule__Dataset__Group__7 : rule__Dataset__Group__7__Impl rule__Dataset__Group__8 ;
    public final void rule__Dataset__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1005:1: ( rule__Dataset__Group__7__Impl rule__Dataset__Group__8 )
            // InternalPapin.g:1006:2: rule__Dataset__Group__7__Impl rule__Dataset__Group__8
            {
            pushFollow(FOLLOW_14);
            rule__Dataset__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dataset__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__7"


    // $ANTLR start "rule__Dataset__Group__7__Impl"
    // InternalPapin.g:1013:1: rule__Dataset__Group__7__Impl : ( ( rule__Dataset__IncludedReferencesAssignment_7 )* ) ;
    public final void rule__Dataset__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1017:1: ( ( ( rule__Dataset__IncludedReferencesAssignment_7 )* ) )
            // InternalPapin.g:1018:1: ( ( rule__Dataset__IncludedReferencesAssignment_7 )* )
            {
            // InternalPapin.g:1018:1: ( ( rule__Dataset__IncludedReferencesAssignment_7 )* )
            // InternalPapin.g:1019:2: ( rule__Dataset__IncludedReferencesAssignment_7 )*
            {
             before(grammarAccess.getDatasetAccess().getIncludedReferencesAssignment_7()); 
            // InternalPapin.g:1020:2: ( rule__Dataset__IncludedReferencesAssignment_7 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==22||LA13_0==24) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalPapin.g:1020:3: rule__Dataset__IncludedReferencesAssignment_7
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Dataset__IncludedReferencesAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getDatasetAccess().getIncludedReferencesAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__7__Impl"


    // $ANTLR start "rule__Dataset__Group__8"
    // InternalPapin.g:1028:1: rule__Dataset__Group__8 : rule__Dataset__Group__8__Impl rule__Dataset__Group__9 ;
    public final void rule__Dataset__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1032:1: ( rule__Dataset__Group__8__Impl rule__Dataset__Group__9 )
            // InternalPapin.g:1033:2: rule__Dataset__Group__8__Impl rule__Dataset__Group__9
            {
            pushFollow(FOLLOW_14);
            rule__Dataset__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dataset__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__8"


    // $ANTLR start "rule__Dataset__Group__8__Impl"
    // InternalPapin.g:1040:1: rule__Dataset__Group__8__Impl : ( ( rule__Dataset__TypeFilterAssignment_8 )? ) ;
    public final void rule__Dataset__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1044:1: ( ( ( rule__Dataset__TypeFilterAssignment_8 )? ) )
            // InternalPapin.g:1045:1: ( ( rule__Dataset__TypeFilterAssignment_8 )? )
            {
            // InternalPapin.g:1045:1: ( ( rule__Dataset__TypeFilterAssignment_8 )? )
            // InternalPapin.g:1046:2: ( rule__Dataset__TypeFilterAssignment_8 )?
            {
             before(grammarAccess.getDatasetAccess().getTypeFilterAssignment_8()); 
            // InternalPapin.g:1047:2: ( rule__Dataset__TypeFilterAssignment_8 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==25||LA14_0==28) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalPapin.g:1047:3: rule__Dataset__TypeFilterAssignment_8
                    {
                    pushFollow(FOLLOW_2);
                    rule__Dataset__TypeFilterAssignment_8();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDatasetAccess().getTypeFilterAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__8__Impl"


    // $ANTLR start "rule__Dataset__Group__9"
    // InternalPapin.g:1055:1: rule__Dataset__Group__9 : rule__Dataset__Group__9__Impl ;
    public final void rule__Dataset__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1059:1: ( rule__Dataset__Group__9__Impl )
            // InternalPapin.g:1060:2: rule__Dataset__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Dataset__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__9"


    // $ANTLR start "rule__Dataset__Group__9__Impl"
    // InternalPapin.g:1066:1: rule__Dataset__Group__9__Impl : ( '}' ) ;
    public final void rule__Dataset__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1070:1: ( ( '}' ) )
            // InternalPapin.g:1071:1: ( '}' )
            {
            // InternalPapin.g:1071:1: ( '}' )
            // InternalPapin.g:1072:2: '}'
            {
             before(grammarAccess.getDatasetAccess().getRightCurlyBracketKeyword_9()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getDatasetAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__Group__9__Impl"


    // $ANTLR start "rule__SimpleReference__Group__0"
    // InternalPapin.g:1082:1: rule__SimpleReference__Group__0 : rule__SimpleReference__Group__0__Impl rule__SimpleReference__Group__1 ;
    public final void rule__SimpleReference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1086:1: ( rule__SimpleReference__Group__0__Impl rule__SimpleReference__Group__1 )
            // InternalPapin.g:1087:2: rule__SimpleReference__Group__0__Impl rule__SimpleReference__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__SimpleReference__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__0"


    // $ANTLR start "rule__SimpleReference__Group__0__Impl"
    // InternalPapin.g:1094:1: rule__SimpleReference__Group__0__Impl : ( 'include' ) ;
    public final void rule__SimpleReference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1098:1: ( ( 'include' ) )
            // InternalPapin.g:1099:1: ( 'include' )
            {
            // InternalPapin.g:1099:1: ( 'include' )
            // InternalPapin.g:1100:2: 'include'
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group__1"
    // InternalPapin.g:1109:1: rule__SimpleReference__Group__1 : rule__SimpleReference__Group__1__Impl rule__SimpleReference__Group__2 ;
    public final void rule__SimpleReference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1113:1: ( rule__SimpleReference__Group__1__Impl rule__SimpleReference__Group__2 )
            // InternalPapin.g:1114:2: rule__SimpleReference__Group__1__Impl rule__SimpleReference__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__SimpleReference__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__1"


    // $ANTLR start "rule__SimpleReference__Group__1__Impl"
    // InternalPapin.g:1121:1: rule__SimpleReference__Group__1__Impl : ( ( rule__SimpleReference__CatAssignment_1 ) ) ;
    public final void rule__SimpleReference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1125:1: ( ( ( rule__SimpleReference__CatAssignment_1 ) ) )
            // InternalPapin.g:1126:1: ( ( rule__SimpleReference__CatAssignment_1 ) )
            {
            // InternalPapin.g:1126:1: ( ( rule__SimpleReference__CatAssignment_1 ) )
            // InternalPapin.g:1127:2: ( rule__SimpleReference__CatAssignment_1 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getCatAssignment_1()); 
            // InternalPapin.g:1128:2: ( rule__SimpleReference__CatAssignment_1 )
            // InternalPapin.g:1128:3: rule__SimpleReference__CatAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__CatAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getCatAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group__2"
    // InternalPapin.g:1136:1: rule__SimpleReference__Group__2 : rule__SimpleReference__Group__2__Impl rule__SimpleReference__Group__3 ;
    public final void rule__SimpleReference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1140:1: ( rule__SimpleReference__Group__2__Impl rule__SimpleReference__Group__3 )
            // InternalPapin.g:1141:2: rule__SimpleReference__Group__2__Impl rule__SimpleReference__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__SimpleReference__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__2"


    // $ANTLR start "rule__SimpleReference__Group__2__Impl"
    // InternalPapin.g:1148:1: rule__SimpleReference__Group__2__Impl : ( ( rule__SimpleReference__AttributeFilterAssignment_2 )? ) ;
    public final void rule__SimpleReference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1152:1: ( ( ( rule__SimpleReference__AttributeFilterAssignment_2 )? ) )
            // InternalPapin.g:1153:1: ( ( rule__SimpleReference__AttributeFilterAssignment_2 )? )
            {
            // InternalPapin.g:1153:1: ( ( rule__SimpleReference__AttributeFilterAssignment_2 )? )
            // InternalPapin.g:1154:2: ( rule__SimpleReference__AttributeFilterAssignment_2 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAssignment_2()); 
            // InternalPapin.g:1155:2: ( rule__SimpleReference__AttributeFilterAssignment_2 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==29) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalPapin.g:1155:3: rule__SimpleReference__AttributeFilterAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__AttributeFilterAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__2__Impl"


    // $ANTLR start "rule__SimpleReference__Group__3"
    // InternalPapin.g:1163:1: rule__SimpleReference__Group__3 : rule__SimpleReference__Group__3__Impl rule__SimpleReference__Group__4 ;
    public final void rule__SimpleReference__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1167:1: ( rule__SimpleReference__Group__3__Impl rule__SimpleReference__Group__4 )
            // InternalPapin.g:1168:2: rule__SimpleReference__Group__3__Impl rule__SimpleReference__Group__4
            {
            pushFollow(FOLLOW_16);
            rule__SimpleReference__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__3"


    // $ANTLR start "rule__SimpleReference__Group__3__Impl"
    // InternalPapin.g:1175:1: rule__SimpleReference__Group__3__Impl : ( ( rule__SimpleReference__Group_3__0 )? ) ;
    public final void rule__SimpleReference__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1179:1: ( ( ( rule__SimpleReference__Group_3__0 )? ) )
            // InternalPapin.g:1180:1: ( ( rule__SimpleReference__Group_3__0 )? )
            {
            // InternalPapin.g:1180:1: ( ( rule__SimpleReference__Group_3__0 )? )
            // InternalPapin.g:1181:2: ( rule__SimpleReference__Group_3__0 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getGroup_3()); 
            // InternalPapin.g:1182:2: ( rule__SimpleReference__Group_3__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==23) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalPapin.g:1182:3: rule__SimpleReference__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__3__Impl"


    // $ANTLR start "rule__SimpleReference__Group__4"
    // InternalPapin.g:1190:1: rule__SimpleReference__Group__4 : rule__SimpleReference__Group__4__Impl ;
    public final void rule__SimpleReference__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1194:1: ( rule__SimpleReference__Group__4__Impl )
            // InternalPapin.g:1195:2: rule__SimpleReference__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__4"


    // $ANTLR start "rule__SimpleReference__Group__4__Impl"
    // InternalPapin.g:1201:1: rule__SimpleReference__Group__4__Impl : ( ( rule__SimpleReference__Group_4__0 )? ) ;
    public final void rule__SimpleReference__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1205:1: ( ( ( rule__SimpleReference__Group_4__0 )? ) )
            // InternalPapin.g:1206:1: ( ( rule__SimpleReference__Group_4__0 )? )
            {
            // InternalPapin.g:1206:1: ( ( rule__SimpleReference__Group_4__0 )? )
            // InternalPapin.g:1207:2: ( rule__SimpleReference__Group_4__0 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getGroup_4()); 
            // InternalPapin.g:1208:2: ( rule__SimpleReference__Group_4__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==20) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalPapin.g:1208:3: rule__SimpleReference__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__4__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3__0"
    // InternalPapin.g:1217:1: rule__SimpleReference__Group_3__0 : rule__SimpleReference__Group_3__0__Impl rule__SimpleReference__Group_3__1 ;
    public final void rule__SimpleReference__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1221:1: ( rule__SimpleReference__Group_3__0__Impl rule__SimpleReference__Group_3__1 )
            // InternalPapin.g:1222:2: rule__SimpleReference__Group_3__0__Impl rule__SimpleReference__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__SimpleReference__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__0"


    // $ANTLR start "rule__SimpleReference__Group_3__0__Impl"
    // InternalPapin.g:1229:1: rule__SimpleReference__Group_3__0__Impl : ( 'by' ) ;
    public final void rule__SimpleReference__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1233:1: ( ( 'by' ) )
            // InternalPapin.g:1234:1: ( 'by' )
            {
            // InternalPapin.g:1234:1: ( 'by' )
            // InternalPapin.g:1235:2: 'by'
            {
             before(grammarAccess.getSimpleReferenceAccess().getByKeyword_3_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getByKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3__1"
    // InternalPapin.g:1244:1: rule__SimpleReference__Group_3__1 : rule__SimpleReference__Group_3__1__Impl rule__SimpleReference__Group_3__2 ;
    public final void rule__SimpleReference__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1248:1: ( rule__SimpleReference__Group_3__1__Impl rule__SimpleReference__Group_3__2 )
            // InternalPapin.g:1249:2: rule__SimpleReference__Group_3__1__Impl rule__SimpleReference__Group_3__2
            {
            pushFollow(FOLLOW_17);
            rule__SimpleReference__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__1"


    // $ANTLR start "rule__SimpleReference__Group_3__1__Impl"
    // InternalPapin.g:1256:1: rule__SimpleReference__Group_3__1__Impl : ( ( rule__SimpleReference__PivotingIdAssignment_3_1 ) ) ;
    public final void rule__SimpleReference__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1260:1: ( ( ( rule__SimpleReference__PivotingIdAssignment_3_1 ) ) )
            // InternalPapin.g:1261:1: ( ( rule__SimpleReference__PivotingIdAssignment_3_1 ) )
            {
            // InternalPapin.g:1261:1: ( ( rule__SimpleReference__PivotingIdAssignment_3_1 ) )
            // InternalPapin.g:1262:2: ( rule__SimpleReference__PivotingIdAssignment_3_1 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getPivotingIdAssignment_3_1()); 
            // InternalPapin.g:1263:2: ( rule__SimpleReference__PivotingIdAssignment_3_1 )
            // InternalPapin.g:1263:3: rule__SimpleReference__PivotingIdAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__PivotingIdAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getPivotingIdAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3__2"
    // InternalPapin.g:1271:1: rule__SimpleReference__Group_3__2 : rule__SimpleReference__Group_3__2__Impl ;
    public final void rule__SimpleReference__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1275:1: ( rule__SimpleReference__Group_3__2__Impl )
            // InternalPapin.g:1276:2: rule__SimpleReference__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__2"


    // $ANTLR start "rule__SimpleReference__Group_3__2__Impl"
    // InternalPapin.g:1282:1: rule__SimpleReference__Group_3__2__Impl : ( ( rule__SimpleReference__InstancesFilterAssignment_3_2 )? ) ;
    public final void rule__SimpleReference__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1286:1: ( ( ( rule__SimpleReference__InstancesFilterAssignment_3_2 )? ) )
            // InternalPapin.g:1287:1: ( ( rule__SimpleReference__InstancesFilterAssignment_3_2 )? )
            {
            // InternalPapin.g:1287:1: ( ( rule__SimpleReference__InstancesFilterAssignment_3_2 )? )
            // InternalPapin.g:1288:2: ( rule__SimpleReference__InstancesFilterAssignment_3_2 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getInstancesFilterAssignment_3_2()); 
            // InternalPapin.g:1289:2: ( rule__SimpleReference__InstancesFilterAssignment_3_2 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==32) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalPapin.g:1289:3: rule__SimpleReference__InstancesFilterAssignment_3_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__InstancesFilterAssignment_3_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getInstancesFilterAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__2__Impl"


    // $ANTLR start "rule__SimpleReference__Group_4__0"
    // InternalPapin.g:1298:1: rule__SimpleReference__Group_4__0 : rule__SimpleReference__Group_4__0__Impl rule__SimpleReference__Group_4__1 ;
    public final void rule__SimpleReference__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1302:1: ( rule__SimpleReference__Group_4__0__Impl rule__SimpleReference__Group_4__1 )
            // InternalPapin.g:1303:2: rule__SimpleReference__Group_4__0__Impl rule__SimpleReference__Group_4__1
            {
            pushFollow(FOLLOW_18);
            rule__SimpleReference__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4__0"


    // $ANTLR start "rule__SimpleReference__Group_4__0__Impl"
    // InternalPapin.g:1310:1: rule__SimpleReference__Group_4__0__Impl : ( '{' ) ;
    public final void rule__SimpleReference__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1314:1: ( ( '{' ) )
            // InternalPapin.g:1315:1: ( '{' )
            {
            // InternalPapin.g:1315:1: ( '{' )
            // InternalPapin.g:1316:2: '{'
            {
             before(grammarAccess.getSimpleReferenceAccess().getLeftCurlyBracketKeyword_4_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getLeftCurlyBracketKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group_4__1"
    // InternalPapin.g:1325:1: rule__SimpleReference__Group_4__1 : rule__SimpleReference__Group_4__1__Impl rule__SimpleReference__Group_4__2 ;
    public final void rule__SimpleReference__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1329:1: ( rule__SimpleReference__Group_4__1__Impl rule__SimpleReference__Group_4__2 )
            // InternalPapin.g:1330:2: rule__SimpleReference__Group_4__1__Impl rule__SimpleReference__Group_4__2
            {
            pushFollow(FOLLOW_18);
            rule__SimpleReference__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4__1"


    // $ANTLR start "rule__SimpleReference__Group_4__1__Impl"
    // InternalPapin.g:1337:1: rule__SimpleReference__Group_4__1__Impl : ( ( rule__SimpleReference__Group_4_1__0 )* ) ;
    public final void rule__SimpleReference__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1341:1: ( ( ( rule__SimpleReference__Group_4_1__0 )* ) )
            // InternalPapin.g:1342:1: ( ( rule__SimpleReference__Group_4_1__0 )* )
            {
            // InternalPapin.g:1342:1: ( ( rule__SimpleReference__Group_4_1__0 )* )
            // InternalPapin.g:1343:2: ( rule__SimpleReference__Group_4_1__0 )*
            {
             before(grammarAccess.getSimpleReferenceAccess().getGroup_4_1()); 
            // InternalPapin.g:1344:2: ( rule__SimpleReference__Group_4_1__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==22) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalPapin.g:1344:3: rule__SimpleReference__Group_4_1__0
            	    {
            	    pushFollow(FOLLOW_19);
            	    rule__SimpleReference__Group_4_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getSimpleReferenceAccess().getGroup_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group_4__2"
    // InternalPapin.g:1352:1: rule__SimpleReference__Group_4__2 : rule__SimpleReference__Group_4__2__Impl rule__SimpleReference__Group_4__3 ;
    public final void rule__SimpleReference__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1356:1: ( rule__SimpleReference__Group_4__2__Impl rule__SimpleReference__Group_4__3 )
            // InternalPapin.g:1357:2: rule__SimpleReference__Group_4__2__Impl rule__SimpleReference__Group_4__3
            {
            pushFollow(FOLLOW_18);
            rule__SimpleReference__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4__2"


    // $ANTLR start "rule__SimpleReference__Group_4__2__Impl"
    // InternalPapin.g:1364:1: rule__SimpleReference__Group_4__2__Impl : ( ( rule__SimpleReference__TypeFilterAssignment_4_2 )? ) ;
    public final void rule__SimpleReference__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1368:1: ( ( ( rule__SimpleReference__TypeFilterAssignment_4_2 )? ) )
            // InternalPapin.g:1369:1: ( ( rule__SimpleReference__TypeFilterAssignment_4_2 )? )
            {
            // InternalPapin.g:1369:1: ( ( rule__SimpleReference__TypeFilterAssignment_4_2 )? )
            // InternalPapin.g:1370:2: ( rule__SimpleReference__TypeFilterAssignment_4_2 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getTypeFilterAssignment_4_2()); 
            // InternalPapin.g:1371:2: ( rule__SimpleReference__TypeFilterAssignment_4_2 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==25||LA20_0==28) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalPapin.g:1371:3: rule__SimpleReference__TypeFilterAssignment_4_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__TypeFilterAssignment_4_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getTypeFilterAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4__2__Impl"


    // $ANTLR start "rule__SimpleReference__Group_4__3"
    // InternalPapin.g:1379:1: rule__SimpleReference__Group_4__3 : rule__SimpleReference__Group_4__3__Impl ;
    public final void rule__SimpleReference__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1383:1: ( rule__SimpleReference__Group_4__3__Impl )
            // InternalPapin.g:1384:2: rule__SimpleReference__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4__3"


    // $ANTLR start "rule__SimpleReference__Group_4__3__Impl"
    // InternalPapin.g:1390:1: rule__SimpleReference__Group_4__3__Impl : ( '}' ) ;
    public final void rule__SimpleReference__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1394:1: ( ( '}' ) )
            // InternalPapin.g:1395:1: ( '}' )
            {
            // InternalPapin.g:1395:1: ( '}' )
            // InternalPapin.g:1396:2: '}'
            {
             before(grammarAccess.getSimpleReferenceAccess().getRightCurlyBracketKeyword_4_3()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getRightCurlyBracketKeyword_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4__3__Impl"


    // $ANTLR start "rule__SimpleReference__Group_4_1__0"
    // InternalPapin.g:1406:1: rule__SimpleReference__Group_4_1__0 : rule__SimpleReference__Group_4_1__0__Impl rule__SimpleReference__Group_4_1__1 ;
    public final void rule__SimpleReference__Group_4_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1410:1: ( rule__SimpleReference__Group_4_1__0__Impl rule__SimpleReference__Group_4_1__1 )
            // InternalPapin.g:1411:2: rule__SimpleReference__Group_4_1__0__Impl rule__SimpleReference__Group_4_1__1
            {
            pushFollow(FOLLOW_11);
            rule__SimpleReference__Group_4_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_4_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4_1__0"


    // $ANTLR start "rule__SimpleReference__Group_4_1__0__Impl"
    // InternalPapin.g:1418:1: rule__SimpleReference__Group_4_1__0__Impl : ( 'include' ) ;
    public final void rule__SimpleReference__Group_4_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1422:1: ( ( 'include' ) )
            // InternalPapin.g:1423:1: ( 'include' )
            {
            // InternalPapin.g:1423:1: ( 'include' )
            // InternalPapin.g:1424:2: 'include'
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_4_1_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4_1__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group_4_1__1"
    // InternalPapin.g:1433:1: rule__SimpleReference__Group_4_1__1 : rule__SimpleReference__Group_4_1__1__Impl ;
    public final void rule__SimpleReference__Group_4_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1437:1: ( rule__SimpleReference__Group_4_1__1__Impl )
            // InternalPapin.g:1438:2: rule__SimpleReference__Group_4_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_4_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4_1__1"


    // $ANTLR start "rule__SimpleReference__Group_4_1__1__Impl"
    // InternalPapin.g:1444:1: rule__SimpleReference__Group_4_1__1__Impl : ( ( rule__SimpleReference__CausesAssignment_4_1_1 ) ) ;
    public final void rule__SimpleReference__Group_4_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1448:1: ( ( ( rule__SimpleReference__CausesAssignment_4_1_1 ) ) )
            // InternalPapin.g:1449:1: ( ( rule__SimpleReference__CausesAssignment_4_1_1 ) )
            {
            // InternalPapin.g:1449:1: ( ( rule__SimpleReference__CausesAssignment_4_1_1 ) )
            // InternalPapin.g:1450:2: ( rule__SimpleReference__CausesAssignment_4_1_1 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getCausesAssignment_4_1_1()); 
            // InternalPapin.g:1451:2: ( rule__SimpleReference__CausesAssignment_4_1_1 )
            // InternalPapin.g:1451:3: rule__SimpleReference__CausesAssignment_4_1_1
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__CausesAssignment_4_1_1();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getCausesAssignment_4_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_4_1__1__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__0"
    // InternalPapin.g:1460:1: rule__AggregatedReference__Group__0 : rule__AggregatedReference__Group__0__Impl rule__AggregatedReference__Group__1 ;
    public final void rule__AggregatedReference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1464:1: ( rule__AggregatedReference__Group__0__Impl rule__AggregatedReference__Group__1 )
            // InternalPapin.g:1465:2: rule__AggregatedReference__Group__0__Impl rule__AggregatedReference__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__AggregatedReference__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__0"


    // $ANTLR start "rule__AggregatedReference__Group__0__Impl"
    // InternalPapin.g:1472:1: rule__AggregatedReference__Group__0__Impl : ( 'calculate' ) ;
    public final void rule__AggregatedReference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1476:1: ( ( 'calculate' ) )
            // InternalPapin.g:1477:1: ( 'calculate' )
            {
            // InternalPapin.g:1477:1: ( 'calculate' )
            // InternalPapin.g:1478:2: 'calculate'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getCalculateKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getCalculateKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__0__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__1"
    // InternalPapin.g:1487:1: rule__AggregatedReference__Group__1 : rule__AggregatedReference__Group__1__Impl rule__AggregatedReference__Group__2 ;
    public final void rule__AggregatedReference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1491:1: ( rule__AggregatedReference__Group__1__Impl rule__AggregatedReference__Group__2 )
            // InternalPapin.g:1492:2: rule__AggregatedReference__Group__1__Impl rule__AggregatedReference__Group__2
            {
            pushFollow(FOLLOW_20);
            rule__AggregatedReference__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__1"


    // $ANTLR start "rule__AggregatedReference__Group__1__Impl"
    // InternalPapin.g:1499:1: rule__AggregatedReference__Group__1__Impl : ( ( rule__AggregatedReference__CatAssignment_1 ) ) ;
    public final void rule__AggregatedReference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1503:1: ( ( ( rule__AggregatedReference__CatAssignment_1 ) ) )
            // InternalPapin.g:1504:1: ( ( rule__AggregatedReference__CatAssignment_1 ) )
            {
            // InternalPapin.g:1504:1: ( ( rule__AggregatedReference__CatAssignment_1 ) )
            // InternalPapin.g:1505:2: ( rule__AggregatedReference__CatAssignment_1 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getCatAssignment_1()); 
            // InternalPapin.g:1506:2: ( rule__AggregatedReference__CatAssignment_1 )
            // InternalPapin.g:1506:3: rule__AggregatedReference__CatAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__CatAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getCatAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__1__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__2"
    // InternalPapin.g:1514:1: rule__AggregatedReference__Group__2 : rule__AggregatedReference__Group__2__Impl rule__AggregatedReference__Group__3 ;
    public final void rule__AggregatedReference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1518:1: ( rule__AggregatedReference__Group__2__Impl rule__AggregatedReference__Group__3 )
            // InternalPapin.g:1519:2: rule__AggregatedReference__Group__2__Impl rule__AggregatedReference__Group__3
            {
            pushFollow(FOLLOW_20);
            rule__AggregatedReference__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__2"


    // $ANTLR start "rule__AggregatedReference__Group__2__Impl"
    // InternalPapin.g:1526:1: rule__AggregatedReference__Group__2__Impl : ( ( rule__AggregatedReference__CausesAssignment_2 )? ) ;
    public final void rule__AggregatedReference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1530:1: ( ( ( rule__AggregatedReference__CausesAssignment_2 )? ) )
            // InternalPapin.g:1531:1: ( ( rule__AggregatedReference__CausesAssignment_2 )? )
            {
            // InternalPapin.g:1531:1: ( ( rule__AggregatedReference__CausesAssignment_2 )? )
            // InternalPapin.g:1532:2: ( rule__AggregatedReference__CausesAssignment_2 )?
            {
             before(grammarAccess.getAggregatedReferenceAccess().getCausesAssignment_2()); 
            // InternalPapin.g:1533:2: ( rule__AggregatedReference__CausesAssignment_2 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==RULE_ID) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalPapin.g:1533:3: rule__AggregatedReference__CausesAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__AggregatedReference__CausesAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAggregatedReferenceAccess().getCausesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__2__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__3"
    // InternalPapin.g:1541:1: rule__AggregatedReference__Group__3 : rule__AggregatedReference__Group__3__Impl rule__AggregatedReference__Group__4 ;
    public final void rule__AggregatedReference__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1545:1: ( rule__AggregatedReference__Group__3__Impl rule__AggregatedReference__Group__4 )
            // InternalPapin.g:1546:2: rule__AggregatedReference__Group__3__Impl rule__AggregatedReference__Group__4
            {
            pushFollow(FOLLOW_21);
            rule__AggregatedReference__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__3"


    // $ANTLR start "rule__AggregatedReference__Group__3__Impl"
    // InternalPapin.g:1553:1: rule__AggregatedReference__Group__3__Impl : ( 'as' ) ;
    public final void rule__AggregatedReference__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1557:1: ( ( 'as' ) )
            // InternalPapin.g:1558:1: ( 'as' )
            {
            // InternalPapin.g:1558:1: ( 'as' )
            // InternalPapin.g:1559:2: 'as'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getAsKeyword_3()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getAsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__3__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__4"
    // InternalPapin.g:1568:1: rule__AggregatedReference__Group__4 : rule__AggregatedReference__Group__4__Impl rule__AggregatedReference__Group__5 ;
    public final void rule__AggregatedReference__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1572:1: ( rule__AggregatedReference__Group__4__Impl rule__AggregatedReference__Group__5 )
            // InternalPapin.g:1573:2: rule__AggregatedReference__Group__4__Impl rule__AggregatedReference__Group__5
            {
            pushFollow(FOLLOW_22);
            rule__AggregatedReference__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__4"


    // $ANTLR start "rule__AggregatedReference__Group__4__Impl"
    // InternalPapin.g:1580:1: rule__AggregatedReference__Group__4__Impl : ( ( rule__AggregatedReference__FunctionAssignment_4 ) ) ;
    public final void rule__AggregatedReference__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1584:1: ( ( ( rule__AggregatedReference__FunctionAssignment_4 ) ) )
            // InternalPapin.g:1585:1: ( ( rule__AggregatedReference__FunctionAssignment_4 ) )
            {
            // InternalPapin.g:1585:1: ( ( rule__AggregatedReference__FunctionAssignment_4 ) )
            // InternalPapin.g:1586:2: ( rule__AggregatedReference__FunctionAssignment_4 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getFunctionAssignment_4()); 
            // InternalPapin.g:1587:2: ( rule__AggregatedReference__FunctionAssignment_4 )
            // InternalPapin.g:1587:3: rule__AggregatedReference__FunctionAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__FunctionAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getFunctionAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__4__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__5"
    // InternalPapin.g:1595:1: rule__AggregatedReference__Group__5 : rule__AggregatedReference__Group__5__Impl rule__AggregatedReference__Group__6 ;
    public final void rule__AggregatedReference__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1599:1: ( rule__AggregatedReference__Group__5__Impl rule__AggregatedReference__Group__6 )
            // InternalPapin.g:1600:2: rule__AggregatedReference__Group__5__Impl rule__AggregatedReference__Group__6
            {
            pushFollow(FOLLOW_11);
            rule__AggregatedReference__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__5"


    // $ANTLR start "rule__AggregatedReference__Group__5__Impl"
    // InternalPapin.g:1607:1: rule__AggregatedReference__Group__5__Impl : ( '(' ) ;
    public final void rule__AggregatedReference__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1611:1: ( ( '(' ) )
            // InternalPapin.g:1612:1: ( '(' )
            {
            // InternalPapin.g:1612:1: ( '(' )
            // InternalPapin.g:1613:2: '('
            {
             before(grammarAccess.getAggregatedReferenceAccess().getLeftParenthesisKeyword_5()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getLeftParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__5__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__6"
    // InternalPapin.g:1622:1: rule__AggregatedReference__Group__6 : rule__AggregatedReference__Group__6__Impl rule__AggregatedReference__Group__7 ;
    public final void rule__AggregatedReference__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1626:1: ( rule__AggregatedReference__Group__6__Impl rule__AggregatedReference__Group__7 )
            // InternalPapin.g:1627:2: rule__AggregatedReference__Group__6__Impl rule__AggregatedReference__Group__7
            {
            pushFollow(FOLLOW_23);
            rule__AggregatedReference__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__6"


    // $ANTLR start "rule__AggregatedReference__Group__6__Impl"
    // InternalPapin.g:1634:1: rule__AggregatedReference__Group__6__Impl : ( ( rule__AggregatedReference__AggValueAssignment_6 ) ) ;
    public final void rule__AggregatedReference__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1638:1: ( ( ( rule__AggregatedReference__AggValueAssignment_6 ) ) )
            // InternalPapin.g:1639:1: ( ( rule__AggregatedReference__AggValueAssignment_6 ) )
            {
            // InternalPapin.g:1639:1: ( ( rule__AggregatedReference__AggValueAssignment_6 ) )
            // InternalPapin.g:1640:2: ( rule__AggregatedReference__AggValueAssignment_6 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getAggValueAssignment_6()); 
            // InternalPapin.g:1641:2: ( rule__AggregatedReference__AggValueAssignment_6 )
            // InternalPapin.g:1641:3: rule__AggregatedReference__AggValueAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__AggValueAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getAggValueAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__6__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__7"
    // InternalPapin.g:1649:1: rule__AggregatedReference__Group__7 : rule__AggregatedReference__Group__7__Impl rule__AggregatedReference__Group__8 ;
    public final void rule__AggregatedReference__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1653:1: ( rule__AggregatedReference__Group__7__Impl rule__AggregatedReference__Group__8 )
            // InternalPapin.g:1654:2: rule__AggregatedReference__Group__7__Impl rule__AggregatedReference__Group__8
            {
            pushFollow(FOLLOW_24);
            rule__AggregatedReference__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__7"


    // $ANTLR start "rule__AggregatedReference__Group__7__Impl"
    // InternalPapin.g:1661:1: rule__AggregatedReference__Group__7__Impl : ( ')' ) ;
    public final void rule__AggregatedReference__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1665:1: ( ( ')' ) )
            // InternalPapin.g:1666:1: ( ')' )
            {
            // InternalPapin.g:1666:1: ( ')' )
            // InternalPapin.g:1667:2: ')'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getRightParenthesisKeyword_7()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getRightParenthesisKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__7__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__8"
    // InternalPapin.g:1676:1: rule__AggregatedReference__Group__8 : rule__AggregatedReference__Group__8__Impl rule__AggregatedReference__Group__9 ;
    public final void rule__AggregatedReference__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1680:1: ( rule__AggregatedReference__Group__8__Impl rule__AggregatedReference__Group__9 )
            // InternalPapin.g:1681:2: rule__AggregatedReference__Group__8__Impl rule__AggregatedReference__Group__9
            {
            pushFollow(FOLLOW_24);
            rule__AggregatedReference__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__8"


    // $ANTLR start "rule__AggregatedReference__Group__8__Impl"
    // InternalPapin.g:1688:1: rule__AggregatedReference__Group__8__Impl : ( ( rule__AggregatedReference__Group_8__0 )? ) ;
    public final void rule__AggregatedReference__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1692:1: ( ( ( rule__AggregatedReference__Group_8__0 )? ) )
            // InternalPapin.g:1693:1: ( ( rule__AggregatedReference__Group_8__0 )? )
            {
            // InternalPapin.g:1693:1: ( ( rule__AggregatedReference__Group_8__0 )? )
            // InternalPapin.g:1694:2: ( rule__AggregatedReference__Group_8__0 )?
            {
             before(grammarAccess.getAggregatedReferenceAccess().getGroup_8()); 
            // InternalPapin.g:1695:2: ( rule__AggregatedReference__Group_8__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==23) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalPapin.g:1695:3: rule__AggregatedReference__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AggregatedReference__Group_8__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAggregatedReferenceAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__8__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__9"
    // InternalPapin.g:1703:1: rule__AggregatedReference__Group__9 : rule__AggregatedReference__Group__9__Impl ;
    public final void rule__AggregatedReference__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1707:1: ( rule__AggregatedReference__Group__9__Impl )
            // InternalPapin.g:1708:2: rule__AggregatedReference__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__9"


    // $ANTLR start "rule__AggregatedReference__Group__9__Impl"
    // InternalPapin.g:1714:1: rule__AggregatedReference__Group__9__Impl : ( ( rule__AggregatedReference__InstancesFilterAssignment_9 )? ) ;
    public final void rule__AggregatedReference__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1718:1: ( ( ( rule__AggregatedReference__InstancesFilterAssignment_9 )? ) )
            // InternalPapin.g:1719:1: ( ( rule__AggregatedReference__InstancesFilterAssignment_9 )? )
            {
            // InternalPapin.g:1719:1: ( ( rule__AggregatedReference__InstancesFilterAssignment_9 )? )
            // InternalPapin.g:1720:2: ( rule__AggregatedReference__InstancesFilterAssignment_9 )?
            {
             before(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterAssignment_9()); 
            // InternalPapin.g:1721:2: ( rule__AggregatedReference__InstancesFilterAssignment_9 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==32) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalPapin.g:1721:3: rule__AggregatedReference__InstancesFilterAssignment_9
                    {
                    pushFollow(FOLLOW_2);
                    rule__AggregatedReference__InstancesFilterAssignment_9();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__9__Impl"


    // $ANTLR start "rule__AggregatedReference__Group_8__0"
    // InternalPapin.g:1730:1: rule__AggregatedReference__Group_8__0 : rule__AggregatedReference__Group_8__0__Impl rule__AggregatedReference__Group_8__1 ;
    public final void rule__AggregatedReference__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1734:1: ( rule__AggregatedReference__Group_8__0__Impl rule__AggregatedReference__Group_8__1 )
            // InternalPapin.g:1735:2: rule__AggregatedReference__Group_8__0__Impl rule__AggregatedReference__Group_8__1
            {
            pushFollow(FOLLOW_11);
            rule__AggregatedReference__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_8__0"


    // $ANTLR start "rule__AggregatedReference__Group_8__0__Impl"
    // InternalPapin.g:1742:1: rule__AggregatedReference__Group_8__0__Impl : ( 'by' ) ;
    public final void rule__AggregatedReference__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1746:1: ( ( 'by' ) )
            // InternalPapin.g:1747:1: ( 'by' )
            {
            // InternalPapin.g:1747:1: ( 'by' )
            // InternalPapin.g:1748:2: 'by'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getByKeyword_8_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getByKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_8__0__Impl"


    // $ANTLR start "rule__AggregatedReference__Group_8__1"
    // InternalPapin.g:1757:1: rule__AggregatedReference__Group_8__1 : rule__AggregatedReference__Group_8__1__Impl ;
    public final void rule__AggregatedReference__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1761:1: ( rule__AggregatedReference__Group_8__1__Impl )
            // InternalPapin.g:1762:2: rule__AggregatedReference__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_8__1"


    // $ANTLR start "rule__AggregatedReference__Group_8__1__Impl"
    // InternalPapin.g:1768:1: rule__AggregatedReference__Group_8__1__Impl : ( ( rule__AggregatedReference__PivotingIdAssignment_8_1 ) ) ;
    public final void rule__AggregatedReference__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1772:1: ( ( ( rule__AggregatedReference__PivotingIdAssignment_8_1 ) ) )
            // InternalPapin.g:1773:1: ( ( rule__AggregatedReference__PivotingIdAssignment_8_1 ) )
            {
            // InternalPapin.g:1773:1: ( ( rule__AggregatedReference__PivotingIdAssignment_8_1 ) )
            // InternalPapin.g:1774:2: ( rule__AggregatedReference__PivotingIdAssignment_8_1 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getPivotingIdAssignment_8_1()); 
            // InternalPapin.g:1775:2: ( rule__AggregatedReference__PivotingIdAssignment_8_1 )
            // InternalPapin.g:1775:3: rule__AggregatedReference__PivotingIdAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__PivotingIdAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getPivotingIdAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_8__1__Impl"


    // $ANTLR start "rule__TypeCompletion__Group__0"
    // InternalPapin.g:1784:1: rule__TypeCompletion__Group__0 : rule__TypeCompletion__Group__0__Impl rule__TypeCompletion__Group__1 ;
    public final void rule__TypeCompletion__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1788:1: ( rule__TypeCompletion__Group__0__Impl rule__TypeCompletion__Group__1 )
            // InternalPapin.g:1789:2: rule__TypeCompletion__Group__0__Impl rule__TypeCompletion__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__TypeCompletion__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCompletion__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__0"


    // $ANTLR start "rule__TypeCompletion__Group__0__Impl"
    // InternalPapin.g:1796:1: rule__TypeCompletion__Group__0__Impl : ( 'as' ) ;
    public final void rule__TypeCompletion__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1800:1: ( ( 'as' ) )
            // InternalPapin.g:1801:1: ( 'as' )
            {
            // InternalPapin.g:1801:1: ( 'as' )
            // InternalPapin.g:1802:2: 'as'
            {
             before(grammarAccess.getTypeCompletionAccess().getAsKeyword_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getTypeCompletionAccess().getAsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__0__Impl"


    // $ANTLR start "rule__TypeCompletion__Group__1"
    // InternalPapin.g:1811:1: rule__TypeCompletion__Group__1 : rule__TypeCompletion__Group__1__Impl ;
    public final void rule__TypeCompletion__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1815:1: ( rule__TypeCompletion__Group__1__Impl )
            // InternalPapin.g:1816:2: rule__TypeCompletion__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeCompletion__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__1"


    // $ANTLR start "rule__TypeCompletion__Group__1__Impl"
    // InternalPapin.g:1822:1: rule__TypeCompletion__Group__1__Impl : ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) ) ;
    public final void rule__TypeCompletion__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1826:1: ( ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) ) )
            // InternalPapin.g:1827:1: ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) )
            {
            // InternalPapin.g:1827:1: ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) )
            // InternalPapin.g:1828:2: ( rule__TypeCompletion__TypeCustomizationsAssignment_1 )
            {
             before(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsAssignment_1()); 
            // InternalPapin.g:1829:2: ( rule__TypeCompletion__TypeCustomizationsAssignment_1 )
            // InternalPapin.g:1829:3: rule__TypeCompletion__TypeCustomizationsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__TypeCompletion__TypeCustomizationsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__1__Impl"


    // $ANTLR start "rule__TypeSelection__Group__0"
    // InternalPapin.g:1838:1: rule__TypeSelection__Group__0 : rule__TypeSelection__Group__0__Impl rule__TypeSelection__Group__1 ;
    public final void rule__TypeSelection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1842:1: ( rule__TypeSelection__Group__0__Impl rule__TypeSelection__Group__1 )
            // InternalPapin.g:1843:2: rule__TypeSelection__Group__0__Impl rule__TypeSelection__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__TypeSelection__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeSelection__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__0"


    // $ANTLR start "rule__TypeSelection__Group__0__Impl"
    // InternalPapin.g:1850:1: rule__TypeSelection__Group__0__Impl : ( 'only_as' ) ;
    public final void rule__TypeSelection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1854:1: ( ( 'only_as' ) )
            // InternalPapin.g:1855:1: ( 'only_as' )
            {
            // InternalPapin.g:1855:1: ( 'only_as' )
            // InternalPapin.g:1856:2: 'only_as'
            {
             before(grammarAccess.getTypeSelectionAccess().getOnly_asKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getTypeSelectionAccess().getOnly_asKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__0__Impl"


    // $ANTLR start "rule__TypeSelection__Group__1"
    // InternalPapin.g:1865:1: rule__TypeSelection__Group__1 : rule__TypeSelection__Group__1__Impl ;
    public final void rule__TypeSelection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1869:1: ( rule__TypeSelection__Group__1__Impl )
            // InternalPapin.g:1870:2: rule__TypeSelection__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeSelection__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__1"


    // $ANTLR start "rule__TypeSelection__Group__1__Impl"
    // InternalPapin.g:1876:1: rule__TypeSelection__Group__1__Impl : ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) ) ;
    public final void rule__TypeSelection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1880:1: ( ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) ) )
            // InternalPapin.g:1881:1: ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) )
            {
            // InternalPapin.g:1881:1: ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) )
            // InternalPapin.g:1882:2: ( rule__TypeSelection__TypeCustomizationsAssignment_1 )
            {
             before(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsAssignment_1()); 
            // InternalPapin.g:1883:2: ( rule__TypeSelection__TypeCustomizationsAssignment_1 )
            // InternalPapin.g:1883:3: rule__TypeSelection__TypeCustomizationsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__TypeSelection__TypeCustomizationsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__1__Impl"


    // $ANTLR start "rule__TypeCustomization__Group__0"
    // InternalPapin.g:1892:1: rule__TypeCustomization__Group__0 : rule__TypeCustomization__Group__0__Impl rule__TypeCustomization__Group__1 ;
    public final void rule__TypeCustomization__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1896:1: ( rule__TypeCustomization__Group__0__Impl rule__TypeCustomization__Group__1 )
            // InternalPapin.g:1897:2: rule__TypeCustomization__Group__0__Impl rule__TypeCustomization__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__TypeCustomization__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__0"


    // $ANTLR start "rule__TypeCustomization__Group__0__Impl"
    // InternalPapin.g:1904:1: rule__TypeCustomization__Group__0__Impl : ( ( rule__TypeCustomization__NameAssignment_0 ) ) ;
    public final void rule__TypeCustomization__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1908:1: ( ( ( rule__TypeCustomization__NameAssignment_0 ) ) )
            // InternalPapin.g:1909:1: ( ( rule__TypeCustomization__NameAssignment_0 ) )
            {
            // InternalPapin.g:1909:1: ( ( rule__TypeCustomization__NameAssignment_0 ) )
            // InternalPapin.g:1910:2: ( rule__TypeCustomization__NameAssignment_0 )
            {
             before(grammarAccess.getTypeCustomizationAccess().getNameAssignment_0()); 
            // InternalPapin.g:1911:2: ( rule__TypeCustomization__NameAssignment_0 )
            // InternalPapin.g:1911:3: rule__TypeCustomization__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTypeCustomizationAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__0__Impl"


    // $ANTLR start "rule__TypeCustomization__Group__1"
    // InternalPapin.g:1919:1: rule__TypeCustomization__Group__1 : rule__TypeCustomization__Group__1__Impl rule__TypeCustomization__Group__2 ;
    public final void rule__TypeCustomization__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1923:1: ( rule__TypeCustomization__Group__1__Impl rule__TypeCustomization__Group__2 )
            // InternalPapin.g:1924:2: rule__TypeCustomization__Group__1__Impl rule__TypeCustomization__Group__2
            {
            pushFollow(FOLLOW_25);
            rule__TypeCustomization__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__1"


    // $ANTLR start "rule__TypeCustomization__Group__1__Impl"
    // InternalPapin.g:1931:1: rule__TypeCustomization__Group__1__Impl : ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? ) ;
    public final void rule__TypeCustomization__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1935:1: ( ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? ) )
            // InternalPapin.g:1936:1: ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? )
            {
            // InternalPapin.g:1936:1: ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? )
            // InternalPapin.g:1937:2: ( rule__TypeCustomization__AttributeFilterAssignment_1 )?
            {
             before(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAssignment_1()); 
            // InternalPapin.g:1938:2: ( rule__TypeCustomization__AttributeFilterAssignment_1 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==29) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalPapin.g:1938:3: rule__TypeCustomization__AttributeFilterAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypeCustomization__AttributeFilterAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__1__Impl"


    // $ANTLR start "rule__TypeCustomization__Group__2"
    // InternalPapin.g:1946:1: rule__TypeCustomization__Group__2 : rule__TypeCustomization__Group__2__Impl ;
    public final void rule__TypeCustomization__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1950:1: ( rule__TypeCustomization__Group__2__Impl )
            // InternalPapin.g:1951:2: rule__TypeCustomization__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__2"


    // $ANTLR start "rule__TypeCustomization__Group__2__Impl"
    // InternalPapin.g:1957:1: rule__TypeCustomization__Group__2__Impl : ( ( rule__TypeCustomization__Group_2__0 )? ) ;
    public final void rule__TypeCustomization__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1961:1: ( ( ( rule__TypeCustomization__Group_2__0 )? ) )
            // InternalPapin.g:1962:1: ( ( rule__TypeCustomization__Group_2__0 )? )
            {
            // InternalPapin.g:1962:1: ( ( rule__TypeCustomization__Group_2__0 )? )
            // InternalPapin.g:1963:2: ( rule__TypeCustomization__Group_2__0 )?
            {
             before(grammarAccess.getTypeCustomizationAccess().getGroup_2()); 
            // InternalPapin.g:1964:2: ( rule__TypeCustomization__Group_2__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==20) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalPapin.g:1964:3: rule__TypeCustomization__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypeCustomization__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTypeCustomizationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__2__Impl"


    // $ANTLR start "rule__TypeCustomization__Group_2__0"
    // InternalPapin.g:1973:1: rule__TypeCustomization__Group_2__0 : rule__TypeCustomization__Group_2__0__Impl rule__TypeCustomization__Group_2__1 ;
    public final void rule__TypeCustomization__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1977:1: ( rule__TypeCustomization__Group_2__0__Impl rule__TypeCustomization__Group_2__1 )
            // InternalPapin.g:1978:2: rule__TypeCustomization__Group_2__0__Impl rule__TypeCustomization__Group_2__1
            {
            pushFollow(FOLLOW_26);
            rule__TypeCustomization__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__0"


    // $ANTLR start "rule__TypeCustomization__Group_2__0__Impl"
    // InternalPapin.g:1985:1: rule__TypeCustomization__Group_2__0__Impl : ( '{' ) ;
    public final void rule__TypeCustomization__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:1989:1: ( ( '{' ) )
            // InternalPapin.g:1990:1: ( '{' )
            {
            // InternalPapin.g:1990:1: ( '{' )
            // InternalPapin.g:1991:2: '{'
            {
             before(grammarAccess.getTypeCustomizationAccess().getLeftCurlyBracketKeyword_2_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getTypeCustomizationAccess().getLeftCurlyBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__0__Impl"


    // $ANTLR start "rule__TypeCustomization__Group_2__1"
    // InternalPapin.g:2000:1: rule__TypeCustomization__Group_2__1 : rule__TypeCustomization__Group_2__1__Impl rule__TypeCustomization__Group_2__2 ;
    public final void rule__TypeCustomization__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2004:1: ( rule__TypeCustomization__Group_2__1__Impl rule__TypeCustomization__Group_2__2 )
            // InternalPapin.g:2005:2: rule__TypeCustomization__Group_2__1__Impl rule__TypeCustomization__Group_2__2
            {
            pushFollow(FOLLOW_26);
            rule__TypeCustomization__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__1"


    // $ANTLR start "rule__TypeCustomization__Group_2__1__Impl"
    // InternalPapin.g:2012:1: rule__TypeCustomization__Group_2__1__Impl : ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* ) ;
    public final void rule__TypeCustomization__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2016:1: ( ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* ) )
            // InternalPapin.g:2017:1: ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* )
            {
            // InternalPapin.g:2017:1: ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* )
            // InternalPapin.g:2018:2: ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )*
            {
             before(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesAssignment_2_1()); 
            // InternalPapin.g:2019:2: ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==22||LA26_0==24) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalPapin.g:2019:3: rule__TypeCustomization__IncludedReferencesAssignment_2_1
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__TypeCustomization__IncludedReferencesAssignment_2_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__1__Impl"


    // $ANTLR start "rule__TypeCustomization__Group_2__2"
    // InternalPapin.g:2027:1: rule__TypeCustomization__Group_2__2 : rule__TypeCustomization__Group_2__2__Impl ;
    public final void rule__TypeCustomization__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2031:1: ( rule__TypeCustomization__Group_2__2__Impl )
            // InternalPapin.g:2032:2: rule__TypeCustomization__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__2"


    // $ANTLR start "rule__TypeCustomization__Group_2__2__Impl"
    // InternalPapin.g:2038:1: rule__TypeCustomization__Group_2__2__Impl : ( '}' ) ;
    public final void rule__TypeCustomization__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2042:1: ( ( '}' ) )
            // InternalPapin.g:2043:1: ( '}' )
            {
            // InternalPapin.g:2043:1: ( '}' )
            // InternalPapin.g:2044:2: '}'
            {
             before(grammarAccess.getTypeCustomizationAccess().getRightCurlyBracketKeyword_2_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getTypeCustomizationAccess().getRightCurlyBracketKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__2__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__0"
    // InternalPapin.g:2054:1: rule__AttributeFilter__Group__0 : rule__AttributeFilter__Group__0__Impl rule__AttributeFilter__Group__1 ;
    public final void rule__AttributeFilter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2058:1: ( rule__AttributeFilter__Group__0__Impl rule__AttributeFilter__Group__1 )
            // InternalPapin.g:2059:2: rule__AttributeFilter__Group__0__Impl rule__AttributeFilter__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__AttributeFilter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__0"


    // $ANTLR start "rule__AttributeFilter__Group__0__Impl"
    // InternalPapin.g:2066:1: rule__AttributeFilter__Group__0__Impl : ( '[' ) ;
    public final void rule__AttributeFilter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2070:1: ( ( '[' ) )
            // InternalPapin.g:2071:1: ( '[' )
            {
            // InternalPapin.g:2071:1: ( '[' )
            // InternalPapin.g:2072:2: '['
            {
             before(grammarAccess.getAttributeFilterAccess().getLeftSquareBracketKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__0__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__1"
    // InternalPapin.g:2081:1: rule__AttributeFilter__Group__1 : rule__AttributeFilter__Group__1__Impl rule__AttributeFilter__Group__2 ;
    public final void rule__AttributeFilter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2085:1: ( rule__AttributeFilter__Group__1__Impl rule__AttributeFilter__Group__2 )
            // InternalPapin.g:2086:2: rule__AttributeFilter__Group__1__Impl rule__AttributeFilter__Group__2
            {
            pushFollow(FOLLOW_27);
            rule__AttributeFilter__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__1"


    // $ANTLR start "rule__AttributeFilter__Group__1__Impl"
    // InternalPapin.g:2093:1: rule__AttributeFilter__Group__1__Impl : ( ( rule__AttributeFilter__AttributesAssignment_1 ) ) ;
    public final void rule__AttributeFilter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2097:1: ( ( ( rule__AttributeFilter__AttributesAssignment_1 ) ) )
            // InternalPapin.g:2098:1: ( ( rule__AttributeFilter__AttributesAssignment_1 ) )
            {
            // InternalPapin.g:2098:1: ( ( rule__AttributeFilter__AttributesAssignment_1 ) )
            // InternalPapin.g:2099:2: ( rule__AttributeFilter__AttributesAssignment_1 )
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_1()); 
            // InternalPapin.g:2100:2: ( rule__AttributeFilter__AttributesAssignment_1 )
            // InternalPapin.g:2100:3: rule__AttributeFilter__AttributesAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__AttributesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__1__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__2"
    // InternalPapin.g:2108:1: rule__AttributeFilter__Group__2 : rule__AttributeFilter__Group__2__Impl rule__AttributeFilter__Group__3 ;
    public final void rule__AttributeFilter__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2112:1: ( rule__AttributeFilter__Group__2__Impl rule__AttributeFilter__Group__3 )
            // InternalPapin.g:2113:2: rule__AttributeFilter__Group__2__Impl rule__AttributeFilter__Group__3
            {
            pushFollow(FOLLOW_27);
            rule__AttributeFilter__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__2"


    // $ANTLR start "rule__AttributeFilter__Group__2__Impl"
    // InternalPapin.g:2120:1: rule__AttributeFilter__Group__2__Impl : ( ( rule__AttributeFilter__Group_2__0 )* ) ;
    public final void rule__AttributeFilter__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2124:1: ( ( ( rule__AttributeFilter__Group_2__0 )* ) )
            // InternalPapin.g:2125:1: ( ( rule__AttributeFilter__Group_2__0 )* )
            {
            // InternalPapin.g:2125:1: ( ( rule__AttributeFilter__Group_2__0 )* )
            // InternalPapin.g:2126:2: ( rule__AttributeFilter__Group_2__0 )*
            {
             before(grammarAccess.getAttributeFilterAccess().getGroup_2()); 
            // InternalPapin.g:2127:2: ( rule__AttributeFilter__Group_2__0 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==31) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalPapin.g:2127:3: rule__AttributeFilter__Group_2__0
            	    {
            	    pushFollow(FOLLOW_28);
            	    rule__AttributeFilter__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getAttributeFilterAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__2__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__3"
    // InternalPapin.g:2135:1: rule__AttributeFilter__Group__3 : rule__AttributeFilter__Group__3__Impl ;
    public final void rule__AttributeFilter__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2139:1: ( rule__AttributeFilter__Group__3__Impl )
            // InternalPapin.g:2140:2: rule__AttributeFilter__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__3"


    // $ANTLR start "rule__AttributeFilter__Group__3__Impl"
    // InternalPapin.g:2146:1: rule__AttributeFilter__Group__3__Impl : ( ']' ) ;
    public final void rule__AttributeFilter__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2150:1: ( ( ']' ) )
            // InternalPapin.g:2151:1: ( ']' )
            {
            // InternalPapin.g:2151:1: ( ']' )
            // InternalPapin.g:2152:2: ']'
            {
             before(grammarAccess.getAttributeFilterAccess().getRightSquareBracketKeyword_3()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getRightSquareBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__3__Impl"


    // $ANTLR start "rule__AttributeFilter__Group_2__0"
    // InternalPapin.g:2162:1: rule__AttributeFilter__Group_2__0 : rule__AttributeFilter__Group_2__0__Impl rule__AttributeFilter__Group_2__1 ;
    public final void rule__AttributeFilter__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2166:1: ( rule__AttributeFilter__Group_2__0__Impl rule__AttributeFilter__Group_2__1 )
            // InternalPapin.g:2167:2: rule__AttributeFilter__Group_2__0__Impl rule__AttributeFilter__Group_2__1
            {
            pushFollow(FOLLOW_11);
            rule__AttributeFilter__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__0"


    // $ANTLR start "rule__AttributeFilter__Group_2__0__Impl"
    // InternalPapin.g:2174:1: rule__AttributeFilter__Group_2__0__Impl : ( ',' ) ;
    public final void rule__AttributeFilter__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2178:1: ( ( ',' ) )
            // InternalPapin.g:2179:1: ( ',' )
            {
            // InternalPapin.g:2179:1: ( ',' )
            // InternalPapin.g:2180:2: ','
            {
             before(grammarAccess.getAttributeFilterAccess().getCommaKeyword_2_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__0__Impl"


    // $ANTLR start "rule__AttributeFilter__Group_2__1"
    // InternalPapin.g:2189:1: rule__AttributeFilter__Group_2__1 : rule__AttributeFilter__Group_2__1__Impl ;
    public final void rule__AttributeFilter__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2193:1: ( rule__AttributeFilter__Group_2__1__Impl )
            // InternalPapin.g:2194:2: rule__AttributeFilter__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__1"


    // $ANTLR start "rule__AttributeFilter__Group_2__1__Impl"
    // InternalPapin.g:2200:1: rule__AttributeFilter__Group_2__1__Impl : ( ( rule__AttributeFilter__AttributesAssignment_2_1 ) ) ;
    public final void rule__AttributeFilter__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2204:1: ( ( ( rule__AttributeFilter__AttributesAssignment_2_1 ) ) )
            // InternalPapin.g:2205:1: ( ( rule__AttributeFilter__AttributesAssignment_2_1 ) )
            {
            // InternalPapin.g:2205:1: ( ( rule__AttributeFilter__AttributesAssignment_2_1 ) )
            // InternalPapin.g:2206:2: ( rule__AttributeFilter__AttributesAssignment_2_1 )
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_2_1()); 
            // InternalPapin.g:2207:2: ( rule__AttributeFilter__AttributesAssignment_2_1 )
            // InternalPapin.g:2207:3: rule__AttributeFilter__AttributesAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__AttributesAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__1__Impl"


    // $ANTLR start "rule__InstancesFilter__Group__0"
    // InternalPapin.g:2216:1: rule__InstancesFilter__Group__0 : rule__InstancesFilter__Group__0__Impl rule__InstancesFilter__Group__1 ;
    public final void rule__InstancesFilter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2220:1: ( rule__InstancesFilter__Group__0__Impl rule__InstancesFilter__Group__1 )
            // InternalPapin.g:2221:2: rule__InstancesFilter__Group__0__Impl rule__InstancesFilter__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__InstancesFilter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstancesFilter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__0"


    // $ANTLR start "rule__InstancesFilter__Group__0__Impl"
    // InternalPapin.g:2228:1: rule__InstancesFilter__Group__0__Impl : ( 'where' ) ;
    public final void rule__InstancesFilter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2232:1: ( ( 'where' ) )
            // InternalPapin.g:2233:1: ( 'where' )
            {
            // InternalPapin.g:2233:1: ( 'where' )
            // InternalPapin.g:2234:2: 'where'
            {
             before(grammarAccess.getInstancesFilterAccess().getWhereKeyword_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getInstancesFilterAccess().getWhereKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__0__Impl"


    // $ANTLR start "rule__InstancesFilter__Group__1"
    // InternalPapin.g:2243:1: rule__InstancesFilter__Group__1 : rule__InstancesFilter__Group__1__Impl ;
    public final void rule__InstancesFilter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2247:1: ( rule__InstancesFilter__Group__1__Impl )
            // InternalPapin.g:2248:2: rule__InstancesFilter__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InstancesFilter__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__1"


    // $ANTLR start "rule__InstancesFilter__Group__1__Impl"
    // InternalPapin.g:2254:1: rule__InstancesFilter__Group__1__Impl : ( ruleAndConjunction ) ;
    public final void rule__InstancesFilter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2258:1: ( ( ruleAndConjunction ) )
            // InternalPapin.g:2259:1: ( ruleAndConjunction )
            {
            // InternalPapin.g:2259:1: ( ruleAndConjunction )
            // InternalPapin.g:2260:2: ruleAndConjunction
            {
             before(grammarAccess.getInstancesFilterAccess().getAndConjunctionParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleAndConjunction();

            state._fsp--;

             after(grammarAccess.getInstancesFilterAccess().getAndConjunctionParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__1__Impl"


    // $ANTLR start "rule__AndConjunction__Group__0"
    // InternalPapin.g:2270:1: rule__AndConjunction__Group__0 : rule__AndConjunction__Group__0__Impl rule__AndConjunction__Group__1 ;
    public final void rule__AndConjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2274:1: ( rule__AndConjunction__Group__0__Impl rule__AndConjunction__Group__1 )
            // InternalPapin.g:2275:2: rule__AndConjunction__Group__0__Impl rule__AndConjunction__Group__1
            {
            pushFollow(FOLLOW_30);
            rule__AndConjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__0"


    // $ANTLR start "rule__AndConjunction__Group__0__Impl"
    // InternalPapin.g:2282:1: rule__AndConjunction__Group__0__Impl : ( ruleOrConjunction ) ;
    public final void rule__AndConjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2286:1: ( ( ruleOrConjunction ) )
            // InternalPapin.g:2287:1: ( ruleOrConjunction )
            {
            // InternalPapin.g:2287:1: ( ruleOrConjunction )
            // InternalPapin.g:2288:2: ruleOrConjunction
            {
             before(grammarAccess.getAndConjunctionAccess().getOrConjunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOrConjunction();

            state._fsp--;

             after(grammarAccess.getAndConjunctionAccess().getOrConjunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__0__Impl"


    // $ANTLR start "rule__AndConjunction__Group__1"
    // InternalPapin.g:2297:1: rule__AndConjunction__Group__1 : rule__AndConjunction__Group__1__Impl ;
    public final void rule__AndConjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2301:1: ( rule__AndConjunction__Group__1__Impl )
            // InternalPapin.g:2302:2: rule__AndConjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__1"


    // $ANTLR start "rule__AndConjunction__Group__1__Impl"
    // InternalPapin.g:2308:1: rule__AndConjunction__Group__1__Impl : ( ( rule__AndConjunction__Group_1__0 )* ) ;
    public final void rule__AndConjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2312:1: ( ( ( rule__AndConjunction__Group_1__0 )* ) )
            // InternalPapin.g:2313:1: ( ( rule__AndConjunction__Group_1__0 )* )
            {
            // InternalPapin.g:2313:1: ( ( rule__AndConjunction__Group_1__0 )* )
            // InternalPapin.g:2314:2: ( rule__AndConjunction__Group_1__0 )*
            {
             before(grammarAccess.getAndConjunctionAccess().getGroup_1()); 
            // InternalPapin.g:2315:2: ( rule__AndConjunction__Group_1__0 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==33) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalPapin.g:2315:3: rule__AndConjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_31);
            	    rule__AndConjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getAndConjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__1__Impl"


    // $ANTLR start "rule__AndConjunction__Group_1__0"
    // InternalPapin.g:2324:1: rule__AndConjunction__Group_1__0 : rule__AndConjunction__Group_1__0__Impl rule__AndConjunction__Group_1__1 ;
    public final void rule__AndConjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2328:1: ( rule__AndConjunction__Group_1__0__Impl rule__AndConjunction__Group_1__1 )
            // InternalPapin.g:2329:2: rule__AndConjunction__Group_1__0__Impl rule__AndConjunction__Group_1__1
            {
            pushFollow(FOLLOW_30);
            rule__AndConjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__0"


    // $ANTLR start "rule__AndConjunction__Group_1__0__Impl"
    // InternalPapin.g:2336:1: rule__AndConjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__AndConjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2340:1: ( ( () ) )
            // InternalPapin.g:2341:1: ( () )
            {
            // InternalPapin.g:2341:1: ( () )
            // InternalPapin.g:2342:2: ()
            {
             before(grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0()); 
            // InternalPapin.g:2343:2: ()
            // InternalPapin.g:2343:3: 
            {
            }

             after(grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__0__Impl"


    // $ANTLR start "rule__AndConjunction__Group_1__1"
    // InternalPapin.g:2351:1: rule__AndConjunction__Group_1__1 : rule__AndConjunction__Group_1__1__Impl rule__AndConjunction__Group_1__2 ;
    public final void rule__AndConjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2355:1: ( rule__AndConjunction__Group_1__1__Impl rule__AndConjunction__Group_1__2 )
            // InternalPapin.g:2356:2: rule__AndConjunction__Group_1__1__Impl rule__AndConjunction__Group_1__2
            {
            pushFollow(FOLLOW_29);
            rule__AndConjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__1"


    // $ANTLR start "rule__AndConjunction__Group_1__1__Impl"
    // InternalPapin.g:2363:1: rule__AndConjunction__Group_1__1__Impl : ( 'and' ) ;
    public final void rule__AndConjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2367:1: ( ( 'and' ) )
            // InternalPapin.g:2368:1: ( 'and' )
            {
            // InternalPapin.g:2368:1: ( 'and' )
            // InternalPapin.g:2369:2: 'and'
            {
             before(grammarAccess.getAndConjunctionAccess().getAndKeyword_1_1()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getAndConjunctionAccess().getAndKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__1__Impl"


    // $ANTLR start "rule__AndConjunction__Group_1__2"
    // InternalPapin.g:2378:1: rule__AndConjunction__Group_1__2 : rule__AndConjunction__Group_1__2__Impl ;
    public final void rule__AndConjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2382:1: ( rule__AndConjunction__Group_1__2__Impl )
            // InternalPapin.g:2383:2: rule__AndConjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__2"


    // $ANTLR start "rule__AndConjunction__Group_1__2__Impl"
    // InternalPapin.g:2389:1: rule__AndConjunction__Group_1__2__Impl : ( ( rule__AndConjunction__RightAssignment_1_2 ) ) ;
    public final void rule__AndConjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2393:1: ( ( ( rule__AndConjunction__RightAssignment_1_2 ) ) )
            // InternalPapin.g:2394:1: ( ( rule__AndConjunction__RightAssignment_1_2 ) )
            {
            // InternalPapin.g:2394:1: ( ( rule__AndConjunction__RightAssignment_1_2 ) )
            // InternalPapin.g:2395:2: ( rule__AndConjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndConjunctionAccess().getRightAssignment_1_2()); 
            // InternalPapin.g:2396:2: ( rule__AndConjunction__RightAssignment_1_2 )
            // InternalPapin.g:2396:3: rule__AndConjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndConjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__2__Impl"


    // $ANTLR start "rule__OrConjunction__Group__0"
    // InternalPapin.g:2405:1: rule__OrConjunction__Group__0 : rule__OrConjunction__Group__0__Impl rule__OrConjunction__Group__1 ;
    public final void rule__OrConjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2409:1: ( rule__OrConjunction__Group__0__Impl rule__OrConjunction__Group__1 )
            // InternalPapin.g:2410:2: rule__OrConjunction__Group__0__Impl rule__OrConjunction__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__OrConjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__0"


    // $ANTLR start "rule__OrConjunction__Group__0__Impl"
    // InternalPapin.g:2417:1: rule__OrConjunction__Group__0__Impl : ( rulePrimary ) ;
    public final void rule__OrConjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2421:1: ( ( rulePrimary ) )
            // InternalPapin.g:2422:1: ( rulePrimary )
            {
            // InternalPapin.g:2422:1: ( rulePrimary )
            // InternalPapin.g:2423:2: rulePrimary
            {
             before(grammarAccess.getOrConjunctionAccess().getPrimaryParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getOrConjunctionAccess().getPrimaryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__0__Impl"


    // $ANTLR start "rule__OrConjunction__Group__1"
    // InternalPapin.g:2432:1: rule__OrConjunction__Group__1 : rule__OrConjunction__Group__1__Impl ;
    public final void rule__OrConjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2436:1: ( rule__OrConjunction__Group__1__Impl )
            // InternalPapin.g:2437:2: rule__OrConjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__1"


    // $ANTLR start "rule__OrConjunction__Group__1__Impl"
    // InternalPapin.g:2443:1: rule__OrConjunction__Group__1__Impl : ( ( rule__OrConjunction__Group_1__0 )* ) ;
    public final void rule__OrConjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2447:1: ( ( ( rule__OrConjunction__Group_1__0 )* ) )
            // InternalPapin.g:2448:1: ( ( rule__OrConjunction__Group_1__0 )* )
            {
            // InternalPapin.g:2448:1: ( ( rule__OrConjunction__Group_1__0 )* )
            // InternalPapin.g:2449:2: ( rule__OrConjunction__Group_1__0 )*
            {
             before(grammarAccess.getOrConjunctionAccess().getGroup_1()); 
            // InternalPapin.g:2450:2: ( rule__OrConjunction__Group_1__0 )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==34) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalPapin.g:2450:3: rule__OrConjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__OrConjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

             after(grammarAccess.getOrConjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__1__Impl"


    // $ANTLR start "rule__OrConjunction__Group_1__0"
    // InternalPapin.g:2459:1: rule__OrConjunction__Group_1__0 : rule__OrConjunction__Group_1__0__Impl rule__OrConjunction__Group_1__1 ;
    public final void rule__OrConjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2463:1: ( rule__OrConjunction__Group_1__0__Impl rule__OrConjunction__Group_1__1 )
            // InternalPapin.g:2464:2: rule__OrConjunction__Group_1__0__Impl rule__OrConjunction__Group_1__1
            {
            pushFollow(FOLLOW_32);
            rule__OrConjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__0"


    // $ANTLR start "rule__OrConjunction__Group_1__0__Impl"
    // InternalPapin.g:2471:1: rule__OrConjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__OrConjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2475:1: ( ( () ) )
            // InternalPapin.g:2476:1: ( () )
            {
            // InternalPapin.g:2476:1: ( () )
            // InternalPapin.g:2477:2: ()
            {
             before(grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0()); 
            // InternalPapin.g:2478:2: ()
            // InternalPapin.g:2478:3: 
            {
            }

             after(grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__0__Impl"


    // $ANTLR start "rule__OrConjunction__Group_1__1"
    // InternalPapin.g:2486:1: rule__OrConjunction__Group_1__1 : rule__OrConjunction__Group_1__1__Impl rule__OrConjunction__Group_1__2 ;
    public final void rule__OrConjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2490:1: ( rule__OrConjunction__Group_1__1__Impl rule__OrConjunction__Group_1__2 )
            // InternalPapin.g:2491:2: rule__OrConjunction__Group_1__1__Impl rule__OrConjunction__Group_1__2
            {
            pushFollow(FOLLOW_29);
            rule__OrConjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__1"


    // $ANTLR start "rule__OrConjunction__Group_1__1__Impl"
    // InternalPapin.g:2498:1: rule__OrConjunction__Group_1__1__Impl : ( 'or' ) ;
    public final void rule__OrConjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2502:1: ( ( 'or' ) )
            // InternalPapin.g:2503:1: ( 'or' )
            {
            // InternalPapin.g:2503:1: ( 'or' )
            // InternalPapin.g:2504:2: 'or'
            {
             before(grammarAccess.getOrConjunctionAccess().getOrKeyword_1_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getOrConjunctionAccess().getOrKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__1__Impl"


    // $ANTLR start "rule__OrConjunction__Group_1__2"
    // InternalPapin.g:2513:1: rule__OrConjunction__Group_1__2 : rule__OrConjunction__Group_1__2__Impl ;
    public final void rule__OrConjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2517:1: ( rule__OrConjunction__Group_1__2__Impl )
            // InternalPapin.g:2518:2: rule__OrConjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__2"


    // $ANTLR start "rule__OrConjunction__Group_1__2__Impl"
    // InternalPapin.g:2524:1: rule__OrConjunction__Group_1__2__Impl : ( ( rule__OrConjunction__RightAssignment_1_2 ) ) ;
    public final void rule__OrConjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2528:1: ( ( ( rule__OrConjunction__RightAssignment_1_2 ) ) )
            // InternalPapin.g:2529:1: ( ( rule__OrConjunction__RightAssignment_1_2 ) )
            {
            // InternalPapin.g:2529:1: ( ( rule__OrConjunction__RightAssignment_1_2 ) )
            // InternalPapin.g:2530:2: ( rule__OrConjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrConjunctionAccess().getRightAssignment_1_2()); 
            // InternalPapin.g:2531:2: ( rule__OrConjunction__RightAssignment_1_2 )
            // InternalPapin.g:2531:3: rule__OrConjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrConjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__2__Impl"


    // $ANTLR start "rule__Primary__Group_1__0"
    // InternalPapin.g:2540:1: rule__Primary__Group_1__0 : rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 ;
    public final void rule__Primary__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2544:1: ( rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 )
            // InternalPapin.g:2545:2: rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1
            {
            pushFollow(FOLLOW_29);
            rule__Primary__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0"


    // $ANTLR start "rule__Primary__Group_1__0__Impl"
    // InternalPapin.g:2552:1: rule__Primary__Group_1__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2556:1: ( ( '(' ) )
            // InternalPapin.g:2557:1: ( '(' )
            {
            // InternalPapin.g:2557:1: ( '(' )
            // InternalPapin.g:2558:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0__Impl"


    // $ANTLR start "rule__Primary__Group_1__1"
    // InternalPapin.g:2567:1: rule__Primary__Group_1__1 : rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 ;
    public final void rule__Primary__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2571:1: ( rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 )
            // InternalPapin.g:2572:2: rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2
            {
            pushFollow(FOLLOW_23);
            rule__Primary__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1"


    // $ANTLR start "rule__Primary__Group_1__1__Impl"
    // InternalPapin.g:2579:1: rule__Primary__Group_1__1__Impl : ( ruleAndConjunction ) ;
    public final void rule__Primary__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2583:1: ( ( ruleAndConjunction ) )
            // InternalPapin.g:2584:1: ( ruleAndConjunction )
            {
            // InternalPapin.g:2584:1: ( ruleAndConjunction )
            // InternalPapin.g:2585:2: ruleAndConjunction
            {
             before(grammarAccess.getPrimaryAccess().getAndConjunctionParserRuleCall_1_1()); 
            pushFollow(FOLLOW_2);
            ruleAndConjunction();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getAndConjunctionParserRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1__Impl"


    // $ANTLR start "rule__Primary__Group_1__2"
    // InternalPapin.g:2594:1: rule__Primary__Group_1__2 : rule__Primary__Group_1__2__Impl ;
    public final void rule__Primary__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2598:1: ( rule__Primary__Group_1__2__Impl )
            // InternalPapin.g:2599:2: rule__Primary__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2"


    // $ANTLR start "rule__Primary__Group_1__2__Impl"
    // InternalPapin.g:2605:1: rule__Primary__Group_1__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2609:1: ( ( ')' ) )
            // InternalPapin.g:2610:1: ( ')' )
            {
            // InternalPapin.g:2610:1: ( ')' )
            // InternalPapin.g:2611:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_2()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2__Impl"


    // $ANTLR start "rule__Comparison__Group_0__0"
    // InternalPapin.g:2621:1: rule__Comparison__Group_0__0 : rule__Comparison__Group_0__0__Impl rule__Comparison__Group_0__1 ;
    public final void rule__Comparison__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2625:1: ( rule__Comparison__Group_0__0__Impl rule__Comparison__Group_0__1 )
            // InternalPapin.g:2626:2: rule__Comparison__Group_0__0__Impl rule__Comparison__Group_0__1
            {
            pushFollow(FOLLOW_11);
            rule__Comparison__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__0"


    // $ANTLR start "rule__Comparison__Group_0__0__Impl"
    // InternalPapin.g:2633:1: rule__Comparison__Group_0__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2637:1: ( ( () ) )
            // InternalPapin.g:2638:1: ( () )
            {
            // InternalPapin.g:2638:1: ( () )
            // InternalPapin.g:2639:2: ()
            {
             before(grammarAccess.getComparisonAccess().getEqualityAction_0_0()); 
            // InternalPapin.g:2640:2: ()
            // InternalPapin.g:2640:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getEqualityAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__0__Impl"


    // $ANTLR start "rule__Comparison__Group_0__1"
    // InternalPapin.g:2648:1: rule__Comparison__Group_0__1 : rule__Comparison__Group_0__1__Impl rule__Comparison__Group_0__2 ;
    public final void rule__Comparison__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2652:1: ( rule__Comparison__Group_0__1__Impl rule__Comparison__Group_0__2 )
            // InternalPapin.g:2653:2: rule__Comparison__Group_0__1__Impl rule__Comparison__Group_0__2
            {
            pushFollow(FOLLOW_34);
            rule__Comparison__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__1"


    // $ANTLR start "rule__Comparison__Group_0__1__Impl"
    // InternalPapin.g:2660:1: rule__Comparison__Group_0__1__Impl : ( ( rule__Comparison__PathAssignment_0_1 ) ) ;
    public final void rule__Comparison__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2664:1: ( ( ( rule__Comparison__PathAssignment_0_1 ) ) )
            // InternalPapin.g:2665:1: ( ( rule__Comparison__PathAssignment_0_1 ) )
            {
            // InternalPapin.g:2665:1: ( ( rule__Comparison__PathAssignment_0_1 ) )
            // InternalPapin.g:2666:2: ( rule__Comparison__PathAssignment_0_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_0_1()); 
            // InternalPapin.g:2667:2: ( rule__Comparison__PathAssignment_0_1 )
            // InternalPapin.g:2667:3: rule__Comparison__PathAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__1__Impl"


    // $ANTLR start "rule__Comparison__Group_0__2"
    // InternalPapin.g:2675:1: rule__Comparison__Group_0__2 : rule__Comparison__Group_0__2__Impl rule__Comparison__Group_0__3 ;
    public final void rule__Comparison__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2679:1: ( rule__Comparison__Group_0__2__Impl rule__Comparison__Group_0__3 )
            // InternalPapin.g:2680:2: rule__Comparison__Group_0__2__Impl rule__Comparison__Group_0__3
            {
            pushFollow(FOLLOW_5);
            rule__Comparison__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__2"


    // $ANTLR start "rule__Comparison__Group_0__2__Impl"
    // InternalPapin.g:2687:1: rule__Comparison__Group_0__2__Impl : ( '=' ) ;
    public final void rule__Comparison__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2691:1: ( ( '=' ) )
            // InternalPapin.g:2692:1: ( '=' )
            {
            // InternalPapin.g:2692:1: ( '=' )
            // InternalPapin.g:2693:2: '='
            {
             before(grammarAccess.getComparisonAccess().getEqualsSignKeyword_0_2()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getEqualsSignKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__2__Impl"


    // $ANTLR start "rule__Comparison__Group_0__3"
    // InternalPapin.g:2702:1: rule__Comparison__Group_0__3 : rule__Comparison__Group_0__3__Impl ;
    public final void rule__Comparison__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2706:1: ( rule__Comparison__Group_0__3__Impl )
            // InternalPapin.g:2707:2: rule__Comparison__Group_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__3"


    // $ANTLR start "rule__Comparison__Group_0__3__Impl"
    // InternalPapin.g:2713:1: rule__Comparison__Group_0__3__Impl : ( ( rule__Comparison__ValueAssignment_0_3 ) ) ;
    public final void rule__Comparison__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2717:1: ( ( ( rule__Comparison__ValueAssignment_0_3 ) ) )
            // InternalPapin.g:2718:1: ( ( rule__Comparison__ValueAssignment_0_3 ) )
            {
            // InternalPapin.g:2718:1: ( ( rule__Comparison__ValueAssignment_0_3 ) )
            // InternalPapin.g:2719:2: ( rule__Comparison__ValueAssignment_0_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_0_3()); 
            // InternalPapin.g:2720:2: ( rule__Comparison__ValueAssignment_0_3 )
            // InternalPapin.g:2720:3: rule__Comparison__ValueAssignment_0_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_0_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__3__Impl"


    // $ANTLR start "rule__Comparison__Group_1__0"
    // InternalPapin.g:2729:1: rule__Comparison__Group_1__0 : rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1 ;
    public final void rule__Comparison__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2733:1: ( rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1 )
            // InternalPapin.g:2734:2: rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Comparison__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__0"


    // $ANTLR start "rule__Comparison__Group_1__0__Impl"
    // InternalPapin.g:2741:1: rule__Comparison__Group_1__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2745:1: ( ( () ) )
            // InternalPapin.g:2746:1: ( () )
            {
            // InternalPapin.g:2746:1: ( () )
            // InternalPapin.g:2747:2: ()
            {
             before(grammarAccess.getComparisonAccess().getInequalityAction_1_0()); 
            // InternalPapin.g:2748:2: ()
            // InternalPapin.g:2748:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getInequalityAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__0__Impl"


    // $ANTLR start "rule__Comparison__Group_1__1"
    // InternalPapin.g:2756:1: rule__Comparison__Group_1__1 : rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2 ;
    public final void rule__Comparison__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2760:1: ( rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2 )
            // InternalPapin.g:2761:2: rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2
            {
            pushFollow(FOLLOW_35);
            rule__Comparison__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__1"


    // $ANTLR start "rule__Comparison__Group_1__1__Impl"
    // InternalPapin.g:2768:1: rule__Comparison__Group_1__1__Impl : ( ( rule__Comparison__PathAssignment_1_1 ) ) ;
    public final void rule__Comparison__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2772:1: ( ( ( rule__Comparison__PathAssignment_1_1 ) ) )
            // InternalPapin.g:2773:1: ( ( rule__Comparison__PathAssignment_1_1 ) )
            {
            // InternalPapin.g:2773:1: ( ( rule__Comparison__PathAssignment_1_1 ) )
            // InternalPapin.g:2774:2: ( rule__Comparison__PathAssignment_1_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_1_1()); 
            // InternalPapin.g:2775:2: ( rule__Comparison__PathAssignment_1_1 )
            // InternalPapin.g:2775:3: rule__Comparison__PathAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__1__Impl"


    // $ANTLR start "rule__Comparison__Group_1__2"
    // InternalPapin.g:2783:1: rule__Comparison__Group_1__2 : rule__Comparison__Group_1__2__Impl rule__Comparison__Group_1__3 ;
    public final void rule__Comparison__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2787:1: ( rule__Comparison__Group_1__2__Impl rule__Comparison__Group_1__3 )
            // InternalPapin.g:2788:2: rule__Comparison__Group_1__2__Impl rule__Comparison__Group_1__3
            {
            pushFollow(FOLLOW_5);
            rule__Comparison__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__2"


    // $ANTLR start "rule__Comparison__Group_1__2__Impl"
    // InternalPapin.g:2795:1: rule__Comparison__Group_1__2__Impl : ( '!=' ) ;
    public final void rule__Comparison__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2799:1: ( ( '!=' ) )
            // InternalPapin.g:2800:1: ( '!=' )
            {
            // InternalPapin.g:2800:1: ( '!=' )
            // InternalPapin.g:2801:2: '!='
            {
             before(grammarAccess.getComparisonAccess().getExclamationMarkEqualsSignKeyword_1_2()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getExclamationMarkEqualsSignKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__2__Impl"


    // $ANTLR start "rule__Comparison__Group_1__3"
    // InternalPapin.g:2810:1: rule__Comparison__Group_1__3 : rule__Comparison__Group_1__3__Impl ;
    public final void rule__Comparison__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2814:1: ( rule__Comparison__Group_1__3__Impl )
            // InternalPapin.g:2815:2: rule__Comparison__Group_1__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__3"


    // $ANTLR start "rule__Comparison__Group_1__3__Impl"
    // InternalPapin.g:2821:1: rule__Comparison__Group_1__3__Impl : ( ( rule__Comparison__ValueAssignment_1_3 ) ) ;
    public final void rule__Comparison__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2825:1: ( ( ( rule__Comparison__ValueAssignment_1_3 ) ) )
            // InternalPapin.g:2826:1: ( ( rule__Comparison__ValueAssignment_1_3 ) )
            {
            // InternalPapin.g:2826:1: ( ( rule__Comparison__ValueAssignment_1_3 ) )
            // InternalPapin.g:2827:2: ( rule__Comparison__ValueAssignment_1_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_1_3()); 
            // InternalPapin.g:2828:2: ( rule__Comparison__ValueAssignment_1_3 )
            // InternalPapin.g:2828:3: rule__Comparison__ValueAssignment_1_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_1_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__3__Impl"


    // $ANTLR start "rule__Comparison__Group_2__0"
    // InternalPapin.g:2837:1: rule__Comparison__Group_2__0 : rule__Comparison__Group_2__0__Impl rule__Comparison__Group_2__1 ;
    public final void rule__Comparison__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2841:1: ( rule__Comparison__Group_2__0__Impl rule__Comparison__Group_2__1 )
            // InternalPapin.g:2842:2: rule__Comparison__Group_2__0__Impl rule__Comparison__Group_2__1
            {
            pushFollow(FOLLOW_11);
            rule__Comparison__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__0"


    // $ANTLR start "rule__Comparison__Group_2__0__Impl"
    // InternalPapin.g:2849:1: rule__Comparison__Group_2__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2853:1: ( ( () ) )
            // InternalPapin.g:2854:1: ( () )
            {
            // InternalPapin.g:2854:1: ( () )
            // InternalPapin.g:2855:2: ()
            {
             before(grammarAccess.getComparisonAccess().getMoreThanAction_2_0()); 
            // InternalPapin.g:2856:2: ()
            // InternalPapin.g:2856:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getMoreThanAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__0__Impl"


    // $ANTLR start "rule__Comparison__Group_2__1"
    // InternalPapin.g:2864:1: rule__Comparison__Group_2__1 : rule__Comparison__Group_2__1__Impl rule__Comparison__Group_2__2 ;
    public final void rule__Comparison__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2868:1: ( rule__Comparison__Group_2__1__Impl rule__Comparison__Group_2__2 )
            // InternalPapin.g:2869:2: rule__Comparison__Group_2__1__Impl rule__Comparison__Group_2__2
            {
            pushFollow(FOLLOW_36);
            rule__Comparison__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__1"


    // $ANTLR start "rule__Comparison__Group_2__1__Impl"
    // InternalPapin.g:2876:1: rule__Comparison__Group_2__1__Impl : ( ( rule__Comparison__PathAssignment_2_1 ) ) ;
    public final void rule__Comparison__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2880:1: ( ( ( rule__Comparison__PathAssignment_2_1 ) ) )
            // InternalPapin.g:2881:1: ( ( rule__Comparison__PathAssignment_2_1 ) )
            {
            // InternalPapin.g:2881:1: ( ( rule__Comparison__PathAssignment_2_1 ) )
            // InternalPapin.g:2882:2: ( rule__Comparison__PathAssignment_2_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_2_1()); 
            // InternalPapin.g:2883:2: ( rule__Comparison__PathAssignment_2_1 )
            // InternalPapin.g:2883:3: rule__Comparison__PathAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__1__Impl"


    // $ANTLR start "rule__Comparison__Group_2__2"
    // InternalPapin.g:2891:1: rule__Comparison__Group_2__2 : rule__Comparison__Group_2__2__Impl rule__Comparison__Group_2__3 ;
    public final void rule__Comparison__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2895:1: ( rule__Comparison__Group_2__2__Impl rule__Comparison__Group_2__3 )
            // InternalPapin.g:2896:2: rule__Comparison__Group_2__2__Impl rule__Comparison__Group_2__3
            {
            pushFollow(FOLLOW_5);
            rule__Comparison__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__2"


    // $ANTLR start "rule__Comparison__Group_2__2__Impl"
    // InternalPapin.g:2903:1: rule__Comparison__Group_2__2__Impl : ( '>' ) ;
    public final void rule__Comparison__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2907:1: ( ( '>' ) )
            // InternalPapin.g:2908:1: ( '>' )
            {
            // InternalPapin.g:2908:1: ( '>' )
            // InternalPapin.g:2909:2: '>'
            {
             before(grammarAccess.getComparisonAccess().getGreaterThanSignKeyword_2_2()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getGreaterThanSignKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__2__Impl"


    // $ANTLR start "rule__Comparison__Group_2__3"
    // InternalPapin.g:2918:1: rule__Comparison__Group_2__3 : rule__Comparison__Group_2__3__Impl ;
    public final void rule__Comparison__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2922:1: ( rule__Comparison__Group_2__3__Impl )
            // InternalPapin.g:2923:2: rule__Comparison__Group_2__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__3"


    // $ANTLR start "rule__Comparison__Group_2__3__Impl"
    // InternalPapin.g:2929:1: rule__Comparison__Group_2__3__Impl : ( ( rule__Comparison__ValueAssignment_2_3 ) ) ;
    public final void rule__Comparison__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2933:1: ( ( ( rule__Comparison__ValueAssignment_2_3 ) ) )
            // InternalPapin.g:2934:1: ( ( rule__Comparison__ValueAssignment_2_3 ) )
            {
            // InternalPapin.g:2934:1: ( ( rule__Comparison__ValueAssignment_2_3 ) )
            // InternalPapin.g:2935:2: ( rule__Comparison__ValueAssignment_2_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_2_3()); 
            // InternalPapin.g:2936:2: ( rule__Comparison__ValueAssignment_2_3 )
            // InternalPapin.g:2936:3: rule__Comparison__ValueAssignment_2_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_2_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__3__Impl"


    // $ANTLR start "rule__Comparison__Group_3__0"
    // InternalPapin.g:2945:1: rule__Comparison__Group_3__0 : rule__Comparison__Group_3__0__Impl rule__Comparison__Group_3__1 ;
    public final void rule__Comparison__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2949:1: ( rule__Comparison__Group_3__0__Impl rule__Comparison__Group_3__1 )
            // InternalPapin.g:2950:2: rule__Comparison__Group_3__0__Impl rule__Comparison__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Comparison__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__0"


    // $ANTLR start "rule__Comparison__Group_3__0__Impl"
    // InternalPapin.g:2957:1: rule__Comparison__Group_3__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2961:1: ( ( () ) )
            // InternalPapin.g:2962:1: ( () )
            {
            // InternalPapin.g:2962:1: ( () )
            // InternalPapin.g:2963:2: ()
            {
             before(grammarAccess.getComparisonAccess().getMoreThanOrEqualAction_3_0()); 
            // InternalPapin.g:2964:2: ()
            // InternalPapin.g:2964:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getMoreThanOrEqualAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__0__Impl"


    // $ANTLR start "rule__Comparison__Group_3__1"
    // InternalPapin.g:2972:1: rule__Comparison__Group_3__1 : rule__Comparison__Group_3__1__Impl rule__Comparison__Group_3__2 ;
    public final void rule__Comparison__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2976:1: ( rule__Comparison__Group_3__1__Impl rule__Comparison__Group_3__2 )
            // InternalPapin.g:2977:2: rule__Comparison__Group_3__1__Impl rule__Comparison__Group_3__2
            {
            pushFollow(FOLLOW_37);
            rule__Comparison__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__1"


    // $ANTLR start "rule__Comparison__Group_3__1__Impl"
    // InternalPapin.g:2984:1: rule__Comparison__Group_3__1__Impl : ( ( rule__Comparison__PathAssignment_3_1 ) ) ;
    public final void rule__Comparison__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:2988:1: ( ( ( rule__Comparison__PathAssignment_3_1 ) ) )
            // InternalPapin.g:2989:1: ( ( rule__Comparison__PathAssignment_3_1 ) )
            {
            // InternalPapin.g:2989:1: ( ( rule__Comparison__PathAssignment_3_1 ) )
            // InternalPapin.g:2990:2: ( rule__Comparison__PathAssignment_3_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_3_1()); 
            // InternalPapin.g:2991:2: ( rule__Comparison__PathAssignment_3_1 )
            // InternalPapin.g:2991:3: rule__Comparison__PathAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__1__Impl"


    // $ANTLR start "rule__Comparison__Group_3__2"
    // InternalPapin.g:2999:1: rule__Comparison__Group_3__2 : rule__Comparison__Group_3__2__Impl rule__Comparison__Group_3__3 ;
    public final void rule__Comparison__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3003:1: ( rule__Comparison__Group_3__2__Impl rule__Comparison__Group_3__3 )
            // InternalPapin.g:3004:2: rule__Comparison__Group_3__2__Impl rule__Comparison__Group_3__3
            {
            pushFollow(FOLLOW_5);
            rule__Comparison__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__2"


    // $ANTLR start "rule__Comparison__Group_3__2__Impl"
    // InternalPapin.g:3011:1: rule__Comparison__Group_3__2__Impl : ( '>=' ) ;
    public final void rule__Comparison__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3015:1: ( ( '>=' ) )
            // InternalPapin.g:3016:1: ( '>=' )
            {
            // InternalPapin.g:3016:1: ( '>=' )
            // InternalPapin.g:3017:2: '>='
            {
             before(grammarAccess.getComparisonAccess().getGreaterThanSignEqualsSignKeyword_3_2()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getGreaterThanSignEqualsSignKeyword_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__2__Impl"


    // $ANTLR start "rule__Comparison__Group_3__3"
    // InternalPapin.g:3026:1: rule__Comparison__Group_3__3 : rule__Comparison__Group_3__3__Impl ;
    public final void rule__Comparison__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3030:1: ( rule__Comparison__Group_3__3__Impl )
            // InternalPapin.g:3031:2: rule__Comparison__Group_3__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__3"


    // $ANTLR start "rule__Comparison__Group_3__3__Impl"
    // InternalPapin.g:3037:1: rule__Comparison__Group_3__3__Impl : ( ( rule__Comparison__ValueAssignment_3_3 ) ) ;
    public final void rule__Comparison__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3041:1: ( ( ( rule__Comparison__ValueAssignment_3_3 ) ) )
            // InternalPapin.g:3042:1: ( ( rule__Comparison__ValueAssignment_3_3 ) )
            {
            // InternalPapin.g:3042:1: ( ( rule__Comparison__ValueAssignment_3_3 ) )
            // InternalPapin.g:3043:2: ( rule__Comparison__ValueAssignment_3_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_3_3()); 
            // InternalPapin.g:3044:2: ( rule__Comparison__ValueAssignment_3_3 )
            // InternalPapin.g:3044:3: rule__Comparison__ValueAssignment_3_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_3_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__3__Impl"


    // $ANTLR start "rule__Comparison__Group_4__0"
    // InternalPapin.g:3053:1: rule__Comparison__Group_4__0 : rule__Comparison__Group_4__0__Impl rule__Comparison__Group_4__1 ;
    public final void rule__Comparison__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3057:1: ( rule__Comparison__Group_4__0__Impl rule__Comparison__Group_4__1 )
            // InternalPapin.g:3058:2: rule__Comparison__Group_4__0__Impl rule__Comparison__Group_4__1
            {
            pushFollow(FOLLOW_11);
            rule__Comparison__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__0"


    // $ANTLR start "rule__Comparison__Group_4__0__Impl"
    // InternalPapin.g:3065:1: rule__Comparison__Group_4__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3069:1: ( ( () ) )
            // InternalPapin.g:3070:1: ( () )
            {
            // InternalPapin.g:3070:1: ( () )
            // InternalPapin.g:3071:2: ()
            {
             before(grammarAccess.getComparisonAccess().getLessThanAction_4_0()); 
            // InternalPapin.g:3072:2: ()
            // InternalPapin.g:3072:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getLessThanAction_4_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__0__Impl"


    // $ANTLR start "rule__Comparison__Group_4__1"
    // InternalPapin.g:3080:1: rule__Comparison__Group_4__1 : rule__Comparison__Group_4__1__Impl rule__Comparison__Group_4__2 ;
    public final void rule__Comparison__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3084:1: ( rule__Comparison__Group_4__1__Impl rule__Comparison__Group_4__2 )
            // InternalPapin.g:3085:2: rule__Comparison__Group_4__1__Impl rule__Comparison__Group_4__2
            {
            pushFollow(FOLLOW_38);
            rule__Comparison__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__1"


    // $ANTLR start "rule__Comparison__Group_4__1__Impl"
    // InternalPapin.g:3092:1: rule__Comparison__Group_4__1__Impl : ( ( rule__Comparison__PathAssignment_4_1 ) ) ;
    public final void rule__Comparison__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3096:1: ( ( ( rule__Comparison__PathAssignment_4_1 ) ) )
            // InternalPapin.g:3097:1: ( ( rule__Comparison__PathAssignment_4_1 ) )
            {
            // InternalPapin.g:3097:1: ( ( rule__Comparison__PathAssignment_4_1 ) )
            // InternalPapin.g:3098:2: ( rule__Comparison__PathAssignment_4_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_4_1()); 
            // InternalPapin.g:3099:2: ( rule__Comparison__PathAssignment_4_1 )
            // InternalPapin.g:3099:3: rule__Comparison__PathAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__1__Impl"


    // $ANTLR start "rule__Comparison__Group_4__2"
    // InternalPapin.g:3107:1: rule__Comparison__Group_4__2 : rule__Comparison__Group_4__2__Impl rule__Comparison__Group_4__3 ;
    public final void rule__Comparison__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3111:1: ( rule__Comparison__Group_4__2__Impl rule__Comparison__Group_4__3 )
            // InternalPapin.g:3112:2: rule__Comparison__Group_4__2__Impl rule__Comparison__Group_4__3
            {
            pushFollow(FOLLOW_5);
            rule__Comparison__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__2"


    // $ANTLR start "rule__Comparison__Group_4__2__Impl"
    // InternalPapin.g:3119:1: rule__Comparison__Group_4__2__Impl : ( '<' ) ;
    public final void rule__Comparison__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3123:1: ( ( '<' ) )
            // InternalPapin.g:3124:1: ( '<' )
            {
            // InternalPapin.g:3124:1: ( '<' )
            // InternalPapin.g:3125:2: '<'
            {
             before(grammarAccess.getComparisonAccess().getLessThanSignKeyword_4_2()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getLessThanSignKeyword_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__2__Impl"


    // $ANTLR start "rule__Comparison__Group_4__3"
    // InternalPapin.g:3134:1: rule__Comparison__Group_4__3 : rule__Comparison__Group_4__3__Impl ;
    public final void rule__Comparison__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3138:1: ( rule__Comparison__Group_4__3__Impl )
            // InternalPapin.g:3139:2: rule__Comparison__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__3"


    // $ANTLR start "rule__Comparison__Group_4__3__Impl"
    // InternalPapin.g:3145:1: rule__Comparison__Group_4__3__Impl : ( ( rule__Comparison__ValueAssignment_4_3 ) ) ;
    public final void rule__Comparison__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3149:1: ( ( ( rule__Comparison__ValueAssignment_4_3 ) ) )
            // InternalPapin.g:3150:1: ( ( rule__Comparison__ValueAssignment_4_3 ) )
            {
            // InternalPapin.g:3150:1: ( ( rule__Comparison__ValueAssignment_4_3 ) )
            // InternalPapin.g:3151:2: ( rule__Comparison__ValueAssignment_4_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_4_3()); 
            // InternalPapin.g:3152:2: ( rule__Comparison__ValueAssignment_4_3 )
            // InternalPapin.g:3152:3: rule__Comparison__ValueAssignment_4_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_4_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__3__Impl"


    // $ANTLR start "rule__Comparison__Group_5__0"
    // InternalPapin.g:3161:1: rule__Comparison__Group_5__0 : rule__Comparison__Group_5__0__Impl rule__Comparison__Group_5__1 ;
    public final void rule__Comparison__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3165:1: ( rule__Comparison__Group_5__0__Impl rule__Comparison__Group_5__1 )
            // InternalPapin.g:3166:2: rule__Comparison__Group_5__0__Impl rule__Comparison__Group_5__1
            {
            pushFollow(FOLLOW_11);
            rule__Comparison__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__0"


    // $ANTLR start "rule__Comparison__Group_5__0__Impl"
    // InternalPapin.g:3173:1: rule__Comparison__Group_5__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3177:1: ( ( () ) )
            // InternalPapin.g:3178:1: ( () )
            {
            // InternalPapin.g:3178:1: ( () )
            // InternalPapin.g:3179:2: ()
            {
             before(grammarAccess.getComparisonAccess().getLessThanOrEqualAction_5_0()); 
            // InternalPapin.g:3180:2: ()
            // InternalPapin.g:3180:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getLessThanOrEqualAction_5_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__0__Impl"


    // $ANTLR start "rule__Comparison__Group_5__1"
    // InternalPapin.g:3188:1: rule__Comparison__Group_5__1 : rule__Comparison__Group_5__1__Impl rule__Comparison__Group_5__2 ;
    public final void rule__Comparison__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3192:1: ( rule__Comparison__Group_5__1__Impl rule__Comparison__Group_5__2 )
            // InternalPapin.g:3193:2: rule__Comparison__Group_5__1__Impl rule__Comparison__Group_5__2
            {
            pushFollow(FOLLOW_39);
            rule__Comparison__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__1"


    // $ANTLR start "rule__Comparison__Group_5__1__Impl"
    // InternalPapin.g:3200:1: rule__Comparison__Group_5__1__Impl : ( ( rule__Comparison__PathAssignment_5_1 ) ) ;
    public final void rule__Comparison__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3204:1: ( ( ( rule__Comparison__PathAssignment_5_1 ) ) )
            // InternalPapin.g:3205:1: ( ( rule__Comparison__PathAssignment_5_1 ) )
            {
            // InternalPapin.g:3205:1: ( ( rule__Comparison__PathAssignment_5_1 ) )
            // InternalPapin.g:3206:2: ( rule__Comparison__PathAssignment_5_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_5_1()); 
            // InternalPapin.g:3207:2: ( rule__Comparison__PathAssignment_5_1 )
            // InternalPapin.g:3207:3: rule__Comparison__PathAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__1__Impl"


    // $ANTLR start "rule__Comparison__Group_5__2"
    // InternalPapin.g:3215:1: rule__Comparison__Group_5__2 : rule__Comparison__Group_5__2__Impl rule__Comparison__Group_5__3 ;
    public final void rule__Comparison__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3219:1: ( rule__Comparison__Group_5__2__Impl rule__Comparison__Group_5__3 )
            // InternalPapin.g:3220:2: rule__Comparison__Group_5__2__Impl rule__Comparison__Group_5__3
            {
            pushFollow(FOLLOW_5);
            rule__Comparison__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__2"


    // $ANTLR start "rule__Comparison__Group_5__2__Impl"
    // InternalPapin.g:3227:1: rule__Comparison__Group_5__2__Impl : ( '<=' ) ;
    public final void rule__Comparison__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3231:1: ( ( '<=' ) )
            // InternalPapin.g:3232:1: ( '<=' )
            {
            // InternalPapin.g:3232:1: ( '<=' )
            // InternalPapin.g:3233:2: '<='
            {
             before(grammarAccess.getComparisonAccess().getLessThanSignEqualsSignKeyword_5_2()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getLessThanSignEqualsSignKeyword_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__2__Impl"


    // $ANTLR start "rule__Comparison__Group_5__3"
    // InternalPapin.g:3242:1: rule__Comparison__Group_5__3 : rule__Comparison__Group_5__3__Impl ;
    public final void rule__Comparison__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3246:1: ( rule__Comparison__Group_5__3__Impl )
            // InternalPapin.g:3247:2: rule__Comparison__Group_5__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__3"


    // $ANTLR start "rule__Comparison__Group_5__3__Impl"
    // InternalPapin.g:3253:1: rule__Comparison__Group_5__3__Impl : ( ( rule__Comparison__ValueAssignment_5_3 ) ) ;
    public final void rule__Comparison__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3257:1: ( ( ( rule__Comparison__ValueAssignment_5_3 ) ) )
            // InternalPapin.g:3258:1: ( ( rule__Comparison__ValueAssignment_5_3 ) )
            {
            // InternalPapin.g:3258:1: ( ( rule__Comparison__ValueAssignment_5_3 ) )
            // InternalPapin.g:3259:2: ( rule__Comparison__ValueAssignment_5_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_5_3()); 
            // InternalPapin.g:3260:2: ( rule__Comparison__ValueAssignment_5_3 )
            // InternalPapin.g:3260:3: rule__Comparison__ValueAssignment_5_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_5_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__3__Impl"


    // $ANTLR start "rule__Path__Group__0"
    // InternalPapin.g:3269:1: rule__Path__Group__0 : rule__Path__Group__0__Impl rule__Path__Group__1 ;
    public final void rule__Path__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3273:1: ( rule__Path__Group__0__Impl rule__Path__Group__1 )
            // InternalPapin.g:3274:2: rule__Path__Group__0__Impl rule__Path__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__Path__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Path__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__0"


    // $ANTLR start "rule__Path__Group__0__Impl"
    // InternalPapin.g:3281:1: rule__Path__Group__0__Impl : ( ( rule__Path__JumpsAssignment_0 ) ) ;
    public final void rule__Path__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3285:1: ( ( ( rule__Path__JumpsAssignment_0 ) ) )
            // InternalPapin.g:3286:1: ( ( rule__Path__JumpsAssignment_0 ) )
            {
            // InternalPapin.g:3286:1: ( ( rule__Path__JumpsAssignment_0 ) )
            // InternalPapin.g:3287:2: ( rule__Path__JumpsAssignment_0 )
            {
             before(grammarAccess.getPathAccess().getJumpsAssignment_0()); 
            // InternalPapin.g:3288:2: ( rule__Path__JumpsAssignment_0 )
            // InternalPapin.g:3288:3: rule__Path__JumpsAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Path__JumpsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPathAccess().getJumpsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__0__Impl"


    // $ANTLR start "rule__Path__Group__1"
    // InternalPapin.g:3296:1: rule__Path__Group__1 : rule__Path__Group__1__Impl ;
    public final void rule__Path__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3300:1: ( rule__Path__Group__1__Impl )
            // InternalPapin.g:3301:2: rule__Path__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Path__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__1"


    // $ANTLR start "rule__Path__Group__1__Impl"
    // InternalPapin.g:3307:1: rule__Path__Group__1__Impl : ( ( rule__Path__Group_1__0 )* ) ;
    public final void rule__Path__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3311:1: ( ( ( rule__Path__Group_1__0 )* ) )
            // InternalPapin.g:3312:1: ( ( rule__Path__Group_1__0 )* )
            {
            // InternalPapin.g:3312:1: ( ( rule__Path__Group_1__0 )* )
            // InternalPapin.g:3313:2: ( rule__Path__Group_1__0 )*
            {
             before(grammarAccess.getPathAccess().getGroup_1()); 
            // InternalPapin.g:3314:2: ( rule__Path__Group_1__0 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==41) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalPapin.g:3314:3: rule__Path__Group_1__0
            	    {
            	    pushFollow(FOLLOW_41);
            	    rule__Path__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

             after(grammarAccess.getPathAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__1__Impl"


    // $ANTLR start "rule__Path__Group_1__0"
    // InternalPapin.g:3323:1: rule__Path__Group_1__0 : rule__Path__Group_1__0__Impl rule__Path__Group_1__1 ;
    public final void rule__Path__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3327:1: ( rule__Path__Group_1__0__Impl rule__Path__Group_1__1 )
            // InternalPapin.g:3328:2: rule__Path__Group_1__0__Impl rule__Path__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Path__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Path__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__0"


    // $ANTLR start "rule__Path__Group_1__0__Impl"
    // InternalPapin.g:3335:1: rule__Path__Group_1__0__Impl : ( '.' ) ;
    public final void rule__Path__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3339:1: ( ( '.' ) )
            // InternalPapin.g:3340:1: ( '.' )
            {
            // InternalPapin.g:3340:1: ( '.' )
            // InternalPapin.g:3341:2: '.'
            {
             before(grammarAccess.getPathAccess().getFullStopKeyword_1_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getPathAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__0__Impl"


    // $ANTLR start "rule__Path__Group_1__1"
    // InternalPapin.g:3350:1: rule__Path__Group_1__1 : rule__Path__Group_1__1__Impl ;
    public final void rule__Path__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3354:1: ( rule__Path__Group_1__1__Impl )
            // InternalPapin.g:3355:2: rule__Path__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Path__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__1"


    // $ANTLR start "rule__Path__Group_1__1__Impl"
    // InternalPapin.g:3361:1: rule__Path__Group_1__1__Impl : ( ( rule__Path__JumpsAssignment_1_1 ) ) ;
    public final void rule__Path__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3365:1: ( ( ( rule__Path__JumpsAssignment_1_1 ) ) )
            // InternalPapin.g:3366:1: ( ( rule__Path__JumpsAssignment_1_1 ) )
            {
            // InternalPapin.g:3366:1: ( ( rule__Path__JumpsAssignment_1_1 ) )
            // InternalPapin.g:3367:2: ( rule__Path__JumpsAssignment_1_1 )
            {
             before(grammarAccess.getPathAccess().getJumpsAssignment_1_1()); 
            // InternalPapin.g:3368:2: ( rule__Path__JumpsAssignment_1_1 )
            // InternalPapin.g:3368:3: rule__Path__JumpsAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Path__JumpsAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getPathAccess().getJumpsAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalPapin.g:3377:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3381:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalPapin.g:3382:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalPapin.g:3389:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3393:1: ( ( RULE_ID ) )
            // InternalPapin.g:3394:1: ( RULE_ID )
            {
            // InternalPapin.g:3394:1: ( RULE_ID )
            // InternalPapin.g:3395:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalPapin.g:3404:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3408:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalPapin.g:3409:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalPapin.g:3415:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3419:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalPapin.g:3420:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalPapin.g:3420:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalPapin.g:3421:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalPapin.g:3422:2: ( rule__QualifiedName__Group_1__0 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==41) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalPapin.g:3422:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_41);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalPapin.g:3431:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3435:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalPapin.g:3436:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalPapin.g:3443:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3447:1: ( ( '.' ) )
            // InternalPapin.g:3448:1: ( '.' )
            {
            // InternalPapin.g:3448:1: ( '.' )
            // InternalPapin.g:3449:2: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalPapin.g:3458:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3462:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalPapin.g:3463:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalPapin.g:3469:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3473:1: ( ( RULE_ID ) )
            // InternalPapin.g:3474:1: ( RULE_ID )
            {
            // InternalPapin.g:3474:1: ( RULE_ID )
            // InternalPapin.g:3475:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Datasets__DomainModelNSURIAssignment_1"
    // InternalPapin.g:3485:1: rule__Datasets__DomainModelNSURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Datasets__DomainModelNSURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3489:1: ( ( RULE_STRING ) )
            // InternalPapin.g:3490:2: ( RULE_STRING )
            {
            // InternalPapin.g:3490:2: ( RULE_STRING )
            // InternalPapin.g:3491:3: RULE_STRING
            {
             before(grammarAccess.getDatasetsAccess().getDomainModelNSURISTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getDatasetsAccess().getDomainModelNSURISTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__DomainModelNSURIAssignment_1"


    // $ANTLR start "rule__Datasets__DomainModelInstanceAssignment_3"
    // InternalPapin.g:3500:1: rule__Datasets__DomainModelInstanceAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Datasets__DomainModelInstanceAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3504:1: ( ( RULE_STRING ) )
            // InternalPapin.g:3505:2: ( RULE_STRING )
            {
            // InternalPapin.g:3505:2: ( RULE_STRING )
            // InternalPapin.g:3506:3: RULE_STRING
            {
             before(grammarAccess.getDatasetsAccess().getDomainModelInstanceSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getDatasetsAccess().getDomainModelInstanceSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__DomainModelInstanceAssignment_3"


    // $ANTLR start "rule__Datasets__DatasetsAssignment_4"
    // InternalPapin.g:3515:1: rule__Datasets__DatasetsAssignment_4 : ( ruleDataset ) ;
    public final void rule__Datasets__DatasetsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3519:1: ( ( ruleDataset ) )
            // InternalPapin.g:3520:2: ( ruleDataset )
            {
            // InternalPapin.g:3520:2: ( ruleDataset )
            // InternalPapin.g:3521:3: ruleDataset
            {
             before(grammarAccess.getDatasetsAccess().getDatasetsDatasetParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleDataset();

            state._fsp--;

             after(grammarAccess.getDatasetsAccess().getDatasetsDatasetParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Datasets__DatasetsAssignment_4"


    // $ANTLR start "rule__Dataset__NameAssignment_1"
    // InternalPapin.g:3530:1: rule__Dataset__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Dataset__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3534:1: ( ( RULE_ID ) )
            // InternalPapin.g:3535:2: ( RULE_ID )
            {
            // InternalPapin.g:3535:2: ( RULE_ID )
            // InternalPapin.g:3536:3: RULE_ID
            {
             before(grammarAccess.getDatasetAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDatasetAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__NameAssignment_1"


    // $ANTLR start "rule__Dataset__UsingAssignment_3"
    // InternalPapin.g:3545:1: rule__Dataset__UsingAssignment_3 : ( ( ruleQualifiedName ) ) ;
    public final void rule__Dataset__UsingAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3549:1: ( ( ( ruleQualifiedName ) ) )
            // InternalPapin.g:3550:2: ( ( ruleQualifiedName ) )
            {
            // InternalPapin.g:3550:2: ( ( ruleQualifiedName ) )
            // InternalPapin.g:3551:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getDatasetAccess().getUsingEffectCrossReference_3_0()); 
            // InternalPapin.g:3552:3: ( ruleQualifiedName )
            // InternalPapin.g:3553:4: ruleQualifiedName
            {
             before(grammarAccess.getDatasetAccess().getUsingEffectQualifiedNameParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getDatasetAccess().getUsingEffectQualifiedNameParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getDatasetAccess().getUsingEffectCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__UsingAssignment_3"


    // $ANTLR start "rule__Dataset__AttributeFilterAssignment_5"
    // InternalPapin.g:3564:1: rule__Dataset__AttributeFilterAssignment_5 : ( ruleAttributeFilter ) ;
    public final void rule__Dataset__AttributeFilterAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3568:1: ( ( ruleAttributeFilter ) )
            // InternalPapin.g:3569:2: ( ruleAttributeFilter )
            {
            // InternalPapin.g:3569:2: ( ruleAttributeFilter )
            // InternalPapin.g:3570:3: ruleAttributeFilter
            {
             before(grammarAccess.getDatasetAccess().getAttributeFilterAttributeFilterParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getDatasetAccess().getAttributeFilterAttributeFilterParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__AttributeFilterAssignment_5"


    // $ANTLR start "rule__Dataset__InstancesFilterAssignment_6"
    // InternalPapin.g:3579:1: rule__Dataset__InstancesFilterAssignment_6 : ( ruleInstancesFilter ) ;
    public final void rule__Dataset__InstancesFilterAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3583:1: ( ( ruleInstancesFilter ) )
            // InternalPapin.g:3584:2: ( ruleInstancesFilter )
            {
            // InternalPapin.g:3584:2: ( ruleInstancesFilter )
            // InternalPapin.g:3585:3: ruleInstancesFilter
            {
             before(grammarAccess.getDatasetAccess().getInstancesFilterInstancesFilterParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getDatasetAccess().getInstancesFilterInstancesFilterParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__InstancesFilterAssignment_6"


    // $ANTLR start "rule__Dataset__IncludedReferencesAssignment_7"
    // InternalPapin.g:3594:1: rule__Dataset__IncludedReferencesAssignment_7 : ( ruleIncludedReference ) ;
    public final void rule__Dataset__IncludedReferencesAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3598:1: ( ( ruleIncludedReference ) )
            // InternalPapin.g:3599:2: ( ruleIncludedReference )
            {
            // InternalPapin.g:3599:2: ( ruleIncludedReference )
            // InternalPapin.g:3600:3: ruleIncludedReference
            {
             before(grammarAccess.getDatasetAccess().getIncludedReferencesIncludedReferenceParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getDatasetAccess().getIncludedReferencesIncludedReferenceParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__IncludedReferencesAssignment_7"


    // $ANTLR start "rule__Dataset__TypeFilterAssignment_8"
    // InternalPapin.g:3609:1: rule__Dataset__TypeFilterAssignment_8 : ( ruleTypeFilter ) ;
    public final void rule__Dataset__TypeFilterAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3613:1: ( ( ruleTypeFilter ) )
            // InternalPapin.g:3614:2: ( ruleTypeFilter )
            {
            // InternalPapin.g:3614:2: ( ruleTypeFilter )
            // InternalPapin.g:3615:3: ruleTypeFilter
            {
             before(grammarAccess.getDatasetAccess().getTypeFilterTypeFilterParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getDatasetAccess().getTypeFilterTypeFilterParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dataset__TypeFilterAssignment_8"


    // $ANTLR start "rule__SimpleReference__CatAssignment_1"
    // InternalPapin.g:3624:1: rule__SimpleReference__CatAssignment_1 : ( ( ruleQualifiedName ) ) ;
    public final void rule__SimpleReference__CatAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3628:1: ( ( ( ruleQualifiedName ) ) )
            // InternalPapin.g:3629:2: ( ( ruleQualifiedName ) )
            {
            // InternalPapin.g:3629:2: ( ( ruleQualifiedName ) )
            // InternalPapin.g:3630:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getSimpleReferenceAccess().getCatCategoryCrossReference_1_0()); 
            // InternalPapin.g:3631:3: ( ruleQualifiedName )
            // InternalPapin.g:3632:4: ruleQualifiedName
            {
             before(grammarAccess.getSimpleReferenceAccess().getCatCategoryQualifiedNameParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getCatCategoryQualifiedNameParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getSimpleReferenceAccess().getCatCategoryCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__CatAssignment_1"


    // $ANTLR start "rule__SimpleReference__AttributeFilterAssignment_2"
    // InternalPapin.g:3643:1: rule__SimpleReference__AttributeFilterAssignment_2 : ( ruleAttributeFilter ) ;
    public final void rule__SimpleReference__AttributeFilterAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3647:1: ( ( ruleAttributeFilter ) )
            // InternalPapin.g:3648:2: ( ruleAttributeFilter )
            {
            // InternalPapin.g:3648:2: ( ruleAttributeFilter )
            // InternalPapin.g:3649:3: ruleAttributeFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAttributeFilterParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAttributeFilterParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__AttributeFilterAssignment_2"


    // $ANTLR start "rule__SimpleReference__PivotingIdAssignment_3_1"
    // InternalPapin.g:3658:1: rule__SimpleReference__PivotingIdAssignment_3_1 : ( rulePath ) ;
    public final void rule__SimpleReference__PivotingIdAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3662:1: ( ( rulePath ) )
            // InternalPapin.g:3663:2: ( rulePath )
            {
            // InternalPapin.g:3663:2: ( rulePath )
            // InternalPapin.g:3664:3: rulePath
            {
             before(grammarAccess.getSimpleReferenceAccess().getPivotingIdPathParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getPivotingIdPathParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__PivotingIdAssignment_3_1"


    // $ANTLR start "rule__SimpleReference__InstancesFilterAssignment_3_2"
    // InternalPapin.g:3673:1: rule__SimpleReference__InstancesFilterAssignment_3_2 : ( ruleInstancesFilter ) ;
    public final void rule__SimpleReference__InstancesFilterAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3677:1: ( ( ruleInstancesFilter ) )
            // InternalPapin.g:3678:2: ( ruleInstancesFilter )
            {
            // InternalPapin.g:3678:2: ( ruleInstancesFilter )
            // InternalPapin.g:3679:3: ruleInstancesFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__InstancesFilterAssignment_3_2"


    // $ANTLR start "rule__SimpleReference__CausesAssignment_4_1_1"
    // InternalPapin.g:3688:1: rule__SimpleReference__CausesAssignment_4_1_1 : ( ( ruleQualifiedName ) ) ;
    public final void rule__SimpleReference__CausesAssignment_4_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3692:1: ( ( ( ruleQualifiedName ) ) )
            // InternalPapin.g:3693:2: ( ( ruleQualifiedName ) )
            {
            // InternalPapin.g:3693:2: ( ( ruleQualifiedName ) )
            // InternalPapin.g:3694:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getSimpleReferenceAccess().getCausesDataLinkedCauseCrossReference_4_1_1_0()); 
            // InternalPapin.g:3695:3: ( ruleQualifiedName )
            // InternalPapin.g:3696:4: ruleQualifiedName
            {
             before(grammarAccess.getSimpleReferenceAccess().getCausesDataLinkedCauseQualifiedNameParserRuleCall_4_1_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getCausesDataLinkedCauseQualifiedNameParserRuleCall_4_1_1_0_1()); 

            }

             after(grammarAccess.getSimpleReferenceAccess().getCausesDataLinkedCauseCrossReference_4_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__CausesAssignment_4_1_1"


    // $ANTLR start "rule__SimpleReference__TypeFilterAssignment_4_2"
    // InternalPapin.g:3707:1: rule__SimpleReference__TypeFilterAssignment_4_2 : ( ruleTypeFilter ) ;
    public final void rule__SimpleReference__TypeFilterAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3711:1: ( ( ruleTypeFilter ) )
            // InternalPapin.g:3712:2: ( ruleTypeFilter )
            {
            // InternalPapin.g:3712:2: ( ruleTypeFilter )
            // InternalPapin.g:3713:3: ruleTypeFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__TypeFilterAssignment_4_2"


    // $ANTLR start "rule__AggregatedReference__CatAssignment_1"
    // InternalPapin.g:3722:1: rule__AggregatedReference__CatAssignment_1 : ( ( ruleQualifiedName ) ) ;
    public final void rule__AggregatedReference__CatAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3726:1: ( ( ( ruleQualifiedName ) ) )
            // InternalPapin.g:3727:2: ( ( ruleQualifiedName ) )
            {
            // InternalPapin.g:3727:2: ( ( ruleQualifiedName ) )
            // InternalPapin.g:3728:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getCatCategoryCrossReference_1_0()); 
            // InternalPapin.g:3729:3: ( ruleQualifiedName )
            // InternalPapin.g:3730:4: ruleQualifiedName
            {
             before(grammarAccess.getAggregatedReferenceAccess().getCatCategoryQualifiedNameParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getCatCategoryQualifiedNameParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getAggregatedReferenceAccess().getCatCategoryCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__CatAssignment_1"


    // $ANTLR start "rule__AggregatedReference__CausesAssignment_2"
    // InternalPapin.g:3741:1: rule__AggregatedReference__CausesAssignment_2 : ( ( ruleQualifiedName ) ) ;
    public final void rule__AggregatedReference__CausesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3745:1: ( ( ( ruleQualifiedName ) ) )
            // InternalPapin.g:3746:2: ( ( ruleQualifiedName ) )
            {
            // InternalPapin.g:3746:2: ( ( ruleQualifiedName ) )
            // InternalPapin.g:3747:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getCausesDataLinkedCauseCrossReference_2_0()); 
            // InternalPapin.g:3748:3: ( ruleQualifiedName )
            // InternalPapin.g:3749:4: ruleQualifiedName
            {
             before(grammarAccess.getAggregatedReferenceAccess().getCausesDataLinkedCauseQualifiedNameParserRuleCall_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getCausesDataLinkedCauseQualifiedNameParserRuleCall_2_0_1()); 

            }

             after(grammarAccess.getAggregatedReferenceAccess().getCausesDataLinkedCauseCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__CausesAssignment_2"


    // $ANTLR start "rule__AggregatedReference__FunctionAssignment_4"
    // InternalPapin.g:3760:1: rule__AggregatedReference__FunctionAssignment_4 : ( ruleAggFunction ) ;
    public final void rule__AggregatedReference__FunctionAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3764:1: ( ( ruleAggFunction ) )
            // InternalPapin.g:3765:2: ( ruleAggFunction )
            {
            // InternalPapin.g:3765:2: ( ruleAggFunction )
            // InternalPapin.g:3766:3: ruleAggFunction
            {
             before(grammarAccess.getAggregatedReferenceAccess().getFunctionAggFunctionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleAggFunction();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getFunctionAggFunctionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__FunctionAssignment_4"


    // $ANTLR start "rule__AggregatedReference__AggValueAssignment_6"
    // InternalPapin.g:3775:1: rule__AggregatedReference__AggValueAssignment_6 : ( rulePath ) ;
    public final void rule__AggregatedReference__AggValueAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3779:1: ( ( rulePath ) )
            // InternalPapin.g:3780:2: ( rulePath )
            {
            // InternalPapin.g:3780:2: ( rulePath )
            // InternalPapin.g:3781:3: rulePath
            {
             before(grammarAccess.getAggregatedReferenceAccess().getAggValuePathParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getAggValuePathParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__AggValueAssignment_6"


    // $ANTLR start "rule__AggregatedReference__PivotingIdAssignment_8_1"
    // InternalPapin.g:3790:1: rule__AggregatedReference__PivotingIdAssignment_8_1 : ( rulePath ) ;
    public final void rule__AggregatedReference__PivotingIdAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3794:1: ( ( rulePath ) )
            // InternalPapin.g:3795:2: ( rulePath )
            {
            // InternalPapin.g:3795:2: ( rulePath )
            // InternalPapin.g:3796:3: rulePath
            {
             before(grammarAccess.getAggregatedReferenceAccess().getPivotingIdPathParserRuleCall_8_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getPivotingIdPathParserRuleCall_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__PivotingIdAssignment_8_1"


    // $ANTLR start "rule__AggregatedReference__InstancesFilterAssignment_9"
    // InternalPapin.g:3805:1: rule__AggregatedReference__InstancesFilterAssignment_9 : ( ruleInstancesFilter ) ;
    public final void rule__AggregatedReference__InstancesFilterAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3809:1: ( ( ruleInstancesFilter ) )
            // InternalPapin.g:3810:2: ( ruleInstancesFilter )
            {
            // InternalPapin.g:3810:2: ( ruleInstancesFilter )
            // InternalPapin.g:3811:3: ruleInstancesFilter
            {
             before(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__InstancesFilterAssignment_9"


    // $ANTLR start "rule__TypeCompletion__TypeCustomizationsAssignment_1"
    // InternalPapin.g:3820:1: rule__TypeCompletion__TypeCustomizationsAssignment_1 : ( ruleTypeCustomization ) ;
    public final void rule__TypeCompletion__TypeCustomizationsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3824:1: ( ( ruleTypeCustomization ) )
            // InternalPapin.g:3825:2: ( ruleTypeCustomization )
            {
            // InternalPapin.g:3825:2: ( ruleTypeCustomization )
            // InternalPapin.g:3826:3: ruleTypeCustomization
            {
             before(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeCustomization();

            state._fsp--;

             after(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__TypeCustomizationsAssignment_1"


    // $ANTLR start "rule__TypeSelection__TypeCustomizationsAssignment_1"
    // InternalPapin.g:3835:1: rule__TypeSelection__TypeCustomizationsAssignment_1 : ( ruleTypeCustomization ) ;
    public final void rule__TypeSelection__TypeCustomizationsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3839:1: ( ( ruleTypeCustomization ) )
            // InternalPapin.g:3840:2: ( ruleTypeCustomization )
            {
            // InternalPapin.g:3840:2: ( ruleTypeCustomization )
            // InternalPapin.g:3841:3: ruleTypeCustomization
            {
             before(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeCustomization();

            state._fsp--;

             after(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__TypeCustomizationsAssignment_1"


    // $ANTLR start "rule__TypeCustomization__NameAssignment_0"
    // InternalPapin.g:3850:1: rule__TypeCustomization__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__TypeCustomization__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3854:1: ( ( RULE_ID ) )
            // InternalPapin.g:3855:2: ( RULE_ID )
            {
            // InternalPapin.g:3855:2: ( RULE_ID )
            // InternalPapin.g:3856:3: RULE_ID
            {
             before(grammarAccess.getTypeCustomizationAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTypeCustomizationAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__NameAssignment_0"


    // $ANTLR start "rule__TypeCustomization__AttributeFilterAssignment_1"
    // InternalPapin.g:3865:1: rule__TypeCustomization__AttributeFilterAssignment_1 : ( ruleAttributeFilter ) ;
    public final void rule__TypeCustomization__AttributeFilterAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3869:1: ( ( ruleAttributeFilter ) )
            // InternalPapin.g:3870:2: ( ruleAttributeFilter )
            {
            // InternalPapin.g:3870:2: ( ruleAttributeFilter )
            // InternalPapin.g:3871:3: ruleAttributeFilter
            {
             before(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__AttributeFilterAssignment_1"


    // $ANTLR start "rule__TypeCustomization__IncludedReferencesAssignment_2_1"
    // InternalPapin.g:3880:1: rule__TypeCustomization__IncludedReferencesAssignment_2_1 : ( ruleIncludedReference ) ;
    public final void rule__TypeCustomization__IncludedReferencesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3884:1: ( ( ruleIncludedReference ) )
            // InternalPapin.g:3885:2: ( ruleIncludedReference )
            {
            // InternalPapin.g:3885:2: ( ruleIncludedReference )
            // InternalPapin.g:3886:3: ruleIncludedReference
            {
             before(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesIncludedReferenceParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesIncludedReferenceParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__IncludedReferencesAssignment_2_1"


    // $ANTLR start "rule__AttributeFilter__AttributesAssignment_1"
    // InternalPapin.g:3895:1: rule__AttributeFilter__AttributesAssignment_1 : ( RULE_ID ) ;
    public final void rule__AttributeFilter__AttributesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3899:1: ( ( RULE_ID ) )
            // InternalPapin.g:3900:2: ( RULE_ID )
            {
            // InternalPapin.g:3900:2: ( RULE_ID )
            // InternalPapin.g:3901:3: RULE_ID
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__AttributesAssignment_1"


    // $ANTLR start "rule__AttributeFilter__AttributesAssignment_2_1"
    // InternalPapin.g:3910:1: rule__AttributeFilter__AttributesAssignment_2_1 : ( RULE_ID ) ;
    public final void rule__AttributeFilter__AttributesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3914:1: ( ( RULE_ID ) )
            // InternalPapin.g:3915:2: ( RULE_ID )
            {
            // InternalPapin.g:3915:2: ( RULE_ID )
            // InternalPapin.g:3916:3: RULE_ID
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__AttributesAssignment_2_1"


    // $ANTLR start "rule__AndConjunction__RightAssignment_1_2"
    // InternalPapin.g:3925:1: rule__AndConjunction__RightAssignment_1_2 : ( ruleOrConjunction ) ;
    public final void rule__AndConjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3929:1: ( ( ruleOrConjunction ) )
            // InternalPapin.g:3930:2: ( ruleOrConjunction )
            {
            // InternalPapin.g:3930:2: ( ruleOrConjunction )
            // InternalPapin.g:3931:3: ruleOrConjunction
            {
             before(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOrConjunction();

            state._fsp--;

             after(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__RightAssignment_1_2"


    // $ANTLR start "rule__OrConjunction__RightAssignment_1_2"
    // InternalPapin.g:3940:1: rule__OrConjunction__RightAssignment_1_2 : ( rulePrimary ) ;
    public final void rule__OrConjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3944:1: ( ( rulePrimary ) )
            // InternalPapin.g:3945:2: ( rulePrimary )
            {
            // InternalPapin.g:3945:2: ( rulePrimary )
            // InternalPapin.g:3946:3: rulePrimary
            {
             before(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__RightAssignment_1_2"


    // $ANTLR start "rule__Comparison__PathAssignment_0_1"
    // InternalPapin.g:3955:1: rule__Comparison__PathAssignment_0_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3959:1: ( ( rulePath ) )
            // InternalPapin.g:3960:2: ( rulePath )
            {
            // InternalPapin.g:3960:2: ( rulePath )
            // InternalPapin.g:3961:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_0_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_0_3"
    // InternalPapin.g:3970:1: rule__Comparison__ValueAssignment_0_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3974:1: ( ( RULE_STRING ) )
            // InternalPapin.g:3975:2: ( RULE_STRING )
            {
            // InternalPapin.g:3975:2: ( RULE_STRING )
            // InternalPapin.g:3976:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_0_3"


    // $ANTLR start "rule__Comparison__PathAssignment_1_1"
    // InternalPapin.g:3985:1: rule__Comparison__PathAssignment_1_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:3989:1: ( ( rulePath ) )
            // InternalPapin.g:3990:2: ( rulePath )
            {
            // InternalPapin.g:3990:2: ( rulePath )
            // InternalPapin.g:3991:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_1_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_1_3"
    // InternalPapin.g:4000:1: rule__Comparison__ValueAssignment_1_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4004:1: ( ( RULE_STRING ) )
            // InternalPapin.g:4005:2: ( RULE_STRING )
            {
            // InternalPapin.g:4005:2: ( RULE_STRING )
            // InternalPapin.g:4006:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_1_3"


    // $ANTLR start "rule__Comparison__PathAssignment_2_1"
    // InternalPapin.g:4015:1: rule__Comparison__PathAssignment_2_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4019:1: ( ( rulePath ) )
            // InternalPapin.g:4020:2: ( rulePath )
            {
            // InternalPapin.g:4020:2: ( rulePath )
            // InternalPapin.g:4021:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_2_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_2_3"
    // InternalPapin.g:4030:1: rule__Comparison__ValueAssignment_2_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4034:1: ( ( RULE_STRING ) )
            // InternalPapin.g:4035:2: ( RULE_STRING )
            {
            // InternalPapin.g:4035:2: ( RULE_STRING )
            // InternalPapin.g:4036:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_2_3"


    // $ANTLR start "rule__Comparison__PathAssignment_3_1"
    // InternalPapin.g:4045:1: rule__Comparison__PathAssignment_3_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4049:1: ( ( rulePath ) )
            // InternalPapin.g:4050:2: ( rulePath )
            {
            // InternalPapin.g:4050:2: ( rulePath )
            // InternalPapin.g:4051:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_3_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_3_3"
    // InternalPapin.g:4060:1: rule__Comparison__ValueAssignment_3_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4064:1: ( ( RULE_STRING ) )
            // InternalPapin.g:4065:2: ( RULE_STRING )
            {
            // InternalPapin.g:4065:2: ( RULE_STRING )
            // InternalPapin.g:4066:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_3_3"


    // $ANTLR start "rule__Comparison__PathAssignment_4_1"
    // InternalPapin.g:4075:1: rule__Comparison__PathAssignment_4_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4079:1: ( ( rulePath ) )
            // InternalPapin.g:4080:2: ( rulePath )
            {
            // InternalPapin.g:4080:2: ( rulePath )
            // InternalPapin.g:4081:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_4_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_4_3"
    // InternalPapin.g:4090:1: rule__Comparison__ValueAssignment_4_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_4_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4094:1: ( ( RULE_STRING ) )
            // InternalPapin.g:4095:2: ( RULE_STRING )
            {
            // InternalPapin.g:4095:2: ( RULE_STRING )
            // InternalPapin.g:4096:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_4_3"


    // $ANTLR start "rule__Comparison__PathAssignment_5_1"
    // InternalPapin.g:4105:1: rule__Comparison__PathAssignment_5_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4109:1: ( ( rulePath ) )
            // InternalPapin.g:4110:2: ( rulePath )
            {
            // InternalPapin.g:4110:2: ( rulePath )
            // InternalPapin.g:4111:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_5_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_5_3"
    // InternalPapin.g:4120:1: rule__Comparison__ValueAssignment_5_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_5_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4124:1: ( ( RULE_STRING ) )
            // InternalPapin.g:4125:2: ( RULE_STRING )
            {
            // InternalPapin.g:4125:2: ( RULE_STRING )
            // InternalPapin.g:4126:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_5_3"


    // $ANTLR start "rule__Path__JumpsAssignment_0"
    // InternalPapin.g:4135:1: rule__Path__JumpsAssignment_0 : ( RULE_ID ) ;
    public final void rule__Path__JumpsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4139:1: ( ( RULE_ID ) )
            // InternalPapin.g:4140:2: ( RULE_ID )
            {
            // InternalPapin.g:4140:2: ( RULE_ID )
            // InternalPapin.g:4141:3: RULE_ID
            {
             before(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__JumpsAssignment_0"


    // $ANTLR start "rule__Path__JumpsAssignment_1_1"
    // InternalPapin.g:4150:1: rule__Path__JumpsAssignment_1_1 : ( RULE_ID ) ;
    public final void rule__Path__JumpsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPapin.g:4154:1: ( ( RULE_ID ) )
            // InternalPapin.g:4155:2: ( RULE_ID )
            {
            // InternalPapin.g:4155:2: ( RULE_ID )
            // InternalPapin.g:4156:3: RULE_ID
            {
             before(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_1_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__JumpsAssignment_1_1"

    // Delegated rules


    protected DFA7 dfa7 = new DFA7(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\4\1\43\1\4\6\uffff\1\43";
    static final String dfa_3s = "\1\4\1\51\1\4\6\uffff\1\51";
    static final String dfa_4s = "\3\uffff\1\4\1\6\1\1\1\3\1\5\1\2\1\uffff";
    static final String dfa_5s = "\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\1",
            "\1\5\1\10\1\6\1\3\1\7\1\4\1\2",
            "\1\11",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\5\1\10\1\6\1\3\1\7\1\4\1\2"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "618:1: rule__Comparison__Alternatives : ( ( ( rule__Comparison__Group_0__0 ) ) | ( ( rule__Comparison__Group_1__0 ) ) | ( ( rule__Comparison__Group_2__0 ) ) | ( ( rule__Comparison__Group_3__0 ) ) | ( ( rule__Comparison__Group_4__0 ) ) | ( ( rule__Comparison__Group_5__0 ) ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000133600000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001400002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000020900000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000012600000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000010L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x000000000000F800L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000100800000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000020100000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000001600000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x00000000C0000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000004000010L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000020000000002L});

}