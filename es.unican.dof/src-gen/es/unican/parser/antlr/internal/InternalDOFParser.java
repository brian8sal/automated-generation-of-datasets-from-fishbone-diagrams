package es.unican.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import es.unican.services.DOFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDOFParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'dof'", "'import'", "'effect'", "'is'", "'category'", "'include'", "'by'", "'{'", "'}'", "'calculate'", "'as'", "'('", "')'", "'count'", "'sum'", "'avg'", "'max'", "'min'", "'only_as'", "'['", "','", "']'", "'where'", "'and'", "'or'", "'='", "'!='", "'>'", "'>='", "'<'", "'<='", "'cause'", "'realizes'", "'contains'", "'notMapped'", "'.*'", "'.'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalDOFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDOFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDOFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDOF.g"; }



     	private DOFGrammarAccess grammarAccess;

        public InternalDOFParser(TokenStream input, DOFGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "DOF";
       	}

       	@Override
       	protected DOFGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDOF"
    // InternalDOF.g:64:1: entryRuleDOF returns [EObject current=null] : iv_ruleDOF= ruleDOF EOF ;
    public final EObject entryRuleDOF() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDOF = null;


        try {
            // InternalDOF.g:64:44: (iv_ruleDOF= ruleDOF EOF )
            // InternalDOF.g:65:2: iv_ruleDOF= ruleDOF EOF
            {
             newCompositeNode(grammarAccess.getDOFRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDOF=ruleDOF();

            state._fsp--;

             current =iv_ruleDOF; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDOF"


    // $ANTLR start "ruleDOF"
    // InternalDOF.g:71:1: ruleDOF returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) ) ) ;
    public final EObject ruleDOF() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_imports_0_0 = null;

        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_effect_3_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:77:2: ( ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) ) ) )
            // InternalDOF.g:78:2: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) ) )
            {
            // InternalDOF.g:78:2: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) ) )
            // InternalDOF.g:79:3: ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) )
            {
            // InternalDOF.g:79:3: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalDOF.g:80:4: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalDOF.g:80:4: (lv_imports_0_0= ruleImport )
            	    // InternalDOF.g:81:5: lv_imports_0_0= ruleImport
            	    {

            	    					newCompositeNode(grammarAccess.getDOFAccess().getImportsImportParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDOFRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_0_0,
            	    						"es.unican.DOF.Import");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_1=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getDOFAccess().getDofKeyword_1());
            		
            // InternalDOF.g:102:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalDOF.g:103:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalDOF.g:103:4: (lv_name_2_0= ruleQualifiedName )
            // InternalDOF.g:104:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getDOFAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDOFRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.DOF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:121:3: ( (lv_effect_3_0= ruleEffect ) )
            // InternalDOF.g:122:4: (lv_effect_3_0= ruleEffect )
            {
            // InternalDOF.g:122:4: (lv_effect_3_0= ruleEffect )
            // InternalDOF.g:123:5: lv_effect_3_0= ruleEffect
            {

            					newCompositeNode(grammarAccess.getDOFAccess().getEffectEffectParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_effect_3_0=ruleEffect();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDOFRule());
            					}
            					set(
            						current,
            						"effect",
            						lv_effect_3_0,
            						"es.unican.DOF.Effect");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDOF"


    // $ANTLR start "entryRuleImport"
    // InternalDOF.g:144:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalDOF.g:144:47: (iv_ruleImport= ruleImport EOF )
            // InternalDOF.g:145:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalDOF.g:151:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:157:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) )
            // InternalDOF.g:158:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            {
            // InternalDOF.g:158:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            // InternalDOF.g:159:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalDOF.g:163:3: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:164:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:164:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // InternalDOF.g:165:5: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImportRule());
            					}
            					set(
            						current,
            						"importedNamespace",
            						lv_importedNamespace_1_0,
            						"es.unican.DOF.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleEffect"
    // InternalDOF.g:186:1: entryRuleEffect returns [EObject current=null] : iv_ruleEffect= ruleEffect EOF ;
    public final EObject entryRuleEffect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEffect = null;


        try {
            // InternalDOF.g:186:47: (iv_ruleEffect= ruleEffect EOF )
            // InternalDOF.g:187:2: iv_ruleEffect= ruleEffect EOF
            {
             newCompositeNode(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEffect=ruleEffect();

            state._fsp--;

             current =iv_ruleEffect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalDOF.g:193:1: ruleEffect returns [EObject current=null] : ( () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* ) ;
    public final EObject ruleEffect() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_dataFeeder_4_0 = null;

        EObject lv_categories_5_0 = null;

        EObject lv_categories_6_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:199:2: ( ( () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* ) )
            // InternalDOF.g:200:2: ( () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )
            {
            // InternalDOF.g:200:2: ( () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )
            // InternalDOF.g:201:3: () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )*
            {
            // InternalDOF.g:201:3: ()
            // InternalDOF.g:202:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEffectAccess().getEffectAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,13,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getEffectAccess().getEffectKeyword_1());
            		
            // InternalDOF.g:212:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalDOF.g:213:4: (lv_name_2_0= RULE_ID )
            {
            // InternalDOF.g:213:4: (lv_name_2_0= RULE_ID )
            // InternalDOF.g:214:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEffectRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_3, grammarAccess.getEffectAccess().getIsKeyword_3());
            		
            // InternalDOF.g:234:3: ( (lv_dataFeeder_4_0= ruleDataFeeder ) )
            // InternalDOF.g:235:4: (lv_dataFeeder_4_0= ruleDataFeeder )
            {
            // InternalDOF.g:235:4: (lv_dataFeeder_4_0= ruleDataFeeder )
            // InternalDOF.g:236:5: lv_dataFeeder_4_0= ruleDataFeeder
            {

            					newCompositeNode(grammarAccess.getEffectAccess().getDataFeederDataFeederParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_7);
            lv_dataFeeder_4_0=ruleDataFeeder();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEffectRule());
            					}
            					set(
            						current,
            						"dataFeeder",
            						lv_dataFeeder_4_0,
            						"es.unican.DOF.DataFeeder");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:253:3: ( (lv_categories_5_0= ruleCategory ) )
            // InternalDOF.g:254:4: (lv_categories_5_0= ruleCategory )
            {
            // InternalDOF.g:254:4: (lv_categories_5_0= ruleCategory )
            // InternalDOF.g:255:5: lv_categories_5_0= ruleCategory
            {

            					newCompositeNode(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_8);
            lv_categories_5_0=ruleCategory();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEffectRule());
            					}
            					add(
            						current,
            						"categories",
            						lv_categories_5_0,
            						"es.unican.DOF.Category");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:272:3: ( (lv_categories_6_0= ruleCategory ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==15) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalDOF.g:273:4: (lv_categories_6_0= ruleCategory )
            	    {
            	    // InternalDOF.g:273:4: (lv_categories_6_0= ruleCategory )
            	    // InternalDOF.g:274:5: lv_categories_6_0= ruleCategory
            	    {

            	    					newCompositeNode(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_categories_6_0=ruleCategory();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEffectRule());
            	    					}
            	    					add(
            	    						current,
            	    						"categories",
            	    						lv_categories_6_0,
            	    						"es.unican.DOF.Category");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalDOF.g:295:1: entryRuleCategory returns [EObject current=null] : iv_ruleCategory= ruleCategory EOF ;
    public final EObject entryRuleCategory() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCategory = null;


        try {
            // InternalDOF.g:295:49: (iv_ruleCategory= ruleCategory EOF )
            // InternalDOF.g:296:2: iv_ruleCategory= ruleCategory EOF
            {
             newCompositeNode(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCategory=ruleCategory();

            state._fsp--;

             current =iv_ruleCategory; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalDOF.g:302:1: ruleCategory returns [EObject current=null] : ( () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )* ) ;
    public final EObject ruleCategory() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        EObject lv_causes_3_0 = null;

        EObject lv_causes_4_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:308:2: ( ( () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )* ) )
            // InternalDOF.g:309:2: ( () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )* )
            {
            // InternalDOF.g:309:2: ( () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )* )
            // InternalDOF.g:310:3: () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )*
            {
            // InternalDOF.g:310:3: ()
            // InternalDOF.g:311:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCategoryAccess().getCategoryAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,15,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getCategoryAccess().getCategoryKeyword_1());
            		
            // InternalDOF.g:321:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalDOF.g:322:4: (lv_name_2_0= RULE_ID )
            {
            // InternalDOF.g:322:4: (lv_name_2_0= RULE_ID )
            // InternalDOF.g:323:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_2_0, grammarAccess.getCategoryAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCategoryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:339:3: ( (lv_causes_3_0= ruleCause ) )
            // InternalDOF.g:340:4: (lv_causes_3_0= ruleCause )
            {
            // InternalDOF.g:340:4: (lv_causes_3_0= ruleCause )
            // InternalDOF.g:341:5: lv_causes_3_0= ruleCause
            {

            					newCompositeNode(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_10);
            lv_causes_3_0=ruleCause();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCategoryRule());
            					}
            					add(
            						current,
            						"causes",
            						lv_causes_3_0,
            						"es.unican.DOF.Cause");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:358:3: ( (lv_causes_4_0= ruleCause ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==42) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalDOF.g:359:4: (lv_causes_4_0= ruleCause )
            	    {
            	    // InternalDOF.g:359:4: (lv_causes_4_0= ruleCause )
            	    // InternalDOF.g:360:5: lv_causes_4_0= ruleCause
            	    {

            	    					newCompositeNode(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_10);
            	    lv_causes_4_0=ruleCause();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCategoryRule());
            	    					}
            	    					add(
            	    						current,
            	    						"causes",
            	    						lv_causes_4_0,
            	    						"es.unican.DOF.Cause");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleDataFeeder"
    // InternalDOF.g:381:1: entryRuleDataFeeder returns [EObject current=null] : iv_ruleDataFeeder= ruleDataFeeder EOF ;
    public final EObject entryRuleDataFeeder() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataFeeder = null;


        try {
            // InternalDOF.g:381:51: (iv_ruleDataFeeder= ruleDataFeeder EOF )
            // InternalDOF.g:382:2: iv_ruleDataFeeder= ruleDataFeeder EOF
            {
             newCompositeNode(grammarAccess.getDataFeederRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataFeeder=ruleDataFeeder();

            state._fsp--;

             current =iv_ruleDataFeeder; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataFeeder"


    // $ANTLR start "ruleDataFeeder"
    // InternalDOF.g:388:1: ruleDataFeeder returns [EObject current=null] : ( () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )? ) ;
    public final EObject ruleDataFeeder() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_attributeFilter_2_0 = null;

        EObject lv_instancesFilter_3_0 = null;

        EObject lv_includedReferences_5_0 = null;

        EObject lv_typeFilter_6_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:394:2: ( ( () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )? ) )
            // InternalDOF.g:395:2: ( () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )? )
            {
            // InternalDOF.g:395:2: ( () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )? )
            // InternalDOF.g:396:3: () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )?
            {
            // InternalDOF.g:396:3: ()
            // InternalDOF.g:397:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDataFeederAccess().getDataFeederAction_0(),
            					current);
            			

            }

            // InternalDOF.g:403:3: ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:404:4: (lv_name_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:404:4: (lv_name_1_0= ruleQualifiedNameWithWildcard )
            // InternalDOF.g:405:5: lv_name_1_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getDataFeederAccess().getNameQualifiedNameWithWildcardParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_11);
            lv_name_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDataFeederRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"es.unican.DOF.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:422:3: ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==30) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalDOF.g:423:4: (lv_attributeFilter_2_0= ruleAttributeFilter )
                    {
                    // InternalDOF.g:423:4: (lv_attributeFilter_2_0= ruleAttributeFilter )
                    // InternalDOF.g:424:5: lv_attributeFilter_2_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getDataFeederAccess().getAttributeFilterAttributeFilterParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_12);
                    lv_attributeFilter_2_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataFeederRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_2_0,
                    						"es.unican.DOF.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:441:3: ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==33) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalDOF.g:442:4: (lv_instancesFilter_3_0= ruleInstancesFilter )
                    {
                    // InternalDOF.g:442:4: (lv_instancesFilter_3_0= ruleInstancesFilter )
                    // InternalDOF.g:443:5: lv_instancesFilter_3_0= ruleInstancesFilter
                    {

                    					newCompositeNode(grammarAccess.getDataFeederAccess().getInstancesFilterInstancesFilterParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_13);
                    lv_instancesFilter_3_0=ruleInstancesFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataFeederRule());
                    					}
                    					set(
                    						current,
                    						"instancesFilter",
                    						lv_instancesFilter_3_0,
                    						"es.unican.DOF.InstancesFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:460:3: (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==16) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalDOF.g:461:4: otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) )
            	    {
            	    otherlv_4=(Token)match(input,16,FOLLOW_14); 

            	    				newLeafNode(otherlv_4, grammarAccess.getDataFeederAccess().getIncludeKeyword_4_0());
            	    			
            	    // InternalDOF.g:465:4: ( (lv_includedReferences_5_0= ruleIncludedReference ) )
            	    // InternalDOF.g:466:5: (lv_includedReferences_5_0= ruleIncludedReference )
            	    {
            	    // InternalDOF.g:466:5: (lv_includedReferences_5_0= ruleIncludedReference )
            	    // InternalDOF.g:467:6: lv_includedReferences_5_0= ruleIncludedReference
            	    {

            	    						newCompositeNode(grammarAccess.getDataFeederAccess().getIncludedReferencesIncludedReferenceParserRuleCall_4_1_0());
            	    					
            	    pushFollow(FOLLOW_13);
            	    lv_includedReferences_5_0=ruleIncludedReference();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getDataFeederRule());
            	    						}
            	    						add(
            	    							current,
            	    							"includedReferences",
            	    							lv_includedReferences_5_0,
            	    							"es.unican.DOF.IncludedReference");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalDOF.g:485:3: ( (lv_typeFilter_6_0= ruleTypeFilter ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21||LA7_0==29) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalDOF.g:486:4: (lv_typeFilter_6_0= ruleTypeFilter )
                    {
                    // InternalDOF.g:486:4: (lv_typeFilter_6_0= ruleTypeFilter )
                    // InternalDOF.g:487:5: lv_typeFilter_6_0= ruleTypeFilter
                    {

                    					newCompositeNode(grammarAccess.getDataFeederAccess().getTypeFilterTypeFilterParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_typeFilter_6_0=ruleTypeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataFeederRule());
                    					}
                    					set(
                    						current,
                    						"typeFilter",
                    						lv_typeFilter_6_0,
                    						"es.unican.DOF.TypeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataFeeder"


    // $ANTLR start "entryRuleIncludedReference"
    // InternalDOF.g:508:1: entryRuleIncludedReference returns [EObject current=null] : iv_ruleIncludedReference= ruleIncludedReference EOF ;
    public final EObject entryRuleIncludedReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIncludedReference = null;


        try {
            // InternalDOF.g:508:58: (iv_ruleIncludedReference= ruleIncludedReference EOF )
            // InternalDOF.g:509:2: iv_ruleIncludedReference= ruleIncludedReference EOF
            {
             newCompositeNode(grammarAccess.getIncludedReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIncludedReference=ruleIncludedReference();

            state._fsp--;

             current =iv_ruleIncludedReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIncludedReference"


    // $ANTLR start "ruleIncludedReference"
    // InternalDOF.g:515:1: ruleIncludedReference returns [EObject current=null] : (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference ) ;
    public final EObject ruleIncludedReference() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleReference_0 = null;

        EObject this_AggregatedReference_1 = null;



        	enterRule();

        try {
            // InternalDOF.g:521:2: ( (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference ) )
            // InternalDOF.g:522:2: (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference )
            {
            // InternalDOF.g:522:2: (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_ID) ) {
                alt8=1;
            }
            else if ( (LA8_0==20) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalDOF.g:523:3: this_SimpleReference_0= ruleSimpleReference
                    {

                    			newCompositeNode(grammarAccess.getIncludedReferenceAccess().getSimpleReferenceParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleReference_0=ruleSimpleReference();

                    state._fsp--;


                    			current = this_SimpleReference_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:532:3: this_AggregatedReference_1= ruleAggregatedReference
                    {

                    			newCompositeNode(grammarAccess.getIncludedReferenceAccess().getAggregatedReferenceParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AggregatedReference_1=ruleAggregatedReference();

                    state._fsp--;


                    			current = this_AggregatedReference_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIncludedReference"


    // $ANTLR start "entryRuleSimpleReference"
    // InternalDOF.g:544:1: entryRuleSimpleReference returns [EObject current=null] : iv_ruleSimpleReference= ruleSimpleReference EOF ;
    public final EObject entryRuleSimpleReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleReference = null;


        try {
            // InternalDOF.g:544:56: (iv_ruleSimpleReference= ruleSimpleReference EOF )
            // InternalDOF.g:545:2: iv_ruleSimpleReference= ruleSimpleReference EOF
            {
             newCompositeNode(grammarAccess.getSimpleReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleReference=ruleSimpleReference();

            state._fsp--;

             current =iv_ruleSimpleReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleReference"


    // $ANTLR start "ruleSimpleReference"
    // InternalDOF.g:551:1: ruleSimpleReference returns [EObject current=null] : ( ( (lv_name_0_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )? ) ;
    public final EObject ruleSimpleReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        EObject lv_attributeFilter_1_0 = null;

        EObject lv_pivotingId_3_0 = null;

        EObject lv_instancesFilter_4_0 = null;

        EObject lv_includedReferences_7_0 = null;

        EObject lv_typeFilter_8_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:557:2: ( ( ( (lv_name_0_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )? ) )
            // InternalDOF.g:558:2: ( ( (lv_name_0_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )? )
            {
            // InternalDOF.g:558:2: ( ( (lv_name_0_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )? )
            // InternalDOF.g:559:3: ( (lv_name_0_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )?
            {
            // InternalDOF.g:559:3: ( (lv_name_0_0= ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:560:4: (lv_name_0_0= ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:560:4: (lv_name_0_0= ruleQualifiedNameWithWildcard )
            // InternalDOF.g:561:5: lv_name_0_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getSimpleReferenceAccess().getNameQualifiedNameWithWildcardParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_15);
            lv_name_0_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"es.unican.DOF.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:578:3: ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==30) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalDOF.g:579:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    {
                    // InternalDOF.g:579:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    // InternalDOF.g:580:5: lv_attributeFilter_1_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_16);
                    lv_attributeFilter_1_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_1_0,
                    						"es.unican.DOF.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:597:3: (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==17) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDOF.g:598:4: otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )?
                    {
                    otherlv_2=(Token)match(input,17,FOLLOW_4); 

                    				newLeafNode(otherlv_2, grammarAccess.getSimpleReferenceAccess().getByKeyword_2_0());
                    			
                    // InternalDOF.g:602:4: ( (lv_pivotingId_3_0= rulePath ) )
                    // InternalDOF.g:603:5: (lv_pivotingId_3_0= rulePath )
                    {
                    // InternalDOF.g:603:5: (lv_pivotingId_3_0= rulePath )
                    // InternalDOF.g:604:6: lv_pivotingId_3_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getSimpleReferenceAccess().getPivotingIdPathParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_pivotingId_3_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                    						}
                    						set(
                    							current,
                    							"pivotingId",
                    							lv_pivotingId_3_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDOF.g:621:4: ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==33) ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalDOF.g:622:5: (lv_instancesFilter_4_0= ruleInstancesFilter )
                            {
                            // InternalDOF.g:622:5: (lv_instancesFilter_4_0= ruleInstancesFilter )
                            // InternalDOF.g:623:6: lv_instancesFilter_4_0= ruleInstancesFilter
                            {

                            						newCompositeNode(grammarAccess.getSimpleReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_2_2_0());
                            					
                            pushFollow(FOLLOW_18);
                            lv_instancesFilter_4_0=ruleInstancesFilter();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                            						}
                            						set(
                            							current,
                            							"instancesFilter",
                            							lv_instancesFilter_4_0,
                            							"es.unican.DOF.InstancesFilter");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalDOF.g:641:3: (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==18) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalDOF.g:642:4: otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}'
                    {
                    otherlv_5=(Token)match(input,18,FOLLOW_19); 

                    				newLeafNode(otherlv_5, grammarAccess.getSimpleReferenceAccess().getLeftCurlyBracketKeyword_3_0());
                    			
                    otherlv_6=(Token)match(input,16,FOLLOW_20); 

                    				newLeafNode(otherlv_6, grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_3_1());
                    			
                    // InternalDOF.g:650:4: ( (lv_includedReferences_7_0= ruleIncludedReference ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==RULE_ID||LA12_0==20) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalDOF.g:651:5: (lv_includedReferences_7_0= ruleIncludedReference )
                    	    {
                    	    // InternalDOF.g:651:5: (lv_includedReferences_7_0= ruleIncludedReference )
                    	    // InternalDOF.g:652:6: lv_includedReferences_7_0= ruleIncludedReference
                    	    {

                    	    						newCompositeNode(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesIncludedReferenceParserRuleCall_3_2_0());
                    	    					
                    	    pushFollow(FOLLOW_20);
                    	    lv_includedReferences_7_0=ruleIncludedReference();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"includedReferences",
                    	    							lv_includedReferences_7_0,
                    	    							"es.unican.DOF.IncludedReference");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    // InternalDOF.g:669:4: ( (lv_typeFilter_8_0= ruleTypeFilter ) )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==21||LA13_0==29) ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalDOF.g:670:5: (lv_typeFilter_8_0= ruleTypeFilter )
                            {
                            // InternalDOF.g:670:5: (lv_typeFilter_8_0= ruleTypeFilter )
                            // InternalDOF.g:671:6: lv_typeFilter_8_0= ruleTypeFilter
                            {

                            						newCompositeNode(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_3_3_0());
                            					
                            pushFollow(FOLLOW_21);
                            lv_typeFilter_8_0=ruleTypeFilter();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                            						}
                            						set(
                            							current,
                            							"typeFilter",
                            							lv_typeFilter_8_0,
                            							"es.unican.DOF.TypeFilter");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    otherlv_9=(Token)match(input,19,FOLLOW_2); 

                    				newLeafNode(otherlv_9, grammarAccess.getSimpleReferenceAccess().getRightCurlyBracketKeyword_3_4());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleReference"


    // $ANTLR start "entryRuleAggregatedReference"
    // InternalDOF.g:697:1: entryRuleAggregatedReference returns [EObject current=null] : iv_ruleAggregatedReference= ruleAggregatedReference EOF ;
    public final EObject entryRuleAggregatedReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAggregatedReference = null;


        try {
            // InternalDOF.g:697:60: (iv_ruleAggregatedReference= ruleAggregatedReference EOF )
            // InternalDOF.g:698:2: iv_ruleAggregatedReference= ruleAggregatedReference EOF
            {
             newCompositeNode(grammarAccess.getAggregatedReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAggregatedReference=ruleAggregatedReference();

            state._fsp--;

             current =iv_ruleAggregatedReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAggregatedReference"


    // $ANTLR start "ruleAggregatedReference"
    // InternalDOF.g:704:1: ruleAggregatedReference returns [EObject current=null] : (otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )? ) ;
    public final EObject ruleAggregatedReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_function_3_0 = null;

        EObject lv_aggValue_5_0 = null;

        EObject lv_pivotingId_8_0 = null;

        EObject lv_instancesFilter_9_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:710:2: ( (otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )? ) )
            // InternalDOF.g:711:2: (otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )? )
            {
            // InternalDOF.g:711:2: (otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )? )
            // InternalDOF.g:712:3: otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )?
            {
            otherlv_0=(Token)match(input,20,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getAggregatedReferenceAccess().getCalculateKeyword_0());
            		
            // InternalDOF.g:716:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDOF.g:717:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDOF.g:717:4: (lv_name_1_0= RULE_ID )
            // InternalDOF.g:718:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_22); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAggregatedReferenceAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAggregatedReferenceRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_23); 

            			newLeafNode(otherlv_2, grammarAccess.getAggregatedReferenceAccess().getAsKeyword_2());
            		
            // InternalDOF.g:738:3: ( (lv_function_3_0= ruleAggFunction ) )
            // InternalDOF.g:739:4: (lv_function_3_0= ruleAggFunction )
            {
            // InternalDOF.g:739:4: (lv_function_3_0= ruleAggFunction )
            // InternalDOF.g:740:5: lv_function_3_0= ruleAggFunction
            {

            					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getFunctionAggFunctionParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_24);
            lv_function_3_0=ruleAggFunction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
            					}
            					set(
            						current,
            						"function",
            						lv_function_3_0,
            						"es.unican.DOF.AggFunction");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,22,FOLLOW_4); 

            			newLeafNode(otherlv_4, grammarAccess.getAggregatedReferenceAccess().getLeftParenthesisKeyword_4());
            		
            // InternalDOF.g:761:3: ( (lv_aggValue_5_0= rulePath ) )
            // InternalDOF.g:762:4: (lv_aggValue_5_0= rulePath )
            {
            // InternalDOF.g:762:4: (lv_aggValue_5_0= rulePath )
            // InternalDOF.g:763:5: lv_aggValue_5_0= rulePath
            {

            					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getAggValuePathParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_25);
            lv_aggValue_5_0=rulePath();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
            					}
            					set(
            						current,
            						"aggValue",
            						lv_aggValue_5_0,
            						"es.unican.DOF.Path");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_26); 

            			newLeafNode(otherlv_6, grammarAccess.getAggregatedReferenceAccess().getRightParenthesisKeyword_6());
            		
            // InternalDOF.g:784:3: (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==17) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalDOF.g:785:4: otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) )
                    {
                    otherlv_7=(Token)match(input,17,FOLLOW_4); 

                    				newLeafNode(otherlv_7, grammarAccess.getAggregatedReferenceAccess().getByKeyword_7_0());
                    			
                    // InternalDOF.g:789:4: ( (lv_pivotingId_8_0= rulePath ) )
                    // InternalDOF.g:790:5: (lv_pivotingId_8_0= rulePath )
                    {
                    // InternalDOF.g:790:5: (lv_pivotingId_8_0= rulePath )
                    // InternalDOF.g:791:6: lv_pivotingId_8_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getPivotingIdPathParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_27);
                    lv_pivotingId_8_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
                    						}
                    						set(
                    							current,
                    							"pivotingId",
                    							lv_pivotingId_8_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDOF.g:809:3: ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==33) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalDOF.g:810:4: (lv_instancesFilter_9_0= ruleInstancesFilter )
                    {
                    // InternalDOF.g:810:4: (lv_instancesFilter_9_0= ruleInstancesFilter )
                    // InternalDOF.g:811:5: lv_instancesFilter_9_0= ruleInstancesFilter
                    {

                    					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_8_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_instancesFilter_9_0=ruleInstancesFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
                    					}
                    					set(
                    						current,
                    						"instancesFilter",
                    						lv_instancesFilter_9_0,
                    						"es.unican.DOF.InstancesFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAggregatedReference"


    // $ANTLR start "entryRuleAggFunction"
    // InternalDOF.g:832:1: entryRuleAggFunction returns [String current=null] : iv_ruleAggFunction= ruleAggFunction EOF ;
    public final String entryRuleAggFunction() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAggFunction = null;


        try {
            // InternalDOF.g:832:51: (iv_ruleAggFunction= ruleAggFunction EOF )
            // InternalDOF.g:833:2: iv_ruleAggFunction= ruleAggFunction EOF
            {
             newCompositeNode(grammarAccess.getAggFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAggFunction=ruleAggFunction();

            state._fsp--;

             current =iv_ruleAggFunction.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAggFunction"


    // $ANTLR start "ruleAggFunction"
    // InternalDOF.g:839:1: ruleAggFunction returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' ) ;
    public final AntlrDatatypeRuleToken ruleAggFunction() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalDOF.g:845:2: ( (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' ) )
            // InternalDOF.g:846:2: (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' )
            {
            // InternalDOF.g:846:2: (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' )
            int alt17=5;
            switch ( input.LA(1) ) {
            case 24:
                {
                alt17=1;
                }
                break;
            case 25:
                {
                alt17=2;
                }
                break;
            case 26:
                {
                alt17=3;
                }
                break;
            case 27:
                {
                alt17=4;
                }
                break;
            case 28:
                {
                alt17=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // InternalDOF.g:847:3: kw= 'count'
                    {
                    kw=(Token)match(input,24,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getCountKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:853:3: kw= 'sum'
                    {
                    kw=(Token)match(input,25,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getSumKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalDOF.g:859:3: kw= 'avg'
                    {
                    kw=(Token)match(input,26,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getAvgKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalDOF.g:865:3: kw= 'max'
                    {
                    kw=(Token)match(input,27,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getMaxKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalDOF.g:871:3: kw= 'min'
                    {
                    kw=(Token)match(input,28,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getMinKeyword_4());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAggFunction"


    // $ANTLR start "entryRuleTypeFilter"
    // InternalDOF.g:880:1: entryRuleTypeFilter returns [EObject current=null] : iv_ruleTypeFilter= ruleTypeFilter EOF ;
    public final EObject entryRuleTypeFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeFilter = null;


        try {
            // InternalDOF.g:880:51: (iv_ruleTypeFilter= ruleTypeFilter EOF )
            // InternalDOF.g:881:2: iv_ruleTypeFilter= ruleTypeFilter EOF
            {
             newCompositeNode(grammarAccess.getTypeFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeFilter=ruleTypeFilter();

            state._fsp--;

             current =iv_ruleTypeFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeFilter"


    // $ANTLR start "ruleTypeFilter"
    // InternalDOF.g:887:1: ruleTypeFilter returns [EObject current=null] : (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection ) ;
    public final EObject ruleTypeFilter() throws RecognitionException {
        EObject current = null;

        EObject this_TypeCompletion_0 = null;

        EObject this_TypeSelection_1 = null;



        	enterRule();

        try {
            // InternalDOF.g:893:2: ( (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection ) )
            // InternalDOF.g:894:2: (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection )
            {
            // InternalDOF.g:894:2: (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==21) ) {
                alt18=1;
            }
            else if ( (LA18_0==29) ) {
                alt18=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalDOF.g:895:3: this_TypeCompletion_0= ruleTypeCompletion
                    {

                    			newCompositeNode(grammarAccess.getTypeFilterAccess().getTypeCompletionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_TypeCompletion_0=ruleTypeCompletion();

                    state._fsp--;


                    			current = this_TypeCompletion_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:904:3: this_TypeSelection_1= ruleTypeSelection
                    {

                    			newCompositeNode(grammarAccess.getTypeFilterAccess().getTypeSelectionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_TypeSelection_1=ruleTypeSelection();

                    state._fsp--;


                    			current = this_TypeSelection_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeFilter"


    // $ANTLR start "entryRuleTypeCompletion"
    // InternalDOF.g:916:1: entryRuleTypeCompletion returns [EObject current=null] : iv_ruleTypeCompletion= ruleTypeCompletion EOF ;
    public final EObject entryRuleTypeCompletion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeCompletion = null;


        try {
            // InternalDOF.g:916:55: (iv_ruleTypeCompletion= ruleTypeCompletion EOF )
            // InternalDOF.g:917:2: iv_ruleTypeCompletion= ruleTypeCompletion EOF
            {
             newCompositeNode(grammarAccess.getTypeCompletionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeCompletion=ruleTypeCompletion();

            state._fsp--;

             current =iv_ruleTypeCompletion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeCompletion"


    // $ANTLR start "ruleTypeCompletion"
    // InternalDOF.g:923:1: ruleTypeCompletion returns [EObject current=null] : (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ ;
    public final EObject ruleTypeCompletion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeCustomizations_1_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:929:2: ( (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ )
            // InternalDOF.g:930:2: (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            {
            // InternalDOF.g:930:2: (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            int cnt19=0;
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==21) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalDOF.g:931:3: otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    {
            	    otherlv_0=(Token)match(input,21,FOLLOW_4); 

            	    			newLeafNode(otherlv_0, grammarAccess.getTypeCompletionAccess().getAsKeyword_0());
            	    		
            	    // InternalDOF.g:935:3: ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    // InternalDOF.g:936:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    {
            	    // InternalDOF.g:936:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    // InternalDOF.g:937:5: lv_typeCustomizations_1_0= ruleTypeCustomization
            	    {

            	    					newCompositeNode(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_28);
            	    lv_typeCustomizations_1_0=ruleTypeCustomization();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTypeCompletionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"typeCustomizations",
            	    						lv_typeCustomizations_1_0,
            	    						"es.unican.DOF.TypeCustomization");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt19 >= 1 ) break loop19;
                        EarlyExitException eee =
                            new EarlyExitException(19, input);
                        throw eee;
                }
                cnt19++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeCompletion"


    // $ANTLR start "entryRuleTypeSelection"
    // InternalDOF.g:958:1: entryRuleTypeSelection returns [EObject current=null] : iv_ruleTypeSelection= ruleTypeSelection EOF ;
    public final EObject entryRuleTypeSelection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeSelection = null;


        try {
            // InternalDOF.g:958:54: (iv_ruleTypeSelection= ruleTypeSelection EOF )
            // InternalDOF.g:959:2: iv_ruleTypeSelection= ruleTypeSelection EOF
            {
             newCompositeNode(grammarAccess.getTypeSelectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeSelection=ruleTypeSelection();

            state._fsp--;

             current =iv_ruleTypeSelection; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeSelection"


    // $ANTLR start "ruleTypeSelection"
    // InternalDOF.g:965:1: ruleTypeSelection returns [EObject current=null] : (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ ;
    public final EObject ruleTypeSelection() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeCustomizations_1_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:971:2: ( (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ )
            // InternalDOF.g:972:2: (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            {
            // InternalDOF.g:972:2: (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            int cnt20=0;
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==29) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalDOF.g:973:3: otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    {
            	    otherlv_0=(Token)match(input,29,FOLLOW_4); 

            	    			newLeafNode(otherlv_0, grammarAccess.getTypeSelectionAccess().getOnly_asKeyword_0());
            	    		
            	    // InternalDOF.g:977:3: ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    // InternalDOF.g:978:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    {
            	    // InternalDOF.g:978:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    // InternalDOF.g:979:5: lv_typeCustomizations_1_0= ruleTypeCustomization
            	    {

            	    					newCompositeNode(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_29);
            	    lv_typeCustomizations_1_0=ruleTypeCustomization();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTypeSelectionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"typeCustomizations",
            	    						lv_typeCustomizations_1_0,
            	    						"es.unican.DOF.TypeCustomization");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt20 >= 1 ) break loop20;
                        EarlyExitException eee =
                            new EarlyExitException(20, input);
                        throw eee;
                }
                cnt20++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeSelection"


    // $ANTLR start "entryRuleTypeCustomization"
    // InternalDOF.g:1000:1: entryRuleTypeCustomization returns [EObject current=null] : iv_ruleTypeCustomization= ruleTypeCustomization EOF ;
    public final EObject entryRuleTypeCustomization() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeCustomization = null;


        try {
            // InternalDOF.g:1000:58: (iv_ruleTypeCustomization= ruleTypeCustomization EOF )
            // InternalDOF.g:1001:2: iv_ruleTypeCustomization= ruleTypeCustomization EOF
            {
             newCompositeNode(grammarAccess.getTypeCustomizationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeCustomization=ruleTypeCustomization();

            state._fsp--;

             current =iv_ruleTypeCustomization; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeCustomization"


    // $ANTLR start "ruleTypeCustomization"
    // InternalDOF.g:1007:1: ruleTypeCustomization returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? ) ;
    public final EObject ruleTypeCustomization() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_attributeFilter_1_0 = null;

        EObject lv_includedReferences_3_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1013:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? ) )
            // InternalDOF.g:1014:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? )
            {
            // InternalDOF.g:1014:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? )
            // InternalDOF.g:1015:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )?
            {
            // InternalDOF.g:1015:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalDOF.g:1016:4: (lv_name_0_0= RULE_ID )
            {
            // InternalDOF.g:1016:4: (lv_name_0_0= RULE_ID )
            // InternalDOF.g:1017:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_30); 

            					newLeafNode(lv_name_0_0, grammarAccess.getTypeCustomizationAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTypeCustomizationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:1033:3: ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==30) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalDOF.g:1034:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    {
                    // InternalDOF.g:1034:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    // InternalDOF.g:1035:5: lv_attributeFilter_1_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_18);
                    lv_attributeFilter_1_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getTypeCustomizationRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_1_0,
                    						"es.unican.DOF.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:1052:3: (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==18) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalDOF.g:1053:4: otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}'
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_31); 

                    				newLeafNode(otherlv_2, grammarAccess.getTypeCustomizationAccess().getLeftCurlyBracketKeyword_2_0());
                    			
                    // InternalDOF.g:1057:4: ( (lv_includedReferences_3_0= ruleIncludedReference ) )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==RULE_ID||LA22_0==20) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // InternalDOF.g:1058:5: (lv_includedReferences_3_0= ruleIncludedReference )
                    	    {
                    	    // InternalDOF.g:1058:5: (lv_includedReferences_3_0= ruleIncludedReference )
                    	    // InternalDOF.g:1059:6: lv_includedReferences_3_0= ruleIncludedReference
                    	    {

                    	    						newCompositeNode(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesIncludedReferenceParserRuleCall_2_1_0());
                    	    					
                    	    pushFollow(FOLLOW_31);
                    	    lv_includedReferences_3_0=ruleIncludedReference();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getTypeCustomizationRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"includedReferences",
                    	    							lv_includedReferences_3_0,
                    	    							"es.unican.DOF.IncludedReference");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,19,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getTypeCustomizationAccess().getRightCurlyBracketKeyword_2_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeCustomization"


    // $ANTLR start "entryRuleAttributeFilter"
    // InternalDOF.g:1085:1: entryRuleAttributeFilter returns [EObject current=null] : iv_ruleAttributeFilter= ruleAttributeFilter EOF ;
    public final EObject entryRuleAttributeFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeFilter = null;


        try {
            // InternalDOF.g:1085:56: (iv_ruleAttributeFilter= ruleAttributeFilter EOF )
            // InternalDOF.g:1086:2: iv_ruleAttributeFilter= ruleAttributeFilter EOF
            {
             newCompositeNode(grammarAccess.getAttributeFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeFilter=ruleAttributeFilter();

            state._fsp--;

             current =iv_ruleAttributeFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeFilter"


    // $ANTLR start "ruleAttributeFilter"
    // InternalDOF.g:1092:1: ruleAttributeFilter returns [EObject current=null] : (otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']' ) ;
    public final EObject ruleAttributeFilter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_attributes_1_0=null;
        Token otherlv_2=null;
        Token lv_attributes_3_0=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalDOF.g:1098:2: ( (otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']' ) )
            // InternalDOF.g:1099:2: (otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']' )
            {
            // InternalDOF.g:1099:2: (otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']' )
            // InternalDOF.g:1100:3: otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,30,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getAttributeFilterAccess().getLeftSquareBracketKeyword_0());
            		
            // InternalDOF.g:1104:3: ( (lv_attributes_1_0= RULE_ID ) )
            // InternalDOF.g:1105:4: (lv_attributes_1_0= RULE_ID )
            {
            // InternalDOF.g:1105:4: (lv_attributes_1_0= RULE_ID )
            // InternalDOF.g:1106:5: lv_attributes_1_0= RULE_ID
            {
            lv_attributes_1_0=(Token)match(input,RULE_ID,FOLLOW_32); 

            					newLeafNode(lv_attributes_1_0, grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeFilterRule());
            					}
            					addWithLastConsumed(
            						current,
            						"attributes",
            						lv_attributes_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:1122:3: (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==31) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalDOF.g:1123:4: otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) )
            	    {
            	    otherlv_2=(Token)match(input,31,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAttributeFilterAccess().getCommaKeyword_2_0());
            	    			
            	    // InternalDOF.g:1127:4: ( (lv_attributes_3_0= RULE_ID ) )
            	    // InternalDOF.g:1128:5: (lv_attributes_3_0= RULE_ID )
            	    {
            	    // InternalDOF.g:1128:5: (lv_attributes_3_0= RULE_ID )
            	    // InternalDOF.g:1129:6: lv_attributes_3_0= RULE_ID
            	    {
            	    lv_attributes_3_0=(Token)match(input,RULE_ID,FOLLOW_32); 

            	    						newLeafNode(lv_attributes_3_0, grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getAttributeFilterRule());
            	    						}
            	    						addWithLastConsumed(
            	    							current,
            	    							"attributes",
            	    							lv_attributes_3_0,
            	    							"org.eclipse.xtext.common.Terminals.ID");
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            otherlv_4=(Token)match(input,32,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getAttributeFilterAccess().getRightSquareBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeFilter"


    // $ANTLR start "entryRuleInstancesFilter"
    // InternalDOF.g:1154:1: entryRuleInstancesFilter returns [EObject current=null] : iv_ruleInstancesFilter= ruleInstancesFilter EOF ;
    public final EObject entryRuleInstancesFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstancesFilter = null;


        try {
            // InternalDOF.g:1154:56: (iv_ruleInstancesFilter= ruleInstancesFilter EOF )
            // InternalDOF.g:1155:2: iv_ruleInstancesFilter= ruleInstancesFilter EOF
            {
             newCompositeNode(grammarAccess.getInstancesFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInstancesFilter=ruleInstancesFilter();

            state._fsp--;

             current =iv_ruleInstancesFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstancesFilter"


    // $ANTLR start "ruleInstancesFilter"
    // InternalDOF.g:1161:1: ruleInstancesFilter returns [EObject current=null] : (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction ) ;
    public final EObject ruleInstancesFilter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_AndConjunction_1 = null;



        	enterRule();

        try {
            // InternalDOF.g:1167:2: ( (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction ) )
            // InternalDOF.g:1168:2: (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction )
            {
            // InternalDOF.g:1168:2: (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction )
            // InternalDOF.g:1169:3: otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction
            {
            otherlv_0=(Token)match(input,33,FOLLOW_33); 

            			newLeafNode(otherlv_0, grammarAccess.getInstancesFilterAccess().getWhereKeyword_0());
            		

            			newCompositeNode(grammarAccess.getInstancesFilterAccess().getAndConjunctionParserRuleCall_1());
            		
            pushFollow(FOLLOW_2);
            this_AndConjunction_1=ruleAndConjunction();

            state._fsp--;


            			current = this_AndConjunction_1;
            			afterParserOrEnumRuleCall();
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstancesFilter"


    // $ANTLR start "entryRuleAndConjunction"
    // InternalDOF.g:1185:1: entryRuleAndConjunction returns [EObject current=null] : iv_ruleAndConjunction= ruleAndConjunction EOF ;
    public final EObject entryRuleAndConjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndConjunction = null;


        try {
            // InternalDOF.g:1185:55: (iv_ruleAndConjunction= ruleAndConjunction EOF )
            // InternalDOF.g:1186:2: iv_ruleAndConjunction= ruleAndConjunction EOF
            {
             newCompositeNode(grammarAccess.getAndConjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAndConjunction=ruleAndConjunction();

            state._fsp--;

             current =iv_ruleAndConjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndConjunction"


    // $ANTLR start "ruleAndConjunction"
    // InternalDOF.g:1192:1: ruleAndConjunction returns [EObject current=null] : (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* ) ;
    public final EObject ruleAndConjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_OrConjunction_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1198:2: ( (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* ) )
            // InternalDOF.g:1199:2: (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* )
            {
            // InternalDOF.g:1199:2: (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* )
            // InternalDOF.g:1200:3: this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndConjunctionAccess().getOrConjunctionParserRuleCall_0());
            		
            pushFollow(FOLLOW_34);
            this_OrConjunction_0=ruleOrConjunction();

            state._fsp--;


            			current = this_OrConjunction_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalDOF.g:1208:3: ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==34) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalDOF.g:1209:4: () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) )
            	    {
            	    // InternalDOF.g:1209:4: ()
            	    // InternalDOF.g:1210:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,34,FOLLOW_33); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAndConjunctionAccess().getAndKeyword_1_1());
            	    			
            	    // InternalDOF.g:1220:4: ( (lv_right_3_0= ruleOrConjunction ) )
            	    // InternalDOF.g:1221:5: (lv_right_3_0= ruleOrConjunction )
            	    {
            	    // InternalDOF.g:1221:5: (lv_right_3_0= ruleOrConjunction )
            	    // InternalDOF.g:1222:6: lv_right_3_0= ruleOrConjunction
            	    {

            	    						newCompositeNode(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_34);
            	    lv_right_3_0=ruleOrConjunction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndConjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"es.unican.DOF.OrConjunction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndConjunction"


    // $ANTLR start "entryRuleOrConjunction"
    // InternalDOF.g:1244:1: entryRuleOrConjunction returns [EObject current=null] : iv_ruleOrConjunction= ruleOrConjunction EOF ;
    public final EObject entryRuleOrConjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrConjunction = null;


        try {
            // InternalDOF.g:1244:54: (iv_ruleOrConjunction= ruleOrConjunction EOF )
            // InternalDOF.g:1245:2: iv_ruleOrConjunction= ruleOrConjunction EOF
            {
             newCompositeNode(grammarAccess.getOrConjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOrConjunction=ruleOrConjunction();

            state._fsp--;

             current =iv_ruleOrConjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrConjunction"


    // $ANTLR start "ruleOrConjunction"
    // InternalDOF.g:1251:1: ruleOrConjunction returns [EObject current=null] : (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* ) ;
    public final EObject ruleOrConjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Primary_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1257:2: ( (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* ) )
            // InternalDOF.g:1258:2: (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* )
            {
            // InternalDOF.g:1258:2: (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* )
            // InternalDOF.g:1259:3: this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrConjunctionAccess().getPrimaryParserRuleCall_0());
            		
            pushFollow(FOLLOW_35);
            this_Primary_0=rulePrimary();

            state._fsp--;


            			current = this_Primary_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalDOF.g:1267:3: ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==35) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalDOF.g:1268:4: () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) )
            	    {
            	    // InternalDOF.g:1268:4: ()
            	    // InternalDOF.g:1269:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,35,FOLLOW_33); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrConjunctionAccess().getOrKeyword_1_1());
            	    			
            	    // InternalDOF.g:1279:4: ( (lv_right_3_0= rulePrimary ) )
            	    // InternalDOF.g:1280:5: (lv_right_3_0= rulePrimary )
            	    {
            	    // InternalDOF.g:1280:5: (lv_right_3_0= rulePrimary )
            	    // InternalDOF.g:1281:6: lv_right_3_0= rulePrimary
            	    {

            	    						newCompositeNode(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_35);
            	    lv_right_3_0=rulePrimary();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrConjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"es.unican.DOF.Primary");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrConjunction"


    // $ANTLR start "entryRulePrimary"
    // InternalDOF.g:1303:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalDOF.g:1303:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalDOF.g:1304:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalDOF.g:1310:1: rulePrimary returns [EObject current=null] : (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Comparison_0 = null;

        EObject this_AndConjunction_2 = null;



        	enterRule();

        try {
            // InternalDOF.g:1316:2: ( (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) ) )
            // InternalDOF.g:1317:2: (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) )
            {
            // InternalDOF.g:1317:2: (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==RULE_ID) ) {
                alt27=1;
            }
            else if ( (LA27_0==22) ) {
                alt27=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }
            switch (alt27) {
                case 1 :
                    // InternalDOF.g:1318:3: this_Comparison_0= ruleComparison
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getComparisonParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Comparison_0=ruleComparison();

                    state._fsp--;


                    			current = this_Comparison_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:1327:3: (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' )
                    {
                    // InternalDOF.g:1327:3: (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' )
                    // InternalDOF.g:1328:4: otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,22,FOLLOW_33); 

                    				newLeafNode(otherlv_1, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getAndConjunctionParserRuleCall_1_1());
                    			
                    pushFollow(FOLLOW_25);
                    this_AndConjunction_2=ruleAndConjunction();

                    state._fsp--;


                    				current = this_AndConjunction_2;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_3=(Token)match(input,23,FOLLOW_2); 

                    				newLeafNode(otherlv_3, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleComparison"
    // InternalDOF.g:1349:1: entryRuleComparison returns [EObject current=null] : iv_ruleComparison= ruleComparison EOF ;
    public final EObject entryRuleComparison() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparison = null;


        try {
            // InternalDOF.g:1349:51: (iv_ruleComparison= ruleComparison EOF )
            // InternalDOF.g:1350:2: iv_ruleComparison= ruleComparison EOF
            {
             newCompositeNode(grammarAccess.getComparisonRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComparison=ruleComparison();

            state._fsp--;

             current =iv_ruleComparison; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // InternalDOF.g:1356:1: ruleComparison returns [EObject current=null] : ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) ) ;
    public final EObject ruleComparison() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token otherlv_6=null;
        Token lv_value_7_0=null;
        Token otherlv_10=null;
        Token lv_value_11_0=null;
        Token otherlv_14=null;
        Token lv_value_15_0=null;
        Token otherlv_18=null;
        Token lv_value_19_0=null;
        Token otherlv_22=null;
        Token lv_value_23_0=null;
        EObject lv_path_1_0 = null;

        EObject lv_path_5_0 = null;

        EObject lv_path_9_0 = null;

        EObject lv_path_13_0 = null;

        EObject lv_path_17_0 = null;

        EObject lv_path_21_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1362:2: ( ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) ) )
            // InternalDOF.g:1363:2: ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) )
            {
            // InternalDOF.g:1363:2: ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) )
            int alt28=6;
            alt28 = dfa28.predict(input);
            switch (alt28) {
                case 1 :
                    // InternalDOF.g:1364:3: ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1364:3: ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
                    // InternalDOF.g:1365:4: () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1365:4: ()
                    // InternalDOF.g:1366:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getEqualityAction_0_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1372:4: ( (lv_path_1_0= rulePath ) )
                    // InternalDOF.g:1373:5: (lv_path_1_0= rulePath )
                    {
                    // InternalDOF.g:1373:5: (lv_path_1_0= rulePath )
                    // InternalDOF.g:1374:6: lv_path_1_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_36);
                    lv_path_1_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_1_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_2=(Token)match(input,36,FOLLOW_37); 

                    				newLeafNode(otherlv_2, grammarAccess.getComparisonAccess().getEqualsSignKeyword_0_2());
                    			
                    // InternalDOF.g:1395:4: ( (lv_value_3_0= RULE_STRING ) )
                    // InternalDOF.g:1396:5: (lv_value_3_0= RULE_STRING )
                    {
                    // InternalDOF.g:1396:5: (lv_value_3_0= RULE_STRING )
                    // InternalDOF.g:1397:6: lv_value_3_0= RULE_STRING
                    {
                    lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:1415:3: ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1415:3: ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) )
                    // InternalDOF.g:1416:4: () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1416:4: ()
                    // InternalDOF.g:1417:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getInequalityAction_1_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1423:4: ( (lv_path_5_0= rulePath ) )
                    // InternalDOF.g:1424:5: (lv_path_5_0= rulePath )
                    {
                    // InternalDOF.g:1424:5: (lv_path_5_0= rulePath )
                    // InternalDOF.g:1425:6: lv_path_5_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_38);
                    lv_path_5_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_5_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_6=(Token)match(input,37,FOLLOW_37); 

                    				newLeafNode(otherlv_6, grammarAccess.getComparisonAccess().getExclamationMarkEqualsSignKeyword_1_2());
                    			
                    // InternalDOF.g:1446:4: ( (lv_value_7_0= RULE_STRING ) )
                    // InternalDOF.g:1447:5: (lv_value_7_0= RULE_STRING )
                    {
                    // InternalDOF.g:1447:5: (lv_value_7_0= RULE_STRING )
                    // InternalDOF.g:1448:6: lv_value_7_0= RULE_STRING
                    {
                    lv_value_7_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_7_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalDOF.g:1466:3: ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1466:3: ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) )
                    // InternalDOF.g:1467:4: () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1467:4: ()
                    // InternalDOF.g:1468:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getMoreThanAction_2_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1474:4: ( (lv_path_9_0= rulePath ) )
                    // InternalDOF.g:1475:5: (lv_path_9_0= rulePath )
                    {
                    // InternalDOF.g:1475:5: (lv_path_9_0= rulePath )
                    // InternalDOF.g:1476:6: lv_path_9_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_39);
                    lv_path_9_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_9_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_10=(Token)match(input,38,FOLLOW_37); 

                    				newLeafNode(otherlv_10, grammarAccess.getComparisonAccess().getGreaterThanSignKeyword_2_2());
                    			
                    // InternalDOF.g:1497:4: ( (lv_value_11_0= RULE_STRING ) )
                    // InternalDOF.g:1498:5: (lv_value_11_0= RULE_STRING )
                    {
                    // InternalDOF.g:1498:5: (lv_value_11_0= RULE_STRING )
                    // InternalDOF.g:1499:6: lv_value_11_0= RULE_STRING
                    {
                    lv_value_11_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_11_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_11_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalDOF.g:1517:3: ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1517:3: ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) )
                    // InternalDOF.g:1518:4: () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1518:4: ()
                    // InternalDOF.g:1519:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getMoreThanOrEqualAction_3_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1525:4: ( (lv_path_13_0= rulePath ) )
                    // InternalDOF.g:1526:5: (lv_path_13_0= rulePath )
                    {
                    // InternalDOF.g:1526:5: (lv_path_13_0= rulePath )
                    // InternalDOF.g:1527:6: lv_path_13_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_path_13_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_13_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_14=(Token)match(input,39,FOLLOW_37); 

                    				newLeafNode(otherlv_14, grammarAccess.getComparisonAccess().getGreaterThanSignEqualsSignKeyword_3_2());
                    			
                    // InternalDOF.g:1548:4: ( (lv_value_15_0= RULE_STRING ) )
                    // InternalDOF.g:1549:5: (lv_value_15_0= RULE_STRING )
                    {
                    // InternalDOF.g:1549:5: (lv_value_15_0= RULE_STRING )
                    // InternalDOF.g:1550:6: lv_value_15_0= RULE_STRING
                    {
                    lv_value_15_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_15_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_15_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalDOF.g:1568:3: ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1568:3: ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) )
                    // InternalDOF.g:1569:4: () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1569:4: ()
                    // InternalDOF.g:1570:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getLessThanAction_4_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1576:4: ( (lv_path_17_0= rulePath ) )
                    // InternalDOF.g:1577:5: (lv_path_17_0= rulePath )
                    {
                    // InternalDOF.g:1577:5: (lv_path_17_0= rulePath )
                    // InternalDOF.g:1578:6: lv_path_17_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_41);
                    lv_path_17_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_17_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_18=(Token)match(input,40,FOLLOW_37); 

                    				newLeafNode(otherlv_18, grammarAccess.getComparisonAccess().getLessThanSignKeyword_4_2());
                    			
                    // InternalDOF.g:1599:4: ( (lv_value_19_0= RULE_STRING ) )
                    // InternalDOF.g:1600:5: (lv_value_19_0= RULE_STRING )
                    {
                    // InternalDOF.g:1600:5: (lv_value_19_0= RULE_STRING )
                    // InternalDOF.g:1601:6: lv_value_19_0= RULE_STRING
                    {
                    lv_value_19_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_19_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_19_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalDOF.g:1619:3: ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1619:3: ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) )
                    // InternalDOF.g:1620:4: () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1620:4: ()
                    // InternalDOF.g:1621:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getLessThanOrEqualAction_5_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1627:4: ( (lv_path_21_0= rulePath ) )
                    // InternalDOF.g:1628:5: (lv_path_21_0= rulePath )
                    {
                    // InternalDOF.g:1628:5: (lv_path_21_0= rulePath )
                    // InternalDOF.g:1629:6: lv_path_21_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_42);
                    lv_path_21_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_21_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_22=(Token)match(input,41,FOLLOW_37); 

                    				newLeafNode(otherlv_22, grammarAccess.getComparisonAccess().getLessThanSignEqualsSignKeyword_5_2());
                    			
                    // InternalDOF.g:1650:4: ( (lv_value_23_0= RULE_STRING ) )
                    // InternalDOF.g:1651:5: (lv_value_23_0= RULE_STRING )
                    {
                    // InternalDOF.g:1651:5: (lv_value_23_0= RULE_STRING )
                    // InternalDOF.g:1652:6: lv_value_23_0= RULE_STRING
                    {
                    lv_value_23_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_23_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_23_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRuleCause"
    // InternalDOF.g:1673:1: entryRuleCause returns [EObject current=null] : iv_ruleCause= ruleCause EOF ;
    public final EObject entryRuleCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCause = null;


        try {
            // InternalDOF.g:1673:46: (iv_ruleCause= ruleCause EOF )
            // InternalDOF.g:1674:2: iv_ruleCause= ruleCause EOF
            {
             newCompositeNode(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCause=ruleCause();

            state._fsp--;

             current =iv_ruleCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalDOF.g:1680:1: ruleCause returns [EObject current=null] : (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause ) ;
    public final EObject ruleCause() throws RecognitionException {
        EObject current = null;

        EObject this_CompoundCause_0 = null;

        EObject this_DataLinkedCause_1 = null;

        EObject this_NotMappedCause_2 = null;



        	enterRule();

        try {
            // InternalDOF.g:1686:2: ( (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause ) )
            // InternalDOF.g:1687:2: (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause )
            {
            // InternalDOF.g:1687:2: (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause )
            int alt29=3;
            alt29 = dfa29.predict(input);
            switch (alt29) {
                case 1 :
                    // InternalDOF.g:1688:3: this_CompoundCause_0= ruleCompoundCause
                    {

                    			newCompositeNode(grammarAccess.getCauseAccess().getCompoundCauseParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_CompoundCause_0=ruleCompoundCause();

                    state._fsp--;


                    			current = this_CompoundCause_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:1697:3: this_DataLinkedCause_1= ruleDataLinkedCause
                    {

                    			newCompositeNode(grammarAccess.getCauseAccess().getDataLinkedCauseParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_DataLinkedCause_1=ruleDataLinkedCause();

                    state._fsp--;


                    			current = this_DataLinkedCause_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalDOF.g:1706:3: this_NotMappedCause_2= ruleNotMappedCause
                    {

                    			newCompositeNode(grammarAccess.getCauseAccess().getNotMappedCauseParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_NotMappedCause_2=ruleNotMappedCause();

                    state._fsp--;


                    			current = this_NotMappedCause_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleCompoundCause"
    // InternalDOF.g:1718:1: entryRuleCompoundCause returns [EObject current=null] : iv_ruleCompoundCause= ruleCompoundCause EOF ;
    public final EObject entryRuleCompoundCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCompoundCause = null;


        try {
            // InternalDOF.g:1718:54: (iv_ruleCompoundCause= ruleCompoundCause EOF )
            // InternalDOF.g:1719:2: iv_ruleCompoundCause= ruleCompoundCause EOF
            {
             newCompositeNode(grammarAccess.getCompoundCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCompoundCause=ruleCompoundCause();

            state._fsp--;

             current =iv_ruleCompoundCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCompoundCause"


    // $ANTLR start "ruleCompoundCause"
    // InternalDOF.g:1725:1: ruleCompoundCause returns [EObject current=null] : ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}' ) ;
    public final EObject ruleCompoundCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_subCauses_7_0 = null;

        EObject lv_subCauses_8_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1731:2: ( ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}' ) )
            // InternalDOF.g:1732:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}' )
            {
            // InternalDOF.g:1732:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}' )
            // InternalDOF.g:1733:3: () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}'
            {
            // InternalDOF.g:1733:3: ()
            // InternalDOF.g:1734:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCompoundCauseAccess().getCompoundCauseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,42,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getCompoundCauseAccess().getCauseKeyword_1());
            		
            // InternalDOF.g:1744:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalDOF.g:1745:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalDOF.g:1745:4: (lv_name_2_0= ruleQualifiedName )
            // InternalDOF.g:1746:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getCompoundCauseAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_43);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCompoundCauseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.DOF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:1763:3: (otherlv_3= 'realizes' ( ( ruleEString ) ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==43) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalDOF.g:1764:4: otherlv_3= 'realizes' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,43,FOLLOW_37); 

                    				newLeafNode(otherlv_3, grammarAccess.getCompoundCauseAccess().getRealizesKeyword_3_0());
                    			
                    // InternalDOF.g:1768:4: ( ( ruleEString ) )
                    // InternalDOF.g:1769:5: ( ruleEString )
                    {
                    // InternalDOF.g:1769:5: ( ruleEString )
                    // InternalDOF.g:1770:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getCompoundCauseRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getCompoundCauseAccess().getRealizesCauseCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_44);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,44,FOLLOW_45); 

            			newLeafNode(otherlv_5, grammarAccess.getCompoundCauseAccess().getContainsKeyword_4());
            		
            otherlv_6=(Token)match(input,18,FOLLOW_9); 

            			newLeafNode(otherlv_6, grammarAccess.getCompoundCauseAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalDOF.g:1793:3: ( (lv_subCauses_7_0= ruleCause ) )
            // InternalDOF.g:1794:4: (lv_subCauses_7_0= ruleCause )
            {
            // InternalDOF.g:1794:4: (lv_subCauses_7_0= ruleCause )
            // InternalDOF.g:1795:5: lv_subCauses_7_0= ruleCause
            {

            					newCompositeNode(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_46);
            lv_subCauses_7_0=ruleCause();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCompoundCauseRule());
            					}
            					add(
            						current,
            						"subCauses",
            						lv_subCauses_7_0,
            						"es.unican.DOF.Cause");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:1812:3: ( (lv_subCauses_8_0= ruleCause ) )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==42) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalDOF.g:1813:4: (lv_subCauses_8_0= ruleCause )
            	    {
            	    // InternalDOF.g:1813:4: (lv_subCauses_8_0= ruleCause )
            	    // InternalDOF.g:1814:5: lv_subCauses_8_0= ruleCause
            	    {

            	    					newCompositeNode(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_46);
            	    lv_subCauses_8_0=ruleCause();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCompoundCauseRule());
            	    					}
            	    					add(
            	    						current,
            	    						"subCauses",
            	    						lv_subCauses_8_0,
            	    						"es.unican.DOF.Cause");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

            otherlv_9=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getCompoundCauseAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompoundCause"


    // $ANTLR start "entryRuleDataLinkedCause"
    // InternalDOF.g:1839:1: entryRuleDataLinkedCause returns [EObject current=null] : iv_ruleDataLinkedCause= ruleDataLinkedCause EOF ;
    public final EObject entryRuleDataLinkedCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataLinkedCause = null;


        try {
            // InternalDOF.g:1839:56: (iv_ruleDataLinkedCause= ruleDataLinkedCause EOF )
            // InternalDOF.g:1840:2: iv_ruleDataLinkedCause= ruleDataLinkedCause EOF
            {
             newCompositeNode(grammarAccess.getDataLinkedCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataLinkedCause=ruleDataLinkedCause();

            state._fsp--;

             current =iv_ruleDataLinkedCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataLinkedCause"


    // $ANTLR start "ruleDataLinkedCause"
    // InternalDOF.g:1846:1: ruleDataLinkedCause returns [EObject current=null] : ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? ) ;
    public final EObject ruleDataLinkedCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_attributeFilter_6_0 = null;

        EObject lv_instancesFilter_7_0 = null;

        EObject lv_includedReferences_8_0 = null;

        EObject lv_typeFilter_9_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1852:2: ( ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? ) )
            // InternalDOF.g:1853:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? )
            {
            // InternalDOF.g:1853:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? )
            // InternalDOF.g:1854:3: () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )?
            {
            // InternalDOF.g:1854:3: ()
            // InternalDOF.g:1855:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDataLinkedCauseAccess().getDataLinkedCauseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,42,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getDataLinkedCauseAccess().getCauseKeyword_1());
            		
            // InternalDOF.g:1865:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalDOF.g:1866:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalDOF.g:1866:4: (lv_name_2_0= ruleQualifiedName )
            // InternalDOF.g:1867:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_47);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.DOF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:1884:3: (otherlv_3= 'realizes' ( ( ruleEString ) ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==43) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalDOF.g:1885:4: otherlv_3= 'realizes' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,43,FOLLOW_37); 

                    				newLeafNode(otherlv_3, grammarAccess.getDataLinkedCauseAccess().getRealizesKeyword_3_0());
                    			
                    // InternalDOF.g:1889:4: ( ( ruleEString ) )
                    // InternalDOF.g:1890:5: ( ruleEString )
                    {
                    // InternalDOF.g:1890:5: ( ruleEString )
                    // InternalDOF.g:1891:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDataLinkedCauseRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,14,FOLLOW_48); 

            			newLeafNode(otherlv_5, grammarAccess.getDataLinkedCauseAccess().getIsKeyword_4());
            		
            // InternalDOF.g:1910:3: ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==30) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalDOF.g:1911:4: (lv_attributeFilter_6_0= ruleAttributeFilter )
                    {
                    // InternalDOF.g:1911:4: (lv_attributeFilter_6_0= ruleAttributeFilter )
                    // InternalDOF.g:1912:5: lv_attributeFilter_6_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAttributeFilterParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_49);
                    lv_attributeFilter_6_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_6_0,
                    						"es.unican.DOF.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:1929:3: ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==33) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalDOF.g:1930:4: (lv_instancesFilter_7_0= ruleInstancesFilter )
                    {
                    // InternalDOF.g:1930:4: (lv_instancesFilter_7_0= ruleInstancesFilter )
                    // InternalDOF.g:1931:5: lv_instancesFilter_7_0= ruleInstancesFilter
                    {

                    					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterInstancesFilterParserRuleCall_6_0());
                    				
                    pushFollow(FOLLOW_50);
                    lv_instancesFilter_7_0=ruleInstancesFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
                    					}
                    					set(
                    						current,
                    						"instancesFilter",
                    						lv_instancesFilter_7_0,
                    						"es.unican.DOF.InstancesFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:1948:3: ( (lv_includedReferences_8_0= ruleIncludedReference ) )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==RULE_ID||LA35_0==20) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalDOF.g:1949:4: (lv_includedReferences_8_0= ruleIncludedReference )
            	    {
            	    // InternalDOF.g:1949:4: (lv_includedReferences_8_0= ruleIncludedReference )
            	    // InternalDOF.g:1950:5: lv_includedReferences_8_0= ruleIncludedReference
            	    {

            	    					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesIncludedReferenceParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_50);
            	    lv_includedReferences_8_0=ruleIncludedReference();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
            	    					}
            	    					add(
            	    						current,
            	    						"includedReferences",
            	    						lv_includedReferences_8_0,
            	    						"es.unican.DOF.IncludedReference");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

            // InternalDOF.g:1967:3: ( (lv_typeFilter_9_0= ruleTypeFilter ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==21||LA36_0==29) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalDOF.g:1968:4: (lv_typeFilter_9_0= ruleTypeFilter )
                    {
                    // InternalDOF.g:1968:4: (lv_typeFilter_9_0= ruleTypeFilter )
                    // InternalDOF.g:1969:5: lv_typeFilter_9_0= ruleTypeFilter
                    {

                    					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getTypeFilterTypeFilterParserRuleCall_8_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_typeFilter_9_0=ruleTypeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
                    					}
                    					set(
                    						current,
                    						"typeFilter",
                    						lv_typeFilter_9_0,
                    						"es.unican.DOF.TypeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataLinkedCause"


    // $ANTLR start "entryRuleNotMappedCause"
    // InternalDOF.g:1990:1: entryRuleNotMappedCause returns [EObject current=null] : iv_ruleNotMappedCause= ruleNotMappedCause EOF ;
    public final EObject entryRuleNotMappedCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotMappedCause = null;


        try {
            // InternalDOF.g:1990:55: (iv_ruleNotMappedCause= ruleNotMappedCause EOF )
            // InternalDOF.g:1991:2: iv_ruleNotMappedCause= ruleNotMappedCause EOF
            {
             newCompositeNode(grammarAccess.getNotMappedCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNotMappedCause=ruleNotMappedCause();

            state._fsp--;

             current =iv_ruleNotMappedCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotMappedCause"


    // $ANTLR start "ruleNotMappedCause"
    // InternalDOF.g:1997:1: ruleNotMappedCause returns [EObject current=null] : ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped' ) ;
    public final EObject ruleNotMappedCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:2003:2: ( ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped' ) )
            // InternalDOF.g:2004:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped' )
            {
            // InternalDOF.g:2004:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped' )
            // InternalDOF.g:2005:3: () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped'
            {
            // InternalDOF.g:2005:3: ()
            // InternalDOF.g:2006:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getNotMappedCauseAccess().getNotMappedCauseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,42,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getNotMappedCauseAccess().getCauseKeyword_1());
            		
            // InternalDOF.g:2016:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalDOF.g:2017:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalDOF.g:2017:4: (lv_name_2_0= ruleQualifiedName )
            // InternalDOF.g:2018:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getNotMappedCauseAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_51);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNotMappedCauseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.DOF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:2035:3: (otherlv_3= 'realizes' ( ( ruleEString ) ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==43) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalDOF.g:2036:4: otherlv_3= 'realizes' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,43,FOLLOW_37); 

                    				newLeafNode(otherlv_3, grammarAccess.getNotMappedCauseAccess().getRealizesKeyword_3_0());
                    			
                    // InternalDOF.g:2040:4: ( ( ruleEString ) )
                    // InternalDOF.g:2041:5: ( ruleEString )
                    {
                    // InternalDOF.g:2041:5: ( ruleEString )
                    // InternalDOF.g:2042:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getNotMappedCauseRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getNotMappedCauseAccess().getRealizesCauseCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_52);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,45,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getNotMappedCauseAccess().getNotMappedKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotMappedCause"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalDOF.g:2065:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalDOF.g:2065:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalDOF.g:2066:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalDOF.g:2072:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:2078:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalDOF.g:2079:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalDOF.g:2079:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalDOF.g:2080:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {

            			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
            		
            pushFollow(FOLLOW_53);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            			current.merge(this_QualifiedName_0);
            		

            			afterParserOrEnumRuleCall();
            		
            // InternalDOF.g:2090:3: (kw= '.*' )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==46) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalDOF.g:2091:4: kw= '.*'
                    {
                    kw=(Token)match(input,46,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalDOF.g:2101:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalDOF.g:2101:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalDOF.g:2102:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalDOF.g:2108:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalDOF.g:2114:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalDOF.g:2115:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalDOF.g:2115:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalDOF.g:2116:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_54); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalDOF.g:2123:3: (kw= '.' this_ID_2= RULE_ID )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==47) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // InternalDOF.g:2124:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,47,FOLLOW_4); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_54); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRulePath"
    // InternalDOF.g:2141:1: entryRulePath returns [EObject current=null] : iv_rulePath= rulePath EOF ;
    public final EObject entryRulePath() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePath = null;


        try {
            // InternalDOF.g:2141:45: (iv_rulePath= rulePath EOF )
            // InternalDOF.g:2142:2: iv_rulePath= rulePath EOF
            {
             newCompositeNode(grammarAccess.getPathRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePath=rulePath();

            state._fsp--;

             current =iv_rulePath; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePath"


    // $ANTLR start "rulePath"
    // InternalDOF.g:2148:1: rulePath returns [EObject current=null] : ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* ) ;
    public final EObject rulePath() throws RecognitionException {
        EObject current = null;

        Token lv_jumps_0_0=null;
        Token otherlv_1=null;
        Token lv_jumps_2_0=null;


        	enterRule();

        try {
            // InternalDOF.g:2154:2: ( ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* ) )
            // InternalDOF.g:2155:2: ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* )
            {
            // InternalDOF.g:2155:2: ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* )
            // InternalDOF.g:2156:3: ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )*
            {
            // InternalDOF.g:2156:3: ( (lv_jumps_0_0= RULE_ID ) )
            // InternalDOF.g:2157:4: (lv_jumps_0_0= RULE_ID )
            {
            // InternalDOF.g:2157:4: (lv_jumps_0_0= RULE_ID )
            // InternalDOF.g:2158:5: lv_jumps_0_0= RULE_ID
            {
            lv_jumps_0_0=(Token)match(input,RULE_ID,FOLLOW_54); 

            					newLeafNode(lv_jumps_0_0, grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPathRule());
            					}
            					addWithLastConsumed(
            						current,
            						"jumps",
            						lv_jumps_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:2174:3: (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )*
            loop40:
            do {
                int alt40=2;
                int LA40_0 = input.LA(1);

                if ( (LA40_0==47) ) {
                    alt40=1;
                }


                switch (alt40) {
            	case 1 :
            	    // InternalDOF.g:2175:4: otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) )
            	    {
            	    otherlv_1=(Token)match(input,47,FOLLOW_4); 

            	    				newLeafNode(otherlv_1, grammarAccess.getPathAccess().getFullStopKeyword_1_0());
            	    			
            	    // InternalDOF.g:2179:4: ( (lv_jumps_2_0= RULE_ID ) )
            	    // InternalDOF.g:2180:5: (lv_jumps_2_0= RULE_ID )
            	    {
            	    // InternalDOF.g:2180:5: (lv_jumps_2_0= RULE_ID )
            	    // InternalDOF.g:2181:6: lv_jumps_2_0= RULE_ID
            	    {
            	    lv_jumps_2_0=(Token)match(input,RULE_ID,FOLLOW_54); 

            	    						newLeafNode(lv_jumps_2_0, grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getPathRule());
            	    						}
            	    						addWithLastConsumed(
            	    							current,
            	    							"jumps",
            	    							lv_jumps_2_0,
            	    							"org.eclipse.xtext.common.Terminals.ID");
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop40;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePath"


    // $ANTLR start "entryRuleEString"
    // InternalDOF.g:2202:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalDOF.g:2202:47: (iv_ruleEString= ruleEString EOF )
            // InternalDOF.g:2203:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDOF.g:2209:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_STRING_0= RULE_STRING ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;


        	enterRule();

        try {
            // InternalDOF.g:2215:2: (this_STRING_0= RULE_STRING )
            // InternalDOF.g:2216:2: this_STRING_0= RULE_STRING
            {
            this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            		current.merge(this_STRING_0);
            	

            		newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"

    // Delegated rules


    protected DFA28 dfa28 = new DFA28(this);
    protected DFA29 dfa29 = new DFA29(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\4\1\44\1\4\6\uffff\1\44";
    static final String dfa_3s = "\1\4\1\57\1\4\6\uffff\1\57";
    static final String dfa_4s = "\3\uffff\1\2\1\4\1\1\1\6\1\3\1\5\1\uffff";
    static final String dfa_5s = "\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\1",
            "\1\5\1\3\1\7\1\4\1\10\1\6\5\uffff\1\2",
            "\1\11",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\5\1\3\1\7\1\4\1\10\1\6\5\uffff\1\2"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA28 extends DFA {

        public DFA28(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 28;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "1363:2: ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) )";
        }
    }
    static final String dfa_7s = "\1\52\1\4\1\16\1\4\1\5\3\uffff\2\16";
    static final String dfa_8s = "\1\52\1\4\1\57\1\4\1\5\3\uffff\1\57\1\55";
    static final String dfa_9s = "\5\uffff\1\3\1\1\1\2\2\uffff";
    static final String[] dfa_10s = {
            "\1\1",
            "\1\2",
            "\1\7\34\uffff\1\4\1\6\1\5\1\uffff\1\3",
            "\1\10",
            "\1\11",
            "",
            "",
            "",
            "\1\7\34\uffff\1\4\1\6\1\5\1\uffff\1\3",
            "\1\7\35\uffff\1\6\1\5"
    };
    static final char[] dfa_7 = DFA.unpackEncodedStringToUnsignedChars(dfa_7s);
    static final char[] dfa_8 = DFA.unpackEncodedStringToUnsignedChars(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final short[][] dfa_10 = unpackEncodedStringArray(dfa_10s);

    class DFA29 extends DFA {

        public DFA29(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 29;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_7;
            this.max = dfa_8;
            this.accept = dfa_9;
            this.special = dfa_5;
            this.transition = dfa_10;
        }
        public String getDescription() {
            return "1687:2: (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000260210002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000220210002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000020210002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000040060002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000060002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000200040002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000020380010L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x000000001F000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000200020002L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000040040002L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000180010L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000180000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000180000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000040000080000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000080000004000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000260300012L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000220300012L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000020300012L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000280000000000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000400000000002L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000800000000002L});

}