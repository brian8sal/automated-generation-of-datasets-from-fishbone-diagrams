/**
 * generated by Xtext 2.25.0
 */
package es.unican.dof;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inequality</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see es.unican.dof.DofPackage#getInequality()
 * @model
 * @generated
 */
public interface Inequality extends Comparison
{
} // Inequality
