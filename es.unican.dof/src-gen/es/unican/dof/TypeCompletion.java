/**
 * generated by Xtext 2.25.0
 */
package es.unican.dof;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Completion</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see es.unican.dof.DofPackage#getTypeCompletion()
 * @model
 * @generated
 */
public interface TypeCompletion extends TypeFilter
{
} // TypeCompletion
