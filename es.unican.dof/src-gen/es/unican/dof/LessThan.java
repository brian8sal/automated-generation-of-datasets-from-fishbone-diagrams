/**
 * generated by Xtext 2.25.0
 */
package es.unican.dof;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Less Than</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see es.unican.dof.DofPackage#getLessThan()
 * @model
 * @generated
 */
public interface LessThan extends Comparison
{
} // LessThan
