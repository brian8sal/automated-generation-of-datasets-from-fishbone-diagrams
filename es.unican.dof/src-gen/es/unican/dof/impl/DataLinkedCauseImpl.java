/**
 * generated by Xtext 2.25.0
 */
package es.unican.dof.impl;

import es.unican.dof.AttributeFilter;
import es.unican.dof.BooleanExpression;
import es.unican.dof.DataLinkedCause;
import es.unican.dof.DofPackage;
import es.unican.dof.IncludedReference;
import es.unican.dof.TypeFilter;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Linked Cause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link es.unican.dof.impl.DataLinkedCauseImpl#getAttributeFilter <em>Attribute Filter</em>}</li>
 *   <li>{@link es.unican.dof.impl.DataLinkedCauseImpl#getInstancesFilter <em>Instances Filter</em>}</li>
 *   <li>{@link es.unican.dof.impl.DataLinkedCauseImpl#getIncludedReferences <em>Included References</em>}</li>
 *   <li>{@link es.unican.dof.impl.DataLinkedCauseImpl#getTypeFilter <em>Type Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataLinkedCauseImpl extends CauseImpl implements DataLinkedCause
{
  /**
   * The cached value of the '{@link #getAttributeFilter() <em>Attribute Filter</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAttributeFilter()
   * @generated
   * @ordered
   */
  protected AttributeFilter attributeFilter;

  /**
   * The cached value of the '{@link #getInstancesFilter() <em>Instances Filter</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInstancesFilter()
   * @generated
   * @ordered
   */
  protected BooleanExpression instancesFilter;

  /**
   * The cached value of the '{@link #getIncludedReferences() <em>Included References</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIncludedReferences()
   * @generated
   * @ordered
   */
  protected EList<IncludedReference> includedReferences;

  /**
   * The cached value of the '{@link #getTypeFilter() <em>Type Filter</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTypeFilter()
   * @generated
   * @ordered
   */
  protected TypeFilter typeFilter;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DataLinkedCauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DofPackage.Literals.DATA_LINKED_CAUSE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AttributeFilter getAttributeFilter()
  {
    return attributeFilter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAttributeFilter(AttributeFilter newAttributeFilter, NotificationChain msgs)
  {
    AttributeFilter oldAttributeFilter = attributeFilter;
    attributeFilter = newAttributeFilter;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DofPackage.DATA_LINKED_CAUSE__ATTRIBUTE_FILTER, oldAttributeFilter, newAttributeFilter);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setAttributeFilter(AttributeFilter newAttributeFilter)
  {
    if (newAttributeFilter != attributeFilter)
    {
      NotificationChain msgs = null;
      if (attributeFilter != null)
        msgs = ((InternalEObject)attributeFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DofPackage.DATA_LINKED_CAUSE__ATTRIBUTE_FILTER, null, msgs);
      if (newAttributeFilter != null)
        msgs = ((InternalEObject)newAttributeFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DofPackage.DATA_LINKED_CAUSE__ATTRIBUTE_FILTER, null, msgs);
      msgs = basicSetAttributeFilter(newAttributeFilter, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DofPackage.DATA_LINKED_CAUSE__ATTRIBUTE_FILTER, newAttributeFilter, newAttributeFilter));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public BooleanExpression getInstancesFilter()
  {
    return instancesFilter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetInstancesFilter(BooleanExpression newInstancesFilter, NotificationChain msgs)
  {
    BooleanExpression oldInstancesFilter = instancesFilter;
    instancesFilter = newInstancesFilter;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DofPackage.DATA_LINKED_CAUSE__INSTANCES_FILTER, oldInstancesFilter, newInstancesFilter);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setInstancesFilter(BooleanExpression newInstancesFilter)
  {
    if (newInstancesFilter != instancesFilter)
    {
      NotificationChain msgs = null;
      if (instancesFilter != null)
        msgs = ((InternalEObject)instancesFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DofPackage.DATA_LINKED_CAUSE__INSTANCES_FILTER, null, msgs);
      if (newInstancesFilter != null)
        msgs = ((InternalEObject)newInstancesFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DofPackage.DATA_LINKED_CAUSE__INSTANCES_FILTER, null, msgs);
      msgs = basicSetInstancesFilter(newInstancesFilter, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DofPackage.DATA_LINKED_CAUSE__INSTANCES_FILTER, newInstancesFilter, newInstancesFilter));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<IncludedReference> getIncludedReferences()
  {
    if (includedReferences == null)
    {
      includedReferences = new EObjectContainmentEList<IncludedReference>(IncludedReference.class, this, DofPackage.DATA_LINKED_CAUSE__INCLUDED_REFERENCES);
    }
    return includedReferences;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public TypeFilter getTypeFilter()
  {
    return typeFilter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTypeFilter(TypeFilter newTypeFilter, NotificationChain msgs)
  {
    TypeFilter oldTypeFilter = typeFilter;
    typeFilter = newTypeFilter;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DofPackage.DATA_LINKED_CAUSE__TYPE_FILTER, oldTypeFilter, newTypeFilter);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setTypeFilter(TypeFilter newTypeFilter)
  {
    if (newTypeFilter != typeFilter)
    {
      NotificationChain msgs = null;
      if (typeFilter != null)
        msgs = ((InternalEObject)typeFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DofPackage.DATA_LINKED_CAUSE__TYPE_FILTER, null, msgs);
      if (newTypeFilter != null)
        msgs = ((InternalEObject)newTypeFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DofPackage.DATA_LINKED_CAUSE__TYPE_FILTER, null, msgs);
      msgs = basicSetTypeFilter(newTypeFilter, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DofPackage.DATA_LINKED_CAUSE__TYPE_FILTER, newTypeFilter, newTypeFilter));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DofPackage.DATA_LINKED_CAUSE__ATTRIBUTE_FILTER:
        return basicSetAttributeFilter(null, msgs);
      case DofPackage.DATA_LINKED_CAUSE__INSTANCES_FILTER:
        return basicSetInstancesFilter(null, msgs);
      case DofPackage.DATA_LINKED_CAUSE__INCLUDED_REFERENCES:
        return ((InternalEList<?>)getIncludedReferences()).basicRemove(otherEnd, msgs);
      case DofPackage.DATA_LINKED_CAUSE__TYPE_FILTER:
        return basicSetTypeFilter(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DofPackage.DATA_LINKED_CAUSE__ATTRIBUTE_FILTER:
        return getAttributeFilter();
      case DofPackage.DATA_LINKED_CAUSE__INSTANCES_FILTER:
        return getInstancesFilter();
      case DofPackage.DATA_LINKED_CAUSE__INCLUDED_REFERENCES:
        return getIncludedReferences();
      case DofPackage.DATA_LINKED_CAUSE__TYPE_FILTER:
        return getTypeFilter();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DofPackage.DATA_LINKED_CAUSE__ATTRIBUTE_FILTER:
        setAttributeFilter((AttributeFilter)newValue);
        return;
      case DofPackage.DATA_LINKED_CAUSE__INSTANCES_FILTER:
        setInstancesFilter((BooleanExpression)newValue);
        return;
      case DofPackage.DATA_LINKED_CAUSE__INCLUDED_REFERENCES:
        getIncludedReferences().clear();
        getIncludedReferences().addAll((Collection<? extends IncludedReference>)newValue);
        return;
      case DofPackage.DATA_LINKED_CAUSE__TYPE_FILTER:
        setTypeFilter((TypeFilter)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DofPackage.DATA_LINKED_CAUSE__ATTRIBUTE_FILTER:
        setAttributeFilter((AttributeFilter)null);
        return;
      case DofPackage.DATA_LINKED_CAUSE__INSTANCES_FILTER:
        setInstancesFilter((BooleanExpression)null);
        return;
      case DofPackage.DATA_LINKED_CAUSE__INCLUDED_REFERENCES:
        getIncludedReferences().clear();
        return;
      case DofPackage.DATA_LINKED_CAUSE__TYPE_FILTER:
        setTypeFilter((TypeFilter)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DofPackage.DATA_LINKED_CAUSE__ATTRIBUTE_FILTER:
        return attributeFilter != null;
      case DofPackage.DATA_LINKED_CAUSE__INSTANCES_FILTER:
        return instancesFilter != null;
      case DofPackage.DATA_LINKED_CAUSE__INCLUDED_REFERENCES:
        return includedReferences != null && !includedReferences.isEmpty();
      case DofPackage.DATA_LINKED_CAUSE__TYPE_FILTER:
        return typeFilter != null;
    }
    return super.eIsSet(featureID);
  }

} //DataLinkedCauseImpl
