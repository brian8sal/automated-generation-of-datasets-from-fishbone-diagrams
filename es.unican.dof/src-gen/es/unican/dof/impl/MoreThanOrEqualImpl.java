/**
 * generated by Xtext 2.25.0
 */
package es.unican.dof.impl;

import es.unican.dof.DofPackage;
import es.unican.dof.MoreThanOrEqual;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>More Than Or Equal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MoreThanOrEqualImpl extends ComparisonImpl implements MoreThanOrEqual
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MoreThanOrEqualImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DofPackage.Literals.MORE_THAN_OR_EQUAL;
  }

} //MoreThanOrEqualImpl
