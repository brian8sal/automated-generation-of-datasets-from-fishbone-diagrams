/**
 * generated by Xtext 2.25.0
 */
package es.unican.dof.impl;

import es.unican.dof.BooleanExpression;
import es.unican.dof.DofPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BooleanExpressionImpl extends MinimalEObjectImpl.Container implements BooleanExpression
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BooleanExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DofPackage.Literals.BOOLEAN_EXPRESSION;
  }

} //BooleanExpressionImpl
