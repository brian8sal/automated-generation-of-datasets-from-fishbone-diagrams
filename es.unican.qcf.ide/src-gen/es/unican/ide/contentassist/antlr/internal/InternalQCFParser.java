package es.unican.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import es.unican.services.QCFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalQCFParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_TAB", "RULE_NEWLINE", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'qcf'", "'effect'", "'description'", "'category'", "'cause'", "'valueOfInterest'", "'contains'", "'{'", "'}'", "'.'"
    };
    public static final int RULE_NEWLINE=7;
    public static final int RULE_TAB=6;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_INT=8;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalQCFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalQCFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalQCFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalQCF.g"; }


    	private QCFGrammarAccess grammarAccess;

    	public void setGrammarAccess(QCFGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleQCF"
    // InternalQCF.g:53:1: entryRuleQCF : ruleQCF EOF ;
    public final void entryRuleQCF() throws RecognitionException {
        try {
            // InternalQCF.g:54:1: ( ruleQCF EOF )
            // InternalQCF.g:55:1: ruleQCF EOF
            {
             before(grammarAccess.getQCFRule()); 
            pushFollow(FOLLOW_1);
            ruleQCF();

            state._fsp--;

             after(grammarAccess.getQCFRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQCF"


    // $ANTLR start "ruleQCF"
    // InternalQCF.g:62:1: ruleQCF : ( ( rule__QCF__Group__0 ) ) ;
    public final void ruleQCF() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:66:2: ( ( ( rule__QCF__Group__0 ) ) )
            // InternalQCF.g:67:2: ( ( rule__QCF__Group__0 ) )
            {
            // InternalQCF.g:67:2: ( ( rule__QCF__Group__0 ) )
            // InternalQCF.g:68:3: ( rule__QCF__Group__0 )
            {
             before(grammarAccess.getQCFAccess().getGroup()); 
            // InternalQCF.g:69:3: ( rule__QCF__Group__0 )
            // InternalQCF.g:69:4: rule__QCF__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QCF__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQCFAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQCF"


    // $ANTLR start "entryRuleEffect"
    // InternalQCF.g:78:1: entryRuleEffect : ruleEffect EOF ;
    public final void entryRuleEffect() throws RecognitionException {
        try {
            // InternalQCF.g:79:1: ( ruleEffect EOF )
            // InternalQCF.g:80:1: ruleEffect EOF
            {
             before(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getEffectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalQCF.g:87:1: ruleEffect : ( ( rule__Effect__Group__0 ) ) ;
    public final void ruleEffect() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:91:2: ( ( ( rule__Effect__Group__0 ) ) )
            // InternalQCF.g:92:2: ( ( rule__Effect__Group__0 ) )
            {
            // InternalQCF.g:92:2: ( ( rule__Effect__Group__0 ) )
            // InternalQCF.g:93:3: ( rule__Effect__Group__0 )
            {
             before(grammarAccess.getEffectAccess().getGroup()); 
            // InternalQCF.g:94:3: ( rule__Effect__Group__0 )
            // InternalQCF.g:94:4: rule__Effect__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalQCF.g:103:1: entryRuleCategory : ruleCategory EOF ;
    public final void entryRuleCategory() throws RecognitionException {
        try {
            // InternalQCF.g:104:1: ( ruleCategory EOF )
            // InternalQCF.g:105:1: ruleCategory EOF
            {
             before(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getCategoryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalQCF.g:112:1: ruleCategory : ( ( rule__Category__Group__0 ) ) ;
    public final void ruleCategory() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:116:2: ( ( ( rule__Category__Group__0 ) ) )
            // InternalQCF.g:117:2: ( ( rule__Category__Group__0 ) )
            {
            // InternalQCF.g:117:2: ( ( rule__Category__Group__0 ) )
            // InternalQCF.g:118:3: ( rule__Category__Group__0 )
            {
             before(grammarAccess.getCategoryAccess().getGroup()); 
            // InternalQCF.g:119:3: ( rule__Category__Group__0 )
            // InternalQCF.g:119:4: rule__Category__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleCause"
    // InternalQCF.g:128:1: entryRuleCause : ruleCause EOF ;
    public final void entryRuleCause() throws RecognitionException {
        try {
            // InternalQCF.g:129:1: ( ruleCause EOF )
            // InternalQCF.g:130:1: ruleCause EOF
            {
             before(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalQCF.g:137:1: ruleCause : ( ( rule__Cause__Group__0 ) ) ;
    public final void ruleCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:141:2: ( ( ( rule__Cause__Group__0 ) ) )
            // InternalQCF.g:142:2: ( ( rule__Cause__Group__0 ) )
            {
            // InternalQCF.g:142:2: ( ( rule__Cause__Group__0 ) )
            // InternalQCF.g:143:3: ( rule__Cause__Group__0 )
            {
             before(grammarAccess.getCauseAccess().getGroup()); 
            // InternalQCF.g:144:3: ( rule__Cause__Group__0 )
            // InternalQCF.g:144:4: rule__Cause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleEString"
    // InternalQCF.g:153:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalQCF.g:154:1: ( ruleEString EOF )
            // InternalQCF.g:155:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalQCF.g:162:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:166:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalQCF.g:167:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalQCF.g:167:2: ( ( rule__EString__Alternatives ) )
            // InternalQCF.g:168:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalQCF.g:169:3: ( rule__EString__Alternatives )
            // InternalQCF.g:169:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalQCF.g:178:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalQCF.g:179:1: ( ruleQualifiedName EOF )
            // InternalQCF.g:180:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalQCF.g:187:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:191:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalQCF.g:192:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalQCF.g:192:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalQCF.g:193:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalQCF.g:194:3: ( rule__QualifiedName__Group__0 )
            // InternalQCF.g:194:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalQCF.g:202:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:206:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_STRING) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_ID) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalQCF.g:207:2: ( RULE_STRING )
                    {
                    // InternalQCF.g:207:2: ( RULE_STRING )
                    // InternalQCF.g:208:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQCF.g:213:2: ( RULE_ID )
                    {
                    // InternalQCF.g:213:2: ( RULE_ID )
                    // InternalQCF.g:214:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__QCF__Group__0"
    // InternalQCF.g:223:1: rule__QCF__Group__0 : rule__QCF__Group__0__Impl rule__QCF__Group__1 ;
    public final void rule__QCF__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:227:1: ( rule__QCF__Group__0__Impl rule__QCF__Group__1 )
            // InternalQCF.g:228:2: rule__QCF__Group__0__Impl rule__QCF__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__QCF__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QCF__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__0"


    // $ANTLR start "rule__QCF__Group__0__Impl"
    // InternalQCF.g:235:1: rule__QCF__Group__0__Impl : ( ( rule__QCF__Group_0__0 )? ) ;
    public final void rule__QCF__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:239:1: ( ( ( rule__QCF__Group_0__0 )? ) )
            // InternalQCF.g:240:1: ( ( rule__QCF__Group_0__0 )? )
            {
            // InternalQCF.g:240:1: ( ( rule__QCF__Group_0__0 )? )
            // InternalQCF.g:241:2: ( rule__QCF__Group_0__0 )?
            {
             before(grammarAccess.getQCFAccess().getGroup_0()); 
            // InternalQCF.g:242:2: ( rule__QCF__Group_0__0 )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalQCF.g:242:3: rule__QCF__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QCF__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQCFAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__0__Impl"


    // $ANTLR start "rule__QCF__Group__1"
    // InternalQCF.g:250:1: rule__QCF__Group__1 : rule__QCF__Group__1__Impl ;
    public final void rule__QCF__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:254:1: ( rule__QCF__Group__1__Impl )
            // InternalQCF.g:255:2: rule__QCF__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QCF__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__1"


    // $ANTLR start "rule__QCF__Group__1__Impl"
    // InternalQCF.g:261:1: rule__QCF__Group__1__Impl : ( ( rule__QCF__EffectsAssignment_1 ) ) ;
    public final void rule__QCF__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:265:1: ( ( ( rule__QCF__EffectsAssignment_1 ) ) )
            // InternalQCF.g:266:1: ( ( rule__QCF__EffectsAssignment_1 ) )
            {
            // InternalQCF.g:266:1: ( ( rule__QCF__EffectsAssignment_1 ) )
            // InternalQCF.g:267:2: ( rule__QCF__EffectsAssignment_1 )
            {
             before(grammarAccess.getQCFAccess().getEffectsAssignment_1()); 
            // InternalQCF.g:268:2: ( rule__QCF__EffectsAssignment_1 )
            // InternalQCF.g:268:3: rule__QCF__EffectsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__QCF__EffectsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQCFAccess().getEffectsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__1__Impl"


    // $ANTLR start "rule__QCF__Group_0__0"
    // InternalQCF.g:277:1: rule__QCF__Group_0__0 : rule__QCF__Group_0__0__Impl rule__QCF__Group_0__1 ;
    public final void rule__QCF__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:281:1: ( rule__QCF__Group_0__0__Impl rule__QCF__Group_0__1 )
            // InternalQCF.g:282:2: rule__QCF__Group_0__0__Impl rule__QCF__Group_0__1
            {
            pushFollow(FOLLOW_4);
            rule__QCF__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QCF__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group_0__0"


    // $ANTLR start "rule__QCF__Group_0__0__Impl"
    // InternalQCF.g:289:1: rule__QCF__Group_0__0__Impl : ( 'qcf' ) ;
    public final void rule__QCF__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:293:1: ( ( 'qcf' ) )
            // InternalQCF.g:294:1: ( 'qcf' )
            {
            // InternalQCF.g:294:1: ( 'qcf' )
            // InternalQCF.g:295:2: 'qcf'
            {
             before(grammarAccess.getQCFAccess().getQcfKeyword_0_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getQCFAccess().getQcfKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group_0__0__Impl"


    // $ANTLR start "rule__QCF__Group_0__1"
    // InternalQCF.g:304:1: rule__QCF__Group_0__1 : rule__QCF__Group_0__1__Impl ;
    public final void rule__QCF__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:308:1: ( rule__QCF__Group_0__1__Impl )
            // InternalQCF.g:309:2: rule__QCF__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QCF__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group_0__1"


    // $ANTLR start "rule__QCF__Group_0__1__Impl"
    // InternalQCF.g:315:1: rule__QCF__Group_0__1__Impl : ( ( rule__QCF__NameAssignment_0_1 ) ) ;
    public final void rule__QCF__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:319:1: ( ( ( rule__QCF__NameAssignment_0_1 ) ) )
            // InternalQCF.g:320:1: ( ( rule__QCF__NameAssignment_0_1 ) )
            {
            // InternalQCF.g:320:1: ( ( rule__QCF__NameAssignment_0_1 ) )
            // InternalQCF.g:321:2: ( rule__QCF__NameAssignment_0_1 )
            {
             before(grammarAccess.getQCFAccess().getNameAssignment_0_1()); 
            // InternalQCF.g:322:2: ( rule__QCF__NameAssignment_0_1 )
            // InternalQCF.g:322:3: rule__QCF__NameAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__QCF__NameAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getQCFAccess().getNameAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group_0__1__Impl"


    // $ANTLR start "rule__Effect__Group__0"
    // InternalQCF.g:331:1: rule__Effect__Group__0 : rule__Effect__Group__0__Impl rule__Effect__Group__1 ;
    public final void rule__Effect__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:335:1: ( rule__Effect__Group__0__Impl rule__Effect__Group__1 )
            // InternalQCF.g:336:2: rule__Effect__Group__0__Impl rule__Effect__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Effect__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0"


    // $ANTLR start "rule__Effect__Group__0__Impl"
    // InternalQCF.g:343:1: rule__Effect__Group__0__Impl : ( () ) ;
    public final void rule__Effect__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:347:1: ( ( () ) )
            // InternalQCF.g:348:1: ( () )
            {
            // InternalQCF.g:348:1: ( () )
            // InternalQCF.g:349:2: ()
            {
             before(grammarAccess.getEffectAccess().getEffectAction_0()); 
            // InternalQCF.g:350:2: ()
            // InternalQCF.g:350:3: 
            {
            }

             after(grammarAccess.getEffectAccess().getEffectAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0__Impl"


    // $ANTLR start "rule__Effect__Group__1"
    // InternalQCF.g:358:1: rule__Effect__Group__1 : rule__Effect__Group__1__Impl rule__Effect__Group__2 ;
    public final void rule__Effect__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:362:1: ( rule__Effect__Group__1__Impl rule__Effect__Group__2 )
            // InternalQCF.g:363:2: rule__Effect__Group__1__Impl rule__Effect__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Effect__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1"


    // $ANTLR start "rule__Effect__Group__1__Impl"
    // InternalQCF.g:370:1: rule__Effect__Group__1__Impl : ( 'effect' ) ;
    public final void rule__Effect__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:374:1: ( ( 'effect' ) )
            // InternalQCF.g:375:1: ( 'effect' )
            {
            // InternalQCF.g:375:1: ( 'effect' )
            // InternalQCF.g:376:2: 'effect'
            {
             before(grammarAccess.getEffectAccess().getEffectKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getEffectKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1__Impl"


    // $ANTLR start "rule__Effect__Group__2"
    // InternalQCF.g:385:1: rule__Effect__Group__2 : rule__Effect__Group__2__Impl rule__Effect__Group__3 ;
    public final void rule__Effect__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:389:1: ( rule__Effect__Group__2__Impl rule__Effect__Group__3 )
            // InternalQCF.g:390:2: rule__Effect__Group__2__Impl rule__Effect__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Effect__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2"


    // $ANTLR start "rule__Effect__Group__2__Impl"
    // InternalQCF.g:397:1: rule__Effect__Group__2__Impl : ( ( rule__Effect__NameAssignment_2 ) ) ;
    public final void rule__Effect__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:401:1: ( ( ( rule__Effect__NameAssignment_2 ) ) )
            // InternalQCF.g:402:1: ( ( rule__Effect__NameAssignment_2 ) )
            {
            // InternalQCF.g:402:1: ( ( rule__Effect__NameAssignment_2 ) )
            // InternalQCF.g:403:2: ( rule__Effect__NameAssignment_2 )
            {
             before(grammarAccess.getEffectAccess().getNameAssignment_2()); 
            // InternalQCF.g:404:2: ( rule__Effect__NameAssignment_2 )
            // InternalQCF.g:404:3: rule__Effect__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Effect__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2__Impl"


    // $ANTLR start "rule__Effect__Group__3"
    // InternalQCF.g:412:1: rule__Effect__Group__3 : rule__Effect__Group__3__Impl rule__Effect__Group__4 ;
    public final void rule__Effect__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:416:1: ( rule__Effect__Group__3__Impl rule__Effect__Group__4 )
            // InternalQCF.g:417:2: rule__Effect__Group__3__Impl rule__Effect__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Effect__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3"


    // $ANTLR start "rule__Effect__Group__3__Impl"
    // InternalQCF.g:424:1: rule__Effect__Group__3__Impl : ( ( rule__Effect__Group_3__0 )? ) ;
    public final void rule__Effect__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:428:1: ( ( ( rule__Effect__Group_3__0 )? ) )
            // InternalQCF.g:429:1: ( ( rule__Effect__Group_3__0 )? )
            {
            // InternalQCF.g:429:1: ( ( rule__Effect__Group_3__0 )? )
            // InternalQCF.g:430:2: ( rule__Effect__Group_3__0 )?
            {
             before(grammarAccess.getEffectAccess().getGroup_3()); 
            // InternalQCF.g:431:2: ( rule__Effect__Group_3__0 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalQCF.g:431:3: rule__Effect__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Effect__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEffectAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3__Impl"


    // $ANTLR start "rule__Effect__Group__4"
    // InternalQCF.g:439:1: rule__Effect__Group__4 : rule__Effect__Group__4__Impl ;
    public final void rule__Effect__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:443:1: ( rule__Effect__Group__4__Impl )
            // InternalQCF.g:444:2: rule__Effect__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4"


    // $ANTLR start "rule__Effect__Group__4__Impl"
    // InternalQCF.g:450:1: rule__Effect__Group__4__Impl : ( ( rule__Effect__Group_4__0 )? ) ;
    public final void rule__Effect__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:454:1: ( ( ( rule__Effect__Group_4__0 )? ) )
            // InternalQCF.g:455:1: ( ( rule__Effect__Group_4__0 )? )
            {
            // InternalQCF.g:455:1: ( ( rule__Effect__Group_4__0 )? )
            // InternalQCF.g:456:2: ( rule__Effect__Group_4__0 )?
            {
             before(grammarAccess.getEffectAccess().getGroup_4()); 
            // InternalQCF.g:457:2: ( rule__Effect__Group_4__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==16) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalQCF.g:457:3: rule__Effect__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Effect__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEffectAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4__Impl"


    // $ANTLR start "rule__Effect__Group_3__0"
    // InternalQCF.g:466:1: rule__Effect__Group_3__0 : rule__Effect__Group_3__0__Impl rule__Effect__Group_3__1 ;
    public final void rule__Effect__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:470:1: ( rule__Effect__Group_3__0__Impl rule__Effect__Group_3__1 )
            // InternalQCF.g:471:2: rule__Effect__Group_3__0__Impl rule__Effect__Group_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Effect__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_3__0"


    // $ANTLR start "rule__Effect__Group_3__0__Impl"
    // InternalQCF.g:478:1: rule__Effect__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__Effect__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:482:1: ( ( 'description' ) )
            // InternalQCF.g:483:1: ( 'description' )
            {
            // InternalQCF.g:483:1: ( 'description' )
            // InternalQCF.g:484:2: 'description'
            {
             before(grammarAccess.getEffectAccess().getDescriptionKeyword_3_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_3__0__Impl"


    // $ANTLR start "rule__Effect__Group_3__1"
    // InternalQCF.g:493:1: rule__Effect__Group_3__1 : rule__Effect__Group_3__1__Impl ;
    public final void rule__Effect__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:497:1: ( rule__Effect__Group_3__1__Impl )
            // InternalQCF.g:498:2: rule__Effect__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_3__1"


    // $ANTLR start "rule__Effect__Group_3__1__Impl"
    // InternalQCF.g:504:1: rule__Effect__Group_3__1__Impl : ( ( rule__Effect__DescriptionAssignment_3_1 ) ) ;
    public final void rule__Effect__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:508:1: ( ( ( rule__Effect__DescriptionAssignment_3_1 ) ) )
            // InternalQCF.g:509:1: ( ( rule__Effect__DescriptionAssignment_3_1 ) )
            {
            // InternalQCF.g:509:1: ( ( rule__Effect__DescriptionAssignment_3_1 ) )
            // InternalQCF.g:510:2: ( rule__Effect__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getEffectAccess().getDescriptionAssignment_3_1()); 
            // InternalQCF.g:511:2: ( rule__Effect__DescriptionAssignment_3_1 )
            // InternalQCF.g:511:3: rule__Effect__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Effect__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_3__1__Impl"


    // $ANTLR start "rule__Effect__Group_4__0"
    // InternalQCF.g:520:1: rule__Effect__Group_4__0 : rule__Effect__Group_4__0__Impl rule__Effect__Group_4__1 ;
    public final void rule__Effect__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:524:1: ( rule__Effect__Group_4__0__Impl rule__Effect__Group_4__1 )
            // InternalQCF.g:525:2: rule__Effect__Group_4__0__Impl rule__Effect__Group_4__1
            {
            pushFollow(FOLLOW_7);
            rule__Effect__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_4__0"


    // $ANTLR start "rule__Effect__Group_4__0__Impl"
    // InternalQCF.g:532:1: rule__Effect__Group_4__0__Impl : ( ( rule__Effect__CategoriesAssignment_4_0 ) ) ;
    public final void rule__Effect__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:536:1: ( ( ( rule__Effect__CategoriesAssignment_4_0 ) ) )
            // InternalQCF.g:537:1: ( ( rule__Effect__CategoriesAssignment_4_0 ) )
            {
            // InternalQCF.g:537:1: ( ( rule__Effect__CategoriesAssignment_4_0 ) )
            // InternalQCF.g:538:2: ( rule__Effect__CategoriesAssignment_4_0 )
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_4_0()); 
            // InternalQCF.g:539:2: ( rule__Effect__CategoriesAssignment_4_0 )
            // InternalQCF.g:539:3: rule__Effect__CategoriesAssignment_4_0
            {
            pushFollow(FOLLOW_2);
            rule__Effect__CategoriesAssignment_4_0();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_4__0__Impl"


    // $ANTLR start "rule__Effect__Group_4__1"
    // InternalQCF.g:547:1: rule__Effect__Group_4__1 : rule__Effect__Group_4__1__Impl ;
    public final void rule__Effect__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:551:1: ( rule__Effect__Group_4__1__Impl )
            // InternalQCF.g:552:2: rule__Effect__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_4__1"


    // $ANTLR start "rule__Effect__Group_4__1__Impl"
    // InternalQCF.g:558:1: rule__Effect__Group_4__1__Impl : ( ( rule__Effect__CategoriesAssignment_4_1 )* ) ;
    public final void rule__Effect__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:562:1: ( ( ( rule__Effect__CategoriesAssignment_4_1 )* ) )
            // InternalQCF.g:563:1: ( ( rule__Effect__CategoriesAssignment_4_1 )* )
            {
            // InternalQCF.g:563:1: ( ( rule__Effect__CategoriesAssignment_4_1 )* )
            // InternalQCF.g:564:2: ( rule__Effect__CategoriesAssignment_4_1 )*
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_4_1()); 
            // InternalQCF.g:565:2: ( rule__Effect__CategoriesAssignment_4_1 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==16) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalQCF.g:565:3: rule__Effect__CategoriesAssignment_4_1
            	    {
            	    pushFollow(FOLLOW_8);
            	    rule__Effect__CategoriesAssignment_4_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_4__1__Impl"


    // $ANTLR start "rule__Category__Group__0"
    // InternalQCF.g:574:1: rule__Category__Group__0 : rule__Category__Group__0__Impl rule__Category__Group__1 ;
    public final void rule__Category__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:578:1: ( rule__Category__Group__0__Impl rule__Category__Group__1 )
            // InternalQCF.g:579:2: rule__Category__Group__0__Impl rule__Category__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Category__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0"


    // $ANTLR start "rule__Category__Group__0__Impl"
    // InternalQCF.g:586:1: rule__Category__Group__0__Impl : ( () ) ;
    public final void rule__Category__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:590:1: ( ( () ) )
            // InternalQCF.g:591:1: ( () )
            {
            // InternalQCF.g:591:1: ( () )
            // InternalQCF.g:592:2: ()
            {
             before(grammarAccess.getCategoryAccess().getCategoryAction_0()); 
            // InternalQCF.g:593:2: ()
            // InternalQCF.g:593:3: 
            {
            }

             after(grammarAccess.getCategoryAccess().getCategoryAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0__Impl"


    // $ANTLR start "rule__Category__Group__1"
    // InternalQCF.g:601:1: rule__Category__Group__1 : rule__Category__Group__1__Impl rule__Category__Group__2 ;
    public final void rule__Category__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:605:1: ( rule__Category__Group__1__Impl rule__Category__Group__2 )
            // InternalQCF.g:606:2: rule__Category__Group__1__Impl rule__Category__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Category__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1"


    // $ANTLR start "rule__Category__Group__1__Impl"
    // InternalQCF.g:613:1: rule__Category__Group__1__Impl : ( 'category' ) ;
    public final void rule__Category__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:617:1: ( ( 'category' ) )
            // InternalQCF.g:618:1: ( 'category' )
            {
            // InternalQCF.g:618:1: ( 'category' )
            // InternalQCF.g:619:2: 'category'
            {
             before(grammarAccess.getCategoryAccess().getCategoryKeyword_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getCategoryKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1__Impl"


    // $ANTLR start "rule__Category__Group__2"
    // InternalQCF.g:628:1: rule__Category__Group__2 : rule__Category__Group__2__Impl rule__Category__Group__3 ;
    public final void rule__Category__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:632:1: ( rule__Category__Group__2__Impl rule__Category__Group__3 )
            // InternalQCF.g:633:2: rule__Category__Group__2__Impl rule__Category__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Category__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2"


    // $ANTLR start "rule__Category__Group__2__Impl"
    // InternalQCF.g:640:1: rule__Category__Group__2__Impl : ( ( rule__Category__NameAssignment_2 ) ) ;
    public final void rule__Category__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:644:1: ( ( ( rule__Category__NameAssignment_2 ) ) )
            // InternalQCF.g:645:1: ( ( rule__Category__NameAssignment_2 ) )
            {
            // InternalQCF.g:645:1: ( ( rule__Category__NameAssignment_2 ) )
            // InternalQCF.g:646:2: ( rule__Category__NameAssignment_2 )
            {
             before(grammarAccess.getCategoryAccess().getNameAssignment_2()); 
            // InternalQCF.g:647:2: ( rule__Category__NameAssignment_2 )
            // InternalQCF.g:647:3: rule__Category__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Category__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2__Impl"


    // $ANTLR start "rule__Category__Group__3"
    // InternalQCF.g:655:1: rule__Category__Group__3 : rule__Category__Group__3__Impl rule__Category__Group__4 ;
    public final void rule__Category__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:659:1: ( rule__Category__Group__3__Impl rule__Category__Group__4 )
            // InternalQCF.g:660:2: rule__Category__Group__3__Impl rule__Category__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Category__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3"


    // $ANTLR start "rule__Category__Group__3__Impl"
    // InternalQCF.g:667:1: rule__Category__Group__3__Impl : ( ( rule__Category__Group_3__0 )? ) ;
    public final void rule__Category__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:671:1: ( ( ( rule__Category__Group_3__0 )? ) )
            // InternalQCF.g:672:1: ( ( rule__Category__Group_3__0 )? )
            {
            // InternalQCF.g:672:1: ( ( rule__Category__Group_3__0 )? )
            // InternalQCF.g:673:2: ( rule__Category__Group_3__0 )?
            {
             before(grammarAccess.getCategoryAccess().getGroup_3()); 
            // InternalQCF.g:674:2: ( rule__Category__Group_3__0 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==15) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalQCF.g:674:3: rule__Category__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Category__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCategoryAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3__Impl"


    // $ANTLR start "rule__Category__Group__4"
    // InternalQCF.g:682:1: rule__Category__Group__4 : rule__Category__Group__4__Impl ;
    public final void rule__Category__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:686:1: ( rule__Category__Group__4__Impl )
            // InternalQCF.g:687:2: rule__Category__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4"


    // $ANTLR start "rule__Category__Group__4__Impl"
    // InternalQCF.g:693:1: rule__Category__Group__4__Impl : ( ( rule__Category__Group_4__0 )? ) ;
    public final void rule__Category__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:697:1: ( ( ( rule__Category__Group_4__0 )? ) )
            // InternalQCF.g:698:1: ( ( rule__Category__Group_4__0 )? )
            {
            // InternalQCF.g:698:1: ( ( rule__Category__Group_4__0 )? )
            // InternalQCF.g:699:2: ( rule__Category__Group_4__0 )?
            {
             before(grammarAccess.getCategoryAccess().getGroup_4()); 
            // InternalQCF.g:700:2: ( rule__Category__Group_4__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalQCF.g:700:3: rule__Category__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Category__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCategoryAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4__Impl"


    // $ANTLR start "rule__Category__Group_3__0"
    // InternalQCF.g:709:1: rule__Category__Group_3__0 : rule__Category__Group_3__0__Impl rule__Category__Group_3__1 ;
    public final void rule__Category__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:713:1: ( rule__Category__Group_3__0__Impl rule__Category__Group_3__1 )
            // InternalQCF.g:714:2: rule__Category__Group_3__0__Impl rule__Category__Group_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Category__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_3__0"


    // $ANTLR start "rule__Category__Group_3__0__Impl"
    // InternalQCF.g:721:1: rule__Category__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__Category__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:725:1: ( ( 'description' ) )
            // InternalQCF.g:726:1: ( 'description' )
            {
            // InternalQCF.g:726:1: ( 'description' )
            // InternalQCF.g:727:2: 'description'
            {
             before(grammarAccess.getCategoryAccess().getDescriptionKeyword_3_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_3__0__Impl"


    // $ANTLR start "rule__Category__Group_3__1"
    // InternalQCF.g:736:1: rule__Category__Group_3__1 : rule__Category__Group_3__1__Impl ;
    public final void rule__Category__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:740:1: ( rule__Category__Group_3__1__Impl )
            // InternalQCF.g:741:2: rule__Category__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_3__1"


    // $ANTLR start "rule__Category__Group_3__1__Impl"
    // InternalQCF.g:747:1: rule__Category__Group_3__1__Impl : ( ( rule__Category__DescriptionAssignment_3_1 ) ) ;
    public final void rule__Category__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:751:1: ( ( ( rule__Category__DescriptionAssignment_3_1 ) ) )
            // InternalQCF.g:752:1: ( ( rule__Category__DescriptionAssignment_3_1 ) )
            {
            // InternalQCF.g:752:1: ( ( rule__Category__DescriptionAssignment_3_1 ) )
            // InternalQCF.g:753:2: ( rule__Category__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getCategoryAccess().getDescriptionAssignment_3_1()); 
            // InternalQCF.g:754:2: ( rule__Category__DescriptionAssignment_3_1 )
            // InternalQCF.g:754:3: rule__Category__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Category__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_3__1__Impl"


    // $ANTLR start "rule__Category__Group_4__0"
    // InternalQCF.g:763:1: rule__Category__Group_4__0 : rule__Category__Group_4__0__Impl rule__Category__Group_4__1 ;
    public final void rule__Category__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:767:1: ( rule__Category__Group_4__0__Impl rule__Category__Group_4__1 )
            // InternalQCF.g:768:2: rule__Category__Group_4__0__Impl rule__Category__Group_4__1
            {
            pushFollow(FOLLOW_10);
            rule__Category__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_4__0"


    // $ANTLR start "rule__Category__Group_4__0__Impl"
    // InternalQCF.g:775:1: rule__Category__Group_4__0__Impl : ( ( rule__Category__CausesAssignment_4_0 ) ) ;
    public final void rule__Category__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:779:1: ( ( ( rule__Category__CausesAssignment_4_0 ) ) )
            // InternalQCF.g:780:1: ( ( rule__Category__CausesAssignment_4_0 ) )
            {
            // InternalQCF.g:780:1: ( ( rule__Category__CausesAssignment_4_0 ) )
            // InternalQCF.g:781:2: ( rule__Category__CausesAssignment_4_0 )
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_4_0()); 
            // InternalQCF.g:782:2: ( rule__Category__CausesAssignment_4_0 )
            // InternalQCF.g:782:3: rule__Category__CausesAssignment_4_0
            {
            pushFollow(FOLLOW_2);
            rule__Category__CausesAssignment_4_0();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getCausesAssignment_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_4__0__Impl"


    // $ANTLR start "rule__Category__Group_4__1"
    // InternalQCF.g:790:1: rule__Category__Group_4__1 : rule__Category__Group_4__1__Impl ;
    public final void rule__Category__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:794:1: ( rule__Category__Group_4__1__Impl )
            // InternalQCF.g:795:2: rule__Category__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_4__1"


    // $ANTLR start "rule__Category__Group_4__1__Impl"
    // InternalQCF.g:801:1: rule__Category__Group_4__1__Impl : ( ( rule__Category__CausesAssignment_4_1 )* ) ;
    public final void rule__Category__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:805:1: ( ( ( rule__Category__CausesAssignment_4_1 )* ) )
            // InternalQCF.g:806:1: ( ( rule__Category__CausesAssignment_4_1 )* )
            {
            // InternalQCF.g:806:1: ( ( rule__Category__CausesAssignment_4_1 )* )
            // InternalQCF.g:807:2: ( rule__Category__CausesAssignment_4_1 )*
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_4_1()); 
            // InternalQCF.g:808:2: ( rule__Category__CausesAssignment_4_1 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==17) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalQCF.g:808:3: rule__Category__CausesAssignment_4_1
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Category__CausesAssignment_4_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getCategoryAccess().getCausesAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_4__1__Impl"


    // $ANTLR start "rule__Cause__Group__0"
    // InternalQCF.g:817:1: rule__Cause__Group__0 : rule__Cause__Group__0__Impl rule__Cause__Group__1 ;
    public final void rule__Cause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:821:1: ( rule__Cause__Group__0__Impl rule__Cause__Group__1 )
            // InternalQCF.g:822:2: rule__Cause__Group__0__Impl rule__Cause__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Cause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__0"


    // $ANTLR start "rule__Cause__Group__0__Impl"
    // InternalQCF.g:829:1: rule__Cause__Group__0__Impl : ( () ) ;
    public final void rule__Cause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:833:1: ( ( () ) )
            // InternalQCF.g:834:1: ( () )
            {
            // InternalQCF.g:834:1: ( () )
            // InternalQCF.g:835:2: ()
            {
             before(grammarAccess.getCauseAccess().getCauseAction_0()); 
            // InternalQCF.g:836:2: ()
            // InternalQCF.g:836:3: 
            {
            }

             after(grammarAccess.getCauseAccess().getCauseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__0__Impl"


    // $ANTLR start "rule__Cause__Group__1"
    // InternalQCF.g:844:1: rule__Cause__Group__1 : rule__Cause__Group__1__Impl rule__Cause__Group__2 ;
    public final void rule__Cause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:848:1: ( rule__Cause__Group__1__Impl rule__Cause__Group__2 )
            // InternalQCF.g:849:2: rule__Cause__Group__1__Impl rule__Cause__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Cause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__1"


    // $ANTLR start "rule__Cause__Group__1__Impl"
    // InternalQCF.g:856:1: rule__Cause__Group__1__Impl : ( 'cause' ) ;
    public final void rule__Cause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:860:1: ( ( 'cause' ) )
            // InternalQCF.g:861:1: ( 'cause' )
            {
            // InternalQCF.g:861:1: ( 'cause' )
            // InternalQCF.g:862:2: 'cause'
            {
             before(grammarAccess.getCauseAccess().getCauseKeyword_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getCauseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__1__Impl"


    // $ANTLR start "rule__Cause__Group__2"
    // InternalQCF.g:871:1: rule__Cause__Group__2 : rule__Cause__Group__2__Impl rule__Cause__Group__3 ;
    public final void rule__Cause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:875:1: ( rule__Cause__Group__2__Impl rule__Cause__Group__3 )
            // InternalQCF.g:876:2: rule__Cause__Group__2__Impl rule__Cause__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__Cause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__2"


    // $ANTLR start "rule__Cause__Group__2__Impl"
    // InternalQCF.g:883:1: rule__Cause__Group__2__Impl : ( ( rule__Cause__NameAssignment_2 ) ) ;
    public final void rule__Cause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:887:1: ( ( ( rule__Cause__NameAssignment_2 ) ) )
            // InternalQCF.g:888:1: ( ( rule__Cause__NameAssignment_2 ) )
            {
            // InternalQCF.g:888:1: ( ( rule__Cause__NameAssignment_2 ) )
            // InternalQCF.g:889:2: ( rule__Cause__NameAssignment_2 )
            {
             before(grammarAccess.getCauseAccess().getNameAssignment_2()); 
            // InternalQCF.g:890:2: ( rule__Cause__NameAssignment_2 )
            // InternalQCF.g:890:3: rule__Cause__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Cause__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__2__Impl"


    // $ANTLR start "rule__Cause__Group__3"
    // InternalQCF.g:898:1: rule__Cause__Group__3 : rule__Cause__Group__3__Impl rule__Cause__Group__4 ;
    public final void rule__Cause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:902:1: ( rule__Cause__Group__3__Impl rule__Cause__Group__4 )
            // InternalQCF.g:903:2: rule__Cause__Group__3__Impl rule__Cause__Group__4
            {
            pushFollow(FOLLOW_12);
            rule__Cause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__3"


    // $ANTLR start "rule__Cause__Group__3__Impl"
    // InternalQCF.g:910:1: rule__Cause__Group__3__Impl : ( ( rule__Cause__Group_3__0 )? ) ;
    public final void rule__Cause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:914:1: ( ( ( rule__Cause__Group_3__0 )? ) )
            // InternalQCF.g:915:1: ( ( rule__Cause__Group_3__0 )? )
            {
            // InternalQCF.g:915:1: ( ( rule__Cause__Group_3__0 )? )
            // InternalQCF.g:916:2: ( rule__Cause__Group_3__0 )?
            {
             before(grammarAccess.getCauseAccess().getGroup_3()); 
            // InternalQCF.g:917:2: ( rule__Cause__Group_3__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==15) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalQCF.g:917:3: rule__Cause__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cause__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__3__Impl"


    // $ANTLR start "rule__Cause__Group__4"
    // InternalQCF.g:925:1: rule__Cause__Group__4 : rule__Cause__Group__4__Impl rule__Cause__Group__5 ;
    public final void rule__Cause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:929:1: ( rule__Cause__Group__4__Impl rule__Cause__Group__5 )
            // InternalQCF.g:930:2: rule__Cause__Group__4__Impl rule__Cause__Group__5
            {
            pushFollow(FOLLOW_12);
            rule__Cause__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__4"


    // $ANTLR start "rule__Cause__Group__4__Impl"
    // InternalQCF.g:937:1: rule__Cause__Group__4__Impl : ( ( rule__Cause__Group_4__0 )? ) ;
    public final void rule__Cause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:941:1: ( ( ( rule__Cause__Group_4__0 )? ) )
            // InternalQCF.g:942:1: ( ( rule__Cause__Group_4__0 )? )
            {
            // InternalQCF.g:942:1: ( ( rule__Cause__Group_4__0 )? )
            // InternalQCF.g:943:2: ( rule__Cause__Group_4__0 )?
            {
             before(grammarAccess.getCauseAccess().getGroup_4()); 
            // InternalQCF.g:944:2: ( rule__Cause__Group_4__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==18) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalQCF.g:944:3: rule__Cause__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cause__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCauseAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__4__Impl"


    // $ANTLR start "rule__Cause__Group__5"
    // InternalQCF.g:952:1: rule__Cause__Group__5 : rule__Cause__Group__5__Impl ;
    public final void rule__Cause__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:956:1: ( rule__Cause__Group__5__Impl )
            // InternalQCF.g:957:2: rule__Cause__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__5"


    // $ANTLR start "rule__Cause__Group__5__Impl"
    // InternalQCF.g:963:1: rule__Cause__Group__5__Impl : ( ( rule__Cause__Group_5__0 )? ) ;
    public final void rule__Cause__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:967:1: ( ( ( rule__Cause__Group_5__0 )? ) )
            // InternalQCF.g:968:1: ( ( rule__Cause__Group_5__0 )? )
            {
            // InternalQCF.g:968:1: ( ( rule__Cause__Group_5__0 )? )
            // InternalQCF.g:969:2: ( rule__Cause__Group_5__0 )?
            {
             before(grammarAccess.getCauseAccess().getGroup_5()); 
            // InternalQCF.g:970:2: ( rule__Cause__Group_5__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==19) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalQCF.g:970:3: rule__Cause__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cause__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCauseAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__5__Impl"


    // $ANTLR start "rule__Cause__Group_3__0"
    // InternalQCF.g:979:1: rule__Cause__Group_3__0 : rule__Cause__Group_3__0__Impl rule__Cause__Group_3__1 ;
    public final void rule__Cause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:983:1: ( rule__Cause__Group_3__0__Impl rule__Cause__Group_3__1 )
            // InternalQCF.g:984:2: rule__Cause__Group_3__0__Impl rule__Cause__Group_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Cause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_3__0"


    // $ANTLR start "rule__Cause__Group_3__0__Impl"
    // InternalQCF.g:991:1: rule__Cause__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__Cause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:995:1: ( ( 'description' ) )
            // InternalQCF.g:996:1: ( 'description' )
            {
            // InternalQCF.g:996:1: ( 'description' )
            // InternalQCF.g:997:2: 'description'
            {
             before(grammarAccess.getCauseAccess().getDescriptionKeyword_3_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_3__0__Impl"


    // $ANTLR start "rule__Cause__Group_3__1"
    // InternalQCF.g:1006:1: rule__Cause__Group_3__1 : rule__Cause__Group_3__1__Impl ;
    public final void rule__Cause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1010:1: ( rule__Cause__Group_3__1__Impl )
            // InternalQCF.g:1011:2: rule__Cause__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_3__1"


    // $ANTLR start "rule__Cause__Group_3__1__Impl"
    // InternalQCF.g:1017:1: rule__Cause__Group_3__1__Impl : ( ( rule__Cause__DescriptionAssignment_3_1 ) ) ;
    public final void rule__Cause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1021:1: ( ( ( rule__Cause__DescriptionAssignment_3_1 ) ) )
            // InternalQCF.g:1022:1: ( ( rule__Cause__DescriptionAssignment_3_1 ) )
            {
            // InternalQCF.g:1022:1: ( ( rule__Cause__DescriptionAssignment_3_1 ) )
            // InternalQCF.g:1023:2: ( rule__Cause__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getCauseAccess().getDescriptionAssignment_3_1()); 
            // InternalQCF.g:1024:2: ( rule__Cause__DescriptionAssignment_3_1 )
            // InternalQCF.g:1024:3: rule__Cause__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Cause__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_3__1__Impl"


    // $ANTLR start "rule__Cause__Group_4__0"
    // InternalQCF.g:1033:1: rule__Cause__Group_4__0 : rule__Cause__Group_4__0__Impl rule__Cause__Group_4__1 ;
    public final void rule__Cause__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1037:1: ( rule__Cause__Group_4__0__Impl rule__Cause__Group_4__1 )
            // InternalQCF.g:1038:2: rule__Cause__Group_4__0__Impl rule__Cause__Group_4__1
            {
            pushFollow(FOLLOW_5);
            rule__Cause__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_4__0"


    // $ANTLR start "rule__Cause__Group_4__0__Impl"
    // InternalQCF.g:1045:1: rule__Cause__Group_4__0__Impl : ( 'valueOfInterest' ) ;
    public final void rule__Cause__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1049:1: ( ( 'valueOfInterest' ) )
            // InternalQCF.g:1050:1: ( 'valueOfInterest' )
            {
            // InternalQCF.g:1050:1: ( 'valueOfInterest' )
            // InternalQCF.g:1051:2: 'valueOfInterest'
            {
             before(grammarAccess.getCauseAccess().getValueOfInterestKeyword_4_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getValueOfInterestKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_4__0__Impl"


    // $ANTLR start "rule__Cause__Group_4__1"
    // InternalQCF.g:1060:1: rule__Cause__Group_4__1 : rule__Cause__Group_4__1__Impl ;
    public final void rule__Cause__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1064:1: ( rule__Cause__Group_4__1__Impl )
            // InternalQCF.g:1065:2: rule__Cause__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_4__1"


    // $ANTLR start "rule__Cause__Group_4__1__Impl"
    // InternalQCF.g:1071:1: rule__Cause__Group_4__1__Impl : ( ( rule__Cause__ValueOfInterestAssignment_4_1 ) ) ;
    public final void rule__Cause__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1075:1: ( ( ( rule__Cause__ValueOfInterestAssignment_4_1 ) ) )
            // InternalQCF.g:1076:1: ( ( rule__Cause__ValueOfInterestAssignment_4_1 ) )
            {
            // InternalQCF.g:1076:1: ( ( rule__Cause__ValueOfInterestAssignment_4_1 ) )
            // InternalQCF.g:1077:2: ( rule__Cause__ValueOfInterestAssignment_4_1 )
            {
             before(grammarAccess.getCauseAccess().getValueOfInterestAssignment_4_1()); 
            // InternalQCF.g:1078:2: ( rule__Cause__ValueOfInterestAssignment_4_1 )
            // InternalQCF.g:1078:3: rule__Cause__ValueOfInterestAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Cause__ValueOfInterestAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getValueOfInterestAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_4__1__Impl"


    // $ANTLR start "rule__Cause__Group_5__0"
    // InternalQCF.g:1087:1: rule__Cause__Group_5__0 : rule__Cause__Group_5__0__Impl rule__Cause__Group_5__1 ;
    public final void rule__Cause__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1091:1: ( rule__Cause__Group_5__0__Impl rule__Cause__Group_5__1 )
            // InternalQCF.g:1092:2: rule__Cause__Group_5__0__Impl rule__Cause__Group_5__1
            {
            pushFollow(FOLLOW_13);
            rule__Cause__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__0"


    // $ANTLR start "rule__Cause__Group_5__0__Impl"
    // InternalQCF.g:1099:1: rule__Cause__Group_5__0__Impl : ( 'contains' ) ;
    public final void rule__Cause__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1103:1: ( ( 'contains' ) )
            // InternalQCF.g:1104:1: ( 'contains' )
            {
            // InternalQCF.g:1104:1: ( 'contains' )
            // InternalQCF.g:1105:2: 'contains'
            {
             before(grammarAccess.getCauseAccess().getContainsKeyword_5_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getContainsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__0__Impl"


    // $ANTLR start "rule__Cause__Group_5__1"
    // InternalQCF.g:1114:1: rule__Cause__Group_5__1 : rule__Cause__Group_5__1__Impl rule__Cause__Group_5__2 ;
    public final void rule__Cause__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1118:1: ( rule__Cause__Group_5__1__Impl rule__Cause__Group_5__2 )
            // InternalQCF.g:1119:2: rule__Cause__Group_5__1__Impl rule__Cause__Group_5__2
            {
            pushFollow(FOLLOW_10);
            rule__Cause__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__1"


    // $ANTLR start "rule__Cause__Group_5__1__Impl"
    // InternalQCF.g:1126:1: rule__Cause__Group_5__1__Impl : ( '{' ) ;
    public final void rule__Cause__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1130:1: ( ( '{' ) )
            // InternalQCF.g:1131:1: ( '{' )
            {
            // InternalQCF.g:1131:1: ( '{' )
            // InternalQCF.g:1132:2: '{'
            {
             before(grammarAccess.getCauseAccess().getLeftCurlyBracketKeyword_5_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getLeftCurlyBracketKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__1__Impl"


    // $ANTLR start "rule__Cause__Group_5__2"
    // InternalQCF.g:1141:1: rule__Cause__Group_5__2 : rule__Cause__Group_5__2__Impl rule__Cause__Group_5__3 ;
    public final void rule__Cause__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1145:1: ( rule__Cause__Group_5__2__Impl rule__Cause__Group_5__3 )
            // InternalQCF.g:1146:2: rule__Cause__Group_5__2__Impl rule__Cause__Group_5__3
            {
            pushFollow(FOLLOW_14);
            rule__Cause__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__2"


    // $ANTLR start "rule__Cause__Group_5__2__Impl"
    // InternalQCF.g:1153:1: rule__Cause__Group_5__2__Impl : ( ( rule__Cause__SubCausesAssignment_5_2 ) ) ;
    public final void rule__Cause__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1157:1: ( ( ( rule__Cause__SubCausesAssignment_5_2 ) ) )
            // InternalQCF.g:1158:1: ( ( rule__Cause__SubCausesAssignment_5_2 ) )
            {
            // InternalQCF.g:1158:1: ( ( rule__Cause__SubCausesAssignment_5_2 ) )
            // InternalQCF.g:1159:2: ( rule__Cause__SubCausesAssignment_5_2 )
            {
             before(grammarAccess.getCauseAccess().getSubCausesAssignment_5_2()); 
            // InternalQCF.g:1160:2: ( rule__Cause__SubCausesAssignment_5_2 )
            // InternalQCF.g:1160:3: rule__Cause__SubCausesAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__Cause__SubCausesAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getSubCausesAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__2__Impl"


    // $ANTLR start "rule__Cause__Group_5__3"
    // InternalQCF.g:1168:1: rule__Cause__Group_5__3 : rule__Cause__Group_5__3__Impl rule__Cause__Group_5__4 ;
    public final void rule__Cause__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1172:1: ( rule__Cause__Group_5__3__Impl rule__Cause__Group_5__4 )
            // InternalQCF.g:1173:2: rule__Cause__Group_5__3__Impl rule__Cause__Group_5__4
            {
            pushFollow(FOLLOW_14);
            rule__Cause__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__3"


    // $ANTLR start "rule__Cause__Group_5__3__Impl"
    // InternalQCF.g:1180:1: rule__Cause__Group_5__3__Impl : ( ( rule__Cause__SubCausesAssignment_5_3 )* ) ;
    public final void rule__Cause__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1184:1: ( ( ( rule__Cause__SubCausesAssignment_5_3 )* ) )
            // InternalQCF.g:1185:1: ( ( rule__Cause__SubCausesAssignment_5_3 )* )
            {
            // InternalQCF.g:1185:1: ( ( rule__Cause__SubCausesAssignment_5_3 )* )
            // InternalQCF.g:1186:2: ( rule__Cause__SubCausesAssignment_5_3 )*
            {
             before(grammarAccess.getCauseAccess().getSubCausesAssignment_5_3()); 
            // InternalQCF.g:1187:2: ( rule__Cause__SubCausesAssignment_5_3 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==17) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalQCF.g:1187:3: rule__Cause__SubCausesAssignment_5_3
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Cause__SubCausesAssignment_5_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getCauseAccess().getSubCausesAssignment_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__3__Impl"


    // $ANTLR start "rule__Cause__Group_5__4"
    // InternalQCF.g:1195:1: rule__Cause__Group_5__4 : rule__Cause__Group_5__4__Impl ;
    public final void rule__Cause__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1199:1: ( rule__Cause__Group_5__4__Impl )
            // InternalQCF.g:1200:2: rule__Cause__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__4"


    // $ANTLR start "rule__Cause__Group_5__4__Impl"
    // InternalQCF.g:1206:1: rule__Cause__Group_5__4__Impl : ( '}' ) ;
    public final void rule__Cause__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1210:1: ( ( '}' ) )
            // InternalQCF.g:1211:1: ( '}' )
            {
            // InternalQCF.g:1211:1: ( '}' )
            // InternalQCF.g:1212:2: '}'
            {
             before(grammarAccess.getCauseAccess().getRightCurlyBracketKeyword_5_4()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getRightCurlyBracketKeyword_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__4__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalQCF.g:1222:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1226:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalQCF.g:1227:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalQCF.g:1234:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1238:1: ( ( RULE_ID ) )
            // InternalQCF.g:1239:1: ( RULE_ID )
            {
            // InternalQCF.g:1239:1: ( RULE_ID )
            // InternalQCF.g:1240:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalQCF.g:1249:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1253:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalQCF.g:1254:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalQCF.g:1260:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1264:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalQCF.g:1265:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalQCF.g:1265:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalQCF.g:1266:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalQCF.g:1267:2: ( rule__QualifiedName__Group_1__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==22) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalQCF.g:1267:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalQCF.g:1276:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1280:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalQCF.g:1281:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_4);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalQCF.g:1288:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1292:1: ( ( '.' ) )
            // InternalQCF.g:1293:1: ( '.' )
            {
            // InternalQCF.g:1293:1: ( '.' )
            // InternalQCF.g:1294:2: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalQCF.g:1303:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1307:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalQCF.g:1308:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalQCF.g:1314:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1318:1: ( ( RULE_ID ) )
            // InternalQCF.g:1319:1: ( RULE_ID )
            {
            // InternalQCF.g:1319:1: ( RULE_ID )
            // InternalQCF.g:1320:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__QCF__NameAssignment_0_1"
    // InternalQCF.g:1330:1: rule__QCF__NameAssignment_0_1 : ( ruleQualifiedName ) ;
    public final void rule__QCF__NameAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1334:1: ( ( ruleQualifiedName ) )
            // InternalQCF.g:1335:2: ( ruleQualifiedName )
            {
            // InternalQCF.g:1335:2: ( ruleQualifiedName )
            // InternalQCF.g:1336:3: ruleQualifiedName
            {
             before(grammarAccess.getQCFAccess().getNameQualifiedNameParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQCFAccess().getNameQualifiedNameParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__NameAssignment_0_1"


    // $ANTLR start "rule__QCF__EffectsAssignment_1"
    // InternalQCF.g:1345:1: rule__QCF__EffectsAssignment_1 : ( ruleEffect ) ;
    public final void rule__QCF__EffectsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1349:1: ( ( ruleEffect ) )
            // InternalQCF.g:1350:2: ( ruleEffect )
            {
            // InternalQCF.g:1350:2: ( ruleEffect )
            // InternalQCF.g:1351:3: ruleEffect
            {
             before(grammarAccess.getQCFAccess().getEffectsEffectParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getQCFAccess().getEffectsEffectParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__EffectsAssignment_1"


    // $ANTLR start "rule__Effect__NameAssignment_2"
    // InternalQCF.g:1360:1: rule__Effect__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Effect__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1364:1: ( ( ruleEString ) )
            // InternalQCF.g:1365:2: ( ruleEString )
            {
            // InternalQCF.g:1365:2: ( ruleEString )
            // InternalQCF.g:1366:3: ruleEString
            {
             before(grammarAccess.getEffectAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__NameAssignment_2"


    // $ANTLR start "rule__Effect__DescriptionAssignment_3_1"
    // InternalQCF.g:1375:1: rule__Effect__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Effect__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1379:1: ( ( ruleEString ) )
            // InternalQCF.g:1380:2: ( ruleEString )
            {
            // InternalQCF.g:1380:2: ( ruleEString )
            // InternalQCF.g:1381:3: ruleEString
            {
             before(grammarAccess.getEffectAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__DescriptionAssignment_3_1"


    // $ANTLR start "rule__Effect__CategoriesAssignment_4_0"
    // InternalQCF.g:1390:1: rule__Effect__CategoriesAssignment_4_0 : ( ruleCategory ) ;
    public final void rule__Effect__CategoriesAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1394:1: ( ( ruleCategory ) )
            // InternalQCF.g:1395:2: ( ruleCategory )
            {
            // InternalQCF.g:1395:2: ( ruleCategory )
            // InternalQCF.g:1396:3: ruleCategory
            {
             before(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_4_0_0()); 
            pushFollow(FOLLOW_2);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__CategoriesAssignment_4_0"


    // $ANTLR start "rule__Effect__CategoriesAssignment_4_1"
    // InternalQCF.g:1405:1: rule__Effect__CategoriesAssignment_4_1 : ( ruleCategory ) ;
    public final void rule__Effect__CategoriesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1409:1: ( ( ruleCategory ) )
            // InternalQCF.g:1410:2: ( ruleCategory )
            {
            // InternalQCF.g:1410:2: ( ruleCategory )
            // InternalQCF.g:1411:3: ruleCategory
            {
             before(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__CategoriesAssignment_4_1"


    // $ANTLR start "rule__Category__NameAssignment_2"
    // InternalQCF.g:1420:1: rule__Category__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Category__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1424:1: ( ( ruleEString ) )
            // InternalQCF.g:1425:2: ( ruleEString )
            {
            // InternalQCF.g:1425:2: ( ruleEString )
            // InternalQCF.g:1426:3: ruleEString
            {
             before(grammarAccess.getCategoryAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__NameAssignment_2"


    // $ANTLR start "rule__Category__DescriptionAssignment_3_1"
    // InternalQCF.g:1435:1: rule__Category__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Category__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1439:1: ( ( ruleEString ) )
            // InternalQCF.g:1440:2: ( ruleEString )
            {
            // InternalQCF.g:1440:2: ( ruleEString )
            // InternalQCF.g:1441:3: ruleEString
            {
             before(grammarAccess.getCategoryAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__DescriptionAssignment_3_1"


    // $ANTLR start "rule__Category__CausesAssignment_4_0"
    // InternalQCF.g:1450:1: rule__Category__CausesAssignment_4_0 : ( ruleCause ) ;
    public final void rule__Category__CausesAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1454:1: ( ( ruleCause ) )
            // InternalQCF.g:1455:2: ( ruleCause )
            {
            // InternalQCF.g:1455:2: ( ruleCause )
            // InternalQCF.g:1456:3: ruleCause
            {
             before(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__CausesAssignment_4_0"


    // $ANTLR start "rule__Category__CausesAssignment_4_1"
    // InternalQCF.g:1465:1: rule__Category__CausesAssignment_4_1 : ( ruleCause ) ;
    public final void rule__Category__CausesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1469:1: ( ( ruleCause ) )
            // InternalQCF.g:1470:2: ( ruleCause )
            {
            // InternalQCF.g:1470:2: ( ruleCause )
            // InternalQCF.g:1471:3: ruleCause
            {
             before(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__CausesAssignment_4_1"


    // $ANTLR start "rule__Cause__NameAssignment_2"
    // InternalQCF.g:1480:1: rule__Cause__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Cause__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1484:1: ( ( ruleEString ) )
            // InternalQCF.g:1485:2: ( ruleEString )
            {
            // InternalQCF.g:1485:2: ( ruleEString )
            // InternalQCF.g:1486:3: ruleEString
            {
             before(grammarAccess.getCauseAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCauseAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__NameAssignment_2"


    // $ANTLR start "rule__Cause__DescriptionAssignment_3_1"
    // InternalQCF.g:1495:1: rule__Cause__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Cause__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1499:1: ( ( ruleEString ) )
            // InternalQCF.g:1500:2: ( ruleEString )
            {
            // InternalQCF.g:1500:2: ( ruleEString )
            // InternalQCF.g:1501:3: ruleEString
            {
             before(grammarAccess.getCauseAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCauseAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__DescriptionAssignment_3_1"


    // $ANTLR start "rule__Cause__ValueOfInterestAssignment_4_1"
    // InternalQCF.g:1510:1: rule__Cause__ValueOfInterestAssignment_4_1 : ( ruleEString ) ;
    public final void rule__Cause__ValueOfInterestAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1514:1: ( ( ruleEString ) )
            // InternalQCF.g:1515:2: ( ruleEString )
            {
            // InternalQCF.g:1515:2: ( ruleEString )
            // InternalQCF.g:1516:3: ruleEString
            {
             before(grammarAccess.getCauseAccess().getValueOfInterestEStringParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCauseAccess().getValueOfInterestEStringParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__ValueOfInterestAssignment_4_1"


    // $ANTLR start "rule__Cause__SubCausesAssignment_5_2"
    // InternalQCF.g:1525:1: rule__Cause__SubCausesAssignment_5_2 : ( ruleCause ) ;
    public final void rule__Cause__SubCausesAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1529:1: ( ( ruleCause ) )
            // InternalQCF.g:1530:2: ( ruleCause )
            {
            // InternalQCF.g:1530:2: ( ruleCause )
            // InternalQCF.g:1531:3: ruleCause
            {
             before(grammarAccess.getCauseAccess().getSubCausesCauseParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCauseAccess().getSubCausesCauseParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__SubCausesAssignment_5_2"


    // $ANTLR start "rule__Cause__SubCausesAssignment_5_3"
    // InternalQCF.g:1540:1: rule__Cause__SubCausesAssignment_5_3 : ( ruleCause ) ;
    public final void rule__Cause__SubCausesAssignment_5_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1544:1: ( ( ruleCause ) )
            // InternalQCF.g:1545:2: ( ruleCause )
            {
            // InternalQCF.g:1545:2: ( ruleCause )
            // InternalQCF.g:1546:3: ruleCause
            {
             before(grammarAccess.getCauseAccess().getSubCausesCauseParserRuleCall_5_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCauseAccess().getSubCausesCauseParserRuleCall_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__SubCausesAssignment_5_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000C8000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000220000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400002L});

}