package es.unican.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import es.unican.services.DOFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDOFParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'count'", "'sum'", "'avg'", "'max'", "'min'", "'dof'", "'import'", "'effect'", "'is'", "'category'", "'include'", "'by'", "'{'", "'}'", "'calculate'", "'as'", "'('", "')'", "'only_as'", "'['", "']'", "','", "'where'", "'and'", "'or'", "'='", "'!='", "'>'", "'>='", "'<'", "'<='", "'cause'", "'contains'", "'realizes'", "'notMapped'", "'.*'", "'.'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalDOFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDOFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDOFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDOF.g"; }


    	private DOFGrammarAccess grammarAccess;

    	public void setGrammarAccess(DOFGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDOF"
    // InternalDOF.g:53:1: entryRuleDOF : ruleDOF EOF ;
    public final void entryRuleDOF() throws RecognitionException {
        try {
            // InternalDOF.g:54:1: ( ruleDOF EOF )
            // InternalDOF.g:55:1: ruleDOF EOF
            {
             before(grammarAccess.getDOFRule()); 
            pushFollow(FOLLOW_1);
            ruleDOF();

            state._fsp--;

             after(grammarAccess.getDOFRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDOF"


    // $ANTLR start "ruleDOF"
    // InternalDOF.g:62:1: ruleDOF : ( ( rule__DOF__Group__0 ) ) ;
    public final void ruleDOF() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:66:2: ( ( ( rule__DOF__Group__0 ) ) )
            // InternalDOF.g:67:2: ( ( rule__DOF__Group__0 ) )
            {
            // InternalDOF.g:67:2: ( ( rule__DOF__Group__0 ) )
            // InternalDOF.g:68:3: ( rule__DOF__Group__0 )
            {
             before(grammarAccess.getDOFAccess().getGroup()); 
            // InternalDOF.g:69:3: ( rule__DOF__Group__0 )
            // InternalDOF.g:69:4: rule__DOF__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DOF__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDOFAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDOF"


    // $ANTLR start "entryRuleImport"
    // InternalDOF.g:78:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalDOF.g:79:1: ( ruleImport EOF )
            // InternalDOF.g:80:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalDOF.g:87:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:91:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalDOF.g:92:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalDOF.g:92:2: ( ( rule__Import__Group__0 ) )
            // InternalDOF.g:93:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalDOF.g:94:3: ( rule__Import__Group__0 )
            // InternalDOF.g:94:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleEffect"
    // InternalDOF.g:103:1: entryRuleEffect : ruleEffect EOF ;
    public final void entryRuleEffect() throws RecognitionException {
        try {
            // InternalDOF.g:104:1: ( ruleEffect EOF )
            // InternalDOF.g:105:1: ruleEffect EOF
            {
             before(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getEffectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalDOF.g:112:1: ruleEffect : ( ( rule__Effect__Group__0 ) ) ;
    public final void ruleEffect() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:116:2: ( ( ( rule__Effect__Group__0 ) ) )
            // InternalDOF.g:117:2: ( ( rule__Effect__Group__0 ) )
            {
            // InternalDOF.g:117:2: ( ( rule__Effect__Group__0 ) )
            // InternalDOF.g:118:3: ( rule__Effect__Group__0 )
            {
             before(grammarAccess.getEffectAccess().getGroup()); 
            // InternalDOF.g:119:3: ( rule__Effect__Group__0 )
            // InternalDOF.g:119:4: rule__Effect__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalDOF.g:128:1: entryRuleCategory : ruleCategory EOF ;
    public final void entryRuleCategory() throws RecognitionException {
        try {
            // InternalDOF.g:129:1: ( ruleCategory EOF )
            // InternalDOF.g:130:1: ruleCategory EOF
            {
             before(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getCategoryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalDOF.g:137:1: ruleCategory : ( ( rule__Category__Group__0 ) ) ;
    public final void ruleCategory() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:141:2: ( ( ( rule__Category__Group__0 ) ) )
            // InternalDOF.g:142:2: ( ( rule__Category__Group__0 ) )
            {
            // InternalDOF.g:142:2: ( ( rule__Category__Group__0 ) )
            // InternalDOF.g:143:3: ( rule__Category__Group__0 )
            {
             before(grammarAccess.getCategoryAccess().getGroup()); 
            // InternalDOF.g:144:3: ( rule__Category__Group__0 )
            // InternalDOF.g:144:4: rule__Category__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleDataFeeder"
    // InternalDOF.g:153:1: entryRuleDataFeeder : ruleDataFeeder EOF ;
    public final void entryRuleDataFeeder() throws RecognitionException {
        try {
            // InternalDOF.g:154:1: ( ruleDataFeeder EOF )
            // InternalDOF.g:155:1: ruleDataFeeder EOF
            {
             before(grammarAccess.getDataFeederRule()); 
            pushFollow(FOLLOW_1);
            ruleDataFeeder();

            state._fsp--;

             after(grammarAccess.getDataFeederRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataFeeder"


    // $ANTLR start "ruleDataFeeder"
    // InternalDOF.g:162:1: ruleDataFeeder : ( ( rule__DataFeeder__Group__0 ) ) ;
    public final void ruleDataFeeder() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:166:2: ( ( ( rule__DataFeeder__Group__0 ) ) )
            // InternalDOF.g:167:2: ( ( rule__DataFeeder__Group__0 ) )
            {
            // InternalDOF.g:167:2: ( ( rule__DataFeeder__Group__0 ) )
            // InternalDOF.g:168:3: ( rule__DataFeeder__Group__0 )
            {
             before(grammarAccess.getDataFeederAccess().getGroup()); 
            // InternalDOF.g:169:3: ( rule__DataFeeder__Group__0 )
            // InternalDOF.g:169:4: rule__DataFeeder__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataFeederAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataFeeder"


    // $ANTLR start "entryRuleIncludedReference"
    // InternalDOF.g:178:1: entryRuleIncludedReference : ruleIncludedReference EOF ;
    public final void entryRuleIncludedReference() throws RecognitionException {
        try {
            // InternalDOF.g:179:1: ( ruleIncludedReference EOF )
            // InternalDOF.g:180:1: ruleIncludedReference EOF
            {
             before(grammarAccess.getIncludedReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getIncludedReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIncludedReference"


    // $ANTLR start "ruleIncludedReference"
    // InternalDOF.g:187:1: ruleIncludedReference : ( ( rule__IncludedReference__Alternatives ) ) ;
    public final void ruleIncludedReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:191:2: ( ( ( rule__IncludedReference__Alternatives ) ) )
            // InternalDOF.g:192:2: ( ( rule__IncludedReference__Alternatives ) )
            {
            // InternalDOF.g:192:2: ( ( rule__IncludedReference__Alternatives ) )
            // InternalDOF.g:193:3: ( rule__IncludedReference__Alternatives )
            {
             before(grammarAccess.getIncludedReferenceAccess().getAlternatives()); 
            // InternalDOF.g:194:3: ( rule__IncludedReference__Alternatives )
            // InternalDOF.g:194:4: rule__IncludedReference__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__IncludedReference__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getIncludedReferenceAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIncludedReference"


    // $ANTLR start "entryRuleSimpleReference"
    // InternalDOF.g:203:1: entryRuleSimpleReference : ruleSimpleReference EOF ;
    public final void entryRuleSimpleReference() throws RecognitionException {
        try {
            // InternalDOF.g:204:1: ( ruleSimpleReference EOF )
            // InternalDOF.g:205:1: ruleSimpleReference EOF
            {
             before(grammarAccess.getSimpleReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleSimpleReference();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleReference"


    // $ANTLR start "ruleSimpleReference"
    // InternalDOF.g:212:1: ruleSimpleReference : ( ( rule__SimpleReference__Group__0 ) ) ;
    public final void ruleSimpleReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:216:2: ( ( ( rule__SimpleReference__Group__0 ) ) )
            // InternalDOF.g:217:2: ( ( rule__SimpleReference__Group__0 ) )
            {
            // InternalDOF.g:217:2: ( ( rule__SimpleReference__Group__0 ) )
            // InternalDOF.g:218:3: ( rule__SimpleReference__Group__0 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getGroup()); 
            // InternalDOF.g:219:3: ( rule__SimpleReference__Group__0 )
            // InternalDOF.g:219:4: rule__SimpleReference__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleReference"


    // $ANTLR start "entryRuleAggregatedReference"
    // InternalDOF.g:228:1: entryRuleAggregatedReference : ruleAggregatedReference EOF ;
    public final void entryRuleAggregatedReference() throws RecognitionException {
        try {
            // InternalDOF.g:229:1: ( ruleAggregatedReference EOF )
            // InternalDOF.g:230:1: ruleAggregatedReference EOF
            {
             before(grammarAccess.getAggregatedReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleAggregatedReference();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAggregatedReference"


    // $ANTLR start "ruleAggregatedReference"
    // InternalDOF.g:237:1: ruleAggregatedReference : ( ( rule__AggregatedReference__Group__0 ) ) ;
    public final void ruleAggregatedReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:241:2: ( ( ( rule__AggregatedReference__Group__0 ) ) )
            // InternalDOF.g:242:2: ( ( rule__AggregatedReference__Group__0 ) )
            {
            // InternalDOF.g:242:2: ( ( rule__AggregatedReference__Group__0 ) )
            // InternalDOF.g:243:3: ( rule__AggregatedReference__Group__0 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getGroup()); 
            // InternalDOF.g:244:3: ( rule__AggregatedReference__Group__0 )
            // InternalDOF.g:244:4: rule__AggregatedReference__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAggregatedReference"


    // $ANTLR start "entryRuleAggFunction"
    // InternalDOF.g:253:1: entryRuleAggFunction : ruleAggFunction EOF ;
    public final void entryRuleAggFunction() throws RecognitionException {
        try {
            // InternalDOF.g:254:1: ( ruleAggFunction EOF )
            // InternalDOF.g:255:1: ruleAggFunction EOF
            {
             before(grammarAccess.getAggFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleAggFunction();

            state._fsp--;

             after(grammarAccess.getAggFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAggFunction"


    // $ANTLR start "ruleAggFunction"
    // InternalDOF.g:262:1: ruleAggFunction : ( ( rule__AggFunction__Alternatives ) ) ;
    public final void ruleAggFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:266:2: ( ( ( rule__AggFunction__Alternatives ) ) )
            // InternalDOF.g:267:2: ( ( rule__AggFunction__Alternatives ) )
            {
            // InternalDOF.g:267:2: ( ( rule__AggFunction__Alternatives ) )
            // InternalDOF.g:268:3: ( rule__AggFunction__Alternatives )
            {
             before(grammarAccess.getAggFunctionAccess().getAlternatives()); 
            // InternalDOF.g:269:3: ( rule__AggFunction__Alternatives )
            // InternalDOF.g:269:4: rule__AggFunction__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AggFunction__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAggFunctionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAggFunction"


    // $ANTLR start "entryRuleTypeFilter"
    // InternalDOF.g:278:1: entryRuleTypeFilter : ruleTypeFilter EOF ;
    public final void entryRuleTypeFilter() throws RecognitionException {
        try {
            // InternalDOF.g:279:1: ( ruleTypeFilter EOF )
            // InternalDOF.g:280:1: ruleTypeFilter EOF
            {
             before(grammarAccess.getTypeFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getTypeFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeFilter"


    // $ANTLR start "ruleTypeFilter"
    // InternalDOF.g:287:1: ruleTypeFilter : ( ( rule__TypeFilter__Alternatives ) ) ;
    public final void ruleTypeFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:291:2: ( ( ( rule__TypeFilter__Alternatives ) ) )
            // InternalDOF.g:292:2: ( ( rule__TypeFilter__Alternatives ) )
            {
            // InternalDOF.g:292:2: ( ( rule__TypeFilter__Alternatives ) )
            // InternalDOF.g:293:3: ( rule__TypeFilter__Alternatives )
            {
             before(grammarAccess.getTypeFilterAccess().getAlternatives()); 
            // InternalDOF.g:294:3: ( rule__TypeFilter__Alternatives )
            // InternalDOF.g:294:4: rule__TypeFilter__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TypeFilter__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeFilterAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeFilter"


    // $ANTLR start "entryRuleTypeCompletion"
    // InternalDOF.g:303:1: entryRuleTypeCompletion : ruleTypeCompletion EOF ;
    public final void entryRuleTypeCompletion() throws RecognitionException {
        try {
            // InternalDOF.g:304:1: ( ruleTypeCompletion EOF )
            // InternalDOF.g:305:1: ruleTypeCompletion EOF
            {
             before(grammarAccess.getTypeCompletionRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeCompletion();

            state._fsp--;

             after(grammarAccess.getTypeCompletionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeCompletion"


    // $ANTLR start "ruleTypeCompletion"
    // InternalDOF.g:312:1: ruleTypeCompletion : ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) ) ;
    public final void ruleTypeCompletion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:316:2: ( ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) ) )
            // InternalDOF.g:317:2: ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) )
            {
            // InternalDOF.g:317:2: ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) )
            // InternalDOF.g:318:3: ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* )
            {
            // InternalDOF.g:318:3: ( ( rule__TypeCompletion__Group__0 ) )
            // InternalDOF.g:319:4: ( rule__TypeCompletion__Group__0 )
            {
             before(grammarAccess.getTypeCompletionAccess().getGroup()); 
            // InternalDOF.g:320:4: ( rule__TypeCompletion__Group__0 )
            // InternalDOF.g:320:5: rule__TypeCompletion__Group__0
            {
            pushFollow(FOLLOW_3);
            rule__TypeCompletion__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeCompletionAccess().getGroup()); 

            }

            // InternalDOF.g:323:3: ( ( rule__TypeCompletion__Group__0 )* )
            // InternalDOF.g:324:4: ( rule__TypeCompletion__Group__0 )*
            {
             before(grammarAccess.getTypeCompletionAccess().getGroup()); 
            // InternalDOF.g:325:4: ( rule__TypeCompletion__Group__0 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==26) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalDOF.g:325:5: rule__TypeCompletion__Group__0
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__TypeCompletion__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getTypeCompletionAccess().getGroup()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeCompletion"


    // $ANTLR start "entryRuleTypeSelection"
    // InternalDOF.g:335:1: entryRuleTypeSelection : ruleTypeSelection EOF ;
    public final void entryRuleTypeSelection() throws RecognitionException {
        try {
            // InternalDOF.g:336:1: ( ruleTypeSelection EOF )
            // InternalDOF.g:337:1: ruleTypeSelection EOF
            {
             before(grammarAccess.getTypeSelectionRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeSelection();

            state._fsp--;

             after(grammarAccess.getTypeSelectionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeSelection"


    // $ANTLR start "ruleTypeSelection"
    // InternalDOF.g:344:1: ruleTypeSelection : ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) ) ;
    public final void ruleTypeSelection() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:348:2: ( ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) ) )
            // InternalDOF.g:349:2: ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) )
            {
            // InternalDOF.g:349:2: ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) )
            // InternalDOF.g:350:3: ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* )
            {
            // InternalDOF.g:350:3: ( ( rule__TypeSelection__Group__0 ) )
            // InternalDOF.g:351:4: ( rule__TypeSelection__Group__0 )
            {
             before(grammarAccess.getTypeSelectionAccess().getGroup()); 
            // InternalDOF.g:352:4: ( rule__TypeSelection__Group__0 )
            // InternalDOF.g:352:5: rule__TypeSelection__Group__0
            {
            pushFollow(FOLLOW_4);
            rule__TypeSelection__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeSelectionAccess().getGroup()); 

            }

            // InternalDOF.g:355:3: ( ( rule__TypeSelection__Group__0 )* )
            // InternalDOF.g:356:4: ( rule__TypeSelection__Group__0 )*
            {
             before(grammarAccess.getTypeSelectionAccess().getGroup()); 
            // InternalDOF.g:357:4: ( rule__TypeSelection__Group__0 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==29) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalDOF.g:357:5: rule__TypeSelection__Group__0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__TypeSelection__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getTypeSelectionAccess().getGroup()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeSelection"


    // $ANTLR start "entryRuleTypeCustomization"
    // InternalDOF.g:367:1: entryRuleTypeCustomization : ruleTypeCustomization EOF ;
    public final void entryRuleTypeCustomization() throws RecognitionException {
        try {
            // InternalDOF.g:368:1: ( ruleTypeCustomization EOF )
            // InternalDOF.g:369:1: ruleTypeCustomization EOF
            {
             before(grammarAccess.getTypeCustomizationRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeCustomization();

            state._fsp--;

             after(grammarAccess.getTypeCustomizationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeCustomization"


    // $ANTLR start "ruleTypeCustomization"
    // InternalDOF.g:376:1: ruleTypeCustomization : ( ( rule__TypeCustomization__Group__0 ) ) ;
    public final void ruleTypeCustomization() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:380:2: ( ( ( rule__TypeCustomization__Group__0 ) ) )
            // InternalDOF.g:381:2: ( ( rule__TypeCustomization__Group__0 ) )
            {
            // InternalDOF.g:381:2: ( ( rule__TypeCustomization__Group__0 ) )
            // InternalDOF.g:382:3: ( rule__TypeCustomization__Group__0 )
            {
             before(grammarAccess.getTypeCustomizationAccess().getGroup()); 
            // InternalDOF.g:383:3: ( rule__TypeCustomization__Group__0 )
            // InternalDOF.g:383:4: rule__TypeCustomization__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeCustomizationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeCustomization"


    // $ANTLR start "entryRuleAttributeFilter"
    // InternalDOF.g:392:1: entryRuleAttributeFilter : ruleAttributeFilter EOF ;
    public final void entryRuleAttributeFilter() throws RecognitionException {
        try {
            // InternalDOF.g:393:1: ( ruleAttributeFilter EOF )
            // InternalDOF.g:394:1: ruleAttributeFilter EOF
            {
             before(grammarAccess.getAttributeFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getAttributeFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttributeFilter"


    // $ANTLR start "ruleAttributeFilter"
    // InternalDOF.g:401:1: ruleAttributeFilter : ( ( rule__AttributeFilter__Group__0 ) ) ;
    public final void ruleAttributeFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:405:2: ( ( ( rule__AttributeFilter__Group__0 ) ) )
            // InternalDOF.g:406:2: ( ( rule__AttributeFilter__Group__0 ) )
            {
            // InternalDOF.g:406:2: ( ( rule__AttributeFilter__Group__0 ) )
            // InternalDOF.g:407:3: ( rule__AttributeFilter__Group__0 )
            {
             before(grammarAccess.getAttributeFilterAccess().getGroup()); 
            // InternalDOF.g:408:3: ( rule__AttributeFilter__Group__0 )
            // InternalDOF.g:408:4: rule__AttributeFilter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeFilterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttributeFilter"


    // $ANTLR start "entryRuleInstancesFilter"
    // InternalDOF.g:417:1: entryRuleInstancesFilter : ruleInstancesFilter EOF ;
    public final void entryRuleInstancesFilter() throws RecognitionException {
        try {
            // InternalDOF.g:418:1: ( ruleInstancesFilter EOF )
            // InternalDOF.g:419:1: ruleInstancesFilter EOF
            {
             before(grammarAccess.getInstancesFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getInstancesFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInstancesFilter"


    // $ANTLR start "ruleInstancesFilter"
    // InternalDOF.g:426:1: ruleInstancesFilter : ( ( rule__InstancesFilter__Group__0 ) ) ;
    public final void ruleInstancesFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:430:2: ( ( ( rule__InstancesFilter__Group__0 ) ) )
            // InternalDOF.g:431:2: ( ( rule__InstancesFilter__Group__0 ) )
            {
            // InternalDOF.g:431:2: ( ( rule__InstancesFilter__Group__0 ) )
            // InternalDOF.g:432:3: ( rule__InstancesFilter__Group__0 )
            {
             before(grammarAccess.getInstancesFilterAccess().getGroup()); 
            // InternalDOF.g:433:3: ( rule__InstancesFilter__Group__0 )
            // InternalDOF.g:433:4: rule__InstancesFilter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InstancesFilter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInstancesFilterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInstancesFilter"


    // $ANTLR start "entryRuleAndConjunction"
    // InternalDOF.g:442:1: entryRuleAndConjunction : ruleAndConjunction EOF ;
    public final void entryRuleAndConjunction() throws RecognitionException {
        try {
            // InternalDOF.g:443:1: ( ruleAndConjunction EOF )
            // InternalDOF.g:444:1: ruleAndConjunction EOF
            {
             before(grammarAccess.getAndConjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleAndConjunction();

            state._fsp--;

             after(grammarAccess.getAndConjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAndConjunction"


    // $ANTLR start "ruleAndConjunction"
    // InternalDOF.g:451:1: ruleAndConjunction : ( ( rule__AndConjunction__Group__0 ) ) ;
    public final void ruleAndConjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:455:2: ( ( ( rule__AndConjunction__Group__0 ) ) )
            // InternalDOF.g:456:2: ( ( rule__AndConjunction__Group__0 ) )
            {
            // InternalDOF.g:456:2: ( ( rule__AndConjunction__Group__0 ) )
            // InternalDOF.g:457:3: ( rule__AndConjunction__Group__0 )
            {
             before(grammarAccess.getAndConjunctionAccess().getGroup()); 
            // InternalDOF.g:458:3: ( rule__AndConjunction__Group__0 )
            // InternalDOF.g:458:4: rule__AndConjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndConjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAndConjunction"


    // $ANTLR start "entryRuleOrConjunction"
    // InternalDOF.g:467:1: entryRuleOrConjunction : ruleOrConjunction EOF ;
    public final void entryRuleOrConjunction() throws RecognitionException {
        try {
            // InternalDOF.g:468:1: ( ruleOrConjunction EOF )
            // InternalDOF.g:469:1: ruleOrConjunction EOF
            {
             before(grammarAccess.getOrConjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleOrConjunction();

            state._fsp--;

             after(grammarAccess.getOrConjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrConjunction"


    // $ANTLR start "ruleOrConjunction"
    // InternalDOF.g:476:1: ruleOrConjunction : ( ( rule__OrConjunction__Group__0 ) ) ;
    public final void ruleOrConjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:480:2: ( ( ( rule__OrConjunction__Group__0 ) ) )
            // InternalDOF.g:481:2: ( ( rule__OrConjunction__Group__0 ) )
            {
            // InternalDOF.g:481:2: ( ( rule__OrConjunction__Group__0 ) )
            // InternalDOF.g:482:3: ( rule__OrConjunction__Group__0 )
            {
             before(grammarAccess.getOrConjunctionAccess().getGroup()); 
            // InternalDOF.g:483:3: ( rule__OrConjunction__Group__0 )
            // InternalDOF.g:483:4: rule__OrConjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrConjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrConjunction"


    // $ANTLR start "entryRulePrimary"
    // InternalDOF.g:492:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalDOF.g:493:1: ( rulePrimary EOF )
            // InternalDOF.g:494:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalDOF.g:501:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:505:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalDOF.g:506:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalDOF.g:506:2: ( ( rule__Primary__Alternatives ) )
            // InternalDOF.g:507:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalDOF.g:508:3: ( rule__Primary__Alternatives )
            // InternalDOF.g:508:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleComparison"
    // InternalDOF.g:517:1: entryRuleComparison : ruleComparison EOF ;
    public final void entryRuleComparison() throws RecognitionException {
        try {
            // InternalDOF.g:518:1: ( ruleComparison EOF )
            // InternalDOF.g:519:1: ruleComparison EOF
            {
             before(grammarAccess.getComparisonRule()); 
            pushFollow(FOLLOW_1);
            ruleComparison();

            state._fsp--;

             after(grammarAccess.getComparisonRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // InternalDOF.g:526:1: ruleComparison : ( ( rule__Comparison__Alternatives ) ) ;
    public final void ruleComparison() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:530:2: ( ( ( rule__Comparison__Alternatives ) ) )
            // InternalDOF.g:531:2: ( ( rule__Comparison__Alternatives ) )
            {
            // InternalDOF.g:531:2: ( ( rule__Comparison__Alternatives ) )
            // InternalDOF.g:532:3: ( rule__Comparison__Alternatives )
            {
             before(grammarAccess.getComparisonAccess().getAlternatives()); 
            // InternalDOF.g:533:3: ( rule__Comparison__Alternatives )
            // InternalDOF.g:533:4: rule__Comparison__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRuleCause"
    // InternalDOF.g:542:1: entryRuleCause : ruleCause EOF ;
    public final void entryRuleCause() throws RecognitionException {
        try {
            // InternalDOF.g:543:1: ( ruleCause EOF )
            // InternalDOF.g:544:1: ruleCause EOF
            {
             before(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalDOF.g:551:1: ruleCause : ( ( rule__Cause__Alternatives ) ) ;
    public final void ruleCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:555:2: ( ( ( rule__Cause__Alternatives ) ) )
            // InternalDOF.g:556:2: ( ( rule__Cause__Alternatives ) )
            {
            // InternalDOF.g:556:2: ( ( rule__Cause__Alternatives ) )
            // InternalDOF.g:557:3: ( rule__Cause__Alternatives )
            {
             before(grammarAccess.getCauseAccess().getAlternatives()); 
            // InternalDOF.g:558:3: ( rule__Cause__Alternatives )
            // InternalDOF.g:558:4: rule__Cause__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleCompoundCause"
    // InternalDOF.g:567:1: entryRuleCompoundCause : ruleCompoundCause EOF ;
    public final void entryRuleCompoundCause() throws RecognitionException {
        try {
            // InternalDOF.g:568:1: ( ruleCompoundCause EOF )
            // InternalDOF.g:569:1: ruleCompoundCause EOF
            {
             before(grammarAccess.getCompoundCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleCompoundCause();

            state._fsp--;

             after(grammarAccess.getCompoundCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCompoundCause"


    // $ANTLR start "ruleCompoundCause"
    // InternalDOF.g:576:1: ruleCompoundCause : ( ( rule__CompoundCause__Group__0 ) ) ;
    public final void ruleCompoundCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:580:2: ( ( ( rule__CompoundCause__Group__0 ) ) )
            // InternalDOF.g:581:2: ( ( rule__CompoundCause__Group__0 ) )
            {
            // InternalDOF.g:581:2: ( ( rule__CompoundCause__Group__0 ) )
            // InternalDOF.g:582:3: ( rule__CompoundCause__Group__0 )
            {
             before(grammarAccess.getCompoundCauseAccess().getGroup()); 
            // InternalDOF.g:583:3: ( rule__CompoundCause__Group__0 )
            // InternalDOF.g:583:4: rule__CompoundCause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCompoundCause"


    // $ANTLR start "entryRuleDataLinkedCause"
    // InternalDOF.g:592:1: entryRuleDataLinkedCause : ruleDataLinkedCause EOF ;
    public final void entryRuleDataLinkedCause() throws RecognitionException {
        try {
            // InternalDOF.g:593:1: ( ruleDataLinkedCause EOF )
            // InternalDOF.g:594:1: ruleDataLinkedCause EOF
            {
             before(grammarAccess.getDataLinkedCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleDataLinkedCause();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataLinkedCause"


    // $ANTLR start "ruleDataLinkedCause"
    // InternalDOF.g:601:1: ruleDataLinkedCause : ( ( rule__DataLinkedCause__Group__0 ) ) ;
    public final void ruleDataLinkedCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:605:2: ( ( ( rule__DataLinkedCause__Group__0 ) ) )
            // InternalDOF.g:606:2: ( ( rule__DataLinkedCause__Group__0 ) )
            {
            // InternalDOF.g:606:2: ( ( rule__DataLinkedCause__Group__0 ) )
            // InternalDOF.g:607:3: ( rule__DataLinkedCause__Group__0 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getGroup()); 
            // InternalDOF.g:608:3: ( rule__DataLinkedCause__Group__0 )
            // InternalDOF.g:608:4: rule__DataLinkedCause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataLinkedCause"


    // $ANTLR start "entryRuleNotMappedCause"
    // InternalDOF.g:617:1: entryRuleNotMappedCause : ruleNotMappedCause EOF ;
    public final void entryRuleNotMappedCause() throws RecognitionException {
        try {
            // InternalDOF.g:618:1: ( ruleNotMappedCause EOF )
            // InternalDOF.g:619:1: ruleNotMappedCause EOF
            {
             before(grammarAccess.getNotMappedCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleNotMappedCause();

            state._fsp--;

             after(grammarAccess.getNotMappedCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNotMappedCause"


    // $ANTLR start "ruleNotMappedCause"
    // InternalDOF.g:626:1: ruleNotMappedCause : ( ( rule__NotMappedCause__Group__0 ) ) ;
    public final void ruleNotMappedCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:630:2: ( ( ( rule__NotMappedCause__Group__0 ) ) )
            // InternalDOF.g:631:2: ( ( rule__NotMappedCause__Group__0 ) )
            {
            // InternalDOF.g:631:2: ( ( rule__NotMappedCause__Group__0 ) )
            // InternalDOF.g:632:3: ( rule__NotMappedCause__Group__0 )
            {
             before(grammarAccess.getNotMappedCauseAccess().getGroup()); 
            // InternalDOF.g:633:3: ( rule__NotMappedCause__Group__0 )
            // InternalDOF.g:633:4: rule__NotMappedCause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNotMappedCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNotMappedCause"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalDOF.g:642:1: entryRuleQualifiedNameWithWildcard : ruleQualifiedNameWithWildcard EOF ;
    public final void entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        try {
            // InternalDOF.g:643:1: ( ruleQualifiedNameWithWildcard EOF )
            // InternalDOF.g:644:1: ruleQualifiedNameWithWildcard EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalDOF.g:651:1: ruleQualifiedNameWithWildcard : ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) ;
    public final void ruleQualifiedNameWithWildcard() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:655:2: ( ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) )
            // InternalDOF.g:656:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            {
            // InternalDOF.g:656:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            // InternalDOF.g:657:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 
            // InternalDOF.g:658:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            // InternalDOF.g:658:4: rule__QualifiedNameWithWildcard__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalDOF.g:667:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalDOF.g:668:1: ( ruleQualifiedName EOF )
            // InternalDOF.g:669:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalDOF.g:676:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:680:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalDOF.g:681:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalDOF.g:681:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalDOF.g:682:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalDOF.g:683:3: ( rule__QualifiedName__Group__0 )
            // InternalDOF.g:683:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRulePath"
    // InternalDOF.g:692:1: entryRulePath : rulePath EOF ;
    public final void entryRulePath() throws RecognitionException {
        try {
            // InternalDOF.g:693:1: ( rulePath EOF )
            // InternalDOF.g:694:1: rulePath EOF
            {
             before(grammarAccess.getPathRule()); 
            pushFollow(FOLLOW_1);
            rulePath();

            state._fsp--;

             after(grammarAccess.getPathRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePath"


    // $ANTLR start "rulePath"
    // InternalDOF.g:701:1: rulePath : ( ( rule__Path__Group__0 ) ) ;
    public final void rulePath() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:705:2: ( ( ( rule__Path__Group__0 ) ) )
            // InternalDOF.g:706:2: ( ( rule__Path__Group__0 ) )
            {
            // InternalDOF.g:706:2: ( ( rule__Path__Group__0 ) )
            // InternalDOF.g:707:3: ( rule__Path__Group__0 )
            {
             before(grammarAccess.getPathAccess().getGroup()); 
            // InternalDOF.g:708:3: ( rule__Path__Group__0 )
            // InternalDOF.g:708:4: rule__Path__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Path__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPathAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePath"


    // $ANTLR start "entryRuleEString"
    // InternalDOF.g:717:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalDOF.g:718:1: ( ruleEString EOF )
            // InternalDOF.g:719:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDOF.g:726:1: ruleEString : ( RULE_STRING ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:730:2: ( ( RULE_STRING ) )
            // InternalDOF.g:731:2: ( RULE_STRING )
            {
            // InternalDOF.g:731:2: ( RULE_STRING )
            // InternalDOF.g:732:3: RULE_STRING
            {
             before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "rule__IncludedReference__Alternatives"
    // InternalDOF.g:741:1: rule__IncludedReference__Alternatives : ( ( ruleSimpleReference ) | ( ruleAggregatedReference ) );
    public final void rule__IncludedReference__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:745:1: ( ( ruleSimpleReference ) | ( ruleAggregatedReference ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            else if ( (LA3_0==25) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalDOF.g:746:2: ( ruleSimpleReference )
                    {
                    // InternalDOF.g:746:2: ( ruleSimpleReference )
                    // InternalDOF.g:747:3: ruleSimpleReference
                    {
                     before(grammarAccess.getIncludedReferenceAccess().getSimpleReferenceParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSimpleReference();

                    state._fsp--;

                     after(grammarAccess.getIncludedReferenceAccess().getSimpleReferenceParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:752:2: ( ruleAggregatedReference )
                    {
                    // InternalDOF.g:752:2: ( ruleAggregatedReference )
                    // InternalDOF.g:753:3: ruleAggregatedReference
                    {
                     before(grammarAccess.getIncludedReferenceAccess().getAggregatedReferenceParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAggregatedReference();

                    state._fsp--;

                     after(grammarAccess.getIncludedReferenceAccess().getAggregatedReferenceParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludedReference__Alternatives"


    // $ANTLR start "rule__AggFunction__Alternatives"
    // InternalDOF.g:762:1: rule__AggFunction__Alternatives : ( ( 'count' ) | ( 'sum' ) | ( 'avg' ) | ( 'max' ) | ( 'min' ) );
    public final void rule__AggFunction__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:766:1: ( ( 'count' ) | ( 'sum' ) | ( 'avg' ) | ( 'max' ) | ( 'min' ) )
            int alt4=5;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt4=1;
                }
                break;
            case 12:
                {
                alt4=2;
                }
                break;
            case 13:
                {
                alt4=3;
                }
                break;
            case 14:
                {
                alt4=4;
                }
                break;
            case 15:
                {
                alt4=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalDOF.g:767:2: ( 'count' )
                    {
                    // InternalDOF.g:767:2: ( 'count' )
                    // InternalDOF.g:768:3: 'count'
                    {
                     before(grammarAccess.getAggFunctionAccess().getCountKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getCountKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:773:2: ( 'sum' )
                    {
                    // InternalDOF.g:773:2: ( 'sum' )
                    // InternalDOF.g:774:3: 'sum'
                    {
                     before(grammarAccess.getAggFunctionAccess().getSumKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getSumKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDOF.g:779:2: ( 'avg' )
                    {
                    // InternalDOF.g:779:2: ( 'avg' )
                    // InternalDOF.g:780:3: 'avg'
                    {
                     before(grammarAccess.getAggFunctionAccess().getAvgKeyword_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getAvgKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDOF.g:785:2: ( 'max' )
                    {
                    // InternalDOF.g:785:2: ( 'max' )
                    // InternalDOF.g:786:3: 'max'
                    {
                     before(grammarAccess.getAggFunctionAccess().getMaxKeyword_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getMaxKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDOF.g:791:2: ( 'min' )
                    {
                    // InternalDOF.g:791:2: ( 'min' )
                    // InternalDOF.g:792:3: 'min'
                    {
                     before(grammarAccess.getAggFunctionAccess().getMinKeyword_4()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getMinKeyword_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggFunction__Alternatives"


    // $ANTLR start "rule__TypeFilter__Alternatives"
    // InternalDOF.g:801:1: rule__TypeFilter__Alternatives : ( ( ruleTypeCompletion ) | ( ruleTypeSelection ) );
    public final void rule__TypeFilter__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:805:1: ( ( ruleTypeCompletion ) | ( ruleTypeSelection ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==26) ) {
                alt5=1;
            }
            else if ( (LA5_0==29) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalDOF.g:806:2: ( ruleTypeCompletion )
                    {
                    // InternalDOF.g:806:2: ( ruleTypeCompletion )
                    // InternalDOF.g:807:3: ruleTypeCompletion
                    {
                     before(grammarAccess.getTypeFilterAccess().getTypeCompletionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleTypeCompletion();

                    state._fsp--;

                     after(grammarAccess.getTypeFilterAccess().getTypeCompletionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:812:2: ( ruleTypeSelection )
                    {
                    // InternalDOF.g:812:2: ( ruleTypeSelection )
                    // InternalDOF.g:813:3: ruleTypeSelection
                    {
                     before(grammarAccess.getTypeFilterAccess().getTypeSelectionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleTypeSelection();

                    state._fsp--;

                     after(grammarAccess.getTypeFilterAccess().getTypeSelectionParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeFilter__Alternatives"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalDOF.g:822:1: rule__Primary__Alternatives : ( ( ruleComparison ) | ( ( rule__Primary__Group_1__0 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:826:1: ( ( ruleComparison ) | ( ( rule__Primary__Group_1__0 ) ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID) ) {
                alt6=1;
            }
            else if ( (LA6_0==27) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalDOF.g:827:2: ( ruleComparison )
                    {
                    // InternalDOF.g:827:2: ( ruleComparison )
                    // InternalDOF.g:828:3: ruleComparison
                    {
                     before(grammarAccess.getPrimaryAccess().getComparisonParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleComparison();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getComparisonParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:833:2: ( ( rule__Primary__Group_1__0 ) )
                    {
                    // InternalDOF.g:833:2: ( ( rule__Primary__Group_1__0 ) )
                    // InternalDOF.g:834:3: ( rule__Primary__Group_1__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_1()); 
                    // InternalDOF.g:835:3: ( rule__Primary__Group_1__0 )
                    // InternalDOF.g:835:4: rule__Primary__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Comparison__Alternatives"
    // InternalDOF.g:843:1: rule__Comparison__Alternatives : ( ( ( rule__Comparison__Group_0__0 ) ) | ( ( rule__Comparison__Group_1__0 ) ) | ( ( rule__Comparison__Group_2__0 ) ) | ( ( rule__Comparison__Group_3__0 ) ) | ( ( rule__Comparison__Group_4__0 ) ) | ( ( rule__Comparison__Group_5__0 ) ) );
    public final void rule__Comparison__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:847:1: ( ( ( rule__Comparison__Group_0__0 ) ) | ( ( rule__Comparison__Group_1__0 ) ) | ( ( rule__Comparison__Group_2__0 ) ) | ( ( rule__Comparison__Group_3__0 ) ) | ( ( rule__Comparison__Group_4__0 ) ) | ( ( rule__Comparison__Group_5__0 ) ) )
            int alt7=6;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // InternalDOF.g:848:2: ( ( rule__Comparison__Group_0__0 ) )
                    {
                    // InternalDOF.g:848:2: ( ( rule__Comparison__Group_0__0 ) )
                    // InternalDOF.g:849:3: ( rule__Comparison__Group_0__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_0()); 
                    // InternalDOF.g:850:3: ( rule__Comparison__Group_0__0 )
                    // InternalDOF.g:850:4: rule__Comparison__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:854:2: ( ( rule__Comparison__Group_1__0 ) )
                    {
                    // InternalDOF.g:854:2: ( ( rule__Comparison__Group_1__0 ) )
                    // InternalDOF.g:855:3: ( rule__Comparison__Group_1__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_1()); 
                    // InternalDOF.g:856:3: ( rule__Comparison__Group_1__0 )
                    // InternalDOF.g:856:4: rule__Comparison__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDOF.g:860:2: ( ( rule__Comparison__Group_2__0 ) )
                    {
                    // InternalDOF.g:860:2: ( ( rule__Comparison__Group_2__0 ) )
                    // InternalDOF.g:861:3: ( rule__Comparison__Group_2__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_2()); 
                    // InternalDOF.g:862:3: ( rule__Comparison__Group_2__0 )
                    // InternalDOF.g:862:4: rule__Comparison__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDOF.g:866:2: ( ( rule__Comparison__Group_3__0 ) )
                    {
                    // InternalDOF.g:866:2: ( ( rule__Comparison__Group_3__0 ) )
                    // InternalDOF.g:867:3: ( rule__Comparison__Group_3__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_3()); 
                    // InternalDOF.g:868:3: ( rule__Comparison__Group_3__0 )
                    // InternalDOF.g:868:4: rule__Comparison__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDOF.g:872:2: ( ( rule__Comparison__Group_4__0 ) )
                    {
                    // InternalDOF.g:872:2: ( ( rule__Comparison__Group_4__0 ) )
                    // InternalDOF.g:873:3: ( rule__Comparison__Group_4__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_4()); 
                    // InternalDOF.g:874:3: ( rule__Comparison__Group_4__0 )
                    // InternalDOF.g:874:4: rule__Comparison__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalDOF.g:878:2: ( ( rule__Comparison__Group_5__0 ) )
                    {
                    // InternalDOF.g:878:2: ( ( rule__Comparison__Group_5__0 ) )
                    // InternalDOF.g:879:3: ( rule__Comparison__Group_5__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_5()); 
                    // InternalDOF.g:880:3: ( rule__Comparison__Group_5__0 )
                    // InternalDOF.g:880:4: rule__Comparison__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Alternatives"


    // $ANTLR start "rule__Cause__Alternatives"
    // InternalDOF.g:888:1: rule__Cause__Alternatives : ( ( ruleCompoundCause ) | ( ruleDataLinkedCause ) | ( ruleNotMappedCause ) );
    public final void rule__Cause__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:892:1: ( ( ruleCompoundCause ) | ( ruleDataLinkedCause ) | ( ruleNotMappedCause ) )
            int alt8=3;
            alt8 = dfa8.predict(input);
            switch (alt8) {
                case 1 :
                    // InternalDOF.g:893:2: ( ruleCompoundCause )
                    {
                    // InternalDOF.g:893:2: ( ruleCompoundCause )
                    // InternalDOF.g:894:3: ruleCompoundCause
                    {
                     before(grammarAccess.getCauseAccess().getCompoundCauseParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleCompoundCause();

                    state._fsp--;

                     after(grammarAccess.getCauseAccess().getCompoundCauseParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:899:2: ( ruleDataLinkedCause )
                    {
                    // InternalDOF.g:899:2: ( ruleDataLinkedCause )
                    // InternalDOF.g:900:3: ruleDataLinkedCause
                    {
                     before(grammarAccess.getCauseAccess().getDataLinkedCauseParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleDataLinkedCause();

                    state._fsp--;

                     after(grammarAccess.getCauseAccess().getDataLinkedCauseParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDOF.g:905:2: ( ruleNotMappedCause )
                    {
                    // InternalDOF.g:905:2: ( ruleNotMappedCause )
                    // InternalDOF.g:906:3: ruleNotMappedCause
                    {
                     before(grammarAccess.getCauseAccess().getNotMappedCauseParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleNotMappedCause();

                    state._fsp--;

                     after(grammarAccess.getCauseAccess().getNotMappedCauseParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Alternatives"


    // $ANTLR start "rule__DOF__Group__0"
    // InternalDOF.g:915:1: rule__DOF__Group__0 : rule__DOF__Group__0__Impl rule__DOF__Group__1 ;
    public final void rule__DOF__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:919:1: ( rule__DOF__Group__0__Impl rule__DOF__Group__1 )
            // InternalDOF.g:920:2: rule__DOF__Group__0__Impl rule__DOF__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__DOF__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DOF__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__0"


    // $ANTLR start "rule__DOF__Group__0__Impl"
    // InternalDOF.g:927:1: rule__DOF__Group__0__Impl : ( ( rule__DOF__ImportsAssignment_0 )* ) ;
    public final void rule__DOF__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:931:1: ( ( ( rule__DOF__ImportsAssignment_0 )* ) )
            // InternalDOF.g:932:1: ( ( rule__DOF__ImportsAssignment_0 )* )
            {
            // InternalDOF.g:932:1: ( ( rule__DOF__ImportsAssignment_0 )* )
            // InternalDOF.g:933:2: ( rule__DOF__ImportsAssignment_0 )*
            {
             before(grammarAccess.getDOFAccess().getImportsAssignment_0()); 
            // InternalDOF.g:934:2: ( rule__DOF__ImportsAssignment_0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==17) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalDOF.g:934:3: rule__DOF__ImportsAssignment_0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__DOF__ImportsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getDOFAccess().getImportsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__0__Impl"


    // $ANTLR start "rule__DOF__Group__1"
    // InternalDOF.g:942:1: rule__DOF__Group__1 : rule__DOF__Group__1__Impl rule__DOF__Group__2 ;
    public final void rule__DOF__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:946:1: ( rule__DOF__Group__1__Impl rule__DOF__Group__2 )
            // InternalDOF.g:947:2: rule__DOF__Group__1__Impl rule__DOF__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__DOF__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DOF__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__1"


    // $ANTLR start "rule__DOF__Group__1__Impl"
    // InternalDOF.g:954:1: rule__DOF__Group__1__Impl : ( 'dof' ) ;
    public final void rule__DOF__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:958:1: ( ( 'dof' ) )
            // InternalDOF.g:959:1: ( 'dof' )
            {
            // InternalDOF.g:959:1: ( 'dof' )
            // InternalDOF.g:960:2: 'dof'
            {
             before(grammarAccess.getDOFAccess().getDofKeyword_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getDOFAccess().getDofKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__1__Impl"


    // $ANTLR start "rule__DOF__Group__2"
    // InternalDOF.g:969:1: rule__DOF__Group__2 : rule__DOF__Group__2__Impl rule__DOF__Group__3 ;
    public final void rule__DOF__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:973:1: ( rule__DOF__Group__2__Impl rule__DOF__Group__3 )
            // InternalDOF.g:974:2: rule__DOF__Group__2__Impl rule__DOF__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__DOF__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DOF__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__2"


    // $ANTLR start "rule__DOF__Group__2__Impl"
    // InternalDOF.g:981:1: rule__DOF__Group__2__Impl : ( ( rule__DOF__NameAssignment_2 ) ) ;
    public final void rule__DOF__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:985:1: ( ( ( rule__DOF__NameAssignment_2 ) ) )
            // InternalDOF.g:986:1: ( ( rule__DOF__NameAssignment_2 ) )
            {
            // InternalDOF.g:986:1: ( ( rule__DOF__NameAssignment_2 ) )
            // InternalDOF.g:987:2: ( rule__DOF__NameAssignment_2 )
            {
             before(grammarAccess.getDOFAccess().getNameAssignment_2()); 
            // InternalDOF.g:988:2: ( rule__DOF__NameAssignment_2 )
            // InternalDOF.g:988:3: rule__DOF__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__DOF__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDOFAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__2__Impl"


    // $ANTLR start "rule__DOF__Group__3"
    // InternalDOF.g:996:1: rule__DOF__Group__3 : rule__DOF__Group__3__Impl ;
    public final void rule__DOF__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1000:1: ( rule__DOF__Group__3__Impl )
            // InternalDOF.g:1001:2: rule__DOF__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DOF__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__3"


    // $ANTLR start "rule__DOF__Group__3__Impl"
    // InternalDOF.g:1007:1: rule__DOF__Group__3__Impl : ( ( rule__DOF__EffectAssignment_3 ) ) ;
    public final void rule__DOF__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1011:1: ( ( ( rule__DOF__EffectAssignment_3 ) ) )
            // InternalDOF.g:1012:1: ( ( rule__DOF__EffectAssignment_3 ) )
            {
            // InternalDOF.g:1012:1: ( ( rule__DOF__EffectAssignment_3 ) )
            // InternalDOF.g:1013:2: ( rule__DOF__EffectAssignment_3 )
            {
             before(grammarAccess.getDOFAccess().getEffectAssignment_3()); 
            // InternalDOF.g:1014:2: ( rule__DOF__EffectAssignment_3 )
            // InternalDOF.g:1014:3: rule__DOF__EffectAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__DOF__EffectAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getDOFAccess().getEffectAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__3__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalDOF.g:1023:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1027:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalDOF.g:1028:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalDOF.g:1035:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1039:1: ( ( 'import' ) )
            // InternalDOF.g:1040:1: ( 'import' )
            {
            // InternalDOF.g:1040:1: ( 'import' )
            // InternalDOF.g:1041:2: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalDOF.g:1050:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1054:1: ( rule__Import__Group__1__Impl )
            // InternalDOF.g:1055:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalDOF.g:1061:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1065:1: ( ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) )
            // InternalDOF.g:1066:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            {
            // InternalDOF.g:1066:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            // InternalDOF.g:1067:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 
            // InternalDOF.g:1068:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            // InternalDOF.g:1068:3: rule__Import__ImportedNamespaceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportedNamespaceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__Effect__Group__0"
    // InternalDOF.g:1077:1: rule__Effect__Group__0 : rule__Effect__Group__0__Impl rule__Effect__Group__1 ;
    public final void rule__Effect__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1081:1: ( rule__Effect__Group__0__Impl rule__Effect__Group__1 )
            // InternalDOF.g:1082:2: rule__Effect__Group__0__Impl rule__Effect__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Effect__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0"


    // $ANTLR start "rule__Effect__Group__0__Impl"
    // InternalDOF.g:1089:1: rule__Effect__Group__0__Impl : ( () ) ;
    public final void rule__Effect__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1093:1: ( ( () ) )
            // InternalDOF.g:1094:1: ( () )
            {
            // InternalDOF.g:1094:1: ( () )
            // InternalDOF.g:1095:2: ()
            {
             before(grammarAccess.getEffectAccess().getEffectAction_0()); 
            // InternalDOF.g:1096:2: ()
            // InternalDOF.g:1096:3: 
            {
            }

             after(grammarAccess.getEffectAccess().getEffectAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0__Impl"


    // $ANTLR start "rule__Effect__Group__1"
    // InternalDOF.g:1104:1: rule__Effect__Group__1 : rule__Effect__Group__1__Impl rule__Effect__Group__2 ;
    public final void rule__Effect__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1108:1: ( rule__Effect__Group__1__Impl rule__Effect__Group__2 )
            // InternalDOF.g:1109:2: rule__Effect__Group__1__Impl rule__Effect__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Effect__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1"


    // $ANTLR start "rule__Effect__Group__1__Impl"
    // InternalDOF.g:1116:1: rule__Effect__Group__1__Impl : ( 'effect' ) ;
    public final void rule__Effect__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1120:1: ( ( 'effect' ) )
            // InternalDOF.g:1121:1: ( 'effect' )
            {
            // InternalDOF.g:1121:1: ( 'effect' )
            // InternalDOF.g:1122:2: 'effect'
            {
             before(grammarAccess.getEffectAccess().getEffectKeyword_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getEffectKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1__Impl"


    // $ANTLR start "rule__Effect__Group__2"
    // InternalDOF.g:1131:1: rule__Effect__Group__2 : rule__Effect__Group__2__Impl rule__Effect__Group__3 ;
    public final void rule__Effect__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1135:1: ( rule__Effect__Group__2__Impl rule__Effect__Group__3 )
            // InternalDOF.g:1136:2: rule__Effect__Group__2__Impl rule__Effect__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Effect__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2"


    // $ANTLR start "rule__Effect__Group__2__Impl"
    // InternalDOF.g:1143:1: rule__Effect__Group__2__Impl : ( ( rule__Effect__NameAssignment_2 ) ) ;
    public final void rule__Effect__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1147:1: ( ( ( rule__Effect__NameAssignment_2 ) ) )
            // InternalDOF.g:1148:1: ( ( rule__Effect__NameAssignment_2 ) )
            {
            // InternalDOF.g:1148:1: ( ( rule__Effect__NameAssignment_2 ) )
            // InternalDOF.g:1149:2: ( rule__Effect__NameAssignment_2 )
            {
             before(grammarAccess.getEffectAccess().getNameAssignment_2()); 
            // InternalDOF.g:1150:2: ( rule__Effect__NameAssignment_2 )
            // InternalDOF.g:1150:3: rule__Effect__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Effect__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2__Impl"


    // $ANTLR start "rule__Effect__Group__3"
    // InternalDOF.g:1158:1: rule__Effect__Group__3 : rule__Effect__Group__3__Impl rule__Effect__Group__4 ;
    public final void rule__Effect__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1162:1: ( rule__Effect__Group__3__Impl rule__Effect__Group__4 )
            // InternalDOF.g:1163:2: rule__Effect__Group__3__Impl rule__Effect__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__Effect__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3"


    // $ANTLR start "rule__Effect__Group__3__Impl"
    // InternalDOF.g:1170:1: rule__Effect__Group__3__Impl : ( 'is' ) ;
    public final void rule__Effect__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1174:1: ( ( 'is' ) )
            // InternalDOF.g:1175:1: ( 'is' )
            {
            // InternalDOF.g:1175:1: ( 'is' )
            // InternalDOF.g:1176:2: 'is'
            {
             before(grammarAccess.getEffectAccess().getIsKeyword_3()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getIsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3__Impl"


    // $ANTLR start "rule__Effect__Group__4"
    // InternalDOF.g:1185:1: rule__Effect__Group__4 : rule__Effect__Group__4__Impl rule__Effect__Group__5 ;
    public final void rule__Effect__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1189:1: ( rule__Effect__Group__4__Impl rule__Effect__Group__5 )
            // InternalDOF.g:1190:2: rule__Effect__Group__4__Impl rule__Effect__Group__5
            {
            pushFollow(FOLLOW_10);
            rule__Effect__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4"


    // $ANTLR start "rule__Effect__Group__4__Impl"
    // InternalDOF.g:1197:1: rule__Effect__Group__4__Impl : ( ( rule__Effect__DataFeederAssignment_4 ) ) ;
    public final void rule__Effect__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1201:1: ( ( ( rule__Effect__DataFeederAssignment_4 ) ) )
            // InternalDOF.g:1202:1: ( ( rule__Effect__DataFeederAssignment_4 ) )
            {
            // InternalDOF.g:1202:1: ( ( rule__Effect__DataFeederAssignment_4 ) )
            // InternalDOF.g:1203:2: ( rule__Effect__DataFeederAssignment_4 )
            {
             before(grammarAccess.getEffectAccess().getDataFeederAssignment_4()); 
            // InternalDOF.g:1204:2: ( rule__Effect__DataFeederAssignment_4 )
            // InternalDOF.g:1204:3: rule__Effect__DataFeederAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Effect__DataFeederAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getDataFeederAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4__Impl"


    // $ANTLR start "rule__Effect__Group__5"
    // InternalDOF.g:1212:1: rule__Effect__Group__5 : rule__Effect__Group__5__Impl rule__Effect__Group__6 ;
    public final void rule__Effect__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1216:1: ( rule__Effect__Group__5__Impl rule__Effect__Group__6 )
            // InternalDOF.g:1217:2: rule__Effect__Group__5__Impl rule__Effect__Group__6
            {
            pushFollow(FOLLOW_10);
            rule__Effect__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__5"


    // $ANTLR start "rule__Effect__Group__5__Impl"
    // InternalDOF.g:1224:1: rule__Effect__Group__5__Impl : ( ( rule__Effect__CategoriesAssignment_5 ) ) ;
    public final void rule__Effect__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1228:1: ( ( ( rule__Effect__CategoriesAssignment_5 ) ) )
            // InternalDOF.g:1229:1: ( ( rule__Effect__CategoriesAssignment_5 ) )
            {
            // InternalDOF.g:1229:1: ( ( rule__Effect__CategoriesAssignment_5 ) )
            // InternalDOF.g:1230:2: ( rule__Effect__CategoriesAssignment_5 )
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_5()); 
            // InternalDOF.g:1231:2: ( rule__Effect__CategoriesAssignment_5 )
            // InternalDOF.g:1231:3: rule__Effect__CategoriesAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Effect__CategoriesAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__5__Impl"


    // $ANTLR start "rule__Effect__Group__6"
    // InternalDOF.g:1239:1: rule__Effect__Group__6 : rule__Effect__Group__6__Impl ;
    public final void rule__Effect__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1243:1: ( rule__Effect__Group__6__Impl )
            // InternalDOF.g:1244:2: rule__Effect__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__6"


    // $ANTLR start "rule__Effect__Group__6__Impl"
    // InternalDOF.g:1250:1: rule__Effect__Group__6__Impl : ( ( rule__Effect__CategoriesAssignment_6 )* ) ;
    public final void rule__Effect__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1254:1: ( ( ( rule__Effect__CategoriesAssignment_6 )* ) )
            // InternalDOF.g:1255:1: ( ( rule__Effect__CategoriesAssignment_6 )* )
            {
            // InternalDOF.g:1255:1: ( ( rule__Effect__CategoriesAssignment_6 )* )
            // InternalDOF.g:1256:2: ( rule__Effect__CategoriesAssignment_6 )*
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_6()); 
            // InternalDOF.g:1257:2: ( rule__Effect__CategoriesAssignment_6 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==20) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalDOF.g:1257:3: rule__Effect__CategoriesAssignment_6
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Effect__CategoriesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__6__Impl"


    // $ANTLR start "rule__Category__Group__0"
    // InternalDOF.g:1266:1: rule__Category__Group__0 : rule__Category__Group__0__Impl rule__Category__Group__1 ;
    public final void rule__Category__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1270:1: ( rule__Category__Group__0__Impl rule__Category__Group__1 )
            // InternalDOF.g:1271:2: rule__Category__Group__0__Impl rule__Category__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Category__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0"


    // $ANTLR start "rule__Category__Group__0__Impl"
    // InternalDOF.g:1278:1: rule__Category__Group__0__Impl : ( () ) ;
    public final void rule__Category__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1282:1: ( ( () ) )
            // InternalDOF.g:1283:1: ( () )
            {
            // InternalDOF.g:1283:1: ( () )
            // InternalDOF.g:1284:2: ()
            {
             before(grammarAccess.getCategoryAccess().getCategoryAction_0()); 
            // InternalDOF.g:1285:2: ()
            // InternalDOF.g:1285:3: 
            {
            }

             after(grammarAccess.getCategoryAccess().getCategoryAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0__Impl"


    // $ANTLR start "rule__Category__Group__1"
    // InternalDOF.g:1293:1: rule__Category__Group__1 : rule__Category__Group__1__Impl rule__Category__Group__2 ;
    public final void rule__Category__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1297:1: ( rule__Category__Group__1__Impl rule__Category__Group__2 )
            // InternalDOF.g:1298:2: rule__Category__Group__1__Impl rule__Category__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Category__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1"


    // $ANTLR start "rule__Category__Group__1__Impl"
    // InternalDOF.g:1305:1: rule__Category__Group__1__Impl : ( 'category' ) ;
    public final void rule__Category__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1309:1: ( ( 'category' ) )
            // InternalDOF.g:1310:1: ( 'category' )
            {
            // InternalDOF.g:1310:1: ( 'category' )
            // InternalDOF.g:1311:2: 'category'
            {
             before(grammarAccess.getCategoryAccess().getCategoryKeyword_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getCategoryKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1__Impl"


    // $ANTLR start "rule__Category__Group__2"
    // InternalDOF.g:1320:1: rule__Category__Group__2 : rule__Category__Group__2__Impl rule__Category__Group__3 ;
    public final void rule__Category__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1324:1: ( rule__Category__Group__2__Impl rule__Category__Group__3 )
            // InternalDOF.g:1325:2: rule__Category__Group__2__Impl rule__Category__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__Category__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2"


    // $ANTLR start "rule__Category__Group__2__Impl"
    // InternalDOF.g:1332:1: rule__Category__Group__2__Impl : ( ( rule__Category__NameAssignment_2 ) ) ;
    public final void rule__Category__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1336:1: ( ( ( rule__Category__NameAssignment_2 ) ) )
            // InternalDOF.g:1337:1: ( ( rule__Category__NameAssignment_2 ) )
            {
            // InternalDOF.g:1337:1: ( ( rule__Category__NameAssignment_2 ) )
            // InternalDOF.g:1338:2: ( rule__Category__NameAssignment_2 )
            {
             before(grammarAccess.getCategoryAccess().getNameAssignment_2()); 
            // InternalDOF.g:1339:2: ( rule__Category__NameAssignment_2 )
            // InternalDOF.g:1339:3: rule__Category__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Category__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2__Impl"


    // $ANTLR start "rule__Category__Group__3"
    // InternalDOF.g:1347:1: rule__Category__Group__3 : rule__Category__Group__3__Impl rule__Category__Group__4 ;
    public final void rule__Category__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1351:1: ( rule__Category__Group__3__Impl rule__Category__Group__4 )
            // InternalDOF.g:1352:2: rule__Category__Group__3__Impl rule__Category__Group__4
            {
            pushFollow(FOLLOW_12);
            rule__Category__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3"


    // $ANTLR start "rule__Category__Group__3__Impl"
    // InternalDOF.g:1359:1: rule__Category__Group__3__Impl : ( ( rule__Category__CausesAssignment_3 ) ) ;
    public final void rule__Category__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1363:1: ( ( ( rule__Category__CausesAssignment_3 ) ) )
            // InternalDOF.g:1364:1: ( ( rule__Category__CausesAssignment_3 ) )
            {
            // InternalDOF.g:1364:1: ( ( rule__Category__CausesAssignment_3 ) )
            // InternalDOF.g:1365:2: ( rule__Category__CausesAssignment_3 )
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_3()); 
            // InternalDOF.g:1366:2: ( rule__Category__CausesAssignment_3 )
            // InternalDOF.g:1366:3: rule__Category__CausesAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Category__CausesAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getCausesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3__Impl"


    // $ANTLR start "rule__Category__Group__4"
    // InternalDOF.g:1374:1: rule__Category__Group__4 : rule__Category__Group__4__Impl ;
    public final void rule__Category__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1378:1: ( rule__Category__Group__4__Impl )
            // InternalDOF.g:1379:2: rule__Category__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4"


    // $ANTLR start "rule__Category__Group__4__Impl"
    // InternalDOF.g:1385:1: rule__Category__Group__4__Impl : ( ( rule__Category__CausesAssignment_4 )* ) ;
    public final void rule__Category__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1389:1: ( ( ( rule__Category__CausesAssignment_4 )* ) )
            // InternalDOF.g:1390:1: ( ( rule__Category__CausesAssignment_4 )* )
            {
            // InternalDOF.g:1390:1: ( ( rule__Category__CausesAssignment_4 )* )
            // InternalDOF.g:1391:2: ( rule__Category__CausesAssignment_4 )*
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 
            // InternalDOF.g:1392:2: ( rule__Category__CausesAssignment_4 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==42) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalDOF.g:1392:3: rule__Category__CausesAssignment_4
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Category__CausesAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4__Impl"


    // $ANTLR start "rule__DataFeeder__Group__0"
    // InternalDOF.g:1401:1: rule__DataFeeder__Group__0 : rule__DataFeeder__Group__0__Impl rule__DataFeeder__Group__1 ;
    public final void rule__DataFeeder__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1405:1: ( rule__DataFeeder__Group__0__Impl rule__DataFeeder__Group__1 )
            // InternalDOF.g:1406:2: rule__DataFeeder__Group__0__Impl rule__DataFeeder__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__DataFeeder__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__0"


    // $ANTLR start "rule__DataFeeder__Group__0__Impl"
    // InternalDOF.g:1413:1: rule__DataFeeder__Group__0__Impl : ( () ) ;
    public final void rule__DataFeeder__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1417:1: ( ( () ) )
            // InternalDOF.g:1418:1: ( () )
            {
            // InternalDOF.g:1418:1: ( () )
            // InternalDOF.g:1419:2: ()
            {
             before(grammarAccess.getDataFeederAccess().getDataFeederAction_0()); 
            // InternalDOF.g:1420:2: ()
            // InternalDOF.g:1420:3: 
            {
            }

             after(grammarAccess.getDataFeederAccess().getDataFeederAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__0__Impl"


    // $ANTLR start "rule__DataFeeder__Group__1"
    // InternalDOF.g:1428:1: rule__DataFeeder__Group__1 : rule__DataFeeder__Group__1__Impl rule__DataFeeder__Group__2 ;
    public final void rule__DataFeeder__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1432:1: ( rule__DataFeeder__Group__1__Impl rule__DataFeeder__Group__2 )
            // InternalDOF.g:1433:2: rule__DataFeeder__Group__1__Impl rule__DataFeeder__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__DataFeeder__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__1"


    // $ANTLR start "rule__DataFeeder__Group__1__Impl"
    // InternalDOF.g:1440:1: rule__DataFeeder__Group__1__Impl : ( ( rule__DataFeeder__NameAssignment_1 ) ) ;
    public final void rule__DataFeeder__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1444:1: ( ( ( rule__DataFeeder__NameAssignment_1 ) ) )
            // InternalDOF.g:1445:1: ( ( rule__DataFeeder__NameAssignment_1 ) )
            {
            // InternalDOF.g:1445:1: ( ( rule__DataFeeder__NameAssignment_1 ) )
            // InternalDOF.g:1446:2: ( rule__DataFeeder__NameAssignment_1 )
            {
             before(grammarAccess.getDataFeederAccess().getNameAssignment_1()); 
            // InternalDOF.g:1447:2: ( rule__DataFeeder__NameAssignment_1 )
            // InternalDOF.g:1447:3: rule__DataFeeder__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataFeederAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__1__Impl"


    // $ANTLR start "rule__DataFeeder__Group__2"
    // InternalDOF.g:1455:1: rule__DataFeeder__Group__2 : rule__DataFeeder__Group__2__Impl rule__DataFeeder__Group__3 ;
    public final void rule__DataFeeder__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1459:1: ( rule__DataFeeder__Group__2__Impl rule__DataFeeder__Group__3 )
            // InternalDOF.g:1460:2: rule__DataFeeder__Group__2__Impl rule__DataFeeder__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__DataFeeder__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__2"


    // $ANTLR start "rule__DataFeeder__Group__2__Impl"
    // InternalDOF.g:1467:1: rule__DataFeeder__Group__2__Impl : ( ( rule__DataFeeder__AttributeFilterAssignment_2 )? ) ;
    public final void rule__DataFeeder__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1471:1: ( ( ( rule__DataFeeder__AttributeFilterAssignment_2 )? ) )
            // InternalDOF.g:1472:1: ( ( rule__DataFeeder__AttributeFilterAssignment_2 )? )
            {
            // InternalDOF.g:1472:1: ( ( rule__DataFeeder__AttributeFilterAssignment_2 )? )
            // InternalDOF.g:1473:2: ( rule__DataFeeder__AttributeFilterAssignment_2 )?
            {
             before(grammarAccess.getDataFeederAccess().getAttributeFilterAssignment_2()); 
            // InternalDOF.g:1474:2: ( rule__DataFeeder__AttributeFilterAssignment_2 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==30) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalDOF.g:1474:3: rule__DataFeeder__AttributeFilterAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataFeeder__AttributeFilterAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataFeederAccess().getAttributeFilterAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__2__Impl"


    // $ANTLR start "rule__DataFeeder__Group__3"
    // InternalDOF.g:1482:1: rule__DataFeeder__Group__3 : rule__DataFeeder__Group__3__Impl rule__DataFeeder__Group__4 ;
    public final void rule__DataFeeder__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1486:1: ( rule__DataFeeder__Group__3__Impl rule__DataFeeder__Group__4 )
            // InternalDOF.g:1487:2: rule__DataFeeder__Group__3__Impl rule__DataFeeder__Group__4
            {
            pushFollow(FOLLOW_14);
            rule__DataFeeder__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__3"


    // $ANTLR start "rule__DataFeeder__Group__3__Impl"
    // InternalDOF.g:1494:1: rule__DataFeeder__Group__3__Impl : ( ( rule__DataFeeder__InstancesFilterAssignment_3 )? ) ;
    public final void rule__DataFeeder__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1498:1: ( ( ( rule__DataFeeder__InstancesFilterAssignment_3 )? ) )
            // InternalDOF.g:1499:1: ( ( rule__DataFeeder__InstancesFilterAssignment_3 )? )
            {
            // InternalDOF.g:1499:1: ( ( rule__DataFeeder__InstancesFilterAssignment_3 )? )
            // InternalDOF.g:1500:2: ( rule__DataFeeder__InstancesFilterAssignment_3 )?
            {
             before(grammarAccess.getDataFeederAccess().getInstancesFilterAssignment_3()); 
            // InternalDOF.g:1501:2: ( rule__DataFeeder__InstancesFilterAssignment_3 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==33) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalDOF.g:1501:3: rule__DataFeeder__InstancesFilterAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataFeeder__InstancesFilterAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataFeederAccess().getInstancesFilterAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__3__Impl"


    // $ANTLR start "rule__DataFeeder__Group__4"
    // InternalDOF.g:1509:1: rule__DataFeeder__Group__4 : rule__DataFeeder__Group__4__Impl rule__DataFeeder__Group__5 ;
    public final void rule__DataFeeder__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1513:1: ( rule__DataFeeder__Group__4__Impl rule__DataFeeder__Group__5 )
            // InternalDOF.g:1514:2: rule__DataFeeder__Group__4__Impl rule__DataFeeder__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__DataFeeder__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__4"


    // $ANTLR start "rule__DataFeeder__Group__4__Impl"
    // InternalDOF.g:1521:1: rule__DataFeeder__Group__4__Impl : ( ( rule__DataFeeder__Group_4__0 )* ) ;
    public final void rule__DataFeeder__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1525:1: ( ( ( rule__DataFeeder__Group_4__0 )* ) )
            // InternalDOF.g:1526:1: ( ( rule__DataFeeder__Group_4__0 )* )
            {
            // InternalDOF.g:1526:1: ( ( rule__DataFeeder__Group_4__0 )* )
            // InternalDOF.g:1527:2: ( rule__DataFeeder__Group_4__0 )*
            {
             before(grammarAccess.getDataFeederAccess().getGroup_4()); 
            // InternalDOF.g:1528:2: ( rule__DataFeeder__Group_4__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==21) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalDOF.g:1528:3: rule__DataFeeder__Group_4__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__DataFeeder__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getDataFeederAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__4__Impl"


    // $ANTLR start "rule__DataFeeder__Group__5"
    // InternalDOF.g:1536:1: rule__DataFeeder__Group__5 : rule__DataFeeder__Group__5__Impl ;
    public final void rule__DataFeeder__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1540:1: ( rule__DataFeeder__Group__5__Impl )
            // InternalDOF.g:1541:2: rule__DataFeeder__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__5"


    // $ANTLR start "rule__DataFeeder__Group__5__Impl"
    // InternalDOF.g:1547:1: rule__DataFeeder__Group__5__Impl : ( ( rule__DataFeeder__TypeFilterAssignment_5 )? ) ;
    public final void rule__DataFeeder__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1551:1: ( ( ( rule__DataFeeder__TypeFilterAssignment_5 )? ) )
            // InternalDOF.g:1552:1: ( ( rule__DataFeeder__TypeFilterAssignment_5 )? )
            {
            // InternalDOF.g:1552:1: ( ( rule__DataFeeder__TypeFilterAssignment_5 )? )
            // InternalDOF.g:1553:2: ( rule__DataFeeder__TypeFilterAssignment_5 )?
            {
             before(grammarAccess.getDataFeederAccess().getTypeFilterAssignment_5()); 
            // InternalDOF.g:1554:2: ( rule__DataFeeder__TypeFilterAssignment_5 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==26||LA15_0==29) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalDOF.g:1554:3: rule__DataFeeder__TypeFilterAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataFeeder__TypeFilterAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataFeederAccess().getTypeFilterAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__5__Impl"


    // $ANTLR start "rule__DataFeeder__Group_4__0"
    // InternalDOF.g:1563:1: rule__DataFeeder__Group_4__0 : rule__DataFeeder__Group_4__0__Impl rule__DataFeeder__Group_4__1 ;
    public final void rule__DataFeeder__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1567:1: ( rule__DataFeeder__Group_4__0__Impl rule__DataFeeder__Group_4__1 )
            // InternalDOF.g:1568:2: rule__DataFeeder__Group_4__0__Impl rule__DataFeeder__Group_4__1
            {
            pushFollow(FOLLOW_16);
            rule__DataFeeder__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group_4__0"


    // $ANTLR start "rule__DataFeeder__Group_4__0__Impl"
    // InternalDOF.g:1575:1: rule__DataFeeder__Group_4__0__Impl : ( 'include' ) ;
    public final void rule__DataFeeder__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1579:1: ( ( 'include' ) )
            // InternalDOF.g:1580:1: ( 'include' )
            {
            // InternalDOF.g:1580:1: ( 'include' )
            // InternalDOF.g:1581:2: 'include'
            {
             before(grammarAccess.getDataFeederAccess().getIncludeKeyword_4_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getDataFeederAccess().getIncludeKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group_4__0__Impl"


    // $ANTLR start "rule__DataFeeder__Group_4__1"
    // InternalDOF.g:1590:1: rule__DataFeeder__Group_4__1 : rule__DataFeeder__Group_4__1__Impl ;
    public final void rule__DataFeeder__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1594:1: ( rule__DataFeeder__Group_4__1__Impl )
            // InternalDOF.g:1595:2: rule__DataFeeder__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group_4__1"


    // $ANTLR start "rule__DataFeeder__Group_4__1__Impl"
    // InternalDOF.g:1601:1: rule__DataFeeder__Group_4__1__Impl : ( ( rule__DataFeeder__IncludedReferencesAssignment_4_1 ) ) ;
    public final void rule__DataFeeder__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1605:1: ( ( ( rule__DataFeeder__IncludedReferencesAssignment_4_1 ) ) )
            // InternalDOF.g:1606:1: ( ( rule__DataFeeder__IncludedReferencesAssignment_4_1 ) )
            {
            // InternalDOF.g:1606:1: ( ( rule__DataFeeder__IncludedReferencesAssignment_4_1 ) )
            // InternalDOF.g:1607:2: ( rule__DataFeeder__IncludedReferencesAssignment_4_1 )
            {
             before(grammarAccess.getDataFeederAccess().getIncludedReferencesAssignment_4_1()); 
            // InternalDOF.g:1608:2: ( rule__DataFeeder__IncludedReferencesAssignment_4_1 )
            // InternalDOF.g:1608:3: rule__DataFeeder__IncludedReferencesAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__IncludedReferencesAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getDataFeederAccess().getIncludedReferencesAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group_4__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group__0"
    // InternalDOF.g:1617:1: rule__SimpleReference__Group__0 : rule__SimpleReference__Group__0__Impl rule__SimpleReference__Group__1 ;
    public final void rule__SimpleReference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1621:1: ( rule__SimpleReference__Group__0__Impl rule__SimpleReference__Group__1 )
            // InternalDOF.g:1622:2: rule__SimpleReference__Group__0__Impl rule__SimpleReference__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__SimpleReference__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__0"


    // $ANTLR start "rule__SimpleReference__Group__0__Impl"
    // InternalDOF.g:1629:1: rule__SimpleReference__Group__0__Impl : ( ( rule__SimpleReference__NameAssignment_0 ) ) ;
    public final void rule__SimpleReference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1633:1: ( ( ( rule__SimpleReference__NameAssignment_0 ) ) )
            // InternalDOF.g:1634:1: ( ( rule__SimpleReference__NameAssignment_0 ) )
            {
            // InternalDOF.g:1634:1: ( ( rule__SimpleReference__NameAssignment_0 ) )
            // InternalDOF.g:1635:2: ( rule__SimpleReference__NameAssignment_0 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getNameAssignment_0()); 
            // InternalDOF.g:1636:2: ( rule__SimpleReference__NameAssignment_0 )
            // InternalDOF.g:1636:3: rule__SimpleReference__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group__1"
    // InternalDOF.g:1644:1: rule__SimpleReference__Group__1 : rule__SimpleReference__Group__1__Impl rule__SimpleReference__Group__2 ;
    public final void rule__SimpleReference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1648:1: ( rule__SimpleReference__Group__1__Impl rule__SimpleReference__Group__2 )
            // InternalDOF.g:1649:2: rule__SimpleReference__Group__1__Impl rule__SimpleReference__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__SimpleReference__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__1"


    // $ANTLR start "rule__SimpleReference__Group__1__Impl"
    // InternalDOF.g:1656:1: rule__SimpleReference__Group__1__Impl : ( ( rule__SimpleReference__AttributeFilterAssignment_1 )? ) ;
    public final void rule__SimpleReference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1660:1: ( ( ( rule__SimpleReference__AttributeFilterAssignment_1 )? ) )
            // InternalDOF.g:1661:1: ( ( rule__SimpleReference__AttributeFilterAssignment_1 )? )
            {
            // InternalDOF.g:1661:1: ( ( rule__SimpleReference__AttributeFilterAssignment_1 )? )
            // InternalDOF.g:1662:2: ( rule__SimpleReference__AttributeFilterAssignment_1 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAssignment_1()); 
            // InternalDOF.g:1663:2: ( rule__SimpleReference__AttributeFilterAssignment_1 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==30) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalDOF.g:1663:3: rule__SimpleReference__AttributeFilterAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__AttributeFilterAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group__2"
    // InternalDOF.g:1671:1: rule__SimpleReference__Group__2 : rule__SimpleReference__Group__2__Impl rule__SimpleReference__Group__3 ;
    public final void rule__SimpleReference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1675:1: ( rule__SimpleReference__Group__2__Impl rule__SimpleReference__Group__3 )
            // InternalDOF.g:1676:2: rule__SimpleReference__Group__2__Impl rule__SimpleReference__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__SimpleReference__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__2"


    // $ANTLR start "rule__SimpleReference__Group__2__Impl"
    // InternalDOF.g:1683:1: rule__SimpleReference__Group__2__Impl : ( ( rule__SimpleReference__Group_2__0 )? ) ;
    public final void rule__SimpleReference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1687:1: ( ( ( rule__SimpleReference__Group_2__0 )? ) )
            // InternalDOF.g:1688:1: ( ( rule__SimpleReference__Group_2__0 )? )
            {
            // InternalDOF.g:1688:1: ( ( rule__SimpleReference__Group_2__0 )? )
            // InternalDOF.g:1689:2: ( rule__SimpleReference__Group_2__0 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getGroup_2()); 
            // InternalDOF.g:1690:2: ( rule__SimpleReference__Group_2__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==22) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalDOF.g:1690:3: rule__SimpleReference__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__2__Impl"


    // $ANTLR start "rule__SimpleReference__Group__3"
    // InternalDOF.g:1698:1: rule__SimpleReference__Group__3 : rule__SimpleReference__Group__3__Impl ;
    public final void rule__SimpleReference__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1702:1: ( rule__SimpleReference__Group__3__Impl )
            // InternalDOF.g:1703:2: rule__SimpleReference__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__3"


    // $ANTLR start "rule__SimpleReference__Group__3__Impl"
    // InternalDOF.g:1709:1: rule__SimpleReference__Group__3__Impl : ( ( rule__SimpleReference__Group_3__0 )? ) ;
    public final void rule__SimpleReference__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1713:1: ( ( ( rule__SimpleReference__Group_3__0 )? ) )
            // InternalDOF.g:1714:1: ( ( rule__SimpleReference__Group_3__0 )? )
            {
            // InternalDOF.g:1714:1: ( ( rule__SimpleReference__Group_3__0 )? )
            // InternalDOF.g:1715:2: ( rule__SimpleReference__Group_3__0 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getGroup_3()); 
            // InternalDOF.g:1716:2: ( rule__SimpleReference__Group_3__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==23) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalDOF.g:1716:3: rule__SimpleReference__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__3__Impl"


    // $ANTLR start "rule__SimpleReference__Group_2__0"
    // InternalDOF.g:1725:1: rule__SimpleReference__Group_2__0 : rule__SimpleReference__Group_2__0__Impl rule__SimpleReference__Group_2__1 ;
    public final void rule__SimpleReference__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1729:1: ( rule__SimpleReference__Group_2__0__Impl rule__SimpleReference__Group_2__1 )
            // InternalDOF.g:1730:2: rule__SimpleReference__Group_2__0__Impl rule__SimpleReference__Group_2__1
            {
            pushFollow(FOLLOW_7);
            rule__SimpleReference__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__0"


    // $ANTLR start "rule__SimpleReference__Group_2__0__Impl"
    // InternalDOF.g:1737:1: rule__SimpleReference__Group_2__0__Impl : ( 'by' ) ;
    public final void rule__SimpleReference__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1741:1: ( ( 'by' ) )
            // InternalDOF.g:1742:1: ( 'by' )
            {
            // InternalDOF.g:1742:1: ( 'by' )
            // InternalDOF.g:1743:2: 'by'
            {
             before(grammarAccess.getSimpleReferenceAccess().getByKeyword_2_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getByKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group_2__1"
    // InternalDOF.g:1752:1: rule__SimpleReference__Group_2__1 : rule__SimpleReference__Group_2__1__Impl rule__SimpleReference__Group_2__2 ;
    public final void rule__SimpleReference__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1756:1: ( rule__SimpleReference__Group_2__1__Impl rule__SimpleReference__Group_2__2 )
            // InternalDOF.g:1757:2: rule__SimpleReference__Group_2__1__Impl rule__SimpleReference__Group_2__2
            {
            pushFollow(FOLLOW_18);
            rule__SimpleReference__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__1"


    // $ANTLR start "rule__SimpleReference__Group_2__1__Impl"
    // InternalDOF.g:1764:1: rule__SimpleReference__Group_2__1__Impl : ( ( rule__SimpleReference__PivotingIdAssignment_2_1 ) ) ;
    public final void rule__SimpleReference__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1768:1: ( ( ( rule__SimpleReference__PivotingIdAssignment_2_1 ) ) )
            // InternalDOF.g:1769:1: ( ( rule__SimpleReference__PivotingIdAssignment_2_1 ) )
            {
            // InternalDOF.g:1769:1: ( ( rule__SimpleReference__PivotingIdAssignment_2_1 ) )
            // InternalDOF.g:1770:2: ( rule__SimpleReference__PivotingIdAssignment_2_1 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getPivotingIdAssignment_2_1()); 
            // InternalDOF.g:1771:2: ( rule__SimpleReference__PivotingIdAssignment_2_1 )
            // InternalDOF.g:1771:3: rule__SimpleReference__PivotingIdAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__PivotingIdAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getPivotingIdAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group_2__2"
    // InternalDOF.g:1779:1: rule__SimpleReference__Group_2__2 : rule__SimpleReference__Group_2__2__Impl ;
    public final void rule__SimpleReference__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1783:1: ( rule__SimpleReference__Group_2__2__Impl )
            // InternalDOF.g:1784:2: rule__SimpleReference__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__2"


    // $ANTLR start "rule__SimpleReference__Group_2__2__Impl"
    // InternalDOF.g:1790:1: rule__SimpleReference__Group_2__2__Impl : ( ( rule__SimpleReference__InstancesFilterAssignment_2_2 )? ) ;
    public final void rule__SimpleReference__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1794:1: ( ( ( rule__SimpleReference__InstancesFilterAssignment_2_2 )? ) )
            // InternalDOF.g:1795:1: ( ( rule__SimpleReference__InstancesFilterAssignment_2_2 )? )
            {
            // InternalDOF.g:1795:1: ( ( rule__SimpleReference__InstancesFilterAssignment_2_2 )? )
            // InternalDOF.g:1796:2: ( rule__SimpleReference__InstancesFilterAssignment_2_2 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getInstancesFilterAssignment_2_2()); 
            // InternalDOF.g:1797:2: ( rule__SimpleReference__InstancesFilterAssignment_2_2 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==33) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalDOF.g:1797:3: rule__SimpleReference__InstancesFilterAssignment_2_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__InstancesFilterAssignment_2_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getInstancesFilterAssignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__2__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3__0"
    // InternalDOF.g:1806:1: rule__SimpleReference__Group_3__0 : rule__SimpleReference__Group_3__0__Impl rule__SimpleReference__Group_3__1 ;
    public final void rule__SimpleReference__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1810:1: ( rule__SimpleReference__Group_3__0__Impl rule__SimpleReference__Group_3__1 )
            // InternalDOF.g:1811:2: rule__SimpleReference__Group_3__0__Impl rule__SimpleReference__Group_3__1
            {
            pushFollow(FOLLOW_19);
            rule__SimpleReference__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__0"


    // $ANTLR start "rule__SimpleReference__Group_3__0__Impl"
    // InternalDOF.g:1818:1: rule__SimpleReference__Group_3__0__Impl : ( '{' ) ;
    public final void rule__SimpleReference__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1822:1: ( ( '{' ) )
            // InternalDOF.g:1823:1: ( '{' )
            {
            // InternalDOF.g:1823:1: ( '{' )
            // InternalDOF.g:1824:2: '{'
            {
             before(grammarAccess.getSimpleReferenceAccess().getLeftCurlyBracketKeyword_3_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getLeftCurlyBracketKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3__1"
    // InternalDOF.g:1833:1: rule__SimpleReference__Group_3__1 : rule__SimpleReference__Group_3__1__Impl rule__SimpleReference__Group_3__2 ;
    public final void rule__SimpleReference__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1837:1: ( rule__SimpleReference__Group_3__1__Impl rule__SimpleReference__Group_3__2 )
            // InternalDOF.g:1838:2: rule__SimpleReference__Group_3__1__Impl rule__SimpleReference__Group_3__2
            {
            pushFollow(FOLLOW_20);
            rule__SimpleReference__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__1"


    // $ANTLR start "rule__SimpleReference__Group_3__1__Impl"
    // InternalDOF.g:1845:1: rule__SimpleReference__Group_3__1__Impl : ( 'include' ) ;
    public final void rule__SimpleReference__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1849:1: ( ( 'include' ) )
            // InternalDOF.g:1850:1: ( 'include' )
            {
            // InternalDOF.g:1850:1: ( 'include' )
            // InternalDOF.g:1851:2: 'include'
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_3_1()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3__2"
    // InternalDOF.g:1860:1: rule__SimpleReference__Group_3__2 : rule__SimpleReference__Group_3__2__Impl rule__SimpleReference__Group_3__3 ;
    public final void rule__SimpleReference__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1864:1: ( rule__SimpleReference__Group_3__2__Impl rule__SimpleReference__Group_3__3 )
            // InternalDOF.g:1865:2: rule__SimpleReference__Group_3__2__Impl rule__SimpleReference__Group_3__3
            {
            pushFollow(FOLLOW_20);
            rule__SimpleReference__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__2"


    // $ANTLR start "rule__SimpleReference__Group_3__2__Impl"
    // InternalDOF.g:1872:1: rule__SimpleReference__Group_3__2__Impl : ( ( rule__SimpleReference__IncludedReferencesAssignment_3_2 )* ) ;
    public final void rule__SimpleReference__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1876:1: ( ( ( rule__SimpleReference__IncludedReferencesAssignment_3_2 )* ) )
            // InternalDOF.g:1877:1: ( ( rule__SimpleReference__IncludedReferencesAssignment_3_2 )* )
            {
            // InternalDOF.g:1877:1: ( ( rule__SimpleReference__IncludedReferencesAssignment_3_2 )* )
            // InternalDOF.g:1878:2: ( rule__SimpleReference__IncludedReferencesAssignment_3_2 )*
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesAssignment_3_2()); 
            // InternalDOF.g:1879:2: ( rule__SimpleReference__IncludedReferencesAssignment_3_2 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==RULE_ID||LA20_0==25) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalDOF.g:1879:3: rule__SimpleReference__IncludedReferencesAssignment_3_2
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__SimpleReference__IncludedReferencesAssignment_3_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__2__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3__3"
    // InternalDOF.g:1887:1: rule__SimpleReference__Group_3__3 : rule__SimpleReference__Group_3__3__Impl rule__SimpleReference__Group_3__4 ;
    public final void rule__SimpleReference__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1891:1: ( rule__SimpleReference__Group_3__3__Impl rule__SimpleReference__Group_3__4 )
            // InternalDOF.g:1892:2: rule__SimpleReference__Group_3__3__Impl rule__SimpleReference__Group_3__4
            {
            pushFollow(FOLLOW_20);
            rule__SimpleReference__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__3"


    // $ANTLR start "rule__SimpleReference__Group_3__3__Impl"
    // InternalDOF.g:1899:1: rule__SimpleReference__Group_3__3__Impl : ( ( rule__SimpleReference__TypeFilterAssignment_3_3 )? ) ;
    public final void rule__SimpleReference__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1903:1: ( ( ( rule__SimpleReference__TypeFilterAssignment_3_3 )? ) )
            // InternalDOF.g:1904:1: ( ( rule__SimpleReference__TypeFilterAssignment_3_3 )? )
            {
            // InternalDOF.g:1904:1: ( ( rule__SimpleReference__TypeFilterAssignment_3_3 )? )
            // InternalDOF.g:1905:2: ( rule__SimpleReference__TypeFilterAssignment_3_3 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getTypeFilterAssignment_3_3()); 
            // InternalDOF.g:1906:2: ( rule__SimpleReference__TypeFilterAssignment_3_3 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==26||LA21_0==29) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalDOF.g:1906:3: rule__SimpleReference__TypeFilterAssignment_3_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__TypeFilterAssignment_3_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getTypeFilterAssignment_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__3__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3__4"
    // InternalDOF.g:1914:1: rule__SimpleReference__Group_3__4 : rule__SimpleReference__Group_3__4__Impl ;
    public final void rule__SimpleReference__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1918:1: ( rule__SimpleReference__Group_3__4__Impl )
            // InternalDOF.g:1919:2: rule__SimpleReference__Group_3__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__4"


    // $ANTLR start "rule__SimpleReference__Group_3__4__Impl"
    // InternalDOF.g:1925:1: rule__SimpleReference__Group_3__4__Impl : ( '}' ) ;
    public final void rule__SimpleReference__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1929:1: ( ( '}' ) )
            // InternalDOF.g:1930:1: ( '}' )
            {
            // InternalDOF.g:1930:1: ( '}' )
            // InternalDOF.g:1931:2: '}'
            {
             before(grammarAccess.getSimpleReferenceAccess().getRightCurlyBracketKeyword_3_4()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getRightCurlyBracketKeyword_3_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3__4__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__0"
    // InternalDOF.g:1941:1: rule__AggregatedReference__Group__0 : rule__AggregatedReference__Group__0__Impl rule__AggregatedReference__Group__1 ;
    public final void rule__AggregatedReference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1945:1: ( rule__AggregatedReference__Group__0__Impl rule__AggregatedReference__Group__1 )
            // InternalDOF.g:1946:2: rule__AggregatedReference__Group__0__Impl rule__AggregatedReference__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__AggregatedReference__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__0"


    // $ANTLR start "rule__AggregatedReference__Group__0__Impl"
    // InternalDOF.g:1953:1: rule__AggregatedReference__Group__0__Impl : ( 'calculate' ) ;
    public final void rule__AggregatedReference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1957:1: ( ( 'calculate' ) )
            // InternalDOF.g:1958:1: ( 'calculate' )
            {
            // InternalDOF.g:1958:1: ( 'calculate' )
            // InternalDOF.g:1959:2: 'calculate'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getCalculateKeyword_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getCalculateKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__0__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__1"
    // InternalDOF.g:1968:1: rule__AggregatedReference__Group__1 : rule__AggregatedReference__Group__1__Impl rule__AggregatedReference__Group__2 ;
    public final void rule__AggregatedReference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1972:1: ( rule__AggregatedReference__Group__1__Impl rule__AggregatedReference__Group__2 )
            // InternalDOF.g:1973:2: rule__AggregatedReference__Group__1__Impl rule__AggregatedReference__Group__2
            {
            pushFollow(FOLLOW_22);
            rule__AggregatedReference__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__1"


    // $ANTLR start "rule__AggregatedReference__Group__1__Impl"
    // InternalDOF.g:1980:1: rule__AggregatedReference__Group__1__Impl : ( ( rule__AggregatedReference__NameAssignment_1 ) ) ;
    public final void rule__AggregatedReference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1984:1: ( ( ( rule__AggregatedReference__NameAssignment_1 ) ) )
            // InternalDOF.g:1985:1: ( ( rule__AggregatedReference__NameAssignment_1 ) )
            {
            // InternalDOF.g:1985:1: ( ( rule__AggregatedReference__NameAssignment_1 ) )
            // InternalDOF.g:1986:2: ( rule__AggregatedReference__NameAssignment_1 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getNameAssignment_1()); 
            // InternalDOF.g:1987:2: ( rule__AggregatedReference__NameAssignment_1 )
            // InternalDOF.g:1987:3: rule__AggregatedReference__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__1__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__2"
    // InternalDOF.g:1995:1: rule__AggregatedReference__Group__2 : rule__AggregatedReference__Group__2__Impl rule__AggregatedReference__Group__3 ;
    public final void rule__AggregatedReference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1999:1: ( rule__AggregatedReference__Group__2__Impl rule__AggregatedReference__Group__3 )
            // InternalDOF.g:2000:2: rule__AggregatedReference__Group__2__Impl rule__AggregatedReference__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__AggregatedReference__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__2"


    // $ANTLR start "rule__AggregatedReference__Group__2__Impl"
    // InternalDOF.g:2007:1: rule__AggregatedReference__Group__2__Impl : ( 'as' ) ;
    public final void rule__AggregatedReference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2011:1: ( ( 'as' ) )
            // InternalDOF.g:2012:1: ( 'as' )
            {
            // InternalDOF.g:2012:1: ( 'as' )
            // InternalDOF.g:2013:2: 'as'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getAsKeyword_2()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getAsKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__2__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__3"
    // InternalDOF.g:2022:1: rule__AggregatedReference__Group__3 : rule__AggregatedReference__Group__3__Impl rule__AggregatedReference__Group__4 ;
    public final void rule__AggregatedReference__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2026:1: ( rule__AggregatedReference__Group__3__Impl rule__AggregatedReference__Group__4 )
            // InternalDOF.g:2027:2: rule__AggregatedReference__Group__3__Impl rule__AggregatedReference__Group__4
            {
            pushFollow(FOLLOW_24);
            rule__AggregatedReference__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__3"


    // $ANTLR start "rule__AggregatedReference__Group__3__Impl"
    // InternalDOF.g:2034:1: rule__AggregatedReference__Group__3__Impl : ( ( rule__AggregatedReference__FunctionAssignment_3 ) ) ;
    public final void rule__AggregatedReference__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2038:1: ( ( ( rule__AggregatedReference__FunctionAssignment_3 ) ) )
            // InternalDOF.g:2039:1: ( ( rule__AggregatedReference__FunctionAssignment_3 ) )
            {
            // InternalDOF.g:2039:1: ( ( rule__AggregatedReference__FunctionAssignment_3 ) )
            // InternalDOF.g:2040:2: ( rule__AggregatedReference__FunctionAssignment_3 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getFunctionAssignment_3()); 
            // InternalDOF.g:2041:2: ( rule__AggregatedReference__FunctionAssignment_3 )
            // InternalDOF.g:2041:3: rule__AggregatedReference__FunctionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__FunctionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getFunctionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__3__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__4"
    // InternalDOF.g:2049:1: rule__AggregatedReference__Group__4 : rule__AggregatedReference__Group__4__Impl rule__AggregatedReference__Group__5 ;
    public final void rule__AggregatedReference__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2053:1: ( rule__AggregatedReference__Group__4__Impl rule__AggregatedReference__Group__5 )
            // InternalDOF.g:2054:2: rule__AggregatedReference__Group__4__Impl rule__AggregatedReference__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__AggregatedReference__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__4"


    // $ANTLR start "rule__AggregatedReference__Group__4__Impl"
    // InternalDOF.g:2061:1: rule__AggregatedReference__Group__4__Impl : ( '(' ) ;
    public final void rule__AggregatedReference__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2065:1: ( ( '(' ) )
            // InternalDOF.g:2066:1: ( '(' )
            {
            // InternalDOF.g:2066:1: ( '(' )
            // InternalDOF.g:2067:2: '('
            {
             before(grammarAccess.getAggregatedReferenceAccess().getLeftParenthesisKeyword_4()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getLeftParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__4__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__5"
    // InternalDOF.g:2076:1: rule__AggregatedReference__Group__5 : rule__AggregatedReference__Group__5__Impl rule__AggregatedReference__Group__6 ;
    public final void rule__AggregatedReference__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2080:1: ( rule__AggregatedReference__Group__5__Impl rule__AggregatedReference__Group__6 )
            // InternalDOF.g:2081:2: rule__AggregatedReference__Group__5__Impl rule__AggregatedReference__Group__6
            {
            pushFollow(FOLLOW_25);
            rule__AggregatedReference__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__5"


    // $ANTLR start "rule__AggregatedReference__Group__5__Impl"
    // InternalDOF.g:2088:1: rule__AggregatedReference__Group__5__Impl : ( ( rule__AggregatedReference__AggValueAssignment_5 ) ) ;
    public final void rule__AggregatedReference__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2092:1: ( ( ( rule__AggregatedReference__AggValueAssignment_5 ) ) )
            // InternalDOF.g:2093:1: ( ( rule__AggregatedReference__AggValueAssignment_5 ) )
            {
            // InternalDOF.g:2093:1: ( ( rule__AggregatedReference__AggValueAssignment_5 ) )
            // InternalDOF.g:2094:2: ( rule__AggregatedReference__AggValueAssignment_5 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getAggValueAssignment_5()); 
            // InternalDOF.g:2095:2: ( rule__AggregatedReference__AggValueAssignment_5 )
            // InternalDOF.g:2095:3: rule__AggregatedReference__AggValueAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__AggValueAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getAggValueAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__5__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__6"
    // InternalDOF.g:2103:1: rule__AggregatedReference__Group__6 : rule__AggregatedReference__Group__6__Impl rule__AggregatedReference__Group__7 ;
    public final void rule__AggregatedReference__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2107:1: ( rule__AggregatedReference__Group__6__Impl rule__AggregatedReference__Group__7 )
            // InternalDOF.g:2108:2: rule__AggregatedReference__Group__6__Impl rule__AggregatedReference__Group__7
            {
            pushFollow(FOLLOW_26);
            rule__AggregatedReference__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__6"


    // $ANTLR start "rule__AggregatedReference__Group__6__Impl"
    // InternalDOF.g:2115:1: rule__AggregatedReference__Group__6__Impl : ( ')' ) ;
    public final void rule__AggregatedReference__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2119:1: ( ( ')' ) )
            // InternalDOF.g:2120:1: ( ')' )
            {
            // InternalDOF.g:2120:1: ( ')' )
            // InternalDOF.g:2121:2: ')'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getRightParenthesisKeyword_6()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getRightParenthesisKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__6__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__7"
    // InternalDOF.g:2130:1: rule__AggregatedReference__Group__7 : rule__AggregatedReference__Group__7__Impl rule__AggregatedReference__Group__8 ;
    public final void rule__AggregatedReference__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2134:1: ( rule__AggregatedReference__Group__7__Impl rule__AggregatedReference__Group__8 )
            // InternalDOF.g:2135:2: rule__AggregatedReference__Group__7__Impl rule__AggregatedReference__Group__8
            {
            pushFollow(FOLLOW_26);
            rule__AggregatedReference__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__7"


    // $ANTLR start "rule__AggregatedReference__Group__7__Impl"
    // InternalDOF.g:2142:1: rule__AggregatedReference__Group__7__Impl : ( ( rule__AggregatedReference__Group_7__0 )? ) ;
    public final void rule__AggregatedReference__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2146:1: ( ( ( rule__AggregatedReference__Group_7__0 )? ) )
            // InternalDOF.g:2147:1: ( ( rule__AggregatedReference__Group_7__0 )? )
            {
            // InternalDOF.g:2147:1: ( ( rule__AggregatedReference__Group_7__0 )? )
            // InternalDOF.g:2148:2: ( rule__AggregatedReference__Group_7__0 )?
            {
             before(grammarAccess.getAggregatedReferenceAccess().getGroup_7()); 
            // InternalDOF.g:2149:2: ( rule__AggregatedReference__Group_7__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==22) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalDOF.g:2149:3: rule__AggregatedReference__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AggregatedReference__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAggregatedReferenceAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__7__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__8"
    // InternalDOF.g:2157:1: rule__AggregatedReference__Group__8 : rule__AggregatedReference__Group__8__Impl ;
    public final void rule__AggregatedReference__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2161:1: ( rule__AggregatedReference__Group__8__Impl )
            // InternalDOF.g:2162:2: rule__AggregatedReference__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__8"


    // $ANTLR start "rule__AggregatedReference__Group__8__Impl"
    // InternalDOF.g:2168:1: rule__AggregatedReference__Group__8__Impl : ( ( rule__AggregatedReference__InstancesFilterAssignment_8 )? ) ;
    public final void rule__AggregatedReference__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2172:1: ( ( ( rule__AggregatedReference__InstancesFilterAssignment_8 )? ) )
            // InternalDOF.g:2173:1: ( ( rule__AggregatedReference__InstancesFilterAssignment_8 )? )
            {
            // InternalDOF.g:2173:1: ( ( rule__AggregatedReference__InstancesFilterAssignment_8 )? )
            // InternalDOF.g:2174:2: ( rule__AggregatedReference__InstancesFilterAssignment_8 )?
            {
             before(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterAssignment_8()); 
            // InternalDOF.g:2175:2: ( rule__AggregatedReference__InstancesFilterAssignment_8 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==33) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalDOF.g:2175:3: rule__AggregatedReference__InstancesFilterAssignment_8
                    {
                    pushFollow(FOLLOW_2);
                    rule__AggregatedReference__InstancesFilterAssignment_8();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__8__Impl"


    // $ANTLR start "rule__AggregatedReference__Group_7__0"
    // InternalDOF.g:2184:1: rule__AggregatedReference__Group_7__0 : rule__AggregatedReference__Group_7__0__Impl rule__AggregatedReference__Group_7__1 ;
    public final void rule__AggregatedReference__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2188:1: ( rule__AggregatedReference__Group_7__0__Impl rule__AggregatedReference__Group_7__1 )
            // InternalDOF.g:2189:2: rule__AggregatedReference__Group_7__0__Impl rule__AggregatedReference__Group_7__1
            {
            pushFollow(FOLLOW_7);
            rule__AggregatedReference__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_7__0"


    // $ANTLR start "rule__AggregatedReference__Group_7__0__Impl"
    // InternalDOF.g:2196:1: rule__AggregatedReference__Group_7__0__Impl : ( 'by' ) ;
    public final void rule__AggregatedReference__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2200:1: ( ( 'by' ) )
            // InternalDOF.g:2201:1: ( 'by' )
            {
            // InternalDOF.g:2201:1: ( 'by' )
            // InternalDOF.g:2202:2: 'by'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getByKeyword_7_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getByKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_7__0__Impl"


    // $ANTLR start "rule__AggregatedReference__Group_7__1"
    // InternalDOF.g:2211:1: rule__AggregatedReference__Group_7__1 : rule__AggregatedReference__Group_7__1__Impl ;
    public final void rule__AggregatedReference__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2215:1: ( rule__AggregatedReference__Group_7__1__Impl )
            // InternalDOF.g:2216:2: rule__AggregatedReference__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_7__1"


    // $ANTLR start "rule__AggregatedReference__Group_7__1__Impl"
    // InternalDOF.g:2222:1: rule__AggregatedReference__Group_7__1__Impl : ( ( rule__AggregatedReference__PivotingIdAssignment_7_1 ) ) ;
    public final void rule__AggregatedReference__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2226:1: ( ( ( rule__AggregatedReference__PivotingIdAssignment_7_1 ) ) )
            // InternalDOF.g:2227:1: ( ( rule__AggregatedReference__PivotingIdAssignment_7_1 ) )
            {
            // InternalDOF.g:2227:1: ( ( rule__AggregatedReference__PivotingIdAssignment_7_1 ) )
            // InternalDOF.g:2228:2: ( rule__AggregatedReference__PivotingIdAssignment_7_1 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getPivotingIdAssignment_7_1()); 
            // InternalDOF.g:2229:2: ( rule__AggregatedReference__PivotingIdAssignment_7_1 )
            // InternalDOF.g:2229:3: rule__AggregatedReference__PivotingIdAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__PivotingIdAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getPivotingIdAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_7__1__Impl"


    // $ANTLR start "rule__TypeCompletion__Group__0"
    // InternalDOF.g:2238:1: rule__TypeCompletion__Group__0 : rule__TypeCompletion__Group__0__Impl rule__TypeCompletion__Group__1 ;
    public final void rule__TypeCompletion__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2242:1: ( rule__TypeCompletion__Group__0__Impl rule__TypeCompletion__Group__1 )
            // InternalDOF.g:2243:2: rule__TypeCompletion__Group__0__Impl rule__TypeCompletion__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__TypeCompletion__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCompletion__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__0"


    // $ANTLR start "rule__TypeCompletion__Group__0__Impl"
    // InternalDOF.g:2250:1: rule__TypeCompletion__Group__0__Impl : ( 'as' ) ;
    public final void rule__TypeCompletion__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2254:1: ( ( 'as' ) )
            // InternalDOF.g:2255:1: ( 'as' )
            {
            // InternalDOF.g:2255:1: ( 'as' )
            // InternalDOF.g:2256:2: 'as'
            {
             before(grammarAccess.getTypeCompletionAccess().getAsKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getTypeCompletionAccess().getAsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__0__Impl"


    // $ANTLR start "rule__TypeCompletion__Group__1"
    // InternalDOF.g:2265:1: rule__TypeCompletion__Group__1 : rule__TypeCompletion__Group__1__Impl ;
    public final void rule__TypeCompletion__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2269:1: ( rule__TypeCompletion__Group__1__Impl )
            // InternalDOF.g:2270:2: rule__TypeCompletion__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeCompletion__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__1"


    // $ANTLR start "rule__TypeCompletion__Group__1__Impl"
    // InternalDOF.g:2276:1: rule__TypeCompletion__Group__1__Impl : ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) ) ;
    public final void rule__TypeCompletion__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2280:1: ( ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) ) )
            // InternalDOF.g:2281:1: ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) )
            {
            // InternalDOF.g:2281:1: ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) )
            // InternalDOF.g:2282:2: ( rule__TypeCompletion__TypeCustomizationsAssignment_1 )
            {
             before(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsAssignment_1()); 
            // InternalDOF.g:2283:2: ( rule__TypeCompletion__TypeCustomizationsAssignment_1 )
            // InternalDOF.g:2283:3: rule__TypeCompletion__TypeCustomizationsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__TypeCompletion__TypeCustomizationsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__1__Impl"


    // $ANTLR start "rule__TypeSelection__Group__0"
    // InternalDOF.g:2292:1: rule__TypeSelection__Group__0 : rule__TypeSelection__Group__0__Impl rule__TypeSelection__Group__1 ;
    public final void rule__TypeSelection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2296:1: ( rule__TypeSelection__Group__0__Impl rule__TypeSelection__Group__1 )
            // InternalDOF.g:2297:2: rule__TypeSelection__Group__0__Impl rule__TypeSelection__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__TypeSelection__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeSelection__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__0"


    // $ANTLR start "rule__TypeSelection__Group__0__Impl"
    // InternalDOF.g:2304:1: rule__TypeSelection__Group__0__Impl : ( 'only_as' ) ;
    public final void rule__TypeSelection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2308:1: ( ( 'only_as' ) )
            // InternalDOF.g:2309:1: ( 'only_as' )
            {
            // InternalDOF.g:2309:1: ( 'only_as' )
            // InternalDOF.g:2310:2: 'only_as'
            {
             before(grammarAccess.getTypeSelectionAccess().getOnly_asKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getTypeSelectionAccess().getOnly_asKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__0__Impl"


    // $ANTLR start "rule__TypeSelection__Group__1"
    // InternalDOF.g:2319:1: rule__TypeSelection__Group__1 : rule__TypeSelection__Group__1__Impl ;
    public final void rule__TypeSelection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2323:1: ( rule__TypeSelection__Group__1__Impl )
            // InternalDOF.g:2324:2: rule__TypeSelection__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeSelection__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__1"


    // $ANTLR start "rule__TypeSelection__Group__1__Impl"
    // InternalDOF.g:2330:1: rule__TypeSelection__Group__1__Impl : ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) ) ;
    public final void rule__TypeSelection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2334:1: ( ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) ) )
            // InternalDOF.g:2335:1: ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) )
            {
            // InternalDOF.g:2335:1: ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) )
            // InternalDOF.g:2336:2: ( rule__TypeSelection__TypeCustomizationsAssignment_1 )
            {
             before(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsAssignment_1()); 
            // InternalDOF.g:2337:2: ( rule__TypeSelection__TypeCustomizationsAssignment_1 )
            // InternalDOF.g:2337:3: rule__TypeSelection__TypeCustomizationsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__TypeSelection__TypeCustomizationsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__1__Impl"


    // $ANTLR start "rule__TypeCustomization__Group__0"
    // InternalDOF.g:2346:1: rule__TypeCustomization__Group__0 : rule__TypeCustomization__Group__0__Impl rule__TypeCustomization__Group__1 ;
    public final void rule__TypeCustomization__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2350:1: ( rule__TypeCustomization__Group__0__Impl rule__TypeCustomization__Group__1 )
            // InternalDOF.g:2351:2: rule__TypeCustomization__Group__0__Impl rule__TypeCustomization__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__TypeCustomization__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__0"


    // $ANTLR start "rule__TypeCustomization__Group__0__Impl"
    // InternalDOF.g:2358:1: rule__TypeCustomization__Group__0__Impl : ( ( rule__TypeCustomization__NameAssignment_0 ) ) ;
    public final void rule__TypeCustomization__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2362:1: ( ( ( rule__TypeCustomization__NameAssignment_0 ) ) )
            // InternalDOF.g:2363:1: ( ( rule__TypeCustomization__NameAssignment_0 ) )
            {
            // InternalDOF.g:2363:1: ( ( rule__TypeCustomization__NameAssignment_0 ) )
            // InternalDOF.g:2364:2: ( rule__TypeCustomization__NameAssignment_0 )
            {
             before(grammarAccess.getTypeCustomizationAccess().getNameAssignment_0()); 
            // InternalDOF.g:2365:2: ( rule__TypeCustomization__NameAssignment_0 )
            // InternalDOF.g:2365:3: rule__TypeCustomization__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTypeCustomizationAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__0__Impl"


    // $ANTLR start "rule__TypeCustomization__Group__1"
    // InternalDOF.g:2373:1: rule__TypeCustomization__Group__1 : rule__TypeCustomization__Group__1__Impl rule__TypeCustomization__Group__2 ;
    public final void rule__TypeCustomization__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2377:1: ( rule__TypeCustomization__Group__1__Impl rule__TypeCustomization__Group__2 )
            // InternalDOF.g:2378:2: rule__TypeCustomization__Group__1__Impl rule__TypeCustomization__Group__2
            {
            pushFollow(FOLLOW_27);
            rule__TypeCustomization__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__1"


    // $ANTLR start "rule__TypeCustomization__Group__1__Impl"
    // InternalDOF.g:2385:1: rule__TypeCustomization__Group__1__Impl : ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? ) ;
    public final void rule__TypeCustomization__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2389:1: ( ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? ) )
            // InternalDOF.g:2390:1: ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? )
            {
            // InternalDOF.g:2390:1: ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? )
            // InternalDOF.g:2391:2: ( rule__TypeCustomization__AttributeFilterAssignment_1 )?
            {
             before(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAssignment_1()); 
            // InternalDOF.g:2392:2: ( rule__TypeCustomization__AttributeFilterAssignment_1 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==30) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalDOF.g:2392:3: rule__TypeCustomization__AttributeFilterAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypeCustomization__AttributeFilterAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__1__Impl"


    // $ANTLR start "rule__TypeCustomization__Group__2"
    // InternalDOF.g:2400:1: rule__TypeCustomization__Group__2 : rule__TypeCustomization__Group__2__Impl ;
    public final void rule__TypeCustomization__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2404:1: ( rule__TypeCustomization__Group__2__Impl )
            // InternalDOF.g:2405:2: rule__TypeCustomization__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__2"


    // $ANTLR start "rule__TypeCustomization__Group__2__Impl"
    // InternalDOF.g:2411:1: rule__TypeCustomization__Group__2__Impl : ( ( rule__TypeCustomization__Group_2__0 )? ) ;
    public final void rule__TypeCustomization__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2415:1: ( ( ( rule__TypeCustomization__Group_2__0 )? ) )
            // InternalDOF.g:2416:1: ( ( rule__TypeCustomization__Group_2__0 )? )
            {
            // InternalDOF.g:2416:1: ( ( rule__TypeCustomization__Group_2__0 )? )
            // InternalDOF.g:2417:2: ( rule__TypeCustomization__Group_2__0 )?
            {
             before(grammarAccess.getTypeCustomizationAccess().getGroup_2()); 
            // InternalDOF.g:2418:2: ( rule__TypeCustomization__Group_2__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==23) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalDOF.g:2418:3: rule__TypeCustomization__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypeCustomization__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTypeCustomizationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__2__Impl"


    // $ANTLR start "rule__TypeCustomization__Group_2__0"
    // InternalDOF.g:2427:1: rule__TypeCustomization__Group_2__0 : rule__TypeCustomization__Group_2__0__Impl rule__TypeCustomization__Group_2__1 ;
    public final void rule__TypeCustomization__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2431:1: ( rule__TypeCustomization__Group_2__0__Impl rule__TypeCustomization__Group_2__1 )
            // InternalDOF.g:2432:2: rule__TypeCustomization__Group_2__0__Impl rule__TypeCustomization__Group_2__1
            {
            pushFollow(FOLLOW_28);
            rule__TypeCustomization__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__0"


    // $ANTLR start "rule__TypeCustomization__Group_2__0__Impl"
    // InternalDOF.g:2439:1: rule__TypeCustomization__Group_2__0__Impl : ( '{' ) ;
    public final void rule__TypeCustomization__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2443:1: ( ( '{' ) )
            // InternalDOF.g:2444:1: ( '{' )
            {
            // InternalDOF.g:2444:1: ( '{' )
            // InternalDOF.g:2445:2: '{'
            {
             before(grammarAccess.getTypeCustomizationAccess().getLeftCurlyBracketKeyword_2_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getTypeCustomizationAccess().getLeftCurlyBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__0__Impl"


    // $ANTLR start "rule__TypeCustomization__Group_2__1"
    // InternalDOF.g:2454:1: rule__TypeCustomization__Group_2__1 : rule__TypeCustomization__Group_2__1__Impl rule__TypeCustomization__Group_2__2 ;
    public final void rule__TypeCustomization__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2458:1: ( rule__TypeCustomization__Group_2__1__Impl rule__TypeCustomization__Group_2__2 )
            // InternalDOF.g:2459:2: rule__TypeCustomization__Group_2__1__Impl rule__TypeCustomization__Group_2__2
            {
            pushFollow(FOLLOW_28);
            rule__TypeCustomization__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__1"


    // $ANTLR start "rule__TypeCustomization__Group_2__1__Impl"
    // InternalDOF.g:2466:1: rule__TypeCustomization__Group_2__1__Impl : ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* ) ;
    public final void rule__TypeCustomization__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2470:1: ( ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* ) )
            // InternalDOF.g:2471:1: ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* )
            {
            // InternalDOF.g:2471:1: ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* )
            // InternalDOF.g:2472:2: ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )*
            {
             before(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesAssignment_2_1()); 
            // InternalDOF.g:2473:2: ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==RULE_ID||LA26_0==25) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalDOF.g:2473:3: rule__TypeCustomization__IncludedReferencesAssignment_2_1
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__TypeCustomization__IncludedReferencesAssignment_2_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__1__Impl"


    // $ANTLR start "rule__TypeCustomization__Group_2__2"
    // InternalDOF.g:2481:1: rule__TypeCustomization__Group_2__2 : rule__TypeCustomization__Group_2__2__Impl ;
    public final void rule__TypeCustomization__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2485:1: ( rule__TypeCustomization__Group_2__2__Impl )
            // InternalDOF.g:2486:2: rule__TypeCustomization__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__2"


    // $ANTLR start "rule__TypeCustomization__Group_2__2__Impl"
    // InternalDOF.g:2492:1: rule__TypeCustomization__Group_2__2__Impl : ( '}' ) ;
    public final void rule__TypeCustomization__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2496:1: ( ( '}' ) )
            // InternalDOF.g:2497:1: ( '}' )
            {
            // InternalDOF.g:2497:1: ( '}' )
            // InternalDOF.g:2498:2: '}'
            {
             before(grammarAccess.getTypeCustomizationAccess().getRightCurlyBracketKeyword_2_2()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getTypeCustomizationAccess().getRightCurlyBracketKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__2__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__0"
    // InternalDOF.g:2508:1: rule__AttributeFilter__Group__0 : rule__AttributeFilter__Group__0__Impl rule__AttributeFilter__Group__1 ;
    public final void rule__AttributeFilter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2512:1: ( rule__AttributeFilter__Group__0__Impl rule__AttributeFilter__Group__1 )
            // InternalDOF.g:2513:2: rule__AttributeFilter__Group__0__Impl rule__AttributeFilter__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__AttributeFilter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__0"


    // $ANTLR start "rule__AttributeFilter__Group__0__Impl"
    // InternalDOF.g:2520:1: rule__AttributeFilter__Group__0__Impl : ( '[' ) ;
    public final void rule__AttributeFilter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2524:1: ( ( '[' ) )
            // InternalDOF.g:2525:1: ( '[' )
            {
            // InternalDOF.g:2525:1: ( '[' )
            // InternalDOF.g:2526:2: '['
            {
             before(grammarAccess.getAttributeFilterAccess().getLeftSquareBracketKeyword_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__0__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__1"
    // InternalDOF.g:2535:1: rule__AttributeFilter__Group__1 : rule__AttributeFilter__Group__1__Impl rule__AttributeFilter__Group__2 ;
    public final void rule__AttributeFilter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2539:1: ( rule__AttributeFilter__Group__1__Impl rule__AttributeFilter__Group__2 )
            // InternalDOF.g:2540:2: rule__AttributeFilter__Group__1__Impl rule__AttributeFilter__Group__2
            {
            pushFollow(FOLLOW_29);
            rule__AttributeFilter__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__1"


    // $ANTLR start "rule__AttributeFilter__Group__1__Impl"
    // InternalDOF.g:2547:1: rule__AttributeFilter__Group__1__Impl : ( ( rule__AttributeFilter__AttributesAssignment_1 ) ) ;
    public final void rule__AttributeFilter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2551:1: ( ( ( rule__AttributeFilter__AttributesAssignment_1 ) ) )
            // InternalDOF.g:2552:1: ( ( rule__AttributeFilter__AttributesAssignment_1 ) )
            {
            // InternalDOF.g:2552:1: ( ( rule__AttributeFilter__AttributesAssignment_1 ) )
            // InternalDOF.g:2553:2: ( rule__AttributeFilter__AttributesAssignment_1 )
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_1()); 
            // InternalDOF.g:2554:2: ( rule__AttributeFilter__AttributesAssignment_1 )
            // InternalDOF.g:2554:3: rule__AttributeFilter__AttributesAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__AttributesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__1__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__2"
    // InternalDOF.g:2562:1: rule__AttributeFilter__Group__2 : rule__AttributeFilter__Group__2__Impl rule__AttributeFilter__Group__3 ;
    public final void rule__AttributeFilter__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2566:1: ( rule__AttributeFilter__Group__2__Impl rule__AttributeFilter__Group__3 )
            // InternalDOF.g:2567:2: rule__AttributeFilter__Group__2__Impl rule__AttributeFilter__Group__3
            {
            pushFollow(FOLLOW_29);
            rule__AttributeFilter__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__2"


    // $ANTLR start "rule__AttributeFilter__Group__2__Impl"
    // InternalDOF.g:2574:1: rule__AttributeFilter__Group__2__Impl : ( ( rule__AttributeFilter__Group_2__0 )* ) ;
    public final void rule__AttributeFilter__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2578:1: ( ( ( rule__AttributeFilter__Group_2__0 )* ) )
            // InternalDOF.g:2579:1: ( ( rule__AttributeFilter__Group_2__0 )* )
            {
            // InternalDOF.g:2579:1: ( ( rule__AttributeFilter__Group_2__0 )* )
            // InternalDOF.g:2580:2: ( rule__AttributeFilter__Group_2__0 )*
            {
             before(grammarAccess.getAttributeFilterAccess().getGroup_2()); 
            // InternalDOF.g:2581:2: ( rule__AttributeFilter__Group_2__0 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==32) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalDOF.g:2581:3: rule__AttributeFilter__Group_2__0
            	    {
            	    pushFollow(FOLLOW_30);
            	    rule__AttributeFilter__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getAttributeFilterAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__2__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__3"
    // InternalDOF.g:2589:1: rule__AttributeFilter__Group__3 : rule__AttributeFilter__Group__3__Impl ;
    public final void rule__AttributeFilter__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2593:1: ( rule__AttributeFilter__Group__3__Impl )
            // InternalDOF.g:2594:2: rule__AttributeFilter__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__3"


    // $ANTLR start "rule__AttributeFilter__Group__3__Impl"
    // InternalDOF.g:2600:1: rule__AttributeFilter__Group__3__Impl : ( ']' ) ;
    public final void rule__AttributeFilter__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2604:1: ( ( ']' ) )
            // InternalDOF.g:2605:1: ( ']' )
            {
            // InternalDOF.g:2605:1: ( ']' )
            // InternalDOF.g:2606:2: ']'
            {
             before(grammarAccess.getAttributeFilterAccess().getRightSquareBracketKeyword_3()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getRightSquareBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__3__Impl"


    // $ANTLR start "rule__AttributeFilter__Group_2__0"
    // InternalDOF.g:2616:1: rule__AttributeFilter__Group_2__0 : rule__AttributeFilter__Group_2__0__Impl rule__AttributeFilter__Group_2__1 ;
    public final void rule__AttributeFilter__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2620:1: ( rule__AttributeFilter__Group_2__0__Impl rule__AttributeFilter__Group_2__1 )
            // InternalDOF.g:2621:2: rule__AttributeFilter__Group_2__0__Impl rule__AttributeFilter__Group_2__1
            {
            pushFollow(FOLLOW_7);
            rule__AttributeFilter__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__0"


    // $ANTLR start "rule__AttributeFilter__Group_2__0__Impl"
    // InternalDOF.g:2628:1: rule__AttributeFilter__Group_2__0__Impl : ( ',' ) ;
    public final void rule__AttributeFilter__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2632:1: ( ( ',' ) )
            // InternalDOF.g:2633:1: ( ',' )
            {
            // InternalDOF.g:2633:1: ( ',' )
            // InternalDOF.g:2634:2: ','
            {
             before(grammarAccess.getAttributeFilterAccess().getCommaKeyword_2_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__0__Impl"


    // $ANTLR start "rule__AttributeFilter__Group_2__1"
    // InternalDOF.g:2643:1: rule__AttributeFilter__Group_2__1 : rule__AttributeFilter__Group_2__1__Impl ;
    public final void rule__AttributeFilter__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2647:1: ( rule__AttributeFilter__Group_2__1__Impl )
            // InternalDOF.g:2648:2: rule__AttributeFilter__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__1"


    // $ANTLR start "rule__AttributeFilter__Group_2__1__Impl"
    // InternalDOF.g:2654:1: rule__AttributeFilter__Group_2__1__Impl : ( ( rule__AttributeFilter__AttributesAssignment_2_1 ) ) ;
    public final void rule__AttributeFilter__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2658:1: ( ( ( rule__AttributeFilter__AttributesAssignment_2_1 ) ) )
            // InternalDOF.g:2659:1: ( ( rule__AttributeFilter__AttributesAssignment_2_1 ) )
            {
            // InternalDOF.g:2659:1: ( ( rule__AttributeFilter__AttributesAssignment_2_1 ) )
            // InternalDOF.g:2660:2: ( rule__AttributeFilter__AttributesAssignment_2_1 )
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_2_1()); 
            // InternalDOF.g:2661:2: ( rule__AttributeFilter__AttributesAssignment_2_1 )
            // InternalDOF.g:2661:3: rule__AttributeFilter__AttributesAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__AttributesAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__1__Impl"


    // $ANTLR start "rule__InstancesFilter__Group__0"
    // InternalDOF.g:2670:1: rule__InstancesFilter__Group__0 : rule__InstancesFilter__Group__0__Impl rule__InstancesFilter__Group__1 ;
    public final void rule__InstancesFilter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2674:1: ( rule__InstancesFilter__Group__0__Impl rule__InstancesFilter__Group__1 )
            // InternalDOF.g:2675:2: rule__InstancesFilter__Group__0__Impl rule__InstancesFilter__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__InstancesFilter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstancesFilter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__0"


    // $ANTLR start "rule__InstancesFilter__Group__0__Impl"
    // InternalDOF.g:2682:1: rule__InstancesFilter__Group__0__Impl : ( 'where' ) ;
    public final void rule__InstancesFilter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2686:1: ( ( 'where' ) )
            // InternalDOF.g:2687:1: ( 'where' )
            {
            // InternalDOF.g:2687:1: ( 'where' )
            // InternalDOF.g:2688:2: 'where'
            {
             before(grammarAccess.getInstancesFilterAccess().getWhereKeyword_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getInstancesFilterAccess().getWhereKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__0__Impl"


    // $ANTLR start "rule__InstancesFilter__Group__1"
    // InternalDOF.g:2697:1: rule__InstancesFilter__Group__1 : rule__InstancesFilter__Group__1__Impl ;
    public final void rule__InstancesFilter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2701:1: ( rule__InstancesFilter__Group__1__Impl )
            // InternalDOF.g:2702:2: rule__InstancesFilter__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InstancesFilter__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__1"


    // $ANTLR start "rule__InstancesFilter__Group__1__Impl"
    // InternalDOF.g:2708:1: rule__InstancesFilter__Group__1__Impl : ( ruleAndConjunction ) ;
    public final void rule__InstancesFilter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2712:1: ( ( ruleAndConjunction ) )
            // InternalDOF.g:2713:1: ( ruleAndConjunction )
            {
            // InternalDOF.g:2713:1: ( ruleAndConjunction )
            // InternalDOF.g:2714:2: ruleAndConjunction
            {
             before(grammarAccess.getInstancesFilterAccess().getAndConjunctionParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleAndConjunction();

            state._fsp--;

             after(grammarAccess.getInstancesFilterAccess().getAndConjunctionParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__1__Impl"


    // $ANTLR start "rule__AndConjunction__Group__0"
    // InternalDOF.g:2724:1: rule__AndConjunction__Group__0 : rule__AndConjunction__Group__0__Impl rule__AndConjunction__Group__1 ;
    public final void rule__AndConjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2728:1: ( rule__AndConjunction__Group__0__Impl rule__AndConjunction__Group__1 )
            // InternalDOF.g:2729:2: rule__AndConjunction__Group__0__Impl rule__AndConjunction__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__AndConjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__0"


    // $ANTLR start "rule__AndConjunction__Group__0__Impl"
    // InternalDOF.g:2736:1: rule__AndConjunction__Group__0__Impl : ( ruleOrConjunction ) ;
    public final void rule__AndConjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2740:1: ( ( ruleOrConjunction ) )
            // InternalDOF.g:2741:1: ( ruleOrConjunction )
            {
            // InternalDOF.g:2741:1: ( ruleOrConjunction )
            // InternalDOF.g:2742:2: ruleOrConjunction
            {
             before(grammarAccess.getAndConjunctionAccess().getOrConjunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOrConjunction();

            state._fsp--;

             after(grammarAccess.getAndConjunctionAccess().getOrConjunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__0__Impl"


    // $ANTLR start "rule__AndConjunction__Group__1"
    // InternalDOF.g:2751:1: rule__AndConjunction__Group__1 : rule__AndConjunction__Group__1__Impl ;
    public final void rule__AndConjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2755:1: ( rule__AndConjunction__Group__1__Impl )
            // InternalDOF.g:2756:2: rule__AndConjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__1"


    // $ANTLR start "rule__AndConjunction__Group__1__Impl"
    // InternalDOF.g:2762:1: rule__AndConjunction__Group__1__Impl : ( ( rule__AndConjunction__Group_1__0 )* ) ;
    public final void rule__AndConjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2766:1: ( ( ( rule__AndConjunction__Group_1__0 )* ) )
            // InternalDOF.g:2767:1: ( ( rule__AndConjunction__Group_1__0 )* )
            {
            // InternalDOF.g:2767:1: ( ( rule__AndConjunction__Group_1__0 )* )
            // InternalDOF.g:2768:2: ( rule__AndConjunction__Group_1__0 )*
            {
             before(grammarAccess.getAndConjunctionAccess().getGroup_1()); 
            // InternalDOF.g:2769:2: ( rule__AndConjunction__Group_1__0 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==34) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalDOF.g:2769:3: rule__AndConjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__AndConjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getAndConjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__1__Impl"


    // $ANTLR start "rule__AndConjunction__Group_1__0"
    // InternalDOF.g:2778:1: rule__AndConjunction__Group_1__0 : rule__AndConjunction__Group_1__0__Impl rule__AndConjunction__Group_1__1 ;
    public final void rule__AndConjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2782:1: ( rule__AndConjunction__Group_1__0__Impl rule__AndConjunction__Group_1__1 )
            // InternalDOF.g:2783:2: rule__AndConjunction__Group_1__0__Impl rule__AndConjunction__Group_1__1
            {
            pushFollow(FOLLOW_32);
            rule__AndConjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__0"


    // $ANTLR start "rule__AndConjunction__Group_1__0__Impl"
    // InternalDOF.g:2790:1: rule__AndConjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__AndConjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2794:1: ( ( () ) )
            // InternalDOF.g:2795:1: ( () )
            {
            // InternalDOF.g:2795:1: ( () )
            // InternalDOF.g:2796:2: ()
            {
             before(grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0()); 
            // InternalDOF.g:2797:2: ()
            // InternalDOF.g:2797:3: 
            {
            }

             after(grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__0__Impl"


    // $ANTLR start "rule__AndConjunction__Group_1__1"
    // InternalDOF.g:2805:1: rule__AndConjunction__Group_1__1 : rule__AndConjunction__Group_1__1__Impl rule__AndConjunction__Group_1__2 ;
    public final void rule__AndConjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2809:1: ( rule__AndConjunction__Group_1__1__Impl rule__AndConjunction__Group_1__2 )
            // InternalDOF.g:2810:2: rule__AndConjunction__Group_1__1__Impl rule__AndConjunction__Group_1__2
            {
            pushFollow(FOLLOW_31);
            rule__AndConjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__1"


    // $ANTLR start "rule__AndConjunction__Group_1__1__Impl"
    // InternalDOF.g:2817:1: rule__AndConjunction__Group_1__1__Impl : ( 'and' ) ;
    public final void rule__AndConjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2821:1: ( ( 'and' ) )
            // InternalDOF.g:2822:1: ( 'and' )
            {
            // InternalDOF.g:2822:1: ( 'and' )
            // InternalDOF.g:2823:2: 'and'
            {
             before(grammarAccess.getAndConjunctionAccess().getAndKeyword_1_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getAndConjunctionAccess().getAndKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__1__Impl"


    // $ANTLR start "rule__AndConjunction__Group_1__2"
    // InternalDOF.g:2832:1: rule__AndConjunction__Group_1__2 : rule__AndConjunction__Group_1__2__Impl ;
    public final void rule__AndConjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2836:1: ( rule__AndConjunction__Group_1__2__Impl )
            // InternalDOF.g:2837:2: rule__AndConjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__2"


    // $ANTLR start "rule__AndConjunction__Group_1__2__Impl"
    // InternalDOF.g:2843:1: rule__AndConjunction__Group_1__2__Impl : ( ( rule__AndConjunction__RightAssignment_1_2 ) ) ;
    public final void rule__AndConjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2847:1: ( ( ( rule__AndConjunction__RightAssignment_1_2 ) ) )
            // InternalDOF.g:2848:1: ( ( rule__AndConjunction__RightAssignment_1_2 ) )
            {
            // InternalDOF.g:2848:1: ( ( rule__AndConjunction__RightAssignment_1_2 ) )
            // InternalDOF.g:2849:2: ( rule__AndConjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndConjunctionAccess().getRightAssignment_1_2()); 
            // InternalDOF.g:2850:2: ( rule__AndConjunction__RightAssignment_1_2 )
            // InternalDOF.g:2850:3: rule__AndConjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndConjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__2__Impl"


    // $ANTLR start "rule__OrConjunction__Group__0"
    // InternalDOF.g:2859:1: rule__OrConjunction__Group__0 : rule__OrConjunction__Group__0__Impl rule__OrConjunction__Group__1 ;
    public final void rule__OrConjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2863:1: ( rule__OrConjunction__Group__0__Impl rule__OrConjunction__Group__1 )
            // InternalDOF.g:2864:2: rule__OrConjunction__Group__0__Impl rule__OrConjunction__Group__1
            {
            pushFollow(FOLLOW_34);
            rule__OrConjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__0"


    // $ANTLR start "rule__OrConjunction__Group__0__Impl"
    // InternalDOF.g:2871:1: rule__OrConjunction__Group__0__Impl : ( rulePrimary ) ;
    public final void rule__OrConjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2875:1: ( ( rulePrimary ) )
            // InternalDOF.g:2876:1: ( rulePrimary )
            {
            // InternalDOF.g:2876:1: ( rulePrimary )
            // InternalDOF.g:2877:2: rulePrimary
            {
             before(grammarAccess.getOrConjunctionAccess().getPrimaryParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getOrConjunctionAccess().getPrimaryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__0__Impl"


    // $ANTLR start "rule__OrConjunction__Group__1"
    // InternalDOF.g:2886:1: rule__OrConjunction__Group__1 : rule__OrConjunction__Group__1__Impl ;
    public final void rule__OrConjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2890:1: ( rule__OrConjunction__Group__1__Impl )
            // InternalDOF.g:2891:2: rule__OrConjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__1"


    // $ANTLR start "rule__OrConjunction__Group__1__Impl"
    // InternalDOF.g:2897:1: rule__OrConjunction__Group__1__Impl : ( ( rule__OrConjunction__Group_1__0 )* ) ;
    public final void rule__OrConjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2901:1: ( ( ( rule__OrConjunction__Group_1__0 )* ) )
            // InternalDOF.g:2902:1: ( ( rule__OrConjunction__Group_1__0 )* )
            {
            // InternalDOF.g:2902:1: ( ( rule__OrConjunction__Group_1__0 )* )
            // InternalDOF.g:2903:2: ( rule__OrConjunction__Group_1__0 )*
            {
             before(grammarAccess.getOrConjunctionAccess().getGroup_1()); 
            // InternalDOF.g:2904:2: ( rule__OrConjunction__Group_1__0 )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==35) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalDOF.g:2904:3: rule__OrConjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_35);
            	    rule__OrConjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

             after(grammarAccess.getOrConjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__1__Impl"


    // $ANTLR start "rule__OrConjunction__Group_1__0"
    // InternalDOF.g:2913:1: rule__OrConjunction__Group_1__0 : rule__OrConjunction__Group_1__0__Impl rule__OrConjunction__Group_1__1 ;
    public final void rule__OrConjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2917:1: ( rule__OrConjunction__Group_1__0__Impl rule__OrConjunction__Group_1__1 )
            // InternalDOF.g:2918:2: rule__OrConjunction__Group_1__0__Impl rule__OrConjunction__Group_1__1
            {
            pushFollow(FOLLOW_34);
            rule__OrConjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__0"


    // $ANTLR start "rule__OrConjunction__Group_1__0__Impl"
    // InternalDOF.g:2925:1: rule__OrConjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__OrConjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2929:1: ( ( () ) )
            // InternalDOF.g:2930:1: ( () )
            {
            // InternalDOF.g:2930:1: ( () )
            // InternalDOF.g:2931:2: ()
            {
             before(grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0()); 
            // InternalDOF.g:2932:2: ()
            // InternalDOF.g:2932:3: 
            {
            }

             after(grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__0__Impl"


    // $ANTLR start "rule__OrConjunction__Group_1__1"
    // InternalDOF.g:2940:1: rule__OrConjunction__Group_1__1 : rule__OrConjunction__Group_1__1__Impl rule__OrConjunction__Group_1__2 ;
    public final void rule__OrConjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2944:1: ( rule__OrConjunction__Group_1__1__Impl rule__OrConjunction__Group_1__2 )
            // InternalDOF.g:2945:2: rule__OrConjunction__Group_1__1__Impl rule__OrConjunction__Group_1__2
            {
            pushFollow(FOLLOW_31);
            rule__OrConjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__1"


    // $ANTLR start "rule__OrConjunction__Group_1__1__Impl"
    // InternalDOF.g:2952:1: rule__OrConjunction__Group_1__1__Impl : ( 'or' ) ;
    public final void rule__OrConjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2956:1: ( ( 'or' ) )
            // InternalDOF.g:2957:1: ( 'or' )
            {
            // InternalDOF.g:2957:1: ( 'or' )
            // InternalDOF.g:2958:2: 'or'
            {
             before(grammarAccess.getOrConjunctionAccess().getOrKeyword_1_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getOrConjunctionAccess().getOrKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__1__Impl"


    // $ANTLR start "rule__OrConjunction__Group_1__2"
    // InternalDOF.g:2967:1: rule__OrConjunction__Group_1__2 : rule__OrConjunction__Group_1__2__Impl ;
    public final void rule__OrConjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2971:1: ( rule__OrConjunction__Group_1__2__Impl )
            // InternalDOF.g:2972:2: rule__OrConjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__2"


    // $ANTLR start "rule__OrConjunction__Group_1__2__Impl"
    // InternalDOF.g:2978:1: rule__OrConjunction__Group_1__2__Impl : ( ( rule__OrConjunction__RightAssignment_1_2 ) ) ;
    public final void rule__OrConjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2982:1: ( ( ( rule__OrConjunction__RightAssignment_1_2 ) ) )
            // InternalDOF.g:2983:1: ( ( rule__OrConjunction__RightAssignment_1_2 ) )
            {
            // InternalDOF.g:2983:1: ( ( rule__OrConjunction__RightAssignment_1_2 ) )
            // InternalDOF.g:2984:2: ( rule__OrConjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrConjunctionAccess().getRightAssignment_1_2()); 
            // InternalDOF.g:2985:2: ( rule__OrConjunction__RightAssignment_1_2 )
            // InternalDOF.g:2985:3: rule__OrConjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrConjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__2__Impl"


    // $ANTLR start "rule__Primary__Group_1__0"
    // InternalDOF.g:2994:1: rule__Primary__Group_1__0 : rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 ;
    public final void rule__Primary__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2998:1: ( rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 )
            // InternalDOF.g:2999:2: rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1
            {
            pushFollow(FOLLOW_31);
            rule__Primary__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0"


    // $ANTLR start "rule__Primary__Group_1__0__Impl"
    // InternalDOF.g:3006:1: rule__Primary__Group_1__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3010:1: ( ( '(' ) )
            // InternalDOF.g:3011:1: ( '(' )
            {
            // InternalDOF.g:3011:1: ( '(' )
            // InternalDOF.g:3012:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0__Impl"


    // $ANTLR start "rule__Primary__Group_1__1"
    // InternalDOF.g:3021:1: rule__Primary__Group_1__1 : rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 ;
    public final void rule__Primary__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3025:1: ( rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 )
            // InternalDOF.g:3026:2: rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2
            {
            pushFollow(FOLLOW_25);
            rule__Primary__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1"


    // $ANTLR start "rule__Primary__Group_1__1__Impl"
    // InternalDOF.g:3033:1: rule__Primary__Group_1__1__Impl : ( ruleAndConjunction ) ;
    public final void rule__Primary__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3037:1: ( ( ruleAndConjunction ) )
            // InternalDOF.g:3038:1: ( ruleAndConjunction )
            {
            // InternalDOF.g:3038:1: ( ruleAndConjunction )
            // InternalDOF.g:3039:2: ruleAndConjunction
            {
             before(grammarAccess.getPrimaryAccess().getAndConjunctionParserRuleCall_1_1()); 
            pushFollow(FOLLOW_2);
            ruleAndConjunction();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getAndConjunctionParserRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1__Impl"


    // $ANTLR start "rule__Primary__Group_1__2"
    // InternalDOF.g:3048:1: rule__Primary__Group_1__2 : rule__Primary__Group_1__2__Impl ;
    public final void rule__Primary__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3052:1: ( rule__Primary__Group_1__2__Impl )
            // InternalDOF.g:3053:2: rule__Primary__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2"


    // $ANTLR start "rule__Primary__Group_1__2__Impl"
    // InternalDOF.g:3059:1: rule__Primary__Group_1__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3063:1: ( ( ')' ) )
            // InternalDOF.g:3064:1: ( ')' )
            {
            // InternalDOF.g:3064:1: ( ')' )
            // InternalDOF.g:3065:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2__Impl"


    // $ANTLR start "rule__Comparison__Group_0__0"
    // InternalDOF.g:3075:1: rule__Comparison__Group_0__0 : rule__Comparison__Group_0__0__Impl rule__Comparison__Group_0__1 ;
    public final void rule__Comparison__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3079:1: ( rule__Comparison__Group_0__0__Impl rule__Comparison__Group_0__1 )
            // InternalDOF.g:3080:2: rule__Comparison__Group_0__0__Impl rule__Comparison__Group_0__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__0"


    // $ANTLR start "rule__Comparison__Group_0__0__Impl"
    // InternalDOF.g:3087:1: rule__Comparison__Group_0__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3091:1: ( ( () ) )
            // InternalDOF.g:3092:1: ( () )
            {
            // InternalDOF.g:3092:1: ( () )
            // InternalDOF.g:3093:2: ()
            {
             before(grammarAccess.getComparisonAccess().getEqualityAction_0_0()); 
            // InternalDOF.g:3094:2: ()
            // InternalDOF.g:3094:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getEqualityAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__0__Impl"


    // $ANTLR start "rule__Comparison__Group_0__1"
    // InternalDOF.g:3102:1: rule__Comparison__Group_0__1 : rule__Comparison__Group_0__1__Impl rule__Comparison__Group_0__2 ;
    public final void rule__Comparison__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3106:1: ( rule__Comparison__Group_0__1__Impl rule__Comparison__Group_0__2 )
            // InternalDOF.g:3107:2: rule__Comparison__Group_0__1__Impl rule__Comparison__Group_0__2
            {
            pushFollow(FOLLOW_36);
            rule__Comparison__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__1"


    // $ANTLR start "rule__Comparison__Group_0__1__Impl"
    // InternalDOF.g:3114:1: rule__Comparison__Group_0__1__Impl : ( ( rule__Comparison__PathAssignment_0_1 ) ) ;
    public final void rule__Comparison__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3118:1: ( ( ( rule__Comparison__PathAssignment_0_1 ) ) )
            // InternalDOF.g:3119:1: ( ( rule__Comparison__PathAssignment_0_1 ) )
            {
            // InternalDOF.g:3119:1: ( ( rule__Comparison__PathAssignment_0_1 ) )
            // InternalDOF.g:3120:2: ( rule__Comparison__PathAssignment_0_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_0_1()); 
            // InternalDOF.g:3121:2: ( rule__Comparison__PathAssignment_0_1 )
            // InternalDOF.g:3121:3: rule__Comparison__PathAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__1__Impl"


    // $ANTLR start "rule__Comparison__Group_0__2"
    // InternalDOF.g:3129:1: rule__Comparison__Group_0__2 : rule__Comparison__Group_0__2__Impl rule__Comparison__Group_0__3 ;
    public final void rule__Comparison__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3133:1: ( rule__Comparison__Group_0__2__Impl rule__Comparison__Group_0__3 )
            // InternalDOF.g:3134:2: rule__Comparison__Group_0__2__Impl rule__Comparison__Group_0__3
            {
            pushFollow(FOLLOW_37);
            rule__Comparison__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__2"


    // $ANTLR start "rule__Comparison__Group_0__2__Impl"
    // InternalDOF.g:3141:1: rule__Comparison__Group_0__2__Impl : ( '=' ) ;
    public final void rule__Comparison__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3145:1: ( ( '=' ) )
            // InternalDOF.g:3146:1: ( '=' )
            {
            // InternalDOF.g:3146:1: ( '=' )
            // InternalDOF.g:3147:2: '='
            {
             before(grammarAccess.getComparisonAccess().getEqualsSignKeyword_0_2()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getEqualsSignKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__2__Impl"


    // $ANTLR start "rule__Comparison__Group_0__3"
    // InternalDOF.g:3156:1: rule__Comparison__Group_0__3 : rule__Comparison__Group_0__3__Impl ;
    public final void rule__Comparison__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3160:1: ( rule__Comparison__Group_0__3__Impl )
            // InternalDOF.g:3161:2: rule__Comparison__Group_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__3"


    // $ANTLR start "rule__Comparison__Group_0__3__Impl"
    // InternalDOF.g:3167:1: rule__Comparison__Group_0__3__Impl : ( ( rule__Comparison__ValueAssignment_0_3 ) ) ;
    public final void rule__Comparison__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3171:1: ( ( ( rule__Comparison__ValueAssignment_0_3 ) ) )
            // InternalDOF.g:3172:1: ( ( rule__Comparison__ValueAssignment_0_3 ) )
            {
            // InternalDOF.g:3172:1: ( ( rule__Comparison__ValueAssignment_0_3 ) )
            // InternalDOF.g:3173:2: ( rule__Comparison__ValueAssignment_0_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_0_3()); 
            // InternalDOF.g:3174:2: ( rule__Comparison__ValueAssignment_0_3 )
            // InternalDOF.g:3174:3: rule__Comparison__ValueAssignment_0_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_0_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__3__Impl"


    // $ANTLR start "rule__Comparison__Group_1__0"
    // InternalDOF.g:3183:1: rule__Comparison__Group_1__0 : rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1 ;
    public final void rule__Comparison__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3187:1: ( rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1 )
            // InternalDOF.g:3188:2: rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__0"


    // $ANTLR start "rule__Comparison__Group_1__0__Impl"
    // InternalDOF.g:3195:1: rule__Comparison__Group_1__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3199:1: ( ( () ) )
            // InternalDOF.g:3200:1: ( () )
            {
            // InternalDOF.g:3200:1: ( () )
            // InternalDOF.g:3201:2: ()
            {
             before(grammarAccess.getComparisonAccess().getInequalityAction_1_0()); 
            // InternalDOF.g:3202:2: ()
            // InternalDOF.g:3202:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getInequalityAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__0__Impl"


    // $ANTLR start "rule__Comparison__Group_1__1"
    // InternalDOF.g:3210:1: rule__Comparison__Group_1__1 : rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2 ;
    public final void rule__Comparison__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3214:1: ( rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2 )
            // InternalDOF.g:3215:2: rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2
            {
            pushFollow(FOLLOW_38);
            rule__Comparison__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__1"


    // $ANTLR start "rule__Comparison__Group_1__1__Impl"
    // InternalDOF.g:3222:1: rule__Comparison__Group_1__1__Impl : ( ( rule__Comparison__PathAssignment_1_1 ) ) ;
    public final void rule__Comparison__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3226:1: ( ( ( rule__Comparison__PathAssignment_1_1 ) ) )
            // InternalDOF.g:3227:1: ( ( rule__Comparison__PathAssignment_1_1 ) )
            {
            // InternalDOF.g:3227:1: ( ( rule__Comparison__PathAssignment_1_1 ) )
            // InternalDOF.g:3228:2: ( rule__Comparison__PathAssignment_1_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_1_1()); 
            // InternalDOF.g:3229:2: ( rule__Comparison__PathAssignment_1_1 )
            // InternalDOF.g:3229:3: rule__Comparison__PathAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__1__Impl"


    // $ANTLR start "rule__Comparison__Group_1__2"
    // InternalDOF.g:3237:1: rule__Comparison__Group_1__2 : rule__Comparison__Group_1__2__Impl rule__Comparison__Group_1__3 ;
    public final void rule__Comparison__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3241:1: ( rule__Comparison__Group_1__2__Impl rule__Comparison__Group_1__3 )
            // InternalDOF.g:3242:2: rule__Comparison__Group_1__2__Impl rule__Comparison__Group_1__3
            {
            pushFollow(FOLLOW_37);
            rule__Comparison__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__2"


    // $ANTLR start "rule__Comparison__Group_1__2__Impl"
    // InternalDOF.g:3249:1: rule__Comparison__Group_1__2__Impl : ( '!=' ) ;
    public final void rule__Comparison__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3253:1: ( ( '!=' ) )
            // InternalDOF.g:3254:1: ( '!=' )
            {
            // InternalDOF.g:3254:1: ( '!=' )
            // InternalDOF.g:3255:2: '!='
            {
             before(grammarAccess.getComparisonAccess().getExclamationMarkEqualsSignKeyword_1_2()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getExclamationMarkEqualsSignKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__2__Impl"


    // $ANTLR start "rule__Comparison__Group_1__3"
    // InternalDOF.g:3264:1: rule__Comparison__Group_1__3 : rule__Comparison__Group_1__3__Impl ;
    public final void rule__Comparison__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3268:1: ( rule__Comparison__Group_1__3__Impl )
            // InternalDOF.g:3269:2: rule__Comparison__Group_1__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__3"


    // $ANTLR start "rule__Comparison__Group_1__3__Impl"
    // InternalDOF.g:3275:1: rule__Comparison__Group_1__3__Impl : ( ( rule__Comparison__ValueAssignment_1_3 ) ) ;
    public final void rule__Comparison__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3279:1: ( ( ( rule__Comparison__ValueAssignment_1_3 ) ) )
            // InternalDOF.g:3280:1: ( ( rule__Comparison__ValueAssignment_1_3 ) )
            {
            // InternalDOF.g:3280:1: ( ( rule__Comparison__ValueAssignment_1_3 ) )
            // InternalDOF.g:3281:2: ( rule__Comparison__ValueAssignment_1_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_1_3()); 
            // InternalDOF.g:3282:2: ( rule__Comparison__ValueAssignment_1_3 )
            // InternalDOF.g:3282:3: rule__Comparison__ValueAssignment_1_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_1_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__3__Impl"


    // $ANTLR start "rule__Comparison__Group_2__0"
    // InternalDOF.g:3291:1: rule__Comparison__Group_2__0 : rule__Comparison__Group_2__0__Impl rule__Comparison__Group_2__1 ;
    public final void rule__Comparison__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3295:1: ( rule__Comparison__Group_2__0__Impl rule__Comparison__Group_2__1 )
            // InternalDOF.g:3296:2: rule__Comparison__Group_2__0__Impl rule__Comparison__Group_2__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__0"


    // $ANTLR start "rule__Comparison__Group_2__0__Impl"
    // InternalDOF.g:3303:1: rule__Comparison__Group_2__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3307:1: ( ( () ) )
            // InternalDOF.g:3308:1: ( () )
            {
            // InternalDOF.g:3308:1: ( () )
            // InternalDOF.g:3309:2: ()
            {
             before(grammarAccess.getComparisonAccess().getMoreThanAction_2_0()); 
            // InternalDOF.g:3310:2: ()
            // InternalDOF.g:3310:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getMoreThanAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__0__Impl"


    // $ANTLR start "rule__Comparison__Group_2__1"
    // InternalDOF.g:3318:1: rule__Comparison__Group_2__1 : rule__Comparison__Group_2__1__Impl rule__Comparison__Group_2__2 ;
    public final void rule__Comparison__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3322:1: ( rule__Comparison__Group_2__1__Impl rule__Comparison__Group_2__2 )
            // InternalDOF.g:3323:2: rule__Comparison__Group_2__1__Impl rule__Comparison__Group_2__2
            {
            pushFollow(FOLLOW_39);
            rule__Comparison__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__1"


    // $ANTLR start "rule__Comparison__Group_2__1__Impl"
    // InternalDOF.g:3330:1: rule__Comparison__Group_2__1__Impl : ( ( rule__Comparison__PathAssignment_2_1 ) ) ;
    public final void rule__Comparison__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3334:1: ( ( ( rule__Comparison__PathAssignment_2_1 ) ) )
            // InternalDOF.g:3335:1: ( ( rule__Comparison__PathAssignment_2_1 ) )
            {
            // InternalDOF.g:3335:1: ( ( rule__Comparison__PathAssignment_2_1 ) )
            // InternalDOF.g:3336:2: ( rule__Comparison__PathAssignment_2_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_2_1()); 
            // InternalDOF.g:3337:2: ( rule__Comparison__PathAssignment_2_1 )
            // InternalDOF.g:3337:3: rule__Comparison__PathAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__1__Impl"


    // $ANTLR start "rule__Comparison__Group_2__2"
    // InternalDOF.g:3345:1: rule__Comparison__Group_2__2 : rule__Comparison__Group_2__2__Impl rule__Comparison__Group_2__3 ;
    public final void rule__Comparison__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3349:1: ( rule__Comparison__Group_2__2__Impl rule__Comparison__Group_2__3 )
            // InternalDOF.g:3350:2: rule__Comparison__Group_2__2__Impl rule__Comparison__Group_2__3
            {
            pushFollow(FOLLOW_37);
            rule__Comparison__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__2"


    // $ANTLR start "rule__Comparison__Group_2__2__Impl"
    // InternalDOF.g:3357:1: rule__Comparison__Group_2__2__Impl : ( '>' ) ;
    public final void rule__Comparison__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3361:1: ( ( '>' ) )
            // InternalDOF.g:3362:1: ( '>' )
            {
            // InternalDOF.g:3362:1: ( '>' )
            // InternalDOF.g:3363:2: '>'
            {
             before(grammarAccess.getComparisonAccess().getGreaterThanSignKeyword_2_2()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getGreaterThanSignKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__2__Impl"


    // $ANTLR start "rule__Comparison__Group_2__3"
    // InternalDOF.g:3372:1: rule__Comparison__Group_2__3 : rule__Comparison__Group_2__3__Impl ;
    public final void rule__Comparison__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3376:1: ( rule__Comparison__Group_2__3__Impl )
            // InternalDOF.g:3377:2: rule__Comparison__Group_2__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__3"


    // $ANTLR start "rule__Comparison__Group_2__3__Impl"
    // InternalDOF.g:3383:1: rule__Comparison__Group_2__3__Impl : ( ( rule__Comparison__ValueAssignment_2_3 ) ) ;
    public final void rule__Comparison__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3387:1: ( ( ( rule__Comparison__ValueAssignment_2_3 ) ) )
            // InternalDOF.g:3388:1: ( ( rule__Comparison__ValueAssignment_2_3 ) )
            {
            // InternalDOF.g:3388:1: ( ( rule__Comparison__ValueAssignment_2_3 ) )
            // InternalDOF.g:3389:2: ( rule__Comparison__ValueAssignment_2_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_2_3()); 
            // InternalDOF.g:3390:2: ( rule__Comparison__ValueAssignment_2_3 )
            // InternalDOF.g:3390:3: rule__Comparison__ValueAssignment_2_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_2_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__3__Impl"


    // $ANTLR start "rule__Comparison__Group_3__0"
    // InternalDOF.g:3399:1: rule__Comparison__Group_3__0 : rule__Comparison__Group_3__0__Impl rule__Comparison__Group_3__1 ;
    public final void rule__Comparison__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3403:1: ( rule__Comparison__Group_3__0__Impl rule__Comparison__Group_3__1 )
            // InternalDOF.g:3404:2: rule__Comparison__Group_3__0__Impl rule__Comparison__Group_3__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__0"


    // $ANTLR start "rule__Comparison__Group_3__0__Impl"
    // InternalDOF.g:3411:1: rule__Comparison__Group_3__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3415:1: ( ( () ) )
            // InternalDOF.g:3416:1: ( () )
            {
            // InternalDOF.g:3416:1: ( () )
            // InternalDOF.g:3417:2: ()
            {
             before(grammarAccess.getComparisonAccess().getMoreThanOrEqualAction_3_0()); 
            // InternalDOF.g:3418:2: ()
            // InternalDOF.g:3418:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getMoreThanOrEqualAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__0__Impl"


    // $ANTLR start "rule__Comparison__Group_3__1"
    // InternalDOF.g:3426:1: rule__Comparison__Group_3__1 : rule__Comparison__Group_3__1__Impl rule__Comparison__Group_3__2 ;
    public final void rule__Comparison__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3430:1: ( rule__Comparison__Group_3__1__Impl rule__Comparison__Group_3__2 )
            // InternalDOF.g:3431:2: rule__Comparison__Group_3__1__Impl rule__Comparison__Group_3__2
            {
            pushFollow(FOLLOW_40);
            rule__Comparison__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__1"


    // $ANTLR start "rule__Comparison__Group_3__1__Impl"
    // InternalDOF.g:3438:1: rule__Comparison__Group_3__1__Impl : ( ( rule__Comparison__PathAssignment_3_1 ) ) ;
    public final void rule__Comparison__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3442:1: ( ( ( rule__Comparison__PathAssignment_3_1 ) ) )
            // InternalDOF.g:3443:1: ( ( rule__Comparison__PathAssignment_3_1 ) )
            {
            // InternalDOF.g:3443:1: ( ( rule__Comparison__PathAssignment_3_1 ) )
            // InternalDOF.g:3444:2: ( rule__Comparison__PathAssignment_3_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_3_1()); 
            // InternalDOF.g:3445:2: ( rule__Comparison__PathAssignment_3_1 )
            // InternalDOF.g:3445:3: rule__Comparison__PathAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__1__Impl"


    // $ANTLR start "rule__Comparison__Group_3__2"
    // InternalDOF.g:3453:1: rule__Comparison__Group_3__2 : rule__Comparison__Group_3__2__Impl rule__Comparison__Group_3__3 ;
    public final void rule__Comparison__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3457:1: ( rule__Comparison__Group_3__2__Impl rule__Comparison__Group_3__3 )
            // InternalDOF.g:3458:2: rule__Comparison__Group_3__2__Impl rule__Comparison__Group_3__3
            {
            pushFollow(FOLLOW_37);
            rule__Comparison__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__2"


    // $ANTLR start "rule__Comparison__Group_3__2__Impl"
    // InternalDOF.g:3465:1: rule__Comparison__Group_3__2__Impl : ( '>=' ) ;
    public final void rule__Comparison__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3469:1: ( ( '>=' ) )
            // InternalDOF.g:3470:1: ( '>=' )
            {
            // InternalDOF.g:3470:1: ( '>=' )
            // InternalDOF.g:3471:2: '>='
            {
             before(grammarAccess.getComparisonAccess().getGreaterThanSignEqualsSignKeyword_3_2()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getGreaterThanSignEqualsSignKeyword_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__2__Impl"


    // $ANTLR start "rule__Comparison__Group_3__3"
    // InternalDOF.g:3480:1: rule__Comparison__Group_3__3 : rule__Comparison__Group_3__3__Impl ;
    public final void rule__Comparison__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3484:1: ( rule__Comparison__Group_3__3__Impl )
            // InternalDOF.g:3485:2: rule__Comparison__Group_3__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__3"


    // $ANTLR start "rule__Comparison__Group_3__3__Impl"
    // InternalDOF.g:3491:1: rule__Comparison__Group_3__3__Impl : ( ( rule__Comparison__ValueAssignment_3_3 ) ) ;
    public final void rule__Comparison__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3495:1: ( ( ( rule__Comparison__ValueAssignment_3_3 ) ) )
            // InternalDOF.g:3496:1: ( ( rule__Comparison__ValueAssignment_3_3 ) )
            {
            // InternalDOF.g:3496:1: ( ( rule__Comparison__ValueAssignment_3_3 ) )
            // InternalDOF.g:3497:2: ( rule__Comparison__ValueAssignment_3_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_3_3()); 
            // InternalDOF.g:3498:2: ( rule__Comparison__ValueAssignment_3_3 )
            // InternalDOF.g:3498:3: rule__Comparison__ValueAssignment_3_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_3_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__3__Impl"


    // $ANTLR start "rule__Comparison__Group_4__0"
    // InternalDOF.g:3507:1: rule__Comparison__Group_4__0 : rule__Comparison__Group_4__0__Impl rule__Comparison__Group_4__1 ;
    public final void rule__Comparison__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3511:1: ( rule__Comparison__Group_4__0__Impl rule__Comparison__Group_4__1 )
            // InternalDOF.g:3512:2: rule__Comparison__Group_4__0__Impl rule__Comparison__Group_4__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__0"


    // $ANTLR start "rule__Comparison__Group_4__0__Impl"
    // InternalDOF.g:3519:1: rule__Comparison__Group_4__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3523:1: ( ( () ) )
            // InternalDOF.g:3524:1: ( () )
            {
            // InternalDOF.g:3524:1: ( () )
            // InternalDOF.g:3525:2: ()
            {
             before(grammarAccess.getComparisonAccess().getLessThanAction_4_0()); 
            // InternalDOF.g:3526:2: ()
            // InternalDOF.g:3526:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getLessThanAction_4_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__0__Impl"


    // $ANTLR start "rule__Comparison__Group_4__1"
    // InternalDOF.g:3534:1: rule__Comparison__Group_4__1 : rule__Comparison__Group_4__1__Impl rule__Comparison__Group_4__2 ;
    public final void rule__Comparison__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3538:1: ( rule__Comparison__Group_4__1__Impl rule__Comparison__Group_4__2 )
            // InternalDOF.g:3539:2: rule__Comparison__Group_4__1__Impl rule__Comparison__Group_4__2
            {
            pushFollow(FOLLOW_41);
            rule__Comparison__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__1"


    // $ANTLR start "rule__Comparison__Group_4__1__Impl"
    // InternalDOF.g:3546:1: rule__Comparison__Group_4__1__Impl : ( ( rule__Comparison__PathAssignment_4_1 ) ) ;
    public final void rule__Comparison__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3550:1: ( ( ( rule__Comparison__PathAssignment_4_1 ) ) )
            // InternalDOF.g:3551:1: ( ( rule__Comparison__PathAssignment_4_1 ) )
            {
            // InternalDOF.g:3551:1: ( ( rule__Comparison__PathAssignment_4_1 ) )
            // InternalDOF.g:3552:2: ( rule__Comparison__PathAssignment_4_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_4_1()); 
            // InternalDOF.g:3553:2: ( rule__Comparison__PathAssignment_4_1 )
            // InternalDOF.g:3553:3: rule__Comparison__PathAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__1__Impl"


    // $ANTLR start "rule__Comparison__Group_4__2"
    // InternalDOF.g:3561:1: rule__Comparison__Group_4__2 : rule__Comparison__Group_4__2__Impl rule__Comparison__Group_4__3 ;
    public final void rule__Comparison__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3565:1: ( rule__Comparison__Group_4__2__Impl rule__Comparison__Group_4__3 )
            // InternalDOF.g:3566:2: rule__Comparison__Group_4__2__Impl rule__Comparison__Group_4__3
            {
            pushFollow(FOLLOW_37);
            rule__Comparison__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__2"


    // $ANTLR start "rule__Comparison__Group_4__2__Impl"
    // InternalDOF.g:3573:1: rule__Comparison__Group_4__2__Impl : ( '<' ) ;
    public final void rule__Comparison__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3577:1: ( ( '<' ) )
            // InternalDOF.g:3578:1: ( '<' )
            {
            // InternalDOF.g:3578:1: ( '<' )
            // InternalDOF.g:3579:2: '<'
            {
             before(grammarAccess.getComparisonAccess().getLessThanSignKeyword_4_2()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getLessThanSignKeyword_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__2__Impl"


    // $ANTLR start "rule__Comparison__Group_4__3"
    // InternalDOF.g:3588:1: rule__Comparison__Group_4__3 : rule__Comparison__Group_4__3__Impl ;
    public final void rule__Comparison__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3592:1: ( rule__Comparison__Group_4__3__Impl )
            // InternalDOF.g:3593:2: rule__Comparison__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__3"


    // $ANTLR start "rule__Comparison__Group_4__3__Impl"
    // InternalDOF.g:3599:1: rule__Comparison__Group_4__3__Impl : ( ( rule__Comparison__ValueAssignment_4_3 ) ) ;
    public final void rule__Comparison__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3603:1: ( ( ( rule__Comparison__ValueAssignment_4_3 ) ) )
            // InternalDOF.g:3604:1: ( ( rule__Comparison__ValueAssignment_4_3 ) )
            {
            // InternalDOF.g:3604:1: ( ( rule__Comparison__ValueAssignment_4_3 ) )
            // InternalDOF.g:3605:2: ( rule__Comparison__ValueAssignment_4_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_4_3()); 
            // InternalDOF.g:3606:2: ( rule__Comparison__ValueAssignment_4_3 )
            // InternalDOF.g:3606:3: rule__Comparison__ValueAssignment_4_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_4_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__3__Impl"


    // $ANTLR start "rule__Comparison__Group_5__0"
    // InternalDOF.g:3615:1: rule__Comparison__Group_5__0 : rule__Comparison__Group_5__0__Impl rule__Comparison__Group_5__1 ;
    public final void rule__Comparison__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3619:1: ( rule__Comparison__Group_5__0__Impl rule__Comparison__Group_5__1 )
            // InternalDOF.g:3620:2: rule__Comparison__Group_5__0__Impl rule__Comparison__Group_5__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__0"


    // $ANTLR start "rule__Comparison__Group_5__0__Impl"
    // InternalDOF.g:3627:1: rule__Comparison__Group_5__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3631:1: ( ( () ) )
            // InternalDOF.g:3632:1: ( () )
            {
            // InternalDOF.g:3632:1: ( () )
            // InternalDOF.g:3633:2: ()
            {
             before(grammarAccess.getComparisonAccess().getLessThanOrEqualAction_5_0()); 
            // InternalDOF.g:3634:2: ()
            // InternalDOF.g:3634:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getLessThanOrEqualAction_5_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__0__Impl"


    // $ANTLR start "rule__Comparison__Group_5__1"
    // InternalDOF.g:3642:1: rule__Comparison__Group_5__1 : rule__Comparison__Group_5__1__Impl rule__Comparison__Group_5__2 ;
    public final void rule__Comparison__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3646:1: ( rule__Comparison__Group_5__1__Impl rule__Comparison__Group_5__2 )
            // InternalDOF.g:3647:2: rule__Comparison__Group_5__1__Impl rule__Comparison__Group_5__2
            {
            pushFollow(FOLLOW_42);
            rule__Comparison__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__1"


    // $ANTLR start "rule__Comparison__Group_5__1__Impl"
    // InternalDOF.g:3654:1: rule__Comparison__Group_5__1__Impl : ( ( rule__Comparison__PathAssignment_5_1 ) ) ;
    public final void rule__Comparison__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3658:1: ( ( ( rule__Comparison__PathAssignment_5_1 ) ) )
            // InternalDOF.g:3659:1: ( ( rule__Comparison__PathAssignment_5_1 ) )
            {
            // InternalDOF.g:3659:1: ( ( rule__Comparison__PathAssignment_5_1 ) )
            // InternalDOF.g:3660:2: ( rule__Comparison__PathAssignment_5_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_5_1()); 
            // InternalDOF.g:3661:2: ( rule__Comparison__PathAssignment_5_1 )
            // InternalDOF.g:3661:3: rule__Comparison__PathAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__1__Impl"


    // $ANTLR start "rule__Comparison__Group_5__2"
    // InternalDOF.g:3669:1: rule__Comparison__Group_5__2 : rule__Comparison__Group_5__2__Impl rule__Comparison__Group_5__3 ;
    public final void rule__Comparison__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3673:1: ( rule__Comparison__Group_5__2__Impl rule__Comparison__Group_5__3 )
            // InternalDOF.g:3674:2: rule__Comparison__Group_5__2__Impl rule__Comparison__Group_5__3
            {
            pushFollow(FOLLOW_37);
            rule__Comparison__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__2"


    // $ANTLR start "rule__Comparison__Group_5__2__Impl"
    // InternalDOF.g:3681:1: rule__Comparison__Group_5__2__Impl : ( '<=' ) ;
    public final void rule__Comparison__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3685:1: ( ( '<=' ) )
            // InternalDOF.g:3686:1: ( '<=' )
            {
            // InternalDOF.g:3686:1: ( '<=' )
            // InternalDOF.g:3687:2: '<='
            {
             before(grammarAccess.getComparisonAccess().getLessThanSignEqualsSignKeyword_5_2()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getLessThanSignEqualsSignKeyword_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__2__Impl"


    // $ANTLR start "rule__Comparison__Group_5__3"
    // InternalDOF.g:3696:1: rule__Comparison__Group_5__3 : rule__Comparison__Group_5__3__Impl ;
    public final void rule__Comparison__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3700:1: ( rule__Comparison__Group_5__3__Impl )
            // InternalDOF.g:3701:2: rule__Comparison__Group_5__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__3"


    // $ANTLR start "rule__Comparison__Group_5__3__Impl"
    // InternalDOF.g:3707:1: rule__Comparison__Group_5__3__Impl : ( ( rule__Comparison__ValueAssignment_5_3 ) ) ;
    public final void rule__Comparison__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3711:1: ( ( ( rule__Comparison__ValueAssignment_5_3 ) ) )
            // InternalDOF.g:3712:1: ( ( rule__Comparison__ValueAssignment_5_3 ) )
            {
            // InternalDOF.g:3712:1: ( ( rule__Comparison__ValueAssignment_5_3 ) )
            // InternalDOF.g:3713:2: ( rule__Comparison__ValueAssignment_5_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_5_3()); 
            // InternalDOF.g:3714:2: ( rule__Comparison__ValueAssignment_5_3 )
            // InternalDOF.g:3714:3: rule__Comparison__ValueAssignment_5_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_5_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__3__Impl"


    // $ANTLR start "rule__CompoundCause__Group__0"
    // InternalDOF.g:3723:1: rule__CompoundCause__Group__0 : rule__CompoundCause__Group__0__Impl rule__CompoundCause__Group__1 ;
    public final void rule__CompoundCause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3727:1: ( rule__CompoundCause__Group__0__Impl rule__CompoundCause__Group__1 )
            // InternalDOF.g:3728:2: rule__CompoundCause__Group__0__Impl rule__CompoundCause__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__CompoundCause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__0"


    // $ANTLR start "rule__CompoundCause__Group__0__Impl"
    // InternalDOF.g:3735:1: rule__CompoundCause__Group__0__Impl : ( () ) ;
    public final void rule__CompoundCause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3739:1: ( ( () ) )
            // InternalDOF.g:3740:1: ( () )
            {
            // InternalDOF.g:3740:1: ( () )
            // InternalDOF.g:3741:2: ()
            {
             before(grammarAccess.getCompoundCauseAccess().getCompoundCauseAction_0()); 
            // InternalDOF.g:3742:2: ()
            // InternalDOF.g:3742:3: 
            {
            }

             after(grammarAccess.getCompoundCauseAccess().getCompoundCauseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__0__Impl"


    // $ANTLR start "rule__CompoundCause__Group__1"
    // InternalDOF.g:3750:1: rule__CompoundCause__Group__1 : rule__CompoundCause__Group__1__Impl rule__CompoundCause__Group__2 ;
    public final void rule__CompoundCause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3754:1: ( rule__CompoundCause__Group__1__Impl rule__CompoundCause__Group__2 )
            // InternalDOF.g:3755:2: rule__CompoundCause__Group__1__Impl rule__CompoundCause__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__CompoundCause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__1"


    // $ANTLR start "rule__CompoundCause__Group__1__Impl"
    // InternalDOF.g:3762:1: rule__CompoundCause__Group__1__Impl : ( 'cause' ) ;
    public final void rule__CompoundCause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3766:1: ( ( 'cause' ) )
            // InternalDOF.g:3767:1: ( 'cause' )
            {
            // InternalDOF.g:3767:1: ( 'cause' )
            // InternalDOF.g:3768:2: 'cause'
            {
             before(grammarAccess.getCompoundCauseAccess().getCauseKeyword_1()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getCauseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__1__Impl"


    // $ANTLR start "rule__CompoundCause__Group__2"
    // InternalDOF.g:3777:1: rule__CompoundCause__Group__2 : rule__CompoundCause__Group__2__Impl rule__CompoundCause__Group__3 ;
    public final void rule__CompoundCause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3781:1: ( rule__CompoundCause__Group__2__Impl rule__CompoundCause__Group__3 )
            // InternalDOF.g:3782:2: rule__CompoundCause__Group__2__Impl rule__CompoundCause__Group__3
            {
            pushFollow(FOLLOW_43);
            rule__CompoundCause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__2"


    // $ANTLR start "rule__CompoundCause__Group__2__Impl"
    // InternalDOF.g:3789:1: rule__CompoundCause__Group__2__Impl : ( ( rule__CompoundCause__NameAssignment_2 ) ) ;
    public final void rule__CompoundCause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3793:1: ( ( ( rule__CompoundCause__NameAssignment_2 ) ) )
            // InternalDOF.g:3794:1: ( ( rule__CompoundCause__NameAssignment_2 ) )
            {
            // InternalDOF.g:3794:1: ( ( rule__CompoundCause__NameAssignment_2 ) )
            // InternalDOF.g:3795:2: ( rule__CompoundCause__NameAssignment_2 )
            {
             before(grammarAccess.getCompoundCauseAccess().getNameAssignment_2()); 
            // InternalDOF.g:3796:2: ( rule__CompoundCause__NameAssignment_2 )
            // InternalDOF.g:3796:3: rule__CompoundCause__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__2__Impl"


    // $ANTLR start "rule__CompoundCause__Group__3"
    // InternalDOF.g:3804:1: rule__CompoundCause__Group__3 : rule__CompoundCause__Group__3__Impl rule__CompoundCause__Group__4 ;
    public final void rule__CompoundCause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3808:1: ( rule__CompoundCause__Group__3__Impl rule__CompoundCause__Group__4 )
            // InternalDOF.g:3809:2: rule__CompoundCause__Group__3__Impl rule__CompoundCause__Group__4
            {
            pushFollow(FOLLOW_43);
            rule__CompoundCause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__3"


    // $ANTLR start "rule__CompoundCause__Group__3__Impl"
    // InternalDOF.g:3816:1: rule__CompoundCause__Group__3__Impl : ( ( rule__CompoundCause__Group_3__0 )? ) ;
    public final void rule__CompoundCause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3820:1: ( ( ( rule__CompoundCause__Group_3__0 )? ) )
            // InternalDOF.g:3821:1: ( ( rule__CompoundCause__Group_3__0 )? )
            {
            // InternalDOF.g:3821:1: ( ( rule__CompoundCause__Group_3__0 )? )
            // InternalDOF.g:3822:2: ( rule__CompoundCause__Group_3__0 )?
            {
             before(grammarAccess.getCompoundCauseAccess().getGroup_3()); 
            // InternalDOF.g:3823:2: ( rule__CompoundCause__Group_3__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==44) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalDOF.g:3823:3: rule__CompoundCause__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CompoundCause__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCompoundCauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__3__Impl"


    // $ANTLR start "rule__CompoundCause__Group__4"
    // InternalDOF.g:3831:1: rule__CompoundCause__Group__4 : rule__CompoundCause__Group__4__Impl rule__CompoundCause__Group__5 ;
    public final void rule__CompoundCause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3835:1: ( rule__CompoundCause__Group__4__Impl rule__CompoundCause__Group__5 )
            // InternalDOF.g:3836:2: rule__CompoundCause__Group__4__Impl rule__CompoundCause__Group__5
            {
            pushFollow(FOLLOW_44);
            rule__CompoundCause__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__4"


    // $ANTLR start "rule__CompoundCause__Group__4__Impl"
    // InternalDOF.g:3843:1: rule__CompoundCause__Group__4__Impl : ( 'contains' ) ;
    public final void rule__CompoundCause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3847:1: ( ( 'contains' ) )
            // InternalDOF.g:3848:1: ( 'contains' )
            {
            // InternalDOF.g:3848:1: ( 'contains' )
            // InternalDOF.g:3849:2: 'contains'
            {
             before(grammarAccess.getCompoundCauseAccess().getContainsKeyword_4()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getContainsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__4__Impl"


    // $ANTLR start "rule__CompoundCause__Group__5"
    // InternalDOF.g:3858:1: rule__CompoundCause__Group__5 : rule__CompoundCause__Group__5__Impl rule__CompoundCause__Group__6 ;
    public final void rule__CompoundCause__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3862:1: ( rule__CompoundCause__Group__5__Impl rule__CompoundCause__Group__6 )
            // InternalDOF.g:3863:2: rule__CompoundCause__Group__5__Impl rule__CompoundCause__Group__6
            {
            pushFollow(FOLLOW_12);
            rule__CompoundCause__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__5"


    // $ANTLR start "rule__CompoundCause__Group__5__Impl"
    // InternalDOF.g:3870:1: rule__CompoundCause__Group__5__Impl : ( '{' ) ;
    public final void rule__CompoundCause__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3874:1: ( ( '{' ) )
            // InternalDOF.g:3875:1: ( '{' )
            {
            // InternalDOF.g:3875:1: ( '{' )
            // InternalDOF.g:3876:2: '{'
            {
             before(grammarAccess.getCompoundCauseAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__5__Impl"


    // $ANTLR start "rule__CompoundCause__Group__6"
    // InternalDOF.g:3885:1: rule__CompoundCause__Group__6 : rule__CompoundCause__Group__6__Impl rule__CompoundCause__Group__7 ;
    public final void rule__CompoundCause__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3889:1: ( rule__CompoundCause__Group__6__Impl rule__CompoundCause__Group__7 )
            // InternalDOF.g:3890:2: rule__CompoundCause__Group__6__Impl rule__CompoundCause__Group__7
            {
            pushFollow(FOLLOW_45);
            rule__CompoundCause__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__6"


    // $ANTLR start "rule__CompoundCause__Group__6__Impl"
    // InternalDOF.g:3897:1: rule__CompoundCause__Group__6__Impl : ( ( rule__CompoundCause__SubCausesAssignment_6 ) ) ;
    public final void rule__CompoundCause__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3901:1: ( ( ( rule__CompoundCause__SubCausesAssignment_6 ) ) )
            // InternalDOF.g:3902:1: ( ( rule__CompoundCause__SubCausesAssignment_6 ) )
            {
            // InternalDOF.g:3902:1: ( ( rule__CompoundCause__SubCausesAssignment_6 ) )
            // InternalDOF.g:3903:2: ( rule__CompoundCause__SubCausesAssignment_6 )
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_6()); 
            // InternalDOF.g:3904:2: ( rule__CompoundCause__SubCausesAssignment_6 )
            // InternalDOF.g:3904:3: rule__CompoundCause__SubCausesAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__SubCausesAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__6__Impl"


    // $ANTLR start "rule__CompoundCause__Group__7"
    // InternalDOF.g:3912:1: rule__CompoundCause__Group__7 : rule__CompoundCause__Group__7__Impl rule__CompoundCause__Group__8 ;
    public final void rule__CompoundCause__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3916:1: ( rule__CompoundCause__Group__7__Impl rule__CompoundCause__Group__8 )
            // InternalDOF.g:3917:2: rule__CompoundCause__Group__7__Impl rule__CompoundCause__Group__8
            {
            pushFollow(FOLLOW_45);
            rule__CompoundCause__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__7"


    // $ANTLR start "rule__CompoundCause__Group__7__Impl"
    // InternalDOF.g:3924:1: rule__CompoundCause__Group__7__Impl : ( ( rule__CompoundCause__SubCausesAssignment_7 )* ) ;
    public final void rule__CompoundCause__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3928:1: ( ( ( rule__CompoundCause__SubCausesAssignment_7 )* ) )
            // InternalDOF.g:3929:1: ( ( rule__CompoundCause__SubCausesAssignment_7 )* )
            {
            // InternalDOF.g:3929:1: ( ( rule__CompoundCause__SubCausesAssignment_7 )* )
            // InternalDOF.g:3930:2: ( rule__CompoundCause__SubCausesAssignment_7 )*
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_7()); 
            // InternalDOF.g:3931:2: ( rule__CompoundCause__SubCausesAssignment_7 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==42) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalDOF.g:3931:3: rule__CompoundCause__SubCausesAssignment_7
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__CompoundCause__SubCausesAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__7__Impl"


    // $ANTLR start "rule__CompoundCause__Group__8"
    // InternalDOF.g:3939:1: rule__CompoundCause__Group__8 : rule__CompoundCause__Group__8__Impl ;
    public final void rule__CompoundCause__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3943:1: ( rule__CompoundCause__Group__8__Impl )
            // InternalDOF.g:3944:2: rule__CompoundCause__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__8"


    // $ANTLR start "rule__CompoundCause__Group__8__Impl"
    // InternalDOF.g:3950:1: rule__CompoundCause__Group__8__Impl : ( '}' ) ;
    public final void rule__CompoundCause__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3954:1: ( ( '}' ) )
            // InternalDOF.g:3955:1: ( '}' )
            {
            // InternalDOF.g:3955:1: ( '}' )
            // InternalDOF.g:3956:2: '}'
            {
             before(grammarAccess.getCompoundCauseAccess().getRightCurlyBracketKeyword_8()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__8__Impl"


    // $ANTLR start "rule__CompoundCause__Group_3__0"
    // InternalDOF.g:3966:1: rule__CompoundCause__Group_3__0 : rule__CompoundCause__Group_3__0__Impl rule__CompoundCause__Group_3__1 ;
    public final void rule__CompoundCause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3970:1: ( rule__CompoundCause__Group_3__0__Impl rule__CompoundCause__Group_3__1 )
            // InternalDOF.g:3971:2: rule__CompoundCause__Group_3__0__Impl rule__CompoundCause__Group_3__1
            {
            pushFollow(FOLLOW_37);
            rule__CompoundCause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_3__0"


    // $ANTLR start "rule__CompoundCause__Group_3__0__Impl"
    // InternalDOF.g:3978:1: rule__CompoundCause__Group_3__0__Impl : ( 'realizes' ) ;
    public final void rule__CompoundCause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3982:1: ( ( 'realizes' ) )
            // InternalDOF.g:3983:1: ( 'realizes' )
            {
            // InternalDOF.g:3983:1: ( 'realizes' )
            // InternalDOF.g:3984:2: 'realizes'
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesKeyword_3_0()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getRealizesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_3__0__Impl"


    // $ANTLR start "rule__CompoundCause__Group_3__1"
    // InternalDOF.g:3993:1: rule__CompoundCause__Group_3__1 : rule__CompoundCause__Group_3__1__Impl ;
    public final void rule__CompoundCause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3997:1: ( rule__CompoundCause__Group_3__1__Impl )
            // InternalDOF.g:3998:2: rule__CompoundCause__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_3__1"


    // $ANTLR start "rule__CompoundCause__Group_3__1__Impl"
    // InternalDOF.g:4004:1: rule__CompoundCause__Group_3__1__Impl : ( ( rule__CompoundCause__RealizesAssignment_3_1 ) ) ;
    public final void rule__CompoundCause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4008:1: ( ( ( rule__CompoundCause__RealizesAssignment_3_1 ) ) )
            // InternalDOF.g:4009:1: ( ( rule__CompoundCause__RealizesAssignment_3_1 ) )
            {
            // InternalDOF.g:4009:1: ( ( rule__CompoundCause__RealizesAssignment_3_1 ) )
            // InternalDOF.g:4010:2: ( rule__CompoundCause__RealizesAssignment_3_1 )
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesAssignment_3_1()); 
            // InternalDOF.g:4011:2: ( rule__CompoundCause__RealizesAssignment_3_1 )
            // InternalDOF.g:4011:3: rule__CompoundCause__RealizesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__RealizesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getRealizesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_3__1__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__0"
    // InternalDOF.g:4020:1: rule__DataLinkedCause__Group__0 : rule__DataLinkedCause__Group__0__Impl rule__DataLinkedCause__Group__1 ;
    public final void rule__DataLinkedCause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4024:1: ( rule__DataLinkedCause__Group__0__Impl rule__DataLinkedCause__Group__1 )
            // InternalDOF.g:4025:2: rule__DataLinkedCause__Group__0__Impl rule__DataLinkedCause__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__DataLinkedCause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__0"


    // $ANTLR start "rule__DataLinkedCause__Group__0__Impl"
    // InternalDOF.g:4032:1: rule__DataLinkedCause__Group__0__Impl : ( () ) ;
    public final void rule__DataLinkedCause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4036:1: ( ( () ) )
            // InternalDOF.g:4037:1: ( () )
            {
            // InternalDOF.g:4037:1: ( () )
            // InternalDOF.g:4038:2: ()
            {
             before(grammarAccess.getDataLinkedCauseAccess().getDataLinkedCauseAction_0()); 
            // InternalDOF.g:4039:2: ()
            // InternalDOF.g:4039:3: 
            {
            }

             after(grammarAccess.getDataLinkedCauseAccess().getDataLinkedCauseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__0__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__1"
    // InternalDOF.g:4047:1: rule__DataLinkedCause__Group__1 : rule__DataLinkedCause__Group__1__Impl rule__DataLinkedCause__Group__2 ;
    public final void rule__DataLinkedCause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4051:1: ( rule__DataLinkedCause__Group__1__Impl rule__DataLinkedCause__Group__2 )
            // InternalDOF.g:4052:2: rule__DataLinkedCause__Group__1__Impl rule__DataLinkedCause__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__DataLinkedCause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__1"


    // $ANTLR start "rule__DataLinkedCause__Group__1__Impl"
    // InternalDOF.g:4059:1: rule__DataLinkedCause__Group__1__Impl : ( 'cause' ) ;
    public final void rule__DataLinkedCause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4063:1: ( ( 'cause' ) )
            // InternalDOF.g:4064:1: ( 'cause' )
            {
            // InternalDOF.g:4064:1: ( 'cause' )
            // InternalDOF.g:4065:2: 'cause'
            {
             before(grammarAccess.getDataLinkedCauseAccess().getCauseKeyword_1()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getDataLinkedCauseAccess().getCauseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__1__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__2"
    // InternalDOF.g:4074:1: rule__DataLinkedCause__Group__2 : rule__DataLinkedCause__Group__2__Impl rule__DataLinkedCause__Group__3 ;
    public final void rule__DataLinkedCause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4078:1: ( rule__DataLinkedCause__Group__2__Impl rule__DataLinkedCause__Group__3 )
            // InternalDOF.g:4079:2: rule__DataLinkedCause__Group__2__Impl rule__DataLinkedCause__Group__3
            {
            pushFollow(FOLLOW_46);
            rule__DataLinkedCause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__2"


    // $ANTLR start "rule__DataLinkedCause__Group__2__Impl"
    // InternalDOF.g:4086:1: rule__DataLinkedCause__Group__2__Impl : ( ( rule__DataLinkedCause__NameAssignment_2 ) ) ;
    public final void rule__DataLinkedCause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4090:1: ( ( ( rule__DataLinkedCause__NameAssignment_2 ) ) )
            // InternalDOF.g:4091:1: ( ( rule__DataLinkedCause__NameAssignment_2 ) )
            {
            // InternalDOF.g:4091:1: ( ( rule__DataLinkedCause__NameAssignment_2 ) )
            // InternalDOF.g:4092:2: ( rule__DataLinkedCause__NameAssignment_2 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getNameAssignment_2()); 
            // InternalDOF.g:4093:2: ( rule__DataLinkedCause__NameAssignment_2 )
            // InternalDOF.g:4093:3: rule__DataLinkedCause__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__2__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__3"
    // InternalDOF.g:4101:1: rule__DataLinkedCause__Group__3 : rule__DataLinkedCause__Group__3__Impl rule__DataLinkedCause__Group__4 ;
    public final void rule__DataLinkedCause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4105:1: ( rule__DataLinkedCause__Group__3__Impl rule__DataLinkedCause__Group__4 )
            // InternalDOF.g:4106:2: rule__DataLinkedCause__Group__3__Impl rule__DataLinkedCause__Group__4
            {
            pushFollow(FOLLOW_46);
            rule__DataLinkedCause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__3"


    // $ANTLR start "rule__DataLinkedCause__Group__3__Impl"
    // InternalDOF.g:4113:1: rule__DataLinkedCause__Group__3__Impl : ( ( rule__DataLinkedCause__Group_3__0 )? ) ;
    public final void rule__DataLinkedCause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4117:1: ( ( ( rule__DataLinkedCause__Group_3__0 )? ) )
            // InternalDOF.g:4118:1: ( ( rule__DataLinkedCause__Group_3__0 )? )
            {
            // InternalDOF.g:4118:1: ( ( rule__DataLinkedCause__Group_3__0 )? )
            // InternalDOF.g:4119:2: ( rule__DataLinkedCause__Group_3__0 )?
            {
             before(grammarAccess.getDataLinkedCauseAccess().getGroup_3()); 
            // InternalDOF.g:4120:2: ( rule__DataLinkedCause__Group_3__0 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==44) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalDOF.g:4120:3: rule__DataLinkedCause__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataLinkedCause__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataLinkedCauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__3__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__4"
    // InternalDOF.g:4128:1: rule__DataLinkedCause__Group__4 : rule__DataLinkedCause__Group__4__Impl rule__DataLinkedCause__Group__5 ;
    public final void rule__DataLinkedCause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4132:1: ( rule__DataLinkedCause__Group__4__Impl rule__DataLinkedCause__Group__5 )
            // InternalDOF.g:4133:2: rule__DataLinkedCause__Group__4__Impl rule__DataLinkedCause__Group__5
            {
            pushFollow(FOLLOW_47);
            rule__DataLinkedCause__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__4"


    // $ANTLR start "rule__DataLinkedCause__Group__4__Impl"
    // InternalDOF.g:4140:1: rule__DataLinkedCause__Group__4__Impl : ( 'is' ) ;
    public final void rule__DataLinkedCause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4144:1: ( ( 'is' ) )
            // InternalDOF.g:4145:1: ( 'is' )
            {
            // InternalDOF.g:4145:1: ( 'is' )
            // InternalDOF.g:4146:2: 'is'
            {
             before(grammarAccess.getDataLinkedCauseAccess().getIsKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getDataLinkedCauseAccess().getIsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__4__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__5"
    // InternalDOF.g:4155:1: rule__DataLinkedCause__Group__5 : rule__DataLinkedCause__Group__5__Impl rule__DataLinkedCause__Group__6 ;
    public final void rule__DataLinkedCause__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4159:1: ( rule__DataLinkedCause__Group__5__Impl rule__DataLinkedCause__Group__6 )
            // InternalDOF.g:4160:2: rule__DataLinkedCause__Group__5__Impl rule__DataLinkedCause__Group__6
            {
            pushFollow(FOLLOW_47);
            rule__DataLinkedCause__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__5"


    // $ANTLR start "rule__DataLinkedCause__Group__5__Impl"
    // InternalDOF.g:4167:1: rule__DataLinkedCause__Group__5__Impl : ( ( rule__DataLinkedCause__AttributeFilterAssignment_5 )? ) ;
    public final void rule__DataLinkedCause__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4171:1: ( ( ( rule__DataLinkedCause__AttributeFilterAssignment_5 )? ) )
            // InternalDOF.g:4172:1: ( ( rule__DataLinkedCause__AttributeFilterAssignment_5 )? )
            {
            // InternalDOF.g:4172:1: ( ( rule__DataLinkedCause__AttributeFilterAssignment_5 )? )
            // InternalDOF.g:4173:2: ( rule__DataLinkedCause__AttributeFilterAssignment_5 )?
            {
             before(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAssignment_5()); 
            // InternalDOF.g:4174:2: ( rule__DataLinkedCause__AttributeFilterAssignment_5 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==30) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalDOF.g:4174:3: rule__DataLinkedCause__AttributeFilterAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataLinkedCause__AttributeFilterAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__5__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__6"
    // InternalDOF.g:4182:1: rule__DataLinkedCause__Group__6 : rule__DataLinkedCause__Group__6__Impl rule__DataLinkedCause__Group__7 ;
    public final void rule__DataLinkedCause__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4186:1: ( rule__DataLinkedCause__Group__6__Impl rule__DataLinkedCause__Group__7 )
            // InternalDOF.g:4187:2: rule__DataLinkedCause__Group__6__Impl rule__DataLinkedCause__Group__7
            {
            pushFollow(FOLLOW_47);
            rule__DataLinkedCause__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__6"


    // $ANTLR start "rule__DataLinkedCause__Group__6__Impl"
    // InternalDOF.g:4194:1: rule__DataLinkedCause__Group__6__Impl : ( ( rule__DataLinkedCause__InstancesFilterAssignment_6 )? ) ;
    public final void rule__DataLinkedCause__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4198:1: ( ( ( rule__DataLinkedCause__InstancesFilterAssignment_6 )? ) )
            // InternalDOF.g:4199:1: ( ( rule__DataLinkedCause__InstancesFilterAssignment_6 )? )
            {
            // InternalDOF.g:4199:1: ( ( rule__DataLinkedCause__InstancesFilterAssignment_6 )? )
            // InternalDOF.g:4200:2: ( rule__DataLinkedCause__InstancesFilterAssignment_6 )?
            {
             before(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterAssignment_6()); 
            // InternalDOF.g:4201:2: ( rule__DataLinkedCause__InstancesFilterAssignment_6 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==33) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalDOF.g:4201:3: rule__DataLinkedCause__InstancesFilterAssignment_6
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataLinkedCause__InstancesFilterAssignment_6();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__6__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__7"
    // InternalDOF.g:4209:1: rule__DataLinkedCause__Group__7 : rule__DataLinkedCause__Group__7__Impl rule__DataLinkedCause__Group__8 ;
    public final void rule__DataLinkedCause__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4213:1: ( rule__DataLinkedCause__Group__7__Impl rule__DataLinkedCause__Group__8 )
            // InternalDOF.g:4214:2: rule__DataLinkedCause__Group__7__Impl rule__DataLinkedCause__Group__8
            {
            pushFollow(FOLLOW_47);
            rule__DataLinkedCause__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__7"


    // $ANTLR start "rule__DataLinkedCause__Group__7__Impl"
    // InternalDOF.g:4221:1: rule__DataLinkedCause__Group__7__Impl : ( ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )* ) ;
    public final void rule__DataLinkedCause__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4225:1: ( ( ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )* ) )
            // InternalDOF.g:4226:1: ( ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )* )
            {
            // InternalDOF.g:4226:1: ( ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )* )
            // InternalDOF.g:4227:2: ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )*
            {
             before(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesAssignment_7()); 
            // InternalDOF.g:4228:2: ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==RULE_ID||LA35_0==25) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalDOF.g:4228:3: rule__DataLinkedCause__IncludedReferencesAssignment_7
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__DataLinkedCause__IncludedReferencesAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

             after(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__7__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__8"
    // InternalDOF.g:4236:1: rule__DataLinkedCause__Group__8 : rule__DataLinkedCause__Group__8__Impl ;
    public final void rule__DataLinkedCause__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4240:1: ( rule__DataLinkedCause__Group__8__Impl )
            // InternalDOF.g:4241:2: rule__DataLinkedCause__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__8"


    // $ANTLR start "rule__DataLinkedCause__Group__8__Impl"
    // InternalDOF.g:4247:1: rule__DataLinkedCause__Group__8__Impl : ( ( rule__DataLinkedCause__TypeFilterAssignment_8 )? ) ;
    public final void rule__DataLinkedCause__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4251:1: ( ( ( rule__DataLinkedCause__TypeFilterAssignment_8 )? ) )
            // InternalDOF.g:4252:1: ( ( rule__DataLinkedCause__TypeFilterAssignment_8 )? )
            {
            // InternalDOF.g:4252:1: ( ( rule__DataLinkedCause__TypeFilterAssignment_8 )? )
            // InternalDOF.g:4253:2: ( rule__DataLinkedCause__TypeFilterAssignment_8 )?
            {
             before(grammarAccess.getDataLinkedCauseAccess().getTypeFilterAssignment_8()); 
            // InternalDOF.g:4254:2: ( rule__DataLinkedCause__TypeFilterAssignment_8 )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==26||LA36_0==29) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalDOF.g:4254:3: rule__DataLinkedCause__TypeFilterAssignment_8
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataLinkedCause__TypeFilterAssignment_8();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataLinkedCauseAccess().getTypeFilterAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__8__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group_3__0"
    // InternalDOF.g:4263:1: rule__DataLinkedCause__Group_3__0 : rule__DataLinkedCause__Group_3__0__Impl rule__DataLinkedCause__Group_3__1 ;
    public final void rule__DataLinkedCause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4267:1: ( rule__DataLinkedCause__Group_3__0__Impl rule__DataLinkedCause__Group_3__1 )
            // InternalDOF.g:4268:2: rule__DataLinkedCause__Group_3__0__Impl rule__DataLinkedCause__Group_3__1
            {
            pushFollow(FOLLOW_37);
            rule__DataLinkedCause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_3__0"


    // $ANTLR start "rule__DataLinkedCause__Group_3__0__Impl"
    // InternalDOF.g:4275:1: rule__DataLinkedCause__Group_3__0__Impl : ( 'realizes' ) ;
    public final void rule__DataLinkedCause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4279:1: ( ( 'realizes' ) )
            // InternalDOF.g:4280:1: ( 'realizes' )
            {
            // InternalDOF.g:4280:1: ( 'realizes' )
            // InternalDOF.g:4281:2: 'realizes'
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesKeyword_3_0()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getDataLinkedCauseAccess().getRealizesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_3__0__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group_3__1"
    // InternalDOF.g:4290:1: rule__DataLinkedCause__Group_3__1 : rule__DataLinkedCause__Group_3__1__Impl ;
    public final void rule__DataLinkedCause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4294:1: ( rule__DataLinkedCause__Group_3__1__Impl )
            // InternalDOF.g:4295:2: rule__DataLinkedCause__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_3__1"


    // $ANTLR start "rule__DataLinkedCause__Group_3__1__Impl"
    // InternalDOF.g:4301:1: rule__DataLinkedCause__Group_3__1__Impl : ( ( rule__DataLinkedCause__RealizesAssignment_3_1 ) ) ;
    public final void rule__DataLinkedCause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4305:1: ( ( ( rule__DataLinkedCause__RealizesAssignment_3_1 ) ) )
            // InternalDOF.g:4306:1: ( ( rule__DataLinkedCause__RealizesAssignment_3_1 ) )
            {
            // InternalDOF.g:4306:1: ( ( rule__DataLinkedCause__RealizesAssignment_3_1 ) )
            // InternalDOF.g:4307:2: ( rule__DataLinkedCause__RealizesAssignment_3_1 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesAssignment_3_1()); 
            // InternalDOF.g:4308:2: ( rule__DataLinkedCause__RealizesAssignment_3_1 )
            // InternalDOF.g:4308:3: rule__DataLinkedCause__RealizesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__RealizesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getRealizesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_3__1__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__0"
    // InternalDOF.g:4317:1: rule__NotMappedCause__Group__0 : rule__NotMappedCause__Group__0__Impl rule__NotMappedCause__Group__1 ;
    public final void rule__NotMappedCause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4321:1: ( rule__NotMappedCause__Group__0__Impl rule__NotMappedCause__Group__1 )
            // InternalDOF.g:4322:2: rule__NotMappedCause__Group__0__Impl rule__NotMappedCause__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__NotMappedCause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__0"


    // $ANTLR start "rule__NotMappedCause__Group__0__Impl"
    // InternalDOF.g:4329:1: rule__NotMappedCause__Group__0__Impl : ( () ) ;
    public final void rule__NotMappedCause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4333:1: ( ( () ) )
            // InternalDOF.g:4334:1: ( () )
            {
            // InternalDOF.g:4334:1: ( () )
            // InternalDOF.g:4335:2: ()
            {
             before(grammarAccess.getNotMappedCauseAccess().getNotMappedCauseAction_0()); 
            // InternalDOF.g:4336:2: ()
            // InternalDOF.g:4336:3: 
            {
            }

             after(grammarAccess.getNotMappedCauseAccess().getNotMappedCauseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__0__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__1"
    // InternalDOF.g:4344:1: rule__NotMappedCause__Group__1 : rule__NotMappedCause__Group__1__Impl rule__NotMappedCause__Group__2 ;
    public final void rule__NotMappedCause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4348:1: ( rule__NotMappedCause__Group__1__Impl rule__NotMappedCause__Group__2 )
            // InternalDOF.g:4349:2: rule__NotMappedCause__Group__1__Impl rule__NotMappedCause__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__NotMappedCause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__1"


    // $ANTLR start "rule__NotMappedCause__Group__1__Impl"
    // InternalDOF.g:4356:1: rule__NotMappedCause__Group__1__Impl : ( 'cause' ) ;
    public final void rule__NotMappedCause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4360:1: ( ( 'cause' ) )
            // InternalDOF.g:4361:1: ( 'cause' )
            {
            // InternalDOF.g:4361:1: ( 'cause' )
            // InternalDOF.g:4362:2: 'cause'
            {
             before(grammarAccess.getNotMappedCauseAccess().getCauseKeyword_1()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getCauseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__1__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__2"
    // InternalDOF.g:4371:1: rule__NotMappedCause__Group__2 : rule__NotMappedCause__Group__2__Impl rule__NotMappedCause__Group__3 ;
    public final void rule__NotMappedCause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4375:1: ( rule__NotMappedCause__Group__2__Impl rule__NotMappedCause__Group__3 )
            // InternalDOF.g:4376:2: rule__NotMappedCause__Group__2__Impl rule__NotMappedCause__Group__3
            {
            pushFollow(FOLLOW_48);
            rule__NotMappedCause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__2"


    // $ANTLR start "rule__NotMappedCause__Group__2__Impl"
    // InternalDOF.g:4383:1: rule__NotMappedCause__Group__2__Impl : ( ( rule__NotMappedCause__NameAssignment_2 ) ) ;
    public final void rule__NotMappedCause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4387:1: ( ( ( rule__NotMappedCause__NameAssignment_2 ) ) )
            // InternalDOF.g:4388:1: ( ( rule__NotMappedCause__NameAssignment_2 ) )
            {
            // InternalDOF.g:4388:1: ( ( rule__NotMappedCause__NameAssignment_2 ) )
            // InternalDOF.g:4389:2: ( rule__NotMappedCause__NameAssignment_2 )
            {
             before(grammarAccess.getNotMappedCauseAccess().getNameAssignment_2()); 
            // InternalDOF.g:4390:2: ( rule__NotMappedCause__NameAssignment_2 )
            // InternalDOF.g:4390:3: rule__NotMappedCause__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getNotMappedCauseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__2__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__3"
    // InternalDOF.g:4398:1: rule__NotMappedCause__Group__3 : rule__NotMappedCause__Group__3__Impl rule__NotMappedCause__Group__4 ;
    public final void rule__NotMappedCause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4402:1: ( rule__NotMappedCause__Group__3__Impl rule__NotMappedCause__Group__4 )
            // InternalDOF.g:4403:2: rule__NotMappedCause__Group__3__Impl rule__NotMappedCause__Group__4
            {
            pushFollow(FOLLOW_48);
            rule__NotMappedCause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__3"


    // $ANTLR start "rule__NotMappedCause__Group__3__Impl"
    // InternalDOF.g:4410:1: rule__NotMappedCause__Group__3__Impl : ( ( rule__NotMappedCause__Group_3__0 )? ) ;
    public final void rule__NotMappedCause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4414:1: ( ( ( rule__NotMappedCause__Group_3__0 )? ) )
            // InternalDOF.g:4415:1: ( ( rule__NotMappedCause__Group_3__0 )? )
            {
            // InternalDOF.g:4415:1: ( ( rule__NotMappedCause__Group_3__0 )? )
            // InternalDOF.g:4416:2: ( rule__NotMappedCause__Group_3__0 )?
            {
             before(grammarAccess.getNotMappedCauseAccess().getGroup_3()); 
            // InternalDOF.g:4417:2: ( rule__NotMappedCause__Group_3__0 )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==44) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalDOF.g:4417:3: rule__NotMappedCause__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__NotMappedCause__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNotMappedCauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__3__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__4"
    // InternalDOF.g:4425:1: rule__NotMappedCause__Group__4 : rule__NotMappedCause__Group__4__Impl ;
    public final void rule__NotMappedCause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4429:1: ( rule__NotMappedCause__Group__4__Impl )
            // InternalDOF.g:4430:2: rule__NotMappedCause__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__4"


    // $ANTLR start "rule__NotMappedCause__Group__4__Impl"
    // InternalDOF.g:4436:1: rule__NotMappedCause__Group__4__Impl : ( 'notMapped' ) ;
    public final void rule__NotMappedCause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4440:1: ( ( 'notMapped' ) )
            // InternalDOF.g:4441:1: ( 'notMapped' )
            {
            // InternalDOF.g:4441:1: ( 'notMapped' )
            // InternalDOF.g:4442:2: 'notMapped'
            {
             before(grammarAccess.getNotMappedCauseAccess().getNotMappedKeyword_4()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getNotMappedKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__4__Impl"


    // $ANTLR start "rule__NotMappedCause__Group_3__0"
    // InternalDOF.g:4452:1: rule__NotMappedCause__Group_3__0 : rule__NotMappedCause__Group_3__0__Impl rule__NotMappedCause__Group_3__1 ;
    public final void rule__NotMappedCause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4456:1: ( rule__NotMappedCause__Group_3__0__Impl rule__NotMappedCause__Group_3__1 )
            // InternalDOF.g:4457:2: rule__NotMappedCause__Group_3__0__Impl rule__NotMappedCause__Group_3__1
            {
            pushFollow(FOLLOW_37);
            rule__NotMappedCause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__0"


    // $ANTLR start "rule__NotMappedCause__Group_3__0__Impl"
    // InternalDOF.g:4464:1: rule__NotMappedCause__Group_3__0__Impl : ( 'realizes' ) ;
    public final void rule__NotMappedCause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4468:1: ( ( 'realizes' ) )
            // InternalDOF.g:4469:1: ( 'realizes' )
            {
            // InternalDOF.g:4469:1: ( 'realizes' )
            // InternalDOF.g:4470:2: 'realizes'
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesKeyword_3_0()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getRealizesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__0__Impl"


    // $ANTLR start "rule__NotMappedCause__Group_3__1"
    // InternalDOF.g:4479:1: rule__NotMappedCause__Group_3__1 : rule__NotMappedCause__Group_3__1__Impl ;
    public final void rule__NotMappedCause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4483:1: ( rule__NotMappedCause__Group_3__1__Impl )
            // InternalDOF.g:4484:2: rule__NotMappedCause__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__1"


    // $ANTLR start "rule__NotMappedCause__Group_3__1__Impl"
    // InternalDOF.g:4490:1: rule__NotMappedCause__Group_3__1__Impl : ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) ) ;
    public final void rule__NotMappedCause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4494:1: ( ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) ) )
            // InternalDOF.g:4495:1: ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) )
            {
            // InternalDOF.g:4495:1: ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) )
            // InternalDOF.g:4496:2: ( rule__NotMappedCause__RealizesAssignment_3_1 )
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesAssignment_3_1()); 
            // InternalDOF.g:4497:2: ( rule__NotMappedCause__RealizesAssignment_3_1 )
            // InternalDOF.g:4497:3: rule__NotMappedCause__RealizesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__RealizesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getNotMappedCauseAccess().getRealizesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0"
    // InternalDOF.g:4506:1: rule__QualifiedNameWithWildcard__Group__0 : rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 ;
    public final void rule__QualifiedNameWithWildcard__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4510:1: ( rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 )
            // InternalDOF.g:4511:2: rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1
            {
            pushFollow(FOLLOW_49);
            rule__QualifiedNameWithWildcard__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0__Impl"
    // InternalDOF.g:4518:1: rule__QualifiedNameWithWildcard__Group__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4522:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:4523:1: ( ruleQualifiedName )
            {
            // InternalDOF.g:4523:1: ( ruleQualifiedName )
            // InternalDOF.g:4524:2: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1"
    // InternalDOF.g:4533:1: rule__QualifiedNameWithWildcard__Group__1 : rule__QualifiedNameWithWildcard__Group__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4537:1: ( rule__QualifiedNameWithWildcard__Group__1__Impl )
            // InternalDOF.g:4538:2: rule__QualifiedNameWithWildcard__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1__Impl"
    // InternalDOF.g:4544:1: rule__QualifiedNameWithWildcard__Group__1__Impl : ( ( '.*' )? ) ;
    public final void rule__QualifiedNameWithWildcard__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4548:1: ( ( ( '.*' )? ) )
            // InternalDOF.g:4549:1: ( ( '.*' )? )
            {
            // InternalDOF.g:4549:1: ( ( '.*' )? )
            // InternalDOF.g:4550:2: ( '.*' )?
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
            // InternalDOF.g:4551:2: ( '.*' )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==46) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalDOF.g:4551:3: '.*'
                    {
                    match(input,46,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalDOF.g:4560:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4564:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalDOF.g:4565:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_50);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalDOF.g:4572:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4576:1: ( ( RULE_ID ) )
            // InternalDOF.g:4577:1: ( RULE_ID )
            {
            // InternalDOF.g:4577:1: ( RULE_ID )
            // InternalDOF.g:4578:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalDOF.g:4587:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4591:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalDOF.g:4592:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalDOF.g:4598:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4602:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalDOF.g:4603:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalDOF.g:4603:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalDOF.g:4604:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalDOF.g:4605:2: ( rule__QualifiedName__Group_1__0 )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==47) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // InternalDOF.g:4605:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_51);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalDOF.g:4614:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4618:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalDOF.g:4619:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalDOF.g:4626:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4630:1: ( ( '.' ) )
            // InternalDOF.g:4631:1: ( '.' )
            {
            // InternalDOF.g:4631:1: ( '.' )
            // InternalDOF.g:4632:2: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalDOF.g:4641:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4645:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalDOF.g:4646:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalDOF.g:4652:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4656:1: ( ( RULE_ID ) )
            // InternalDOF.g:4657:1: ( RULE_ID )
            {
            // InternalDOF.g:4657:1: ( RULE_ID )
            // InternalDOF.g:4658:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Path__Group__0"
    // InternalDOF.g:4668:1: rule__Path__Group__0 : rule__Path__Group__0__Impl rule__Path__Group__1 ;
    public final void rule__Path__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4672:1: ( rule__Path__Group__0__Impl rule__Path__Group__1 )
            // InternalDOF.g:4673:2: rule__Path__Group__0__Impl rule__Path__Group__1
            {
            pushFollow(FOLLOW_50);
            rule__Path__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Path__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__0"


    // $ANTLR start "rule__Path__Group__0__Impl"
    // InternalDOF.g:4680:1: rule__Path__Group__0__Impl : ( ( rule__Path__JumpsAssignment_0 ) ) ;
    public final void rule__Path__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4684:1: ( ( ( rule__Path__JumpsAssignment_0 ) ) )
            // InternalDOF.g:4685:1: ( ( rule__Path__JumpsAssignment_0 ) )
            {
            // InternalDOF.g:4685:1: ( ( rule__Path__JumpsAssignment_0 ) )
            // InternalDOF.g:4686:2: ( rule__Path__JumpsAssignment_0 )
            {
             before(grammarAccess.getPathAccess().getJumpsAssignment_0()); 
            // InternalDOF.g:4687:2: ( rule__Path__JumpsAssignment_0 )
            // InternalDOF.g:4687:3: rule__Path__JumpsAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Path__JumpsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPathAccess().getJumpsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__0__Impl"


    // $ANTLR start "rule__Path__Group__1"
    // InternalDOF.g:4695:1: rule__Path__Group__1 : rule__Path__Group__1__Impl ;
    public final void rule__Path__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4699:1: ( rule__Path__Group__1__Impl )
            // InternalDOF.g:4700:2: rule__Path__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Path__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__1"


    // $ANTLR start "rule__Path__Group__1__Impl"
    // InternalDOF.g:4706:1: rule__Path__Group__1__Impl : ( ( rule__Path__Group_1__0 )* ) ;
    public final void rule__Path__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4710:1: ( ( ( rule__Path__Group_1__0 )* ) )
            // InternalDOF.g:4711:1: ( ( rule__Path__Group_1__0 )* )
            {
            // InternalDOF.g:4711:1: ( ( rule__Path__Group_1__0 )* )
            // InternalDOF.g:4712:2: ( rule__Path__Group_1__0 )*
            {
             before(grammarAccess.getPathAccess().getGroup_1()); 
            // InternalDOF.g:4713:2: ( rule__Path__Group_1__0 )*
            loop40:
            do {
                int alt40=2;
                int LA40_0 = input.LA(1);

                if ( (LA40_0==47) ) {
                    alt40=1;
                }


                switch (alt40) {
            	case 1 :
            	    // InternalDOF.g:4713:3: rule__Path__Group_1__0
            	    {
            	    pushFollow(FOLLOW_51);
            	    rule__Path__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop40;
                }
            } while (true);

             after(grammarAccess.getPathAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__1__Impl"


    // $ANTLR start "rule__Path__Group_1__0"
    // InternalDOF.g:4722:1: rule__Path__Group_1__0 : rule__Path__Group_1__0__Impl rule__Path__Group_1__1 ;
    public final void rule__Path__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4726:1: ( rule__Path__Group_1__0__Impl rule__Path__Group_1__1 )
            // InternalDOF.g:4727:2: rule__Path__Group_1__0__Impl rule__Path__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Path__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Path__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__0"


    // $ANTLR start "rule__Path__Group_1__0__Impl"
    // InternalDOF.g:4734:1: rule__Path__Group_1__0__Impl : ( '.' ) ;
    public final void rule__Path__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4738:1: ( ( '.' ) )
            // InternalDOF.g:4739:1: ( '.' )
            {
            // InternalDOF.g:4739:1: ( '.' )
            // InternalDOF.g:4740:2: '.'
            {
             before(grammarAccess.getPathAccess().getFullStopKeyword_1_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getPathAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__0__Impl"


    // $ANTLR start "rule__Path__Group_1__1"
    // InternalDOF.g:4749:1: rule__Path__Group_1__1 : rule__Path__Group_1__1__Impl ;
    public final void rule__Path__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4753:1: ( rule__Path__Group_1__1__Impl )
            // InternalDOF.g:4754:2: rule__Path__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Path__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__1"


    // $ANTLR start "rule__Path__Group_1__1__Impl"
    // InternalDOF.g:4760:1: rule__Path__Group_1__1__Impl : ( ( rule__Path__JumpsAssignment_1_1 ) ) ;
    public final void rule__Path__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4764:1: ( ( ( rule__Path__JumpsAssignment_1_1 ) ) )
            // InternalDOF.g:4765:1: ( ( rule__Path__JumpsAssignment_1_1 ) )
            {
            // InternalDOF.g:4765:1: ( ( rule__Path__JumpsAssignment_1_1 ) )
            // InternalDOF.g:4766:2: ( rule__Path__JumpsAssignment_1_1 )
            {
             before(grammarAccess.getPathAccess().getJumpsAssignment_1_1()); 
            // InternalDOF.g:4767:2: ( rule__Path__JumpsAssignment_1_1 )
            // InternalDOF.g:4767:3: rule__Path__JumpsAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Path__JumpsAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getPathAccess().getJumpsAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__1__Impl"


    // $ANTLR start "rule__DOF__ImportsAssignment_0"
    // InternalDOF.g:4776:1: rule__DOF__ImportsAssignment_0 : ( ruleImport ) ;
    public final void rule__DOF__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4780:1: ( ( ruleImport ) )
            // InternalDOF.g:4781:2: ( ruleImport )
            {
            // InternalDOF.g:4781:2: ( ruleImport )
            // InternalDOF.g:4782:3: ruleImport
            {
             before(grammarAccess.getDOFAccess().getImportsImportParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getDOFAccess().getImportsImportParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__ImportsAssignment_0"


    // $ANTLR start "rule__DOF__NameAssignment_2"
    // InternalDOF.g:4791:1: rule__DOF__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__DOF__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4795:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:4796:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:4796:2: ( ruleQualifiedName )
            // InternalDOF.g:4797:3: ruleQualifiedName
            {
             before(grammarAccess.getDOFAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getDOFAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__NameAssignment_2"


    // $ANTLR start "rule__DOF__EffectAssignment_3"
    // InternalDOF.g:4806:1: rule__DOF__EffectAssignment_3 : ( ruleEffect ) ;
    public final void rule__DOF__EffectAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4810:1: ( ( ruleEffect ) )
            // InternalDOF.g:4811:2: ( ruleEffect )
            {
            // InternalDOF.g:4811:2: ( ruleEffect )
            // InternalDOF.g:4812:3: ruleEffect
            {
             before(grammarAccess.getDOFAccess().getEffectEffectParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getDOFAccess().getEffectEffectParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__EffectAssignment_3"


    // $ANTLR start "rule__Import__ImportedNamespaceAssignment_1"
    // InternalDOF.g:4821:1: rule__Import__ImportedNamespaceAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__Import__ImportedNamespaceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4825:1: ( ( ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:4826:2: ( ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:4826:2: ( ruleQualifiedNameWithWildcard )
            // InternalDOF.g:4827:3: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportedNamespaceAssignment_1"


    // $ANTLR start "rule__Effect__NameAssignment_2"
    // InternalDOF.g:4836:1: rule__Effect__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Effect__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4840:1: ( ( RULE_ID ) )
            // InternalDOF.g:4841:2: ( RULE_ID )
            {
            // InternalDOF.g:4841:2: ( RULE_ID )
            // InternalDOF.g:4842:3: RULE_ID
            {
             before(grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__NameAssignment_2"


    // $ANTLR start "rule__Effect__DataFeederAssignment_4"
    // InternalDOF.g:4851:1: rule__Effect__DataFeederAssignment_4 : ( ruleDataFeeder ) ;
    public final void rule__Effect__DataFeederAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4855:1: ( ( ruleDataFeeder ) )
            // InternalDOF.g:4856:2: ( ruleDataFeeder )
            {
            // InternalDOF.g:4856:2: ( ruleDataFeeder )
            // InternalDOF.g:4857:3: ruleDataFeeder
            {
             before(grammarAccess.getEffectAccess().getDataFeederDataFeederParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleDataFeeder();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getDataFeederDataFeederParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__DataFeederAssignment_4"


    // $ANTLR start "rule__Effect__CategoriesAssignment_5"
    // InternalDOF.g:4866:1: rule__Effect__CategoriesAssignment_5 : ( ruleCategory ) ;
    public final void rule__Effect__CategoriesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4870:1: ( ( ruleCategory ) )
            // InternalDOF.g:4871:2: ( ruleCategory )
            {
            // InternalDOF.g:4871:2: ( ruleCategory )
            // InternalDOF.g:4872:3: ruleCategory
            {
             before(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__CategoriesAssignment_5"


    // $ANTLR start "rule__Effect__CategoriesAssignment_6"
    // InternalDOF.g:4881:1: rule__Effect__CategoriesAssignment_6 : ( ruleCategory ) ;
    public final void rule__Effect__CategoriesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4885:1: ( ( ruleCategory ) )
            // InternalDOF.g:4886:2: ( ruleCategory )
            {
            // InternalDOF.g:4886:2: ( ruleCategory )
            // InternalDOF.g:4887:3: ruleCategory
            {
             before(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__CategoriesAssignment_6"


    // $ANTLR start "rule__Category__NameAssignment_2"
    // InternalDOF.g:4896:1: rule__Category__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Category__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4900:1: ( ( RULE_ID ) )
            // InternalDOF.g:4901:2: ( RULE_ID )
            {
            // InternalDOF.g:4901:2: ( RULE_ID )
            // InternalDOF.g:4902:3: RULE_ID
            {
             before(grammarAccess.getCategoryAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__NameAssignment_2"


    // $ANTLR start "rule__Category__CausesAssignment_3"
    // InternalDOF.g:4911:1: rule__Category__CausesAssignment_3 : ( ruleCause ) ;
    public final void rule__Category__CausesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4915:1: ( ( ruleCause ) )
            // InternalDOF.g:4916:2: ( ruleCause )
            {
            // InternalDOF.g:4916:2: ( ruleCause )
            // InternalDOF.g:4917:3: ruleCause
            {
             before(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__CausesAssignment_3"


    // $ANTLR start "rule__Category__CausesAssignment_4"
    // InternalDOF.g:4926:1: rule__Category__CausesAssignment_4 : ( ruleCause ) ;
    public final void rule__Category__CausesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4930:1: ( ( ruleCause ) )
            // InternalDOF.g:4931:2: ( ruleCause )
            {
            // InternalDOF.g:4931:2: ( ruleCause )
            // InternalDOF.g:4932:3: ruleCause
            {
             before(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__CausesAssignment_4"


    // $ANTLR start "rule__DataFeeder__NameAssignment_1"
    // InternalDOF.g:4941:1: rule__DataFeeder__NameAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__DataFeeder__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4945:1: ( ( ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:4946:2: ( ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:4946:2: ( ruleQualifiedNameWithWildcard )
            // InternalDOF.g:4947:3: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getDataFeederAccess().getNameQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getNameQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__NameAssignment_1"


    // $ANTLR start "rule__DataFeeder__AttributeFilterAssignment_2"
    // InternalDOF.g:4956:1: rule__DataFeeder__AttributeFilterAssignment_2 : ( ruleAttributeFilter ) ;
    public final void rule__DataFeeder__AttributeFilterAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4960:1: ( ( ruleAttributeFilter ) )
            // InternalDOF.g:4961:2: ( ruleAttributeFilter )
            {
            // InternalDOF.g:4961:2: ( ruleAttributeFilter )
            // InternalDOF.g:4962:3: ruleAttributeFilter
            {
             before(grammarAccess.getDataFeederAccess().getAttributeFilterAttributeFilterParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getAttributeFilterAttributeFilterParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__AttributeFilterAssignment_2"


    // $ANTLR start "rule__DataFeeder__InstancesFilterAssignment_3"
    // InternalDOF.g:4971:1: rule__DataFeeder__InstancesFilterAssignment_3 : ( ruleInstancesFilter ) ;
    public final void rule__DataFeeder__InstancesFilterAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4975:1: ( ( ruleInstancesFilter ) )
            // InternalDOF.g:4976:2: ( ruleInstancesFilter )
            {
            // InternalDOF.g:4976:2: ( ruleInstancesFilter )
            // InternalDOF.g:4977:3: ruleInstancesFilter
            {
             before(grammarAccess.getDataFeederAccess().getInstancesFilterInstancesFilterParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getInstancesFilterInstancesFilterParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__InstancesFilterAssignment_3"


    // $ANTLR start "rule__DataFeeder__IncludedReferencesAssignment_4_1"
    // InternalDOF.g:4986:1: rule__DataFeeder__IncludedReferencesAssignment_4_1 : ( ruleIncludedReference ) ;
    public final void rule__DataFeeder__IncludedReferencesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4990:1: ( ( ruleIncludedReference ) )
            // InternalDOF.g:4991:2: ( ruleIncludedReference )
            {
            // InternalDOF.g:4991:2: ( ruleIncludedReference )
            // InternalDOF.g:4992:3: ruleIncludedReference
            {
             before(grammarAccess.getDataFeederAccess().getIncludedReferencesIncludedReferenceParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getIncludedReferencesIncludedReferenceParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__IncludedReferencesAssignment_4_1"


    // $ANTLR start "rule__DataFeeder__TypeFilterAssignment_5"
    // InternalDOF.g:5001:1: rule__DataFeeder__TypeFilterAssignment_5 : ( ruleTypeFilter ) ;
    public final void rule__DataFeeder__TypeFilterAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5005:1: ( ( ruleTypeFilter ) )
            // InternalDOF.g:5006:2: ( ruleTypeFilter )
            {
            // InternalDOF.g:5006:2: ( ruleTypeFilter )
            // InternalDOF.g:5007:3: ruleTypeFilter
            {
             before(grammarAccess.getDataFeederAccess().getTypeFilterTypeFilterParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getTypeFilterTypeFilterParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__TypeFilterAssignment_5"


    // $ANTLR start "rule__SimpleReference__NameAssignment_0"
    // InternalDOF.g:5016:1: rule__SimpleReference__NameAssignment_0 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__SimpleReference__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5020:1: ( ( ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:5021:2: ( ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:5021:2: ( ruleQualifiedNameWithWildcard )
            // InternalDOF.g:5022:3: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getSimpleReferenceAccess().getNameQualifiedNameWithWildcardParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getNameQualifiedNameWithWildcardParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__NameAssignment_0"


    // $ANTLR start "rule__SimpleReference__AttributeFilterAssignment_1"
    // InternalDOF.g:5031:1: rule__SimpleReference__AttributeFilterAssignment_1 : ( ruleAttributeFilter ) ;
    public final void rule__SimpleReference__AttributeFilterAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5035:1: ( ( ruleAttributeFilter ) )
            // InternalDOF.g:5036:2: ( ruleAttributeFilter )
            {
            // InternalDOF.g:5036:2: ( ruleAttributeFilter )
            // InternalDOF.g:5037:3: ruleAttributeFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__AttributeFilterAssignment_1"


    // $ANTLR start "rule__SimpleReference__PivotingIdAssignment_2_1"
    // InternalDOF.g:5046:1: rule__SimpleReference__PivotingIdAssignment_2_1 : ( rulePath ) ;
    public final void rule__SimpleReference__PivotingIdAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5050:1: ( ( rulePath ) )
            // InternalDOF.g:5051:2: ( rulePath )
            {
            // InternalDOF.g:5051:2: ( rulePath )
            // InternalDOF.g:5052:3: rulePath
            {
             before(grammarAccess.getSimpleReferenceAccess().getPivotingIdPathParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getPivotingIdPathParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__PivotingIdAssignment_2_1"


    // $ANTLR start "rule__SimpleReference__InstancesFilterAssignment_2_2"
    // InternalDOF.g:5061:1: rule__SimpleReference__InstancesFilterAssignment_2_2 : ( ruleInstancesFilter ) ;
    public final void rule__SimpleReference__InstancesFilterAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5065:1: ( ( ruleInstancesFilter ) )
            // InternalDOF.g:5066:2: ( ruleInstancesFilter )
            {
            // InternalDOF.g:5066:2: ( ruleInstancesFilter )
            // InternalDOF.g:5067:3: ruleInstancesFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__InstancesFilterAssignment_2_2"


    // $ANTLR start "rule__SimpleReference__IncludedReferencesAssignment_3_2"
    // InternalDOF.g:5076:1: rule__SimpleReference__IncludedReferencesAssignment_3_2 : ( ruleIncludedReference ) ;
    public final void rule__SimpleReference__IncludedReferencesAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5080:1: ( ( ruleIncludedReference ) )
            // InternalDOF.g:5081:2: ( ruleIncludedReference )
            {
            // InternalDOF.g:5081:2: ( ruleIncludedReference )
            // InternalDOF.g:5082:3: ruleIncludedReference
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesIncludedReferenceParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesIncludedReferenceParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__IncludedReferencesAssignment_3_2"


    // $ANTLR start "rule__SimpleReference__TypeFilterAssignment_3_3"
    // InternalDOF.g:5091:1: rule__SimpleReference__TypeFilterAssignment_3_3 : ( ruleTypeFilter ) ;
    public final void rule__SimpleReference__TypeFilterAssignment_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5095:1: ( ( ruleTypeFilter ) )
            // InternalDOF.g:5096:2: ( ruleTypeFilter )
            {
            // InternalDOF.g:5096:2: ( ruleTypeFilter )
            // InternalDOF.g:5097:3: ruleTypeFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_3_3_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__TypeFilterAssignment_3_3"


    // $ANTLR start "rule__AggregatedReference__NameAssignment_1"
    // InternalDOF.g:5106:1: rule__AggregatedReference__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__AggregatedReference__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5110:1: ( ( RULE_ID ) )
            // InternalDOF.g:5111:2: ( RULE_ID )
            {
            // InternalDOF.g:5111:2: ( RULE_ID )
            // InternalDOF.g:5112:3: RULE_ID
            {
             before(grammarAccess.getAggregatedReferenceAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__NameAssignment_1"


    // $ANTLR start "rule__AggregatedReference__FunctionAssignment_3"
    // InternalDOF.g:5121:1: rule__AggregatedReference__FunctionAssignment_3 : ( ruleAggFunction ) ;
    public final void rule__AggregatedReference__FunctionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5125:1: ( ( ruleAggFunction ) )
            // InternalDOF.g:5126:2: ( ruleAggFunction )
            {
            // InternalDOF.g:5126:2: ( ruleAggFunction )
            // InternalDOF.g:5127:3: ruleAggFunction
            {
             before(grammarAccess.getAggregatedReferenceAccess().getFunctionAggFunctionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleAggFunction();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getFunctionAggFunctionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__FunctionAssignment_3"


    // $ANTLR start "rule__AggregatedReference__AggValueAssignment_5"
    // InternalDOF.g:5136:1: rule__AggregatedReference__AggValueAssignment_5 : ( rulePath ) ;
    public final void rule__AggregatedReference__AggValueAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5140:1: ( ( rulePath ) )
            // InternalDOF.g:5141:2: ( rulePath )
            {
            // InternalDOF.g:5141:2: ( rulePath )
            // InternalDOF.g:5142:3: rulePath
            {
             before(grammarAccess.getAggregatedReferenceAccess().getAggValuePathParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getAggValuePathParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__AggValueAssignment_5"


    // $ANTLR start "rule__AggregatedReference__PivotingIdAssignment_7_1"
    // InternalDOF.g:5151:1: rule__AggregatedReference__PivotingIdAssignment_7_1 : ( rulePath ) ;
    public final void rule__AggregatedReference__PivotingIdAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5155:1: ( ( rulePath ) )
            // InternalDOF.g:5156:2: ( rulePath )
            {
            // InternalDOF.g:5156:2: ( rulePath )
            // InternalDOF.g:5157:3: rulePath
            {
             before(grammarAccess.getAggregatedReferenceAccess().getPivotingIdPathParserRuleCall_7_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getPivotingIdPathParserRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__PivotingIdAssignment_7_1"


    // $ANTLR start "rule__AggregatedReference__InstancesFilterAssignment_8"
    // InternalDOF.g:5166:1: rule__AggregatedReference__InstancesFilterAssignment_8 : ( ruleInstancesFilter ) ;
    public final void rule__AggregatedReference__InstancesFilterAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5170:1: ( ( ruleInstancesFilter ) )
            // InternalDOF.g:5171:2: ( ruleInstancesFilter )
            {
            // InternalDOF.g:5171:2: ( ruleInstancesFilter )
            // InternalDOF.g:5172:3: ruleInstancesFilter
            {
             before(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__InstancesFilterAssignment_8"


    // $ANTLR start "rule__TypeCompletion__TypeCustomizationsAssignment_1"
    // InternalDOF.g:5181:1: rule__TypeCompletion__TypeCustomizationsAssignment_1 : ( ruleTypeCustomization ) ;
    public final void rule__TypeCompletion__TypeCustomizationsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5185:1: ( ( ruleTypeCustomization ) )
            // InternalDOF.g:5186:2: ( ruleTypeCustomization )
            {
            // InternalDOF.g:5186:2: ( ruleTypeCustomization )
            // InternalDOF.g:5187:3: ruleTypeCustomization
            {
             before(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeCustomization();

            state._fsp--;

             after(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__TypeCustomizationsAssignment_1"


    // $ANTLR start "rule__TypeSelection__TypeCustomizationsAssignment_1"
    // InternalDOF.g:5196:1: rule__TypeSelection__TypeCustomizationsAssignment_1 : ( ruleTypeCustomization ) ;
    public final void rule__TypeSelection__TypeCustomizationsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5200:1: ( ( ruleTypeCustomization ) )
            // InternalDOF.g:5201:2: ( ruleTypeCustomization )
            {
            // InternalDOF.g:5201:2: ( ruleTypeCustomization )
            // InternalDOF.g:5202:3: ruleTypeCustomization
            {
             before(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeCustomization();

            state._fsp--;

             after(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__TypeCustomizationsAssignment_1"


    // $ANTLR start "rule__TypeCustomization__NameAssignment_0"
    // InternalDOF.g:5211:1: rule__TypeCustomization__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__TypeCustomization__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5215:1: ( ( RULE_ID ) )
            // InternalDOF.g:5216:2: ( RULE_ID )
            {
            // InternalDOF.g:5216:2: ( RULE_ID )
            // InternalDOF.g:5217:3: RULE_ID
            {
             before(grammarAccess.getTypeCustomizationAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTypeCustomizationAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__NameAssignment_0"


    // $ANTLR start "rule__TypeCustomization__AttributeFilterAssignment_1"
    // InternalDOF.g:5226:1: rule__TypeCustomization__AttributeFilterAssignment_1 : ( ruleAttributeFilter ) ;
    public final void rule__TypeCustomization__AttributeFilterAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5230:1: ( ( ruleAttributeFilter ) )
            // InternalDOF.g:5231:2: ( ruleAttributeFilter )
            {
            // InternalDOF.g:5231:2: ( ruleAttributeFilter )
            // InternalDOF.g:5232:3: ruleAttributeFilter
            {
             before(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__AttributeFilterAssignment_1"


    // $ANTLR start "rule__TypeCustomization__IncludedReferencesAssignment_2_1"
    // InternalDOF.g:5241:1: rule__TypeCustomization__IncludedReferencesAssignment_2_1 : ( ruleIncludedReference ) ;
    public final void rule__TypeCustomization__IncludedReferencesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5245:1: ( ( ruleIncludedReference ) )
            // InternalDOF.g:5246:2: ( ruleIncludedReference )
            {
            // InternalDOF.g:5246:2: ( ruleIncludedReference )
            // InternalDOF.g:5247:3: ruleIncludedReference
            {
             before(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesIncludedReferenceParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesIncludedReferenceParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__IncludedReferencesAssignment_2_1"


    // $ANTLR start "rule__AttributeFilter__AttributesAssignment_1"
    // InternalDOF.g:5256:1: rule__AttributeFilter__AttributesAssignment_1 : ( RULE_ID ) ;
    public final void rule__AttributeFilter__AttributesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5260:1: ( ( RULE_ID ) )
            // InternalDOF.g:5261:2: ( RULE_ID )
            {
            // InternalDOF.g:5261:2: ( RULE_ID )
            // InternalDOF.g:5262:3: RULE_ID
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__AttributesAssignment_1"


    // $ANTLR start "rule__AttributeFilter__AttributesAssignment_2_1"
    // InternalDOF.g:5271:1: rule__AttributeFilter__AttributesAssignment_2_1 : ( RULE_ID ) ;
    public final void rule__AttributeFilter__AttributesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5275:1: ( ( RULE_ID ) )
            // InternalDOF.g:5276:2: ( RULE_ID )
            {
            // InternalDOF.g:5276:2: ( RULE_ID )
            // InternalDOF.g:5277:3: RULE_ID
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__AttributesAssignment_2_1"


    // $ANTLR start "rule__AndConjunction__RightAssignment_1_2"
    // InternalDOF.g:5286:1: rule__AndConjunction__RightAssignment_1_2 : ( ruleOrConjunction ) ;
    public final void rule__AndConjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5290:1: ( ( ruleOrConjunction ) )
            // InternalDOF.g:5291:2: ( ruleOrConjunction )
            {
            // InternalDOF.g:5291:2: ( ruleOrConjunction )
            // InternalDOF.g:5292:3: ruleOrConjunction
            {
             before(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOrConjunction();

            state._fsp--;

             after(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__RightAssignment_1_2"


    // $ANTLR start "rule__OrConjunction__RightAssignment_1_2"
    // InternalDOF.g:5301:1: rule__OrConjunction__RightAssignment_1_2 : ( rulePrimary ) ;
    public final void rule__OrConjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5305:1: ( ( rulePrimary ) )
            // InternalDOF.g:5306:2: ( rulePrimary )
            {
            // InternalDOF.g:5306:2: ( rulePrimary )
            // InternalDOF.g:5307:3: rulePrimary
            {
             before(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__RightAssignment_1_2"


    // $ANTLR start "rule__Comparison__PathAssignment_0_1"
    // InternalDOF.g:5316:1: rule__Comparison__PathAssignment_0_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5320:1: ( ( rulePath ) )
            // InternalDOF.g:5321:2: ( rulePath )
            {
            // InternalDOF.g:5321:2: ( rulePath )
            // InternalDOF.g:5322:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_0_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_0_3"
    // InternalDOF.g:5331:1: rule__Comparison__ValueAssignment_0_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5335:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5336:2: ( RULE_STRING )
            {
            // InternalDOF.g:5336:2: ( RULE_STRING )
            // InternalDOF.g:5337:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_0_3"


    // $ANTLR start "rule__Comparison__PathAssignment_1_1"
    // InternalDOF.g:5346:1: rule__Comparison__PathAssignment_1_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5350:1: ( ( rulePath ) )
            // InternalDOF.g:5351:2: ( rulePath )
            {
            // InternalDOF.g:5351:2: ( rulePath )
            // InternalDOF.g:5352:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_1_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_1_3"
    // InternalDOF.g:5361:1: rule__Comparison__ValueAssignment_1_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5365:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5366:2: ( RULE_STRING )
            {
            // InternalDOF.g:5366:2: ( RULE_STRING )
            // InternalDOF.g:5367:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_1_3"


    // $ANTLR start "rule__Comparison__PathAssignment_2_1"
    // InternalDOF.g:5376:1: rule__Comparison__PathAssignment_2_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5380:1: ( ( rulePath ) )
            // InternalDOF.g:5381:2: ( rulePath )
            {
            // InternalDOF.g:5381:2: ( rulePath )
            // InternalDOF.g:5382:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_2_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_2_3"
    // InternalDOF.g:5391:1: rule__Comparison__ValueAssignment_2_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5395:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5396:2: ( RULE_STRING )
            {
            // InternalDOF.g:5396:2: ( RULE_STRING )
            // InternalDOF.g:5397:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_2_3"


    // $ANTLR start "rule__Comparison__PathAssignment_3_1"
    // InternalDOF.g:5406:1: rule__Comparison__PathAssignment_3_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5410:1: ( ( rulePath ) )
            // InternalDOF.g:5411:2: ( rulePath )
            {
            // InternalDOF.g:5411:2: ( rulePath )
            // InternalDOF.g:5412:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_3_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_3_3"
    // InternalDOF.g:5421:1: rule__Comparison__ValueAssignment_3_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5425:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5426:2: ( RULE_STRING )
            {
            // InternalDOF.g:5426:2: ( RULE_STRING )
            // InternalDOF.g:5427:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_3_3"


    // $ANTLR start "rule__Comparison__PathAssignment_4_1"
    // InternalDOF.g:5436:1: rule__Comparison__PathAssignment_4_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5440:1: ( ( rulePath ) )
            // InternalDOF.g:5441:2: ( rulePath )
            {
            // InternalDOF.g:5441:2: ( rulePath )
            // InternalDOF.g:5442:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_4_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_4_3"
    // InternalDOF.g:5451:1: rule__Comparison__ValueAssignment_4_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_4_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5455:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5456:2: ( RULE_STRING )
            {
            // InternalDOF.g:5456:2: ( RULE_STRING )
            // InternalDOF.g:5457:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_4_3"


    // $ANTLR start "rule__Comparison__PathAssignment_5_1"
    // InternalDOF.g:5466:1: rule__Comparison__PathAssignment_5_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5470:1: ( ( rulePath ) )
            // InternalDOF.g:5471:2: ( rulePath )
            {
            // InternalDOF.g:5471:2: ( rulePath )
            // InternalDOF.g:5472:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_5_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_5_3"
    // InternalDOF.g:5481:1: rule__Comparison__ValueAssignment_5_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_5_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5485:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5486:2: ( RULE_STRING )
            {
            // InternalDOF.g:5486:2: ( RULE_STRING )
            // InternalDOF.g:5487:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_5_3"


    // $ANTLR start "rule__CompoundCause__NameAssignment_2"
    // InternalDOF.g:5496:1: rule__CompoundCause__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__CompoundCause__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5500:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:5501:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:5501:2: ( ruleQualifiedName )
            // InternalDOF.g:5502:3: ruleQualifiedName
            {
             before(grammarAccess.getCompoundCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__NameAssignment_2"


    // $ANTLR start "rule__CompoundCause__RealizesAssignment_3_1"
    // InternalDOF.g:5511:1: rule__CompoundCause__RealizesAssignment_3_1 : ( ( ruleEString ) ) ;
    public final void rule__CompoundCause__RealizesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5515:1: ( ( ( ruleEString ) ) )
            // InternalDOF.g:5516:2: ( ( ruleEString ) )
            {
            // InternalDOF.g:5516:2: ( ( ruleEString ) )
            // InternalDOF.g:5517:3: ( ruleEString )
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesCauseCrossReference_3_1_0()); 
            // InternalDOF.g:5518:3: ( ruleEString )
            // InternalDOF.g:5519:4: ruleEString
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getCompoundCauseAccess().getRealizesCauseCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__RealizesAssignment_3_1"


    // $ANTLR start "rule__CompoundCause__SubCausesAssignment_6"
    // InternalDOF.g:5530:1: rule__CompoundCause__SubCausesAssignment_6 : ( ruleCause ) ;
    public final void rule__CompoundCause__SubCausesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5534:1: ( ( ruleCause ) )
            // InternalDOF.g:5535:2: ( ruleCause )
            {
            // InternalDOF.g:5535:2: ( ruleCause )
            // InternalDOF.g:5536:3: ruleCause
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__SubCausesAssignment_6"


    // $ANTLR start "rule__CompoundCause__SubCausesAssignment_7"
    // InternalDOF.g:5545:1: rule__CompoundCause__SubCausesAssignment_7 : ( ruleCause ) ;
    public final void rule__CompoundCause__SubCausesAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5549:1: ( ( ruleCause ) )
            // InternalDOF.g:5550:2: ( ruleCause )
            {
            // InternalDOF.g:5550:2: ( ruleCause )
            // InternalDOF.g:5551:3: ruleCause
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__SubCausesAssignment_7"


    // $ANTLR start "rule__DataLinkedCause__NameAssignment_2"
    // InternalDOF.g:5560:1: rule__DataLinkedCause__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__DataLinkedCause__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5564:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:5565:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:5565:2: ( ruleQualifiedName )
            // InternalDOF.g:5566:3: ruleQualifiedName
            {
             before(grammarAccess.getDataLinkedCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__NameAssignment_2"


    // $ANTLR start "rule__DataLinkedCause__RealizesAssignment_3_1"
    // InternalDOF.g:5575:1: rule__DataLinkedCause__RealizesAssignment_3_1 : ( ( ruleEString ) ) ;
    public final void rule__DataLinkedCause__RealizesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5579:1: ( ( ( ruleEString ) ) )
            // InternalDOF.g:5580:2: ( ( ruleEString ) )
            {
            // InternalDOF.g:5580:2: ( ( ruleEString ) )
            // InternalDOF.g:5581:3: ( ruleEString )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseCrossReference_3_1_0()); 
            // InternalDOF.g:5582:3: ( ruleEString )
            // InternalDOF.g:5583:4: ruleEString
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__RealizesAssignment_3_1"


    // $ANTLR start "rule__DataLinkedCause__AttributeFilterAssignment_5"
    // InternalDOF.g:5594:1: rule__DataLinkedCause__AttributeFilterAssignment_5 : ( ruleAttributeFilter ) ;
    public final void rule__DataLinkedCause__AttributeFilterAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5598:1: ( ( ruleAttributeFilter ) )
            // InternalDOF.g:5599:2: ( ruleAttributeFilter )
            {
            // InternalDOF.g:5599:2: ( ruleAttributeFilter )
            // InternalDOF.g:5600:3: ruleAttributeFilter
            {
             before(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAttributeFilterParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAttributeFilterParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__AttributeFilterAssignment_5"


    // $ANTLR start "rule__DataLinkedCause__InstancesFilterAssignment_6"
    // InternalDOF.g:5609:1: rule__DataLinkedCause__InstancesFilterAssignment_6 : ( ruleInstancesFilter ) ;
    public final void rule__DataLinkedCause__InstancesFilterAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5613:1: ( ( ruleInstancesFilter ) )
            // InternalDOF.g:5614:2: ( ruleInstancesFilter )
            {
            // InternalDOF.g:5614:2: ( ruleInstancesFilter )
            // InternalDOF.g:5615:3: ruleInstancesFilter
            {
             before(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterInstancesFilterParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterInstancesFilterParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__InstancesFilterAssignment_6"


    // $ANTLR start "rule__DataLinkedCause__IncludedReferencesAssignment_7"
    // InternalDOF.g:5624:1: rule__DataLinkedCause__IncludedReferencesAssignment_7 : ( ruleIncludedReference ) ;
    public final void rule__DataLinkedCause__IncludedReferencesAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5628:1: ( ( ruleIncludedReference ) )
            // InternalDOF.g:5629:2: ( ruleIncludedReference )
            {
            // InternalDOF.g:5629:2: ( ruleIncludedReference )
            // InternalDOF.g:5630:3: ruleIncludedReference
            {
             before(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesIncludedReferenceParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesIncludedReferenceParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__IncludedReferencesAssignment_7"


    // $ANTLR start "rule__DataLinkedCause__TypeFilterAssignment_8"
    // InternalDOF.g:5639:1: rule__DataLinkedCause__TypeFilterAssignment_8 : ( ruleTypeFilter ) ;
    public final void rule__DataLinkedCause__TypeFilterAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5643:1: ( ( ruleTypeFilter ) )
            // InternalDOF.g:5644:2: ( ruleTypeFilter )
            {
            // InternalDOF.g:5644:2: ( ruleTypeFilter )
            // InternalDOF.g:5645:3: ruleTypeFilter
            {
             before(grammarAccess.getDataLinkedCauseAccess().getTypeFilterTypeFilterParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getTypeFilterTypeFilterParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__TypeFilterAssignment_8"


    // $ANTLR start "rule__NotMappedCause__NameAssignment_2"
    // InternalDOF.g:5654:1: rule__NotMappedCause__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__NotMappedCause__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5658:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:5659:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:5659:2: ( ruleQualifiedName )
            // InternalDOF.g:5660:3: ruleQualifiedName
            {
             before(grammarAccess.getNotMappedCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getNotMappedCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__NameAssignment_2"


    // $ANTLR start "rule__NotMappedCause__RealizesAssignment_3_1"
    // InternalDOF.g:5669:1: rule__NotMappedCause__RealizesAssignment_3_1 : ( ( ruleEString ) ) ;
    public final void rule__NotMappedCause__RealizesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5673:1: ( ( ( ruleEString ) ) )
            // InternalDOF.g:5674:2: ( ( ruleEString ) )
            {
            // InternalDOF.g:5674:2: ( ( ruleEString ) )
            // InternalDOF.g:5675:3: ( ruleEString )
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesCauseCrossReference_3_1_0()); 
            // InternalDOF.g:5676:3: ( ruleEString )
            // InternalDOF.g:5677:4: ruleEString
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getNotMappedCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getNotMappedCauseAccess().getRealizesCauseCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__RealizesAssignment_3_1"


    // $ANTLR start "rule__Path__JumpsAssignment_0"
    // InternalDOF.g:5688:1: rule__Path__JumpsAssignment_0 : ( RULE_ID ) ;
    public final void rule__Path__JumpsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5692:1: ( ( RULE_ID ) )
            // InternalDOF.g:5693:2: ( RULE_ID )
            {
            // InternalDOF.g:5693:2: ( RULE_ID )
            // InternalDOF.g:5694:3: RULE_ID
            {
             before(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__JumpsAssignment_0"


    // $ANTLR start "rule__Path__JumpsAssignment_1_1"
    // InternalDOF.g:5703:1: rule__Path__JumpsAssignment_1_1 : ( RULE_ID ) ;
    public final void rule__Path__JumpsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5707:1: ( ( RULE_ID ) )
            // InternalDOF.g:5708:2: ( RULE_ID )
            {
            // InternalDOF.g:5708:2: ( RULE_ID )
            // InternalDOF.g:5709:3: RULE_ID
            {
             before(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_1_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__JumpsAssignment_1_1"

    // Delegated rules


    protected DFA7 dfa7 = new DFA7(this);
    protected DFA8 dfa8 = new DFA8(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\5\1\44\1\5\6\uffff\1\44";
    static final String dfa_3s = "\1\5\1\57\1\5\6\uffff\1\57";
    static final String dfa_4s = "\3\uffff\1\5\1\2\1\4\1\6\1\1\1\3\1\uffff";
    static final String dfa_5s = "\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\1",
            "\1\7\1\4\1\10\1\5\1\3\1\6\5\uffff\1\2",
            "\1\11",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\7\1\4\1\10\1\5\1\3\1\6\5\uffff\1\2"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "843:1: rule__Comparison__Alternatives : ( ( ( rule__Comparison__Group_0__0 ) ) | ( ( rule__Comparison__Group_1__0 ) ) | ( ( rule__Comparison__Group_2__0 ) ) | ( ( rule__Comparison__Group_3__0 ) ) | ( ( rule__Comparison__Group_4__0 ) ) | ( ( rule__Comparison__Group_5__0 ) ) );";
        }
    }
    static final String dfa_7s = "\1\52\1\5\1\23\1\5\1\4\3\uffff\2\23";
    static final String dfa_8s = "\1\52\1\5\1\57\1\5\1\4\3\uffff\1\57\1\55";
    static final String dfa_9s = "\5\uffff\1\3\1\2\1\1\2\uffff";
    static final String[] dfa_10s = {
            "\1\1",
            "\1\2",
            "\1\6\27\uffff\1\7\1\4\1\5\1\uffff\1\3",
            "\1\10",
            "\1\11",
            "",
            "",
            "",
            "\1\6\27\uffff\1\7\1\4\1\5\1\uffff\1\3",
            "\1\6\27\uffff\1\7\1\uffff\1\5"
    };
    static final char[] dfa_7 = DFA.unpackEncodedStringToUnsignedChars(dfa_7s);
    static final char[] dfa_8 = DFA.unpackEncodedStringToUnsignedChars(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final short[][] dfa_10 = unpackEncodedStringArray(dfa_10s);

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_7;
            this.max = dfa_8;
            this.accept = dfa_9;
            this.special = dfa_5;
            this.transition = dfa_10;
        }
        public String getDescription() {
            return "888:1: rule__Cause__Alternatives : ( ( ruleCompoundCause ) | ( ruleDataLinkedCause ) | ( ruleNotMappedCause ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000264200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000002000020L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000040C00000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000027000020L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000002000022L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x000000000000F800L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000200400000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000040800000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000003000020L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000180000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000008000020L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000180000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000040001000000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000100000080000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000266000020L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000300000000000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000800000000002L});

}