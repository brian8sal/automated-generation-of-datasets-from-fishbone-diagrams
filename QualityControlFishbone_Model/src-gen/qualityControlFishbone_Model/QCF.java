/**
 */
package qualityControlFishbone_Model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>QCF</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link qualityControlFishbone_Model.QCF#getName <em>Name</em>}</li>
 *   <li>{@link qualityControlFishbone_Model.QCF#getEffects <em>Effects</em>}</li>
 * </ul>
 *
 * @see qualityControlFishbone_Model.QualityControlFishbone_ModelPackage#getQCF()
 * @model
 * @generated
 */
public interface QCF extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see qualityControlFishbone_Model.QualityControlFishbone_ModelPackage#getQCF_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link qualityControlFishbone_Model.QCF#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Effects</b></em>' containment reference list.
	 * The list contents are of type {@link qualityControlFishbone_Model.Effect}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Effects</em>' containment reference list.
	 * @see qualityControlFishbone_Model.QualityControlFishbone_ModelPackage#getQCF_Effects()
	 * @model containment="true"
	 * @generated
	 */
	EList<Effect> getEffects();

} // QCF
