/**
 */
package qualityControlFishbone_Model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link qualityControlFishbone_Model.Cause#getName <em>Name</em>}</li>
 *   <li>{@link qualityControlFishbone_Model.Cause#getDescription <em>Description</em>}</li>
 *   <li>{@link qualityControlFishbone_Model.Cause#getValueOfInterest <em>Value Of Interest</em>}</li>
 *   <li>{@link qualityControlFishbone_Model.Cause#getSubCauses <em>Sub Causes</em>}</li>
 * </ul>
 *
 * @see qualityControlFishbone_Model.QualityControlFishbone_ModelPackage#getCause()
 * @model
 * @generated
 */
public interface Cause extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see qualityControlFishbone_Model.QualityControlFishbone_ModelPackage#getCause_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link qualityControlFishbone_Model.Cause#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see qualityControlFishbone_Model.QualityControlFishbone_ModelPackage#getCause_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link qualityControlFishbone_Model.Cause#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Value Of Interest</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Of Interest</em>' attribute.
	 * @see #setValueOfInterest(String)
	 * @see qualityControlFishbone_Model.QualityControlFishbone_ModelPackage#getCause_ValueOfInterest()
	 * @model
	 * @generated
	 */
	String getValueOfInterest();

	/**
	 * Sets the value of the '{@link qualityControlFishbone_Model.Cause#getValueOfInterest <em>Value Of Interest</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Of Interest</em>' attribute.
	 * @see #getValueOfInterest()
	 * @generated
	 */
	void setValueOfInterest(String value);

	/**
	 * Returns the value of the '<em><b>Sub Causes</b></em>' containment reference list.
	 * The list contents are of type {@link qualityControlFishbone_Model.Cause}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Causes</em>' containment reference list.
	 * @see qualityControlFishbone_Model.QualityControlFishbone_ModelPackage#getCause_SubCauses()
	 * @model containment="true"
	 * @generated
	 */
	EList<Cause> getSubCauses();

} // Cause
