/**
 */
package qualityControlFishbone_Model.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import qualityControlFishbone_Model.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class QualityControlFishbone_ModelFactoryImpl extends EFactoryImpl
		implements QualityControlFishbone_ModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static QualityControlFishbone_ModelFactory init() {
		try {
			QualityControlFishbone_ModelFactory theQualityControlFishbone_ModelFactory = (QualityControlFishbone_ModelFactory) EPackage.Registry.INSTANCE
					.getEFactory(QualityControlFishbone_ModelPackage.eNS_URI);
			if (theQualityControlFishbone_ModelFactory != null) {
				return theQualityControlFishbone_ModelFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new QualityControlFishbone_ModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualityControlFishbone_ModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case QualityControlFishbone_ModelPackage.QCF:
			return createQCF();
		case QualityControlFishbone_ModelPackage.EFFECT:
			return createEffect();
		case QualityControlFishbone_ModelPackage.CATEGORY:
			return createCategory();
		case QualityControlFishbone_ModelPackage.CAUSE:
			return createCause();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QCF createQCF() {
		QCFImpl qcf = new QCFImpl();
		return qcf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Effect createEffect() {
		EffectImpl effect = new EffectImpl();
		return effect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category createCategory() {
		CategoryImpl category = new CategoryImpl();
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cause createCause() {
		CauseImpl cause = new CauseImpl();
		return cause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualityControlFishbone_ModelPackage getQualityControlFishbone_ModelPackage() {
		return (QualityControlFishbone_ModelPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static QualityControlFishbone_ModelPackage getPackage() {
		return QualityControlFishbone_ModelPackage.eINSTANCE;
	}

} //QualityControlFishbone_ModelFactoryImpl
