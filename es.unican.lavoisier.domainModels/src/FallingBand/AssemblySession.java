/**
 */
package FallingBand;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assembly Session</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.AssemblySession#getStart <em>Start</em>}</li>
 *   <li>{@link FallingBand.AssemblySession#getStop <em>Stop</em>}</li>
 *   <li>{@link FallingBand.AssemblySession#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getAssemblySession()
 * @model
 * @generated
 */
public interface AssemblySession extends EObject {
	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see #setStart(Date)
	 * @see FallingBand.FallingPackage#getAssemblySession_Start()
	 * @model
	 * @generated
	 */
	Date getStart();

	/**
	 * Sets the value of the '{@link FallingBand.AssemblySession#getStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' attribute.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(Date value);

	/**
	 * Returns the value of the '<em><b>Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stop</em>' attribute.
	 * @see #setStop(Date)
	 * @see FallingBand.FallingPackage#getAssemblySession_Stop()
	 * @model
	 * @generated
	 */
	Date getStop();

	/**
	 * Sets the value of the '{@link FallingBand.AssemblySession#getStop <em>Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stop</em>' attribute.
	 * @see #getStop()
	 * @generated
	 */
	void setStop(Date value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference.
	 * @see #setParameters(MonitoredParameters)
	 * @see FallingBand.FallingPackage#getAssemblySession_Parameters()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MonitoredParameters getParameters();

	/**
	 * Sets the value of the '{@link FallingBand.AssemblySession#getParameters <em>Parameters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameters</em>' containment reference.
	 * @see #getParameters()
	 * @generated
	 */
	void setParameters(MonitoredParameters value);

} // AssemblySession
