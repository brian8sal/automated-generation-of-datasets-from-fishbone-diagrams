/**
 */
package FallingBand;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitored Parameters</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.MonitoredParameters#getTemperature <em>Temperature</em>}</li>
 *   <li>{@link FallingBand.MonitoredParameters#getHumidity <em>Humidity</em>}</li>
 *   <li>{@link FallingBand.MonitoredParameters#getPliersFastenings <em>Pliers Fastenings</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getMonitoredParameters()
 * @model
 * @generated
 */
public interface MonitoredParameters extends EObject {
	/**
	 * Returns the value of the '<em><b>Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temperature</em>' attribute.
	 * @see #setTemperature(Double)
	 * @see FallingBand.FallingPackage#getMonitoredParameters_Temperature()
	 * @model
	 * @generated
	 */
	Double getTemperature();

	/**
	 * Sets the value of the '{@link FallingBand.MonitoredParameters#getTemperature <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temperature</em>' attribute.
	 * @see #getTemperature()
	 * @generated
	 */
	void setTemperature(Double value);

	/**
	 * Returns the value of the '<em><b>Humidity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Humidity</em>' attribute.
	 * @see #setHumidity(Double)
	 * @see FallingBand.FallingPackage#getMonitoredParameters_Humidity()
	 * @model
	 * @generated
	 */
	Double getHumidity();

	/**
	 * Sets the value of the '{@link FallingBand.MonitoredParameters#getHumidity <em>Humidity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Humidity</em>' attribute.
	 * @see #getHumidity()
	 * @generated
	 */
	void setHumidity(Double value);

	/**
	 * Returns the value of the '<em><b>Pliers Fastenings</b></em>' containment reference list.
	 * The list contents are of type {@link FallingBand.PliersData}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pliers Fastenings</em>' containment reference list.
	 * @see FallingBand.FallingPackage#getMonitoredParameters_PliersFastenings()
	 * @model containment="true" lower="4" upper="4"
	 * @generated
	 */
	EList<PliersData> getPliersFastenings();

} // MonitoredParameters
