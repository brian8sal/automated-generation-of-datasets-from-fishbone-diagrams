/**
 */
package FallingBand.impl;

import FallingBand.AssemblySession;
import FallingBand.Band;
import FallingBand.BandBatchParameters;
import FallingBand.ComplianceReport;
import FallingBand.DriveHalfShaft;
import FallingBand.FallingFactory;
import FallingBand.FallingPackage;
import FallingBand.MonitoredParameters;
import FallingBand.PliersData;
import FallingBand.Provider;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FallingPackageImpl extends EPackageImpl implements FallingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass driveHalfShaftEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bandEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bandBatchParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assemblySessionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitoredParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pliersDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complianceReportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass providerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see FallingBand.FallingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FallingPackageImpl() {
		super(eNS_URI, FallingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FallingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FallingPackage init() {
		if (isInited) return (FallingPackage)EPackage.Registry.INSTANCE.getEPackage(FallingPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFallingPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FallingPackageImpl theFallingPackage = registeredFallingPackage instanceof FallingPackageImpl ? (FallingPackageImpl)registeredFallingPackage : new FallingPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theFallingPackage.createPackageContents();

		// Initialize created meta-data
		theFallingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFallingPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FallingPackage.eNS_URI, theFallingPackage);
		return theFallingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDriveHalfShaft() {
		return driveHalfShaftEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDriveHalfShaft_Id() {
		return (EAttribute)driveHalfShaftEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDriveHalfShaft_ManufacturedTime() {
		return (EAttribute)driveHalfShaftEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_EngineBand() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_WheelBand() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_AssemblySession() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_Report() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBand() {
		return bandEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBand_Model() {
		return (EAttribute)bandEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBand_Parameters() {
		return (EReference)bandEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBandBatchParameters() {
		return bandBatchParametersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBandBatchParameters_MaxThickness() {
		return (EAttribute)bandBatchParametersEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBandBatchParameters_MinThickness() {
		return (EAttribute)bandBatchParametersEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBandBatchParameters_AvgThickness() {
		return (EAttribute)bandBatchParametersEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBandBatchParameters_BatchId() {
		return (EAttribute)bandBatchParametersEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBandBatchParameters_Provider() {
		return (EReference)bandBatchParametersEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssemblySession() {
		return assemblySessionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssemblySession_Start() {
		return (EAttribute)assemblySessionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssemblySession_Stop() {
		return (EAttribute)assemblySessionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssemblySession_Parameters() {
		return (EReference)assemblySessionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitoredParameters() {
		return monitoredParametersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoredParameters_Temperature() {
		return (EAttribute)monitoredParametersEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoredParameters_Humidity() {
		return (EAttribute)monitoredParametersEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMonitoredParameters_PliersFastenings() {
		return (EReference)monitoredParametersEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPliersData() {
		return pliersDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPliersData_Pressure() {
		return (EAttribute)pliersDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPliersData_Number() {
		return (EAttribute)pliersDataEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplianceReport() {
		return complianceReportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplianceReport_FallingWheelBand() {
		return (EAttribute)complianceReportEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplianceReport_FallingEngineBand() {
		return (EAttribute)complianceReportEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvider() {
		return providerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProvider_Provider() {
		return (EAttribute)providerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FallingFactory getFallingFactory() {
		return (FallingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		driveHalfShaftEClass = createEClass(DRIVE_HALF_SHAFT);
		createEAttribute(driveHalfShaftEClass, DRIVE_HALF_SHAFT__ID);
		createEAttribute(driveHalfShaftEClass, DRIVE_HALF_SHAFT__MANUFACTURED_TIME);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__ENGINE_BAND);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__WHEEL_BAND);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__ASSEMBLY_SESSION);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__REPORT);

		bandEClass = createEClass(BAND);
		createEAttribute(bandEClass, BAND__MODEL);
		createEReference(bandEClass, BAND__PARAMETERS);

		bandBatchParametersEClass = createEClass(BAND_BATCH_PARAMETERS);
		createEAttribute(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__MAX_THICKNESS);
		createEAttribute(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__MIN_THICKNESS);
		createEAttribute(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__AVG_THICKNESS);
		createEAttribute(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__BATCH_ID);
		createEReference(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__PROVIDER);

		assemblySessionEClass = createEClass(ASSEMBLY_SESSION);
		createEAttribute(assemblySessionEClass, ASSEMBLY_SESSION__START);
		createEAttribute(assemblySessionEClass, ASSEMBLY_SESSION__STOP);
		createEReference(assemblySessionEClass, ASSEMBLY_SESSION__PARAMETERS);

		monitoredParametersEClass = createEClass(MONITORED_PARAMETERS);
		createEAttribute(monitoredParametersEClass, MONITORED_PARAMETERS__TEMPERATURE);
		createEAttribute(monitoredParametersEClass, MONITORED_PARAMETERS__HUMIDITY);
		createEReference(monitoredParametersEClass, MONITORED_PARAMETERS__PLIERS_FASTENINGS);

		pliersDataEClass = createEClass(PLIERS_DATA);
		createEAttribute(pliersDataEClass, PLIERS_DATA__PRESSURE);
		createEAttribute(pliersDataEClass, PLIERS_DATA__NUMBER);

		complianceReportEClass = createEClass(COMPLIANCE_REPORT);
		createEAttribute(complianceReportEClass, COMPLIANCE_REPORT__FALLING_WHEEL_BAND);
		createEAttribute(complianceReportEClass, COMPLIANCE_REPORT__FALLING_ENGINE_BAND);

		providerEClass = createEClass(PROVIDER);
		createEAttribute(providerEClass, PROVIDER__PROVIDER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(driveHalfShaftEClass, DriveHalfShaft.class, "DriveHalfShaft", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDriveHalfShaft_Id(), ecorePackage.getEString(), "id", null, 0, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDriveHalfShaft_ManufacturedTime(), ecorePackage.getEDate(), "manufacturedTime", null, 0, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_EngineBand(), this.getBand(), null, "engineBand", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_WheelBand(), this.getBand(), null, "wheelBand", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_AssemblySession(), this.getAssemblySession(), null, "assemblySession", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_Report(), this.getComplianceReport(), null, "report", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bandEClass, Band.class, "Band", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBand_Model(), ecorePackage.getEString(), "model", null, 0, 1, Band.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBand_Parameters(), this.getBandBatchParameters(), null, "parameters", null, 1, 1, Band.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bandBatchParametersEClass, BandBatchParameters.class, "BandBatchParameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBandBatchParameters_MaxThickness(), ecorePackage.getEDoubleObject(), "maxThickness", null, 0, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBandBatchParameters_MinThickness(), ecorePackage.getEDoubleObject(), "minThickness", null, 0, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBandBatchParameters_AvgThickness(), ecorePackage.getEDoubleObject(), "avgThickness", null, 0, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBandBatchParameters_BatchId(), ecorePackage.getEDoubleObject(), "batchId", null, 0, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBandBatchParameters_Provider(), this.getProvider(), null, "provider", null, 1, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assemblySessionEClass, AssemblySession.class, "AssemblySession", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssemblySession_Start(), ecorePackage.getEDate(), "start", null, 0, 1, AssemblySession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssemblySession_Stop(), ecorePackage.getEDate(), "stop", null, 0, 1, AssemblySession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssemblySession_Parameters(), this.getMonitoredParameters(), null, "parameters", null, 1, 1, AssemblySession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(monitoredParametersEClass, MonitoredParameters.class, "MonitoredParameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMonitoredParameters_Temperature(), ecorePackage.getEDoubleObject(), "temperature", null, 0, 1, MonitoredParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoredParameters_Humidity(), ecorePackage.getEDoubleObject(), "humidity", null, 0, 1, MonitoredParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMonitoredParameters_PliersFastenings(), this.getPliersData(), null, "pliersFastenings", null, 4, 4, MonitoredParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pliersDataEClass, PliersData.class, "PliersData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPliersData_Pressure(), ecorePackage.getEDoubleObject(), "pressure", null, 0, 1, PliersData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPliersData_Number(), ecorePackage.getEIntegerObject(), "number", null, 0, 1, PliersData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complianceReportEClass, ComplianceReport.class, "ComplianceReport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplianceReport_FallingWheelBand(), ecorePackage.getEBooleanObject(), "fallingWheelBand", null, 0, 1, ComplianceReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplianceReport_FallingEngineBand(), ecorePackage.getEBooleanObject(), "fallingEngineBand", null, 0, 1, ComplianceReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(providerEClass, Provider.class, "Provider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProvider_Provider(), ecorePackage.getEString(), "provider", null, 0, 1, Provider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //FallingPackageImpl
