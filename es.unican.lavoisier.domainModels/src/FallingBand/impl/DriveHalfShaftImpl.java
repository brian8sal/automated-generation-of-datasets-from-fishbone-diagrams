/**
 */
package FallingBand.impl;

import FallingBand.AssemblySession;
import FallingBand.Band;
import FallingBand.ComplianceReport;
import FallingBand.DriveHalfShaft;
import FallingBand.FallingPackage;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Drive Half Shaft</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.impl.DriveHalfShaftImpl#getId <em>Id</em>}</li>
 *   <li>{@link FallingBand.impl.DriveHalfShaftImpl#getManufacturedTime <em>Manufactured Time</em>}</li>
 *   <li>{@link FallingBand.impl.DriveHalfShaftImpl#getEngineBand <em>Engine Band</em>}</li>
 *   <li>{@link FallingBand.impl.DriveHalfShaftImpl#getWheelBand <em>Wheel Band</em>}</li>
 *   <li>{@link FallingBand.impl.DriveHalfShaftImpl#getAssemblySession <em>Assembly Session</em>}</li>
 *   <li>{@link FallingBand.impl.DriveHalfShaftImpl#getReport <em>Report</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DriveHalfShaftImpl extends MinimalEObjectImpl.Container implements DriveHalfShaft {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getManufacturedTime() <em>Manufactured Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturedTime()
	 * @generated
	 * @ordered
	 */
	protected static final Date MANUFACTURED_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getManufacturedTime() <em>Manufactured Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturedTime()
	 * @generated
	 * @ordered
	 */
	protected Date manufacturedTime = MANUFACTURED_TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEngineBand() <em>Engine Band</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEngineBand()
	 * @generated
	 * @ordered
	 */
	protected Band engineBand;

	/**
	 * The cached value of the '{@link #getWheelBand() <em>Wheel Band</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWheelBand()
	 * @generated
	 * @ordered
	 */
	protected Band wheelBand;

	/**
	 * The cached value of the '{@link #getAssemblySession() <em>Assembly Session</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblySession()
	 * @generated
	 * @ordered
	 */
	protected AssemblySession assemblySession;

	/**
	 * The cached value of the '{@link #getReport() <em>Report</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReport()
	 * @generated
	 * @ordered
	 */
	protected ComplianceReport report;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DriveHalfShaftImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FallingPackage.Literals.DRIVE_HALF_SHAFT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getManufacturedTime() {
		return manufacturedTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setManufacturedTime(Date newManufacturedTime) {
		Date oldManufacturedTime = manufacturedTime;
		manufacturedTime = newManufacturedTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__MANUFACTURED_TIME, oldManufacturedTime, manufacturedTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Band getEngineBand() {
		return engineBand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEngineBand(Band newEngineBand, NotificationChain msgs) {
		Band oldEngineBand = engineBand;
		engineBand = newEngineBand;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__ENGINE_BAND, oldEngineBand, newEngineBand);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEngineBand(Band newEngineBand) {
		if (newEngineBand != engineBand) {
			NotificationChain msgs = null;
			if (engineBand != null)
				msgs = ((InternalEObject)engineBand).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.DRIVE_HALF_SHAFT__ENGINE_BAND, null, msgs);
			if (newEngineBand != null)
				msgs = ((InternalEObject)newEngineBand).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.DRIVE_HALF_SHAFT__ENGINE_BAND, null, msgs);
			msgs = basicSetEngineBand(newEngineBand, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__ENGINE_BAND, newEngineBand, newEngineBand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Band getWheelBand() {
		return wheelBand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWheelBand(Band newWheelBand, NotificationChain msgs) {
		Band oldWheelBand = wheelBand;
		wheelBand = newWheelBand;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__WHEEL_BAND, oldWheelBand, newWheelBand);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWheelBand(Band newWheelBand) {
		if (newWheelBand != wheelBand) {
			NotificationChain msgs = null;
			if (wheelBand != null)
				msgs = ((InternalEObject)wheelBand).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.DRIVE_HALF_SHAFT__WHEEL_BAND, null, msgs);
			if (newWheelBand != null)
				msgs = ((InternalEObject)newWheelBand).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.DRIVE_HALF_SHAFT__WHEEL_BAND, null, msgs);
			msgs = basicSetWheelBand(newWheelBand, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__WHEEL_BAND, newWheelBand, newWheelBand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblySession getAssemblySession() {
		return assemblySession;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblySession(AssemblySession newAssemblySession, NotificationChain msgs) {
		AssemblySession oldAssemblySession = assemblySession;
		assemblySession = newAssemblySession;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__ASSEMBLY_SESSION, oldAssemblySession, newAssemblySession);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblySession(AssemblySession newAssemblySession) {
		if (newAssemblySession != assemblySession) {
			NotificationChain msgs = null;
			if (assemblySession != null)
				msgs = ((InternalEObject)assemblySession).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.DRIVE_HALF_SHAFT__ASSEMBLY_SESSION, null, msgs);
			if (newAssemblySession != null)
				msgs = ((InternalEObject)newAssemblySession).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.DRIVE_HALF_SHAFT__ASSEMBLY_SESSION, null, msgs);
			msgs = basicSetAssemblySession(newAssemblySession, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__ASSEMBLY_SESSION, newAssemblySession, newAssemblySession));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplianceReport getReport() {
		return report;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReport(ComplianceReport newReport, NotificationChain msgs) {
		ComplianceReport oldReport = report;
		report = newReport;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__REPORT, oldReport, newReport);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReport(ComplianceReport newReport) {
		if (newReport != report) {
			NotificationChain msgs = null;
			if (report != null)
				msgs = ((InternalEObject)report).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.DRIVE_HALF_SHAFT__REPORT, null, msgs);
			if (newReport != null)
				msgs = ((InternalEObject)newReport).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.DRIVE_HALF_SHAFT__REPORT, null, msgs);
			msgs = basicSetReport(newReport, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.DRIVE_HALF_SHAFT__REPORT, newReport, newReport));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FallingPackage.DRIVE_HALF_SHAFT__ENGINE_BAND:
				return basicSetEngineBand(null, msgs);
			case FallingPackage.DRIVE_HALF_SHAFT__WHEEL_BAND:
				return basicSetWheelBand(null, msgs);
			case FallingPackage.DRIVE_HALF_SHAFT__ASSEMBLY_SESSION:
				return basicSetAssemblySession(null, msgs);
			case FallingPackage.DRIVE_HALF_SHAFT__REPORT:
				return basicSetReport(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FallingPackage.DRIVE_HALF_SHAFT__ID:
				return getId();
			case FallingPackage.DRIVE_HALF_SHAFT__MANUFACTURED_TIME:
				return getManufacturedTime();
			case FallingPackage.DRIVE_HALF_SHAFT__ENGINE_BAND:
				return getEngineBand();
			case FallingPackage.DRIVE_HALF_SHAFT__WHEEL_BAND:
				return getWheelBand();
			case FallingPackage.DRIVE_HALF_SHAFT__ASSEMBLY_SESSION:
				return getAssemblySession();
			case FallingPackage.DRIVE_HALF_SHAFT__REPORT:
				return getReport();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FallingPackage.DRIVE_HALF_SHAFT__ID:
				setId((String)newValue);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__MANUFACTURED_TIME:
				setManufacturedTime((Date)newValue);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__ENGINE_BAND:
				setEngineBand((Band)newValue);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__WHEEL_BAND:
				setWheelBand((Band)newValue);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__ASSEMBLY_SESSION:
				setAssemblySession((AssemblySession)newValue);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__REPORT:
				setReport((ComplianceReport)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FallingPackage.DRIVE_HALF_SHAFT__ID:
				setId(ID_EDEFAULT);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__MANUFACTURED_TIME:
				setManufacturedTime(MANUFACTURED_TIME_EDEFAULT);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__ENGINE_BAND:
				setEngineBand((Band)null);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__WHEEL_BAND:
				setWheelBand((Band)null);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__ASSEMBLY_SESSION:
				setAssemblySession((AssemblySession)null);
				return;
			case FallingPackage.DRIVE_HALF_SHAFT__REPORT:
				setReport((ComplianceReport)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FallingPackage.DRIVE_HALF_SHAFT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case FallingPackage.DRIVE_HALF_SHAFT__MANUFACTURED_TIME:
				return MANUFACTURED_TIME_EDEFAULT == null ? manufacturedTime != null : !MANUFACTURED_TIME_EDEFAULT.equals(manufacturedTime);
			case FallingPackage.DRIVE_HALF_SHAFT__ENGINE_BAND:
				return engineBand != null;
			case FallingPackage.DRIVE_HALF_SHAFT__WHEEL_BAND:
				return wheelBand != null;
			case FallingPackage.DRIVE_HALF_SHAFT__ASSEMBLY_SESSION:
				return assemblySession != null;
			case FallingPackage.DRIVE_HALF_SHAFT__REPORT:
				return report != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", manufacturedTime: ");
		result.append(manufacturedTime);
		result.append(')');
		return result.toString();
	}

} //DriveHalfShaftImpl
