/**
 */
package FallingBand.impl;

import FallingBand.FallingPackage;
import FallingBand.MonitoredParameters;
import FallingBand.PliersData;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitored Parameters</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.impl.MonitoredParametersImpl#getTemperature <em>Temperature</em>}</li>
 *   <li>{@link FallingBand.impl.MonitoredParametersImpl#getHumidity <em>Humidity</em>}</li>
 *   <li>{@link FallingBand.impl.MonitoredParametersImpl#getPliersFastenings <em>Pliers Fastenings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MonitoredParametersImpl extends MinimalEObjectImpl.Container implements MonitoredParameters {
	/**
	 * The default value of the '{@link #getTemperature() <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperature()
	 * @generated
	 * @ordered
	 */
	protected static final Double TEMPERATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTemperature() <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperature()
	 * @generated
	 * @ordered
	 */
	protected Double temperature = TEMPERATURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getHumidity() <em>Humidity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHumidity()
	 * @generated
	 * @ordered
	 */
	protected static final Double HUMIDITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHumidity() <em>Humidity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHumidity()
	 * @generated
	 * @ordered
	 */
	protected Double humidity = HUMIDITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPliersFastenings() <em>Pliers Fastenings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPliersFastenings()
	 * @generated
	 * @ordered
	 */
	protected EList<PliersData> pliersFastenings;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitoredParametersImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FallingPackage.Literals.MONITORED_PARAMETERS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTemperature() {
		return temperature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemperature(Double newTemperature) {
		Double oldTemperature = temperature;
		temperature = newTemperature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.MONITORED_PARAMETERS__TEMPERATURE, oldTemperature, temperature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getHumidity() {
		return humidity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHumidity(Double newHumidity) {
		Double oldHumidity = humidity;
		humidity = newHumidity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.MONITORED_PARAMETERS__HUMIDITY, oldHumidity, humidity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PliersData> getPliersFastenings() {
		if (pliersFastenings == null) {
			pliersFastenings = new EObjectContainmentEList<PliersData>(PliersData.class, this, FallingPackage.MONITORED_PARAMETERS__PLIERS_FASTENINGS);
		}
		return pliersFastenings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FallingPackage.MONITORED_PARAMETERS__PLIERS_FASTENINGS:
				return ((InternalEList<?>)getPliersFastenings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FallingPackage.MONITORED_PARAMETERS__TEMPERATURE:
				return getTemperature();
			case FallingPackage.MONITORED_PARAMETERS__HUMIDITY:
				return getHumidity();
			case FallingPackage.MONITORED_PARAMETERS__PLIERS_FASTENINGS:
				return getPliersFastenings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FallingPackage.MONITORED_PARAMETERS__TEMPERATURE:
				setTemperature((Double)newValue);
				return;
			case FallingPackage.MONITORED_PARAMETERS__HUMIDITY:
				setHumidity((Double)newValue);
				return;
			case FallingPackage.MONITORED_PARAMETERS__PLIERS_FASTENINGS:
				getPliersFastenings().clear();
				getPliersFastenings().addAll((Collection<? extends PliersData>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FallingPackage.MONITORED_PARAMETERS__TEMPERATURE:
				setTemperature(TEMPERATURE_EDEFAULT);
				return;
			case FallingPackage.MONITORED_PARAMETERS__HUMIDITY:
				setHumidity(HUMIDITY_EDEFAULT);
				return;
			case FallingPackage.MONITORED_PARAMETERS__PLIERS_FASTENINGS:
				getPliersFastenings().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FallingPackage.MONITORED_PARAMETERS__TEMPERATURE:
				return TEMPERATURE_EDEFAULT == null ? temperature != null : !TEMPERATURE_EDEFAULT.equals(temperature);
			case FallingPackage.MONITORED_PARAMETERS__HUMIDITY:
				return HUMIDITY_EDEFAULT == null ? humidity != null : !HUMIDITY_EDEFAULT.equals(humidity);
			case FallingPackage.MONITORED_PARAMETERS__PLIERS_FASTENINGS:
				return pliersFastenings != null && !pliersFastenings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (temperature: ");
		result.append(temperature);
		result.append(", humidity: ");
		result.append(humidity);
		result.append(')');
		return result.toString();
	}

} //MonitoredParametersImpl
