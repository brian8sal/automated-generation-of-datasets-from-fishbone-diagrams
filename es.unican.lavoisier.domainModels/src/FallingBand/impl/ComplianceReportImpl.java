/**
 */
package FallingBand.impl;

import FallingBand.ComplianceReport;
import FallingBand.FallingPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Compliance Report</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.impl.ComplianceReportImpl#getFallingWheelBand <em>Falling Wheel Band</em>}</li>
 *   <li>{@link FallingBand.impl.ComplianceReportImpl#getFallingEngineBand <em>Falling Engine Band</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplianceReportImpl extends MinimalEObjectImpl.Container implements ComplianceReport {
	/**
	 * The default value of the '{@link #getFallingWheelBand() <em>Falling Wheel Band</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFallingWheelBand()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean FALLING_WHEEL_BAND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFallingWheelBand() <em>Falling Wheel Band</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFallingWheelBand()
	 * @generated
	 * @ordered
	 */
	protected Boolean fallingWheelBand = FALLING_WHEEL_BAND_EDEFAULT;

	/**
	 * The default value of the '{@link #getFallingEngineBand() <em>Falling Engine Band</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFallingEngineBand()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean FALLING_ENGINE_BAND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFallingEngineBand() <em>Falling Engine Band</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFallingEngineBand()
	 * @generated
	 * @ordered
	 */
	protected Boolean fallingEngineBand = FALLING_ENGINE_BAND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplianceReportImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FallingPackage.Literals.COMPLIANCE_REPORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getFallingWheelBand() {
		return fallingWheelBand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFallingWheelBand(Boolean newFallingWheelBand) {
		Boolean oldFallingWheelBand = fallingWheelBand;
		fallingWheelBand = newFallingWheelBand;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.COMPLIANCE_REPORT__FALLING_WHEEL_BAND, oldFallingWheelBand, fallingWheelBand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getFallingEngineBand() {
		return fallingEngineBand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFallingEngineBand(Boolean newFallingEngineBand) {
		Boolean oldFallingEngineBand = fallingEngineBand;
		fallingEngineBand = newFallingEngineBand;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.COMPLIANCE_REPORT__FALLING_ENGINE_BAND, oldFallingEngineBand, fallingEngineBand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FallingPackage.COMPLIANCE_REPORT__FALLING_WHEEL_BAND:
				return getFallingWheelBand();
			case FallingPackage.COMPLIANCE_REPORT__FALLING_ENGINE_BAND:
				return getFallingEngineBand();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FallingPackage.COMPLIANCE_REPORT__FALLING_WHEEL_BAND:
				setFallingWheelBand((Boolean)newValue);
				return;
			case FallingPackage.COMPLIANCE_REPORT__FALLING_ENGINE_BAND:
				setFallingEngineBand((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FallingPackage.COMPLIANCE_REPORT__FALLING_WHEEL_BAND:
				setFallingWheelBand(FALLING_WHEEL_BAND_EDEFAULT);
				return;
			case FallingPackage.COMPLIANCE_REPORT__FALLING_ENGINE_BAND:
				setFallingEngineBand(FALLING_ENGINE_BAND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FallingPackage.COMPLIANCE_REPORT__FALLING_WHEEL_BAND:
				return FALLING_WHEEL_BAND_EDEFAULT == null ? fallingWheelBand != null : !FALLING_WHEEL_BAND_EDEFAULT.equals(fallingWheelBand);
			case FallingPackage.COMPLIANCE_REPORT__FALLING_ENGINE_BAND:
				return FALLING_ENGINE_BAND_EDEFAULT == null ? fallingEngineBand != null : !FALLING_ENGINE_BAND_EDEFAULT.equals(fallingEngineBand);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (fallingWheelBand: ");
		result.append(fallingWheelBand);
		result.append(", fallingEngineBand: ");
		result.append(fallingEngineBand);
		result.append(')');
		return result.toString();
	}

} //ComplianceReportImpl
