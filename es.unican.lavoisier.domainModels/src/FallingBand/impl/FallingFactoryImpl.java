/**
 */
package FallingBand.impl;

import FallingBand.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FallingFactoryImpl extends EFactoryImpl implements FallingFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FallingFactory init() {
		try {
			FallingFactory theFallingFactory = (FallingFactory)EPackage.Registry.INSTANCE.getEFactory(FallingPackage.eNS_URI);
			if (theFallingFactory != null) {
				return theFallingFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FallingFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FallingFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FallingPackage.DRIVE_HALF_SHAFT: return createDriveHalfShaft();
			case FallingPackage.BAND: return createBand();
			case FallingPackage.BAND_BATCH_PARAMETERS: return createBandBatchParameters();
			case FallingPackage.ASSEMBLY_SESSION: return createAssemblySession();
			case FallingPackage.MONITORED_PARAMETERS: return createMonitoredParameters();
			case FallingPackage.PLIERS_DATA: return createPliersData();
			case FallingPackage.COMPLIANCE_REPORT: return createComplianceReport();
			case FallingPackage.PROVIDER: return createProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DriveHalfShaft createDriveHalfShaft() {
		DriveHalfShaftImpl driveHalfShaft = new DriveHalfShaftImpl();
		return driveHalfShaft;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Band createBand() {
		BandImpl band = new BandImpl();
		return band;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BandBatchParameters createBandBatchParameters() {
		BandBatchParametersImpl bandBatchParameters = new BandBatchParametersImpl();
		return bandBatchParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblySession createAssemblySession() {
		AssemblySessionImpl assemblySession = new AssemblySessionImpl();
		return assemblySession;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitoredParameters createMonitoredParameters() {
		MonitoredParametersImpl monitoredParameters = new MonitoredParametersImpl();
		return monitoredParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PliersData createPliersData() {
		PliersDataImpl pliersData = new PliersDataImpl();
		return pliersData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplianceReport createComplianceReport() {
		ComplianceReportImpl complianceReport = new ComplianceReportImpl();
		return complianceReport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Provider createProvider() {
		ProviderImpl provider = new ProviderImpl();
		return provider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FallingPackage getFallingPackage() {
		return (FallingPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FallingPackage getPackage() {
		return FallingPackage.eINSTANCE;
	}

} //FallingFactoryImpl
