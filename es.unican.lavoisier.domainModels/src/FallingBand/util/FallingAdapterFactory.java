/**
 */
package FallingBand.util;

import FallingBand.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see FallingBand.FallingPackage
 * @generated
 */
public class FallingAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FallingPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FallingAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FallingPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FallingSwitch<Adapter> modelSwitch =
		new FallingSwitch<Adapter>() {
			@Override
			public Adapter caseDriveHalfShaft(DriveHalfShaft object) {
				return createDriveHalfShaftAdapter();
			}
			@Override
			public Adapter caseBand(Band object) {
				return createBandAdapter();
			}
			@Override
			public Adapter caseBandBatchParameters(BandBatchParameters object) {
				return createBandBatchParametersAdapter();
			}
			@Override
			public Adapter caseAssemblySession(AssemblySession object) {
				return createAssemblySessionAdapter();
			}
			@Override
			public Adapter caseMonitoredParameters(MonitoredParameters object) {
				return createMonitoredParametersAdapter();
			}
			@Override
			public Adapter casePliersData(PliersData object) {
				return createPliersDataAdapter();
			}
			@Override
			public Adapter caseComplianceReport(ComplianceReport object) {
				return createComplianceReportAdapter();
			}
			@Override
			public Adapter caseProvider(Provider object) {
				return createProviderAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link FallingBand.DriveHalfShaft <em>Drive Half Shaft</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FallingBand.DriveHalfShaft
	 * @generated
	 */
	public Adapter createDriveHalfShaftAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FallingBand.Band <em>Band</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FallingBand.Band
	 * @generated
	 */
	public Adapter createBandAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FallingBand.BandBatchParameters <em>Band Batch Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FallingBand.BandBatchParameters
	 * @generated
	 */
	public Adapter createBandBatchParametersAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FallingBand.AssemblySession <em>Assembly Session</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FallingBand.AssemblySession
	 * @generated
	 */
	public Adapter createAssemblySessionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FallingBand.MonitoredParameters <em>Monitored Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FallingBand.MonitoredParameters
	 * @generated
	 */
	public Adapter createMonitoredParametersAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FallingBand.PliersData <em>Pliers Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FallingBand.PliersData
	 * @generated
	 */
	public Adapter createPliersDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FallingBand.ComplianceReport <em>Compliance Report</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FallingBand.ComplianceReport
	 * @generated
	 */
	public Adapter createComplianceReportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FallingBand.Provider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FallingBand.Provider
	 * @generated
	 */
	public Adapter createProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FallingAdapterFactory
