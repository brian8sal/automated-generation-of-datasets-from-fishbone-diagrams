/**
 */
package FallingBand;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see FallingBand.FallingPackage
 * @generated
 */
public interface FallingFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FallingFactory eINSTANCE = FallingBand.impl.FallingFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Drive Half Shaft</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Drive Half Shaft</em>'.
	 * @generated
	 */
	DriveHalfShaft createDriveHalfShaft();

	/**
	 * Returns a new object of class '<em>Band</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Band</em>'.
	 * @generated
	 */
	Band createBand();

	/**
	 * Returns a new object of class '<em>Band Batch Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Band Batch Parameters</em>'.
	 * @generated
	 */
	BandBatchParameters createBandBatchParameters();

	/**
	 * Returns a new object of class '<em>Assembly Session</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assembly Session</em>'.
	 * @generated
	 */
	AssemblySession createAssemblySession();

	/**
	 * Returns a new object of class '<em>Monitored Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitored Parameters</em>'.
	 * @generated
	 */
	MonitoredParameters createMonitoredParameters();

	/**
	 * Returns a new object of class '<em>Pliers Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pliers Data</em>'.
	 * @generated
	 */
	PliersData createPliersData();

	/**
	 * Returns a new object of class '<em>Compliance Report</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Compliance Report</em>'.
	 * @generated
	 */
	ComplianceReport createComplianceReport();

	/**
	 * Returns a new object of class '<em>Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Provider</em>'.
	 * @generated
	 */
	Provider createProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FallingPackage getFallingPackage();

} //FallingFactory
