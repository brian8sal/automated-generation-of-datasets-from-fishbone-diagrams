/**
 */
package FallingBand;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Drive Half Shaft</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.DriveHalfShaft#getId <em>Id</em>}</li>
 *   <li>{@link FallingBand.DriveHalfShaft#getManufacturedTime <em>Manufactured Time</em>}</li>
 *   <li>{@link FallingBand.DriveHalfShaft#getEngineBand <em>Engine Band</em>}</li>
 *   <li>{@link FallingBand.DriveHalfShaft#getWheelBand <em>Wheel Band</em>}</li>
 *   <li>{@link FallingBand.DriveHalfShaft#getAssemblySession <em>Assembly Session</em>}</li>
 *   <li>{@link FallingBand.DriveHalfShaft#getReport <em>Report</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getDriveHalfShaft()
 * @model
 * @generated
 */
public interface DriveHalfShaft extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see FallingBand.FallingPackage#getDriveHalfShaft_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link FallingBand.DriveHalfShaft#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Manufactured Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manufactured Time</em>' attribute.
	 * @see #setManufacturedTime(Date)
	 * @see FallingBand.FallingPackage#getDriveHalfShaft_ManufacturedTime()
	 * @model
	 * @generated
	 */
	Date getManufacturedTime();

	/**
	 * Sets the value of the '{@link FallingBand.DriveHalfShaft#getManufacturedTime <em>Manufactured Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Manufactured Time</em>' attribute.
	 * @see #getManufacturedTime()
	 * @generated
	 */
	void setManufacturedTime(Date value);

	/**
	 * Returns the value of the '<em><b>Engine Band</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Engine Band</em>' containment reference.
	 * @see #setEngineBand(Band)
	 * @see FallingBand.FallingPackage#getDriveHalfShaft_EngineBand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Band getEngineBand();

	/**
	 * Sets the value of the '{@link FallingBand.DriveHalfShaft#getEngineBand <em>Engine Band</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Engine Band</em>' containment reference.
	 * @see #getEngineBand()
	 * @generated
	 */
	void setEngineBand(Band value);

	/**
	 * Returns the value of the '<em><b>Wheel Band</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wheel Band</em>' containment reference.
	 * @see #setWheelBand(Band)
	 * @see FallingBand.FallingPackage#getDriveHalfShaft_WheelBand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Band getWheelBand();

	/**
	 * Sets the value of the '{@link FallingBand.DriveHalfShaft#getWheelBand <em>Wheel Band</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wheel Band</em>' containment reference.
	 * @see #getWheelBand()
	 * @generated
	 */
	void setWheelBand(Band value);

	/**
	 * Returns the value of the '<em><b>Assembly Session</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Session</em>' containment reference.
	 * @see #setAssemblySession(AssemblySession)
	 * @see FallingBand.FallingPackage#getDriveHalfShaft_AssemblySession()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AssemblySession getAssemblySession();

	/**
	 * Sets the value of the '{@link FallingBand.DriveHalfShaft#getAssemblySession <em>Assembly Session</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Session</em>' containment reference.
	 * @see #getAssemblySession()
	 * @generated
	 */
	void setAssemblySession(AssemblySession value);

	/**
	 * Returns the value of the '<em><b>Report</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Report</em>' containment reference.
	 * @see #setReport(ComplianceReport)
	 * @see FallingBand.FallingPackage#getDriveHalfShaft_Report()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ComplianceReport getReport();

	/**
	 * Sets the value of the '{@link FallingBand.DriveHalfShaft#getReport <em>Report</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Report</em>' containment reference.
	 * @see #getReport()
	 * @generated
	 */
	void setReport(ComplianceReport value);

} // DriveHalfShaft
