/**
 */
package FallingBand;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see FallingBand.FallingFactory
 * @model kind="package"
 * @generated
 */
public interface FallingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "FallingBand";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fallingband.unican.es";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "falling";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FallingPackage eINSTANCE = FallingBand.impl.FallingPackageImpl.init();

	/**
	 * The meta object id for the '{@link FallingBand.impl.DriveHalfShaftImpl <em>Drive Half Shaft</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.DriveHalfShaftImpl
	 * @see FallingBand.impl.FallingPackageImpl#getDriveHalfShaft()
	 * @generated
	 */
	int DRIVE_HALF_SHAFT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__ID = 0;

	/**
	 * The feature id for the '<em><b>Manufactured Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__MANUFACTURED_TIME = 1;

	/**
	 * The feature id for the '<em><b>Engine Band</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__ENGINE_BAND = 2;

	/**
	 * The feature id for the '<em><b>Wheel Band</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__WHEEL_BAND = 3;

	/**
	 * The feature id for the '<em><b>Assembly Session</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__ASSEMBLY_SESSION = 4;

	/**
	 * The feature id for the '<em><b>Report</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__REPORT = 5;

	/**
	 * The number of structural features of the '<em>Drive Half Shaft</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Drive Half Shaft</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.BandImpl <em>Band</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.BandImpl
	 * @see FallingBand.impl.FallingPackageImpl#getBand()
	 * @generated
	 */
	int BAND = 1;

	/**
	 * The feature id for the '<em><b>Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND__MODEL = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND__PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Band</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Band</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.BandBatchParametersImpl <em>Band Batch Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.BandBatchParametersImpl
	 * @see FallingBand.impl.FallingPackageImpl#getBandBatchParameters()
	 * @generated
	 */
	int BAND_BATCH_PARAMETERS = 2;

	/**
	 * The feature id for the '<em><b>Max Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__MAX_THICKNESS = 0;

	/**
	 * The feature id for the '<em><b>Min Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__MIN_THICKNESS = 1;

	/**
	 * The feature id for the '<em><b>Avg Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__AVG_THICKNESS = 2;

	/**
	 * The feature id for the '<em><b>Batch Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__BATCH_ID = 3;

	/**
	 * The feature id for the '<em><b>Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__PROVIDER = 4;

	/**
	 * The number of structural features of the '<em>Band Batch Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Band Batch Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.AssemblySessionImpl <em>Assembly Session</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.AssemblySessionImpl
	 * @see FallingBand.impl.FallingPackageImpl#getAssemblySession()
	 * @generated
	 */
	int ASSEMBLY_SESSION = 3;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION__START = 0;

	/**
	 * The feature id for the '<em><b>Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION__STOP = 1;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION__PARAMETERS = 2;

	/**
	 * The number of structural features of the '<em>Assembly Session</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Assembly Session</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.MonitoredParametersImpl <em>Monitored Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.MonitoredParametersImpl
	 * @see FallingBand.impl.FallingPackageImpl#getMonitoredParameters()
	 * @generated
	 */
	int MONITORED_PARAMETERS = 4;

	/**
	 * The feature id for the '<em><b>Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS__TEMPERATURE = 0;

	/**
	 * The feature id for the '<em><b>Humidity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS__HUMIDITY = 1;

	/**
	 * The feature id for the '<em><b>Pliers Fastenings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS__PLIERS_FASTENINGS = 2;

	/**
	 * The number of structural features of the '<em>Monitored Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Monitored Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.PliersDataImpl <em>Pliers Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.PliersDataImpl
	 * @see FallingBand.impl.FallingPackageImpl#getPliersData()
	 * @generated
	 */
	int PLIERS_DATA = 5;

	/**
	 * The feature id for the '<em><b>Pressure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLIERS_DATA__PRESSURE = 0;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLIERS_DATA__NUMBER = 1;

	/**
	 * The number of structural features of the '<em>Pliers Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLIERS_DATA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Pliers Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLIERS_DATA_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.ComplianceReportImpl <em>Compliance Report</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.ComplianceReportImpl
	 * @see FallingBand.impl.FallingPackageImpl#getComplianceReport()
	 * @generated
	 */
	int COMPLIANCE_REPORT = 6;

	/**
	 * The feature id for the '<em><b>Falling Wheel Band</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_REPORT__FALLING_WHEEL_BAND = 0;

	/**
	 * The feature id for the '<em><b>Falling Engine Band</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_REPORT__FALLING_ENGINE_BAND = 1;

	/**
	 * The number of structural features of the '<em>Compliance Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_REPORT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Compliance Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_REPORT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.ProviderImpl <em>Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.ProviderImpl
	 * @see FallingBand.impl.FallingPackageImpl#getProvider()
	 * @generated
	 */
	int PROVIDER = 7;

	/**
	 * The feature id for the '<em><b>Provider</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER__PROVIDER = 0;

	/**
	 * The number of structural features of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link FallingBand.DriveHalfShaft <em>Drive Half Shaft</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Drive Half Shaft</em>'.
	 * @see FallingBand.DriveHalfShaft
	 * @generated
	 */
	EClass getDriveHalfShaft();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.DriveHalfShaft#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see FallingBand.DriveHalfShaft#getId()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EAttribute getDriveHalfShaft_Id();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.DriveHalfShaft#getManufacturedTime <em>Manufactured Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Manufactured Time</em>'.
	 * @see FallingBand.DriveHalfShaft#getManufacturedTime()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EAttribute getDriveHalfShaft_ManufacturedTime();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getEngineBand <em>Engine Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Engine Band</em>'.
	 * @see FallingBand.DriveHalfShaft#getEngineBand()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_EngineBand();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getWheelBand <em>Wheel Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Wheel Band</em>'.
	 * @see FallingBand.DriveHalfShaft#getWheelBand()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_WheelBand();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getAssemblySession <em>Assembly Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assembly Session</em>'.
	 * @see FallingBand.DriveHalfShaft#getAssemblySession()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_AssemblySession();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getReport <em>Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Report</em>'.
	 * @see FallingBand.DriveHalfShaft#getReport()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_Report();

	/**
	 * Returns the meta object for class '{@link FallingBand.Band <em>Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Band</em>'.
	 * @see FallingBand.Band
	 * @generated
	 */
	EClass getBand();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Band#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model</em>'.
	 * @see FallingBand.Band#getModel()
	 * @see #getBand()
	 * @generated
	 */
	EAttribute getBand_Model();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.Band#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see FallingBand.Band#getParameters()
	 * @see #getBand()
	 * @generated
	 */
	EReference getBand_Parameters();

	/**
	 * Returns the meta object for class '{@link FallingBand.BandBatchParameters <em>Band Batch Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Band Batch Parameters</em>'.
	 * @see FallingBand.BandBatchParameters
	 * @generated
	 */
	EClass getBandBatchParameters();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.BandBatchParameters#getMaxThickness <em>Max Thickness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Thickness</em>'.
	 * @see FallingBand.BandBatchParameters#getMaxThickness()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EAttribute getBandBatchParameters_MaxThickness();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.BandBatchParameters#getMinThickness <em>Min Thickness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Thickness</em>'.
	 * @see FallingBand.BandBatchParameters#getMinThickness()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EAttribute getBandBatchParameters_MinThickness();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.BandBatchParameters#getAvgThickness <em>Avg Thickness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Thickness</em>'.
	 * @see FallingBand.BandBatchParameters#getAvgThickness()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EAttribute getBandBatchParameters_AvgThickness();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.BandBatchParameters#getBatchId <em>Batch Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Batch Id</em>'.
	 * @see FallingBand.BandBatchParameters#getBatchId()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EAttribute getBandBatchParameters_BatchId();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.BandBatchParameters#getProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provider</em>'.
	 * @see FallingBand.BandBatchParameters#getProvider()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EReference getBandBatchParameters_Provider();

	/**
	 * Returns the meta object for class '{@link FallingBand.AssemblySession <em>Assembly Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assembly Session</em>'.
	 * @see FallingBand.AssemblySession
	 * @generated
	 */
	EClass getAssemblySession();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.AssemblySession#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start</em>'.
	 * @see FallingBand.AssemblySession#getStart()
	 * @see #getAssemblySession()
	 * @generated
	 */
	EAttribute getAssemblySession_Start();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.AssemblySession#getStop <em>Stop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stop</em>'.
	 * @see FallingBand.AssemblySession#getStop()
	 * @see #getAssemblySession()
	 * @generated
	 */
	EAttribute getAssemblySession_Stop();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.AssemblySession#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see FallingBand.AssemblySession#getParameters()
	 * @see #getAssemblySession()
	 * @generated
	 */
	EReference getAssemblySession_Parameters();

	/**
	 * Returns the meta object for class '{@link FallingBand.MonitoredParameters <em>Monitored Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitored Parameters</em>'.
	 * @see FallingBand.MonitoredParameters
	 * @generated
	 */
	EClass getMonitoredParameters();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.MonitoredParameters#getTemperature <em>Temperature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Temperature</em>'.
	 * @see FallingBand.MonitoredParameters#getTemperature()
	 * @see #getMonitoredParameters()
	 * @generated
	 */
	EAttribute getMonitoredParameters_Temperature();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.MonitoredParameters#getHumidity <em>Humidity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Humidity</em>'.
	 * @see FallingBand.MonitoredParameters#getHumidity()
	 * @see #getMonitoredParameters()
	 * @generated
	 */
	EAttribute getMonitoredParameters_Humidity();

	/**
	 * Returns the meta object for the containment reference list '{@link FallingBand.MonitoredParameters#getPliersFastenings <em>Pliers Fastenings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pliers Fastenings</em>'.
	 * @see FallingBand.MonitoredParameters#getPliersFastenings()
	 * @see #getMonitoredParameters()
	 * @generated
	 */
	EReference getMonitoredParameters_PliersFastenings();

	/**
	 * Returns the meta object for class '{@link FallingBand.PliersData <em>Pliers Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pliers Data</em>'.
	 * @see FallingBand.PliersData
	 * @generated
	 */
	EClass getPliersData();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.PliersData#getPressure <em>Pressure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pressure</em>'.
	 * @see FallingBand.PliersData#getPressure()
	 * @see #getPliersData()
	 * @generated
	 */
	EAttribute getPliersData_Pressure();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.PliersData#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see FallingBand.PliersData#getNumber()
	 * @see #getPliersData()
	 * @generated
	 */
	EAttribute getPliersData_Number();

	/**
	 * Returns the meta object for class '{@link FallingBand.ComplianceReport <em>Compliance Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compliance Report</em>'.
	 * @see FallingBand.ComplianceReport
	 * @generated
	 */
	EClass getComplianceReport();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ComplianceReport#getFallingWheelBand <em>Falling Wheel Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Falling Wheel Band</em>'.
	 * @see FallingBand.ComplianceReport#getFallingWheelBand()
	 * @see #getComplianceReport()
	 * @generated
	 */
	EAttribute getComplianceReport_FallingWheelBand();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ComplianceReport#getFallingEngineBand <em>Falling Engine Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Falling Engine Band</em>'.
	 * @see FallingBand.ComplianceReport#getFallingEngineBand()
	 * @see #getComplianceReport()
	 * @generated
	 */
	EAttribute getComplianceReport_FallingEngineBand();

	/**
	 * Returns the meta object for class '{@link FallingBand.Provider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provider</em>'.
	 * @see FallingBand.Provider
	 * @generated
	 */
	EClass getProvider();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Provider#getProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Provider</em>'.
	 * @see FallingBand.Provider#getProvider()
	 * @see #getProvider()
	 * @generated
	 */
	EAttribute getProvider_Provider();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FallingFactory getFallingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link FallingBand.impl.DriveHalfShaftImpl <em>Drive Half Shaft</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.DriveHalfShaftImpl
		 * @see FallingBand.impl.FallingPackageImpl#getDriveHalfShaft()
		 * @generated
		 */
		EClass DRIVE_HALF_SHAFT = eINSTANCE.getDriveHalfShaft();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DRIVE_HALF_SHAFT__ID = eINSTANCE.getDriveHalfShaft_Id();

		/**
		 * The meta object literal for the '<em><b>Manufactured Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DRIVE_HALF_SHAFT__MANUFACTURED_TIME = eINSTANCE.getDriveHalfShaft_ManufacturedTime();

		/**
		 * The meta object literal for the '<em><b>Engine Band</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__ENGINE_BAND = eINSTANCE.getDriveHalfShaft_EngineBand();

		/**
		 * The meta object literal for the '<em><b>Wheel Band</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__WHEEL_BAND = eINSTANCE.getDriveHalfShaft_WheelBand();

		/**
		 * The meta object literal for the '<em><b>Assembly Session</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__ASSEMBLY_SESSION = eINSTANCE.getDriveHalfShaft_AssemblySession();

		/**
		 * The meta object literal for the '<em><b>Report</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__REPORT = eINSTANCE.getDriveHalfShaft_Report();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.BandImpl <em>Band</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.BandImpl
		 * @see FallingBand.impl.FallingPackageImpl#getBand()
		 * @generated
		 */
		EClass BAND = eINSTANCE.getBand();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND__MODEL = eINSTANCE.getBand_Model();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BAND__PARAMETERS = eINSTANCE.getBand_Parameters();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.BandBatchParametersImpl <em>Band Batch Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.BandBatchParametersImpl
		 * @see FallingBand.impl.FallingPackageImpl#getBandBatchParameters()
		 * @generated
		 */
		EClass BAND_BATCH_PARAMETERS = eINSTANCE.getBandBatchParameters();

		/**
		 * The meta object literal for the '<em><b>Max Thickness</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND_BATCH_PARAMETERS__MAX_THICKNESS = eINSTANCE.getBandBatchParameters_MaxThickness();

		/**
		 * The meta object literal for the '<em><b>Min Thickness</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND_BATCH_PARAMETERS__MIN_THICKNESS = eINSTANCE.getBandBatchParameters_MinThickness();

		/**
		 * The meta object literal for the '<em><b>Avg Thickness</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND_BATCH_PARAMETERS__AVG_THICKNESS = eINSTANCE.getBandBatchParameters_AvgThickness();

		/**
		 * The meta object literal for the '<em><b>Batch Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND_BATCH_PARAMETERS__BATCH_ID = eINSTANCE.getBandBatchParameters_BatchId();

		/**
		 * The meta object literal for the '<em><b>Provider</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BAND_BATCH_PARAMETERS__PROVIDER = eINSTANCE.getBandBatchParameters_Provider();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.AssemblySessionImpl <em>Assembly Session</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.AssemblySessionImpl
		 * @see FallingBand.impl.FallingPackageImpl#getAssemblySession()
		 * @generated
		 */
		EClass ASSEMBLY_SESSION = eINSTANCE.getAssemblySession();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSEMBLY_SESSION__START = eINSTANCE.getAssemblySession_Start();

		/**
		 * The meta object literal for the '<em><b>Stop</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSEMBLY_SESSION__STOP = eINSTANCE.getAssemblySession_Stop();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSEMBLY_SESSION__PARAMETERS = eINSTANCE.getAssemblySession_Parameters();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.MonitoredParametersImpl <em>Monitored Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.MonitoredParametersImpl
		 * @see FallingBand.impl.FallingPackageImpl#getMonitoredParameters()
		 * @generated
		 */
		EClass MONITORED_PARAMETERS = eINSTANCE.getMonitoredParameters();

		/**
		 * The meta object literal for the '<em><b>Temperature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORED_PARAMETERS__TEMPERATURE = eINSTANCE.getMonitoredParameters_Temperature();

		/**
		 * The meta object literal for the '<em><b>Humidity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORED_PARAMETERS__HUMIDITY = eINSTANCE.getMonitoredParameters_Humidity();

		/**
		 * The meta object literal for the '<em><b>Pliers Fastenings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORED_PARAMETERS__PLIERS_FASTENINGS = eINSTANCE.getMonitoredParameters_PliersFastenings();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.PliersDataImpl <em>Pliers Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.PliersDataImpl
		 * @see FallingBand.impl.FallingPackageImpl#getPliersData()
		 * @generated
		 */
		EClass PLIERS_DATA = eINSTANCE.getPliersData();

		/**
		 * The meta object literal for the '<em><b>Pressure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLIERS_DATA__PRESSURE = eINSTANCE.getPliersData_Pressure();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLIERS_DATA__NUMBER = eINSTANCE.getPliersData_Number();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.ComplianceReportImpl <em>Compliance Report</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.ComplianceReportImpl
		 * @see FallingBand.impl.FallingPackageImpl#getComplianceReport()
		 * @generated
		 */
		EClass COMPLIANCE_REPORT = eINSTANCE.getComplianceReport();

		/**
		 * The meta object literal for the '<em><b>Falling Wheel Band</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLIANCE_REPORT__FALLING_WHEEL_BAND = eINSTANCE.getComplianceReport_FallingWheelBand();

		/**
		 * The meta object literal for the '<em><b>Falling Engine Band</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLIANCE_REPORT__FALLING_ENGINE_BAND = eINSTANCE.getComplianceReport_FallingEngineBand();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.ProviderImpl <em>Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.ProviderImpl
		 * @see FallingBand.impl.FallingPackageImpl#getProvider()
		 * @generated
		 */
		EClass PROVIDER = eINSTANCE.getProvider();

		/**
		 * The meta object literal for the '<em><b>Provider</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROVIDER__PROVIDER = eINSTANCE.getProvider_Provider();

	}

} //FallingPackage
