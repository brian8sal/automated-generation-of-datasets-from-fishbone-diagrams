package es.unican.domainModelProvider;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

@SuppressWarnings("all")
public class DomainModelProvider {
  private static String DOMAIN_MODEL_NSURI = "";
  
  private static String DOMAIN_MODEL_INSTANCE_FILE = "";
  
  private static EPackage DOMAIN_MODEL;
  
  private static Resource DOMAIN_INSTANCE_RESOURCE;
  
  private static List<IDomainModelUser> domainModelUsers = new ArrayList<IDomainModelUser>();
  
  private DomainModelProvider() {
  }
  
  public static Object init() {
    return null;
  }
  
  private static boolean domainReferenceChanges(final String domainModelNSURI, final String domainModelInstanceFile) {
    return ((!DomainModelProvider.DOMAIN_MODEL_NSURI.equals(domainModelNSURI)) || 
      (!DomainModelProvider.DOMAIN_MODEL_INSTANCE_FILE.equals(domainModelInstanceFile)));
  }
  
  public static void loadDomainModelResources(final ResourceSet resourceSet, final String domainModelNSURI, final String domainModelInstance) {
    boolean _domainReferenceChanges = DomainModelProvider.domainReferenceChanges(domainModelNSURI, domainModelInstance);
    boolean _not = (!_domainReferenceChanges);
    if (_not) {
    }
    DomainModelProvider.DOMAIN_MODEL_NSURI = domainModelNSURI;
    DomainModelProvider.DOMAIN_MODEL_INSTANCE_FILE = domainModelInstance;
    EObject _get = resourceSet.getResource(URI.createURI(domainModelNSURI), true).getContents().get(0);
    DomainModelProvider.DOMAIN_MODEL = ((EPackage) _get);
    DomainModelProvider.DOMAIN_INSTANCE_RESOURCE = resourceSet.getResource(
      URI.createPlatformResourceURI(domainModelInstance, true), 
      true);
    for (final IDomainModelUser user : DomainModelProvider.domainModelUsers) {
      user.setDomainModel(DomainModelProvider.DOMAIN_MODEL);
    }
  }
  
  public static EPackage getDomainModel() {
    return DomainModelProvider.DOMAIN_MODEL;
  }
  
  public static Resource getDomainInstanceResource() {
    return DomainModelProvider.DOMAIN_INSTANCE_RESOURCE;
  }
  
  public static void linkDomainModel(final IDomainModelUser user) {
    DomainModelProvider.domainModelUsers.add(user);
    user.setDomainModel(DomainModelProvider.DOMAIN_MODEL);
  }
}
