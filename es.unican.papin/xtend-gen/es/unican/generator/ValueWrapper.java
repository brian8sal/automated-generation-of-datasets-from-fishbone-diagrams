package es.unican.generator;

import com.google.common.base.Objects;

@SuppressWarnings("all")
public class ValueWrapper {
  private Object value;
  
  public ValueWrapper(final Object value) {
    this.value = value;
  }
  
  public Object get() {
    return this.value;
  }
  
  public void set(final Object value) {
    this.value = value;
  }
  
  @Override
  public String toString() {
    String _xifexpression = null;
    boolean _equals = Objects.equal(this.value, null);
    if (_equals) {
      _xifexpression = "";
    } else {
      _xifexpression = this.value.toString();
    }
    return _xifexpression;
  }
}
