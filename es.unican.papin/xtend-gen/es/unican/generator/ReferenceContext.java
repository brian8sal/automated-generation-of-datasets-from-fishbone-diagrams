package es.unican.generator;

import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

@SuppressWarnings("all")
public class ReferenceContext {
  private EClass mainEClass;
  
  private List<EReference> referenceJumps;
  
  private String prefix;
  
  public ReferenceContext(final EClass mainEClass) {
    this.mainEClass = mainEClass;
    ArrayList<EReference> _arrayList = new ArrayList<EReference>();
    this.referenceJumps = _arrayList;
    String _string = new String();
    this.prefix = _string;
  }
  
  public ReferenceContext(final ReferenceContext rc) {
    this.mainEClass = rc.mainEClass;
    ArrayList<EReference> _arrayList = new ArrayList<EReference>(rc.referenceJumps);
    this.referenceJumps = _arrayList;
    String _string = new String(rc.prefix);
    this.prefix = _string;
  }
  
  public EClass getMainEClass() {
    return this.mainEClass;
  }
  
  public List<EReference> getReferenceJumps() {
    return this.referenceJumps;
  }
  
  public EClass getCurrentEClass() {
    boolean _isEmpty = this.referenceJumps.isEmpty();
    if (_isEmpty) {
      return this.mainEClass;
    } else {
      int _size = this.referenceJumps.size();
      int _minus = (_size - 1);
      return this.referenceJumps.get(_minus).getEReferenceType();
    }
  }
  
  public EObject getValue(final EObject instance, final EReference reference) {
    EObject result = instance;
    for (final EReference eRef : this.referenceJumps) {
      {
        Object _eGet = result.eGet(eRef);
        result = ((EObject) _eGet);
        boolean _equals = Objects.equal(result, null);
        if (_equals) {
          return null;
        }
      }
    }
    Object _eGet = result.eGet(reference);
    return ((EObject) _eGet);
  }
  
  public String getReferenceAttributeName(final String separator, final String refName, final String attrName) {
    return String.format("%s%s%s", 
      this.getPrefix(separator, refName), separator, attrName);
  }
  
  private String getPrefix(final String separator, final String lastFragment) {
    boolean _isEmpty = this.prefix.isEmpty();
    if (_isEmpty) {
      return lastFragment;
    }
    return String.format("%s%s%s", this.prefix, separator, lastFragment);
  }
  
  public String addReferenceJump(final EReference ref, final String separator, final String prefixFragment) {
    String _xblockexpression = null;
    {
      this.referenceJumps.add(ref);
      String _xifexpression = null;
      boolean _isEmpty = this.prefix.isEmpty();
      if (_isEmpty) {
        _xifexpression = this.prefix = prefixFragment;
      } else {
        _xifexpression = this.prefix = String.format("%s%s%s", this.prefix, separator, prefixFragment);
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
}
