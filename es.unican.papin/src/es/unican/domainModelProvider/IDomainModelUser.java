package es.unican.domainModelProvider;

import org.eclipse.emf.ecore.EPackage;

public interface IDomainModelUser {
  public void setDomainModel(EPackage domainModel);
}
