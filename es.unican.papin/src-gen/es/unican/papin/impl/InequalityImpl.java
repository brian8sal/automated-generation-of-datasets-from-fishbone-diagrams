/**
 * generated by Xtext 2.25.0
 */
package es.unican.papin.impl;

import es.unican.papin.Inequality;
import es.unican.papin.PapinPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inequality</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InequalityImpl extends ComparisonImpl implements Inequality
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InequalityImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PapinPackage.Literals.INEQUALITY;
  }

} //InequalityImpl
