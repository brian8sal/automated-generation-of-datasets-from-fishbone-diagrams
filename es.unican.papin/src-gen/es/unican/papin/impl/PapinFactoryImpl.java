/**
 * generated by Xtext 2.25.0
 */
package es.unican.papin.impl;

import es.unican.papin.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PapinFactoryImpl extends EFactoryImpl implements PapinFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static PapinFactory init()
  {
    try
    {
      PapinFactory thePapinFactory = (PapinFactory)EPackage.Registry.INSTANCE.getEFactory(PapinPackage.eNS_URI);
      if (thePapinFactory != null)
      {
        return thePapinFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new PapinFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PapinFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case PapinPackage.DATASETS: return createDatasets();
      case PapinPackage.DATASET: return createDataset();
      case PapinPackage.INCLUDED_REFERENCE: return createIncludedReference();
      case PapinPackage.SIMPLE_REFERENCE: return createSimpleReference();
      case PapinPackage.AGGREGATED_REFERENCE: return createAggregatedReference();
      case PapinPackage.TYPE_FILTER: return createTypeFilter();
      case PapinPackage.TYPE_COMPLETION: return createTypeCompletion();
      case PapinPackage.TYPE_SELECTION: return createTypeSelection();
      case PapinPackage.TYPE_CUSTOMIZATION: return createTypeCustomization();
      case PapinPackage.ATTRIBUTE_FILTER: return createAttributeFilter();
      case PapinPackage.BOOLEAN_EXPRESSION: return createBooleanExpression();
      case PapinPackage.COMPARISON: return createComparison();
      case PapinPackage.PATH: return createPath();
      case PapinPackage.AND_CONJUNCTION: return createAndConjunction();
      case PapinPackage.OR_CONJUNCTION: return createOrConjunction();
      case PapinPackage.EQUALITY: return createEquality();
      case PapinPackage.INEQUALITY: return createInequality();
      case PapinPackage.MORE_THAN: return createMoreThan();
      case PapinPackage.MORE_THAN_OR_EQUAL: return createMoreThanOrEqual();
      case PapinPackage.LESS_THAN: return createLessThan();
      case PapinPackage.LESS_THAN_OR_EQUAL: return createLessThanOrEqual();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Datasets createDatasets()
  {
    DatasetsImpl datasets = new DatasetsImpl();
    return datasets;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Dataset createDataset()
  {
    DatasetImpl dataset = new DatasetImpl();
    return dataset;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public IncludedReference createIncludedReference()
  {
    IncludedReferenceImpl includedReference = new IncludedReferenceImpl();
    return includedReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SimpleReference createSimpleReference()
  {
    SimpleReferenceImpl simpleReference = new SimpleReferenceImpl();
    return simpleReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AggregatedReference createAggregatedReference()
  {
    AggregatedReferenceImpl aggregatedReference = new AggregatedReferenceImpl();
    return aggregatedReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public TypeFilter createTypeFilter()
  {
    TypeFilterImpl typeFilter = new TypeFilterImpl();
    return typeFilter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public TypeCompletion createTypeCompletion()
  {
    TypeCompletionImpl typeCompletion = new TypeCompletionImpl();
    return typeCompletion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public TypeSelection createTypeSelection()
  {
    TypeSelectionImpl typeSelection = new TypeSelectionImpl();
    return typeSelection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public TypeCustomization createTypeCustomization()
  {
    TypeCustomizationImpl typeCustomization = new TypeCustomizationImpl();
    return typeCustomization;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AttributeFilter createAttributeFilter()
  {
    AttributeFilterImpl attributeFilter = new AttributeFilterImpl();
    return attributeFilter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public BooleanExpression createBooleanExpression()
  {
    BooleanExpressionImpl booleanExpression = new BooleanExpressionImpl();
    return booleanExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Comparison createComparison()
  {
    ComparisonImpl comparison = new ComparisonImpl();
    return comparison;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Path createPath()
  {
    PathImpl path = new PathImpl();
    return path;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AndConjunction createAndConjunction()
  {
    AndConjunctionImpl andConjunction = new AndConjunctionImpl();
    return andConjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OrConjunction createOrConjunction()
  {
    OrConjunctionImpl orConjunction = new OrConjunctionImpl();
    return orConjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Equality createEquality()
  {
    EqualityImpl equality = new EqualityImpl();
    return equality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Inequality createInequality()
  {
    InequalityImpl inequality = new InequalityImpl();
    return inequality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MoreThan createMoreThan()
  {
    MoreThanImpl moreThan = new MoreThanImpl();
    return moreThan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MoreThanOrEqual createMoreThanOrEqual()
  {
    MoreThanOrEqualImpl moreThanOrEqual = new MoreThanOrEqualImpl();
    return moreThanOrEqual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public LessThan createLessThan()
  {
    LessThanImpl lessThan = new LessThanImpl();
    return lessThan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public LessThanOrEqual createLessThanOrEqual()
  {
    LessThanOrEqualImpl lessThanOrEqual = new LessThanOrEqualImpl();
    return lessThanOrEqual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public PapinPackage getPapinPackage()
  {
    return (PapinPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static PapinPackage getPackage()
  {
    return PapinPackage.eINSTANCE;
  }

} //PapinFactoryImpl
