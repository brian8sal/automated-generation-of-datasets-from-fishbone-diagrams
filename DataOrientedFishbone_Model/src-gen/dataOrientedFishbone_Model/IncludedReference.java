/**
 */
package dataOrientedFishbone_Model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Included Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getIncludedReference()
 * @model
 * @generated
 */
public interface IncludedReference extends EObject {
} // IncludedReference
