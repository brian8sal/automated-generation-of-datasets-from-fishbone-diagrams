/**
 */
package dataOrientedFishbone_Model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Effect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dataOrientedFishbone_Model.Effect#getName <em>Name</em>}</li>
 *   <li>{@link dataOrientedFishbone_Model.Effect#getDataFeeder <em>Data Feeder</em>}</li>
 *   <li>{@link dataOrientedFishbone_Model.Effect#getCategories <em>Categories</em>}</li>
 * </ul>
 *
 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getEffect()
 * @model
 * @generated
 */
public interface Effect extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getEffect_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dataOrientedFishbone_Model.Effect#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Data Feeder</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Feeder</em>' containment reference.
	 * @see #setDataFeeder(DataFeeder)
	 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getEffect_DataFeeder()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DataFeeder getDataFeeder();

	/**
	 * Sets the value of the '{@link dataOrientedFishbone_Model.Effect#getDataFeeder <em>Data Feeder</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Feeder</em>' containment reference.
	 * @see #getDataFeeder()
	 * @generated
	 */
	void setDataFeeder(DataFeeder value);

	/**
	 * Returns the value of the '<em><b>Categories</b></em>' containment reference list.
	 * The list contents are of type {@link dataOrientedFishbone_Model.Category}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Categories</em>' containment reference list.
	 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getEffect_Categories()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Category> getCategories();

} // Effect
