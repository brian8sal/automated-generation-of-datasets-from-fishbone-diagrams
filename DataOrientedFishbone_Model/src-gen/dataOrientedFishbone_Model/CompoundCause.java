/**
 */
package dataOrientedFishbone_Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compound Cause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dataOrientedFishbone_Model.CompoundCause#getSubCauses <em>Sub Causes</em>}</li>
 * </ul>
 *
 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getCompoundCause()
 * @model
 * @generated
 */
public interface CompoundCause extends Cause {
	/**
	 * Returns the value of the '<em><b>Sub Causes</b></em>' containment reference list.
	 * The list contents are of type {@link dataOrientedFishbone_Model.Cause}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Causes</em>' containment reference list.
	 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getCompoundCause_SubCauses()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Cause> getSubCauses();

} // CompoundCause
