/**
 */
package dataOrientedFishbone_Model.impl;

import dataOrientedFishbone_Model.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DataOrientedFishbone_ModelFactoryImpl extends EFactoryImpl implements DataOrientedFishbone_ModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DataOrientedFishbone_ModelFactory init() {
		try {
			DataOrientedFishbone_ModelFactory theDataOrientedFishbone_ModelFactory = (DataOrientedFishbone_ModelFactory) EPackage.Registry.INSTANCE
					.getEFactory(DataOrientedFishbone_ModelPackage.eNS_URI);
			if (theDataOrientedFishbone_ModelFactory != null) {
				return theDataOrientedFishbone_ModelFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DataOrientedFishbone_ModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataOrientedFishbone_ModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case DataOrientedFishbone_ModelPackage.DOF:
			return createDOF();
		case DataOrientedFishbone_ModelPackage.EFFECT:
			return createEffect();
		case DataOrientedFishbone_ModelPackage.CATEGORY:
			return createCategory();
		case DataOrientedFishbone_ModelPackage.COMPOUND_CAUSE:
			return createCompoundCause();
		case DataOrientedFishbone_ModelPackage.DATA_LINKED_CAUSE:
			return createDataLinkedCause();
		case DataOrientedFishbone_ModelPackage.NOT_MAPPED_CAUSE:
			return createNotMappedCause();
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER:
			return createDataFeeder();
		case DataOrientedFishbone_ModelPackage.ATTRIBUTE_FILTER:
			return createAttributeFilter();
		case DataOrientedFishbone_ModelPackage.INCLUDED_REFERENCE:
			return createIncludedReference();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DOF createDOF() {
		DOFImpl dof = new DOFImpl();
		return dof;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Effect createEffect() {
		EffectImpl effect = new EffectImpl();
		return effect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category createCategory() {
		CategoryImpl category = new CategoryImpl();
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompoundCause createCompoundCause() {
		CompoundCauseImpl compoundCause = new CompoundCauseImpl();
		return compoundCause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataLinkedCause createDataLinkedCause() {
		DataLinkedCauseImpl dataLinkedCause = new DataLinkedCauseImpl();
		return dataLinkedCause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotMappedCause createNotMappedCause() {
		NotMappedCauseImpl notMappedCause = new NotMappedCauseImpl();
		return notMappedCause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFeeder createDataFeeder() {
		DataFeederImpl dataFeeder = new DataFeederImpl();
		return dataFeeder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeFilter createAttributeFilter() {
		AttributeFilterImpl attributeFilter = new AttributeFilterImpl();
		return attributeFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IncludedReference createIncludedReference() {
		IncludedReferenceImpl includedReference = new IncludedReferenceImpl();
		return includedReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataOrientedFishbone_ModelPackage getDataOrientedFishbone_ModelPackage() {
		return (DataOrientedFishbone_ModelPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DataOrientedFishbone_ModelPackage getPackage() {
		return DataOrientedFishbone_ModelPackage.eINSTANCE;
	}

} //DataOrientedFishbone_ModelFactoryImpl
