/**
 */
package dataOrientedFishbone_Model.impl;

import dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage;
import dataOrientedFishbone_Model.IncludedReference;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Included Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IncludedReferenceImpl extends MinimalEObjectImpl.Container implements IncludedReference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IncludedReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataOrientedFishbone_ModelPackage.Literals.INCLUDED_REFERENCE;
	}

} //IncludedReferenceImpl
