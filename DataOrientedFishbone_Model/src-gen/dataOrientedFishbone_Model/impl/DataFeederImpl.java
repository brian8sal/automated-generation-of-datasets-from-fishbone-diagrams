/**
 */
package dataOrientedFishbone_Model.impl;

import dataOrientedFishbone_Model.AttributeFilter;
import dataOrientedFishbone_Model.DataFeeder;
import dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage;

import dataOrientedFishbone_Model.IncludedReference;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Feeder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dataOrientedFishbone_Model.impl.DataFeederImpl#getDomainElementSelector <em>Domain Element Selector</em>}</li>
 *   <li>{@link dataOrientedFishbone_Model.impl.DataFeederImpl#getAttributeFilter <em>Attribute Filter</em>}</li>
 *   <li>{@link dataOrientedFishbone_Model.impl.DataFeederImpl#getIncludedReferences <em>Included References</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataFeederImpl extends MinimalEObjectImpl.Container implements DataFeeder {
	/**
	 * The default value of the '{@link #getDomainElementSelector() <em>Domain Element Selector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainElementSelector()
	 * @generated
	 * @ordered
	 */
	protected static final String DOMAIN_ELEMENT_SELECTOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomainElementSelector() <em>Domain Element Selector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainElementSelector()
	 * @generated
	 * @ordered
	 */
	protected String domainElementSelector = DOMAIN_ELEMENT_SELECTOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAttributeFilter() <em>Attribute Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeFilter()
	 * @generated
	 * @ordered
	 */
	protected AttributeFilter attributeFilter;

	/**
	 * The cached value of the '{@link #getIncludedReferences() <em>Included References</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludedReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<IncludedReference> includedReferences;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataFeederImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataOrientedFishbone_ModelPackage.Literals.DATA_FEEDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomainElementSelector() {
		return domainElementSelector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomainElementSelector(String newDomainElementSelector) {
		String oldDomainElementSelector = domainElementSelector;
		domainElementSelector = newDomainElementSelector;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					DataOrientedFishbone_ModelPackage.DATA_FEEDER__DOMAIN_ELEMENT_SELECTOR, oldDomainElementSelector,
					domainElementSelector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeFilter getAttributeFilter() {
		return attributeFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttributeFilter(AttributeFilter newAttributeFilter, NotificationChain msgs) {
		AttributeFilter oldAttributeFilter = attributeFilter;
		attributeFilter = newAttributeFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DataOrientedFishbone_ModelPackage.DATA_FEEDER__ATTRIBUTE_FILTER, oldAttributeFilter,
					newAttributeFilter);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeFilter(AttributeFilter newAttributeFilter) {
		if (newAttributeFilter != attributeFilter) {
			NotificationChain msgs = null;
			if (attributeFilter != null)
				msgs = ((InternalEObject) attributeFilter).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - DataOrientedFishbone_ModelPackage.DATA_FEEDER__ATTRIBUTE_FILTER, null,
						msgs);
			if (newAttributeFilter != null)
				msgs = ((InternalEObject) newAttributeFilter).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - DataOrientedFishbone_ModelPackage.DATA_FEEDER__ATTRIBUTE_FILTER, null,
						msgs);
			msgs = basicSetAttributeFilter(newAttributeFilter, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					DataOrientedFishbone_ModelPackage.DATA_FEEDER__ATTRIBUTE_FILTER, newAttributeFilter,
					newAttributeFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IncludedReference> getIncludedReferences() {
		if (includedReferences == null) {
			includedReferences = new EObjectContainmentEList<IncludedReference>(IncludedReference.class, this,
					DataOrientedFishbone_ModelPackage.DATA_FEEDER__INCLUDED_REFERENCES);
		}
		return includedReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__ATTRIBUTE_FILTER:
			return basicSetAttributeFilter(null, msgs);
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__INCLUDED_REFERENCES:
			return ((InternalEList<?>) getIncludedReferences()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__DOMAIN_ELEMENT_SELECTOR:
			return getDomainElementSelector();
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__ATTRIBUTE_FILTER:
			return getAttributeFilter();
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__INCLUDED_REFERENCES:
			return getIncludedReferences();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__DOMAIN_ELEMENT_SELECTOR:
			setDomainElementSelector((String) newValue);
			return;
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__ATTRIBUTE_FILTER:
			setAttributeFilter((AttributeFilter) newValue);
			return;
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__INCLUDED_REFERENCES:
			getIncludedReferences().clear();
			getIncludedReferences().addAll((Collection<? extends IncludedReference>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__DOMAIN_ELEMENT_SELECTOR:
			setDomainElementSelector(DOMAIN_ELEMENT_SELECTOR_EDEFAULT);
			return;
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__ATTRIBUTE_FILTER:
			setAttributeFilter((AttributeFilter) null);
			return;
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__INCLUDED_REFERENCES:
			getIncludedReferences().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__DOMAIN_ELEMENT_SELECTOR:
			return DOMAIN_ELEMENT_SELECTOR_EDEFAULT == null ? domainElementSelector != null
					: !DOMAIN_ELEMENT_SELECTOR_EDEFAULT.equals(domainElementSelector);
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__ATTRIBUTE_FILTER:
			return attributeFilter != null;
		case DataOrientedFishbone_ModelPackage.DATA_FEEDER__INCLUDED_REFERENCES:
			return includedReferences != null && !includedReferences.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (domainElementSelector: ");
		result.append(domainElementSelector);
		result.append(')');
		return result.toString();
	}

} //DataFeederImpl
