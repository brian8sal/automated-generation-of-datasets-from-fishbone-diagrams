/**
 */
package dataOrientedFishbone_Model.impl;

import dataOrientedFishbone_Model.AttributeFilter;
import dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AttributeFilterImpl extends MinimalEObjectImpl.Container implements AttributeFilter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataOrientedFishbone_ModelPackage.Literals.ATTRIBUTE_FILTER;
	}

} //AttributeFilterImpl
