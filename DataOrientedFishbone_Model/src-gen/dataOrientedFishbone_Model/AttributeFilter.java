/**
 */
package dataOrientedFishbone_Model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getAttributeFilter()
 * @model
 * @generated
 */
public interface AttributeFilter extends EObject {
} // AttributeFilter
