package es.unican.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import es.unican.services.QCFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalQCFParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_TAB", "RULE_NEWLINE", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'qcf'", "'effect'", "'description'", "'category'", "'cause'", "'valueOfInterest'", "'contains'", "'{'", "'}'", "'.'"
    };
    public static final int RULE_NEWLINE=7;
    public static final int RULE_TAB=6;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_INT=8;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalQCFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalQCFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalQCFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalQCF.g"; }



     	private QCFGrammarAccess grammarAccess;

        public InternalQCFParser(TokenStream input, QCFGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "QCF";
       	}

       	@Override
       	protected QCFGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleQCF"
    // InternalQCF.g:64:1: entryRuleQCF returns [EObject current=null] : iv_ruleQCF= ruleQCF EOF ;
    public final EObject entryRuleQCF() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQCF = null;


        try {
            // InternalQCF.g:64:44: (iv_ruleQCF= ruleQCF EOF )
            // InternalQCF.g:65:2: iv_ruleQCF= ruleQCF EOF
            {
             newCompositeNode(grammarAccess.getQCFRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQCF=ruleQCF();

            state._fsp--;

             current =iv_ruleQCF; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQCF"


    // $ANTLR start "ruleQCF"
    // InternalQCF.g:71:1: ruleQCF returns [EObject current=null] : ( (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_effects_2_0= ruleEffect ) ) ) ;
    public final EObject ruleQCF() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_effects_2_0 = null;



        	enterRule();

        try {
            // InternalQCF.g:77:2: ( ( (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_effects_2_0= ruleEffect ) ) ) )
            // InternalQCF.g:78:2: ( (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_effects_2_0= ruleEffect ) ) )
            {
            // InternalQCF.g:78:2: ( (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_effects_2_0= ruleEffect ) ) )
            // InternalQCF.g:79:3: (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_effects_2_0= ruleEffect ) )
            {
            // InternalQCF.g:79:3: (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==13) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalQCF.g:80:4: otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) )
                    {
                    otherlv_0=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_0, grammarAccess.getQCFAccess().getQcfKeyword_0_0());
                    			
                    // InternalQCF.g:84:4: ( (lv_name_1_0= ruleQualifiedName ) )
                    // InternalQCF.g:85:5: (lv_name_1_0= ruleQualifiedName )
                    {
                    // InternalQCF.g:85:5: (lv_name_1_0= ruleQualifiedName )
                    // InternalQCF.g:86:6: lv_name_1_0= ruleQualifiedName
                    {

                    						newCompositeNode(grammarAccess.getQCFAccess().getNameQualifiedNameParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_4);
                    lv_name_1_0=ruleQualifiedName();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getQCFRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_1_0,
                    							"es.unican.QCF.QualifiedName");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQCF.g:104:3: ( (lv_effects_2_0= ruleEffect ) )
            // InternalQCF.g:105:4: (lv_effects_2_0= ruleEffect )
            {
            // InternalQCF.g:105:4: (lv_effects_2_0= ruleEffect )
            // InternalQCF.g:106:5: lv_effects_2_0= ruleEffect
            {

            					newCompositeNode(grammarAccess.getQCFAccess().getEffectsEffectParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_effects_2_0=ruleEffect();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQCFRule());
            					}
            					add(
            						current,
            						"effects",
            						lv_effects_2_0,
            						"es.unican.QCF.Effect");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQCF"


    // $ANTLR start "entryRuleEffect"
    // InternalQCF.g:127:1: entryRuleEffect returns [EObject current=null] : iv_ruleEffect= ruleEffect EOF ;
    public final EObject entryRuleEffect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEffect = null;


        try {
            // InternalQCF.g:127:47: (iv_ruleEffect= ruleEffect EOF )
            // InternalQCF.g:128:2: iv_ruleEffect= ruleEffect EOF
            {
             newCompositeNode(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEffect=ruleEffect();

            state._fsp--;

             current =iv_ruleEffect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalQCF.g:134:1: ruleEffect returns [EObject current=null] : ( () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )? ) ;
    public final EObject ruleEffect() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        EObject lv_categories_5_0 = null;

        EObject lv_categories_6_0 = null;



        	enterRule();

        try {
            // InternalQCF.g:140:2: ( ( () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )? ) )
            // InternalQCF.g:141:2: ( () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )? )
            {
            // InternalQCF.g:141:2: ( () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )? )
            // InternalQCF.g:142:3: () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )?
            {
            // InternalQCF.g:142:3: ()
            // InternalQCF.g:143:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEffectAccess().getEffectAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getEffectAccess().getEffectKeyword_1());
            		
            // InternalQCF.g:153:3: ( (lv_name_2_0= ruleEString ) )
            // InternalQCF.g:154:4: (lv_name_2_0= ruleEString )
            {
            // InternalQCF.g:154:4: (lv_name_2_0= ruleEString )
            // InternalQCF.g:155:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getEffectAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEffectRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.QCF.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQCF.g:172:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==15) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalQCF.g:173:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,15,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getEffectAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalQCF.g:177:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalQCF.g:178:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalQCF.g:178:5: (lv_description_4_0= ruleEString )
                    // InternalQCF.g:179:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getEffectAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_7);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEffectRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"es.unican.QCF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQCF.g:197:3: ( ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==16) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalQCF.g:198:4: ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )*
                    {
                    // InternalQCF.g:198:4: ( (lv_categories_5_0= ruleCategory ) )
                    // InternalQCF.g:199:5: (lv_categories_5_0= ruleCategory )
                    {
                    // InternalQCF.g:199:5: (lv_categories_5_0= ruleCategory )
                    // InternalQCF.g:200:6: lv_categories_5_0= ruleCategory
                    {

                    						newCompositeNode(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_7);
                    lv_categories_5_0=ruleCategory();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEffectRule());
                    						}
                    						add(
                    							current,
                    							"categories",
                    							lv_categories_5_0,
                    							"es.unican.QCF.Category");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalQCF.g:217:4: ( (lv_categories_6_0= ruleCategory ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==16) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalQCF.g:218:5: (lv_categories_6_0= ruleCategory )
                    	    {
                    	    // InternalQCF.g:218:5: (lv_categories_6_0= ruleCategory )
                    	    // InternalQCF.g:219:6: lv_categories_6_0= ruleCategory
                    	    {

                    	    						newCompositeNode(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_4_1_0());
                    	    					
                    	    pushFollow(FOLLOW_7);
                    	    lv_categories_6_0=ruleCategory();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getEffectRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"categories",
                    	    							lv_categories_6_0,
                    	    							"es.unican.QCF.Category");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalQCF.g:241:1: entryRuleCategory returns [EObject current=null] : iv_ruleCategory= ruleCategory EOF ;
    public final EObject entryRuleCategory() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCategory = null;


        try {
            // InternalQCF.g:241:49: (iv_ruleCategory= ruleCategory EOF )
            // InternalQCF.g:242:2: iv_ruleCategory= ruleCategory EOF
            {
             newCompositeNode(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCategory=ruleCategory();

            state._fsp--;

             current =iv_ruleCategory; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalQCF.g:248:1: ruleCategory returns [EObject current=null] : ( () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_causes_5_0= ruleCause ) ) ( (lv_causes_6_0= ruleCause ) )* )? ) ;
    public final EObject ruleCategory() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        EObject lv_causes_5_0 = null;

        EObject lv_causes_6_0 = null;



        	enterRule();

        try {
            // InternalQCF.g:254:2: ( ( () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_causes_5_0= ruleCause ) ) ( (lv_causes_6_0= ruleCause ) )* )? ) )
            // InternalQCF.g:255:2: ( () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_causes_5_0= ruleCause ) ) ( (lv_causes_6_0= ruleCause ) )* )? )
            {
            // InternalQCF.g:255:2: ( () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_causes_5_0= ruleCause ) ) ( (lv_causes_6_0= ruleCause ) )* )? )
            // InternalQCF.g:256:3: () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( ( (lv_causes_5_0= ruleCause ) ) ( (lv_causes_6_0= ruleCause ) )* )?
            {
            // InternalQCF.g:256:3: ()
            // InternalQCF.g:257:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCategoryAccess().getCategoryAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,16,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getCategoryAccess().getCategoryKeyword_1());
            		
            // InternalQCF.g:267:3: ( (lv_name_2_0= ruleEString ) )
            // InternalQCF.g:268:4: (lv_name_2_0= ruleEString )
            {
            // InternalQCF.g:268:4: (lv_name_2_0= ruleEString )
            // InternalQCF.g:269:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCategoryAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_8);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCategoryRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.QCF.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQCF.g:286:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalQCF.g:287:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,15,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getCategoryAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalQCF.g:291:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalQCF.g:292:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalQCF.g:292:5: (lv_description_4_0= ruleEString )
                    // InternalQCF.g:293:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCategoryAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCategoryRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"es.unican.QCF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQCF.g:311:3: ( ( (lv_causes_5_0= ruleCause ) ) ( (lv_causes_6_0= ruleCause ) )* )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalQCF.g:312:4: ( (lv_causes_5_0= ruleCause ) ) ( (lv_causes_6_0= ruleCause ) )*
                    {
                    // InternalQCF.g:312:4: ( (lv_causes_5_0= ruleCause ) )
                    // InternalQCF.g:313:5: (lv_causes_5_0= ruleCause )
                    {
                    // InternalQCF.g:313:5: (lv_causes_5_0= ruleCause )
                    // InternalQCF.g:314:6: lv_causes_5_0= ruleCause
                    {

                    						newCompositeNode(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_causes_5_0=ruleCause();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCategoryRule());
                    						}
                    						add(
                    							current,
                    							"causes",
                    							lv_causes_5_0,
                    							"es.unican.QCF.Cause");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalQCF.g:331:4: ( (lv_causes_6_0= ruleCause ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==17) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalQCF.g:332:5: (lv_causes_6_0= ruleCause )
                    	    {
                    	    // InternalQCF.g:332:5: (lv_causes_6_0= ruleCause )
                    	    // InternalQCF.g:333:6: lv_causes_6_0= ruleCause
                    	    {

                    	    						newCompositeNode(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_1_0());
                    	    					
                    	    pushFollow(FOLLOW_9);
                    	    lv_causes_6_0=ruleCause();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getCategoryRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"causes",
                    	    							lv_causes_6_0,
                    	    							"es.unican.QCF.Cause");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleCause"
    // InternalQCF.g:355:1: entryRuleCause returns [EObject current=null] : iv_ruleCause= ruleCause EOF ;
    public final EObject entryRuleCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCause = null;


        try {
            // InternalQCF.g:355:46: (iv_ruleCause= ruleCause EOF )
            // InternalQCF.g:356:2: iv_ruleCause= ruleCause EOF
            {
             newCompositeNode(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCause=ruleCause();

            state._fsp--;

             current =iv_ruleCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalQCF.g:362:1: ruleCause returns [EObject current=null] : ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) ) ( (lv_subCauses_10_0= ruleCause ) )* otherlv_11= '}' )? ) ;
    public final EObject ruleCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_valueOfInterest_6_0 = null;

        EObject lv_subCauses_9_0 = null;

        EObject lv_subCauses_10_0 = null;



        	enterRule();

        try {
            // InternalQCF.g:368:2: ( ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) ) ( (lv_subCauses_10_0= ruleCause ) )* otherlv_11= '}' )? ) )
            // InternalQCF.g:369:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) ) ( (lv_subCauses_10_0= ruleCause ) )* otherlv_11= '}' )? )
            {
            // InternalQCF.g:369:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) ) ( (lv_subCauses_10_0= ruleCause ) )* otherlv_11= '}' )? )
            // InternalQCF.g:370:3: () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) ) ( (lv_subCauses_10_0= ruleCause ) )* otherlv_11= '}' )?
            {
            // InternalQCF.g:370:3: ()
            // InternalQCF.g:371:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCauseAccess().getCauseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,17,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getCauseAccess().getCauseKeyword_1());
            		
            // InternalQCF.g:381:3: ( (lv_name_2_0= ruleEString ) )
            // InternalQCF.g:382:4: (lv_name_2_0= ruleEString )
            {
            // InternalQCF.g:382:4: (lv_name_2_0= ruleEString )
            // InternalQCF.g:383:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCauseAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_10);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCauseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.QCF.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQCF.g:400:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==15) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalQCF.g:401:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,15,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getCauseAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalQCF.g:405:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalQCF.g:406:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalQCF.g:406:5: (lv_description_4_0= ruleEString )
                    // InternalQCF.g:407:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCauseAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCauseRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"es.unican.QCF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQCF.g:425:3: (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==18) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalQCF.g:426:4: otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,18,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getCauseAccess().getValueOfInterestKeyword_4_0());
                    			
                    // InternalQCF.g:430:4: ( (lv_valueOfInterest_6_0= ruleEString ) )
                    // InternalQCF.g:431:5: (lv_valueOfInterest_6_0= ruleEString )
                    {
                    // InternalQCF.g:431:5: (lv_valueOfInterest_6_0= ruleEString )
                    // InternalQCF.g:432:6: lv_valueOfInterest_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCauseAccess().getValueOfInterestEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_12);
                    lv_valueOfInterest_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCauseRule());
                    						}
                    						set(
                    							current,
                    							"valueOfInterest",
                    							lv_valueOfInterest_6_0,
                    							"es.unican.QCF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQCF.g:450:3: (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) ) ( (lv_subCauses_10_0= ruleCause ) )* otherlv_11= '}' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==19) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalQCF.g:451:4: otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) ) ( (lv_subCauses_10_0= ruleCause ) )* otherlv_11= '}'
                    {
                    otherlv_7=(Token)match(input,19,FOLLOW_13); 

                    				newLeafNode(otherlv_7, grammarAccess.getCauseAccess().getContainsKeyword_5_0());
                    			
                    otherlv_8=(Token)match(input,20,FOLLOW_14); 

                    				newLeafNode(otherlv_8, grammarAccess.getCauseAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalQCF.g:459:4: ( (lv_subCauses_9_0= ruleCause ) )
                    // InternalQCF.g:460:5: (lv_subCauses_9_0= ruleCause )
                    {
                    // InternalQCF.g:460:5: (lv_subCauses_9_0= ruleCause )
                    // InternalQCF.g:461:6: lv_subCauses_9_0= ruleCause
                    {

                    						newCompositeNode(grammarAccess.getCauseAccess().getSubCausesCauseParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_subCauses_9_0=ruleCause();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCauseRule());
                    						}
                    						add(
                    							current,
                    							"subCauses",
                    							lv_subCauses_9_0,
                    							"es.unican.QCF.Cause");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalQCF.g:478:4: ( (lv_subCauses_10_0= ruleCause ) )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==17) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalQCF.g:479:5: (lv_subCauses_10_0= ruleCause )
                    	    {
                    	    // InternalQCF.g:479:5: (lv_subCauses_10_0= ruleCause )
                    	    // InternalQCF.g:480:6: lv_subCauses_10_0= ruleCause
                    	    {

                    	    						newCompositeNode(grammarAccess.getCauseAccess().getSubCausesCauseParserRuleCall_5_3_0());
                    	    					
                    	    pushFollow(FOLLOW_15);
                    	    lv_subCauses_10_0=ruleCause();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getCauseRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"subCauses",
                    	    							lv_subCauses_10_0,
                    	    							"es.unican.QCF.Cause");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,21,FOLLOW_2); 

                    				newLeafNode(otherlv_11, grammarAccess.getCauseAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleEString"
    // InternalQCF.g:506:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalQCF.g:506:47: (iv_ruleEString= ruleEString EOF )
            // InternalQCF.g:507:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalQCF.g:513:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalQCF.g:519:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalQCF.g:520:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalQCF.g:520:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_STRING) ) {
                alt12=1;
            }
            else if ( (LA12_0==RULE_ID) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalQCF.g:521:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalQCF.g:529:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalQCF.g:540:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalQCF.g:540:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalQCF.g:541:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalQCF.g:547:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalQCF.g:553:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalQCF.g:554:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalQCF.g:554:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalQCF.g:555:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_16); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalQCF.g:562:3: (kw= '.' this_ID_2= RULE_ID )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==22) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalQCF.g:563:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,22,FOLLOW_3); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_16); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000018002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000028002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000000C8002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000000000C0002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000220000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400002L});

}